USE [master]
GO
/****** Object:  Database [SED360]    Script Date: 07/08/2013 12:20:31 ******/
CREATE DATABASE [SED360] ON  PRIMARY 
( NAME = N'SED360', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\SED360.mdf' , SIZE = 2304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SED360_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\SED360_log.LDF' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SED360] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SED360].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SED360] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [SED360] SET ANSI_NULLS OFF
GO
ALTER DATABASE [SED360] SET ANSI_PADDING OFF
GO
ALTER DATABASE [SED360] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [SED360] SET ARITHABORT OFF
GO
ALTER DATABASE [SED360] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [SED360] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [SED360] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [SED360] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [SED360] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [SED360] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [SED360] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [SED360] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [SED360] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [SED360] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [SED360] SET  DISABLE_BROKER
GO
ALTER DATABASE [SED360] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [SED360] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [SED360] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [SED360] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [SED360] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [SED360] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [SED360] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [SED360] SET  READ_WRITE
GO
ALTER DATABASE [SED360] SET RECOVERY FULL
GO
ALTER DATABASE [SED360] SET  MULTI_USER
GO
ALTER DATABASE [SED360] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [SED360] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'SED360', N'ON'
GO
USE [SED360]
GO
/****** Object:  Table [dbo].[Niveles]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Niveles](
	[id_nivel] [int] NOT NULL,
	[descripcion] [text] NOT NULL,
 CONSTRAINT [PK_Niveles] PRIMARY KEY CLUSTERED 
(
	[id_nivel] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Instrumentos]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Instrumentos](
	[id_instrumento] [int] NOT NULL,
	[descripcion] [text] NOT NULL,
 CONSTRAINT [PK_Intrumentos] PRIMARY KEY CLUSTERED 
(
	[id_instrumento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlanEstrategico]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PlanEstrategico](
	[id_plan_estrategico] [char](10) NOT NULL,
	[descripcion] [text] NOT NULL,
	[fecha_inicio] [datetime] NOT NULL,
	[fecha_fin] [datetime] NOT NULL,
 CONSTRAINT [PK_PlanEstrategico] PRIMARY KEY CLUSTERED 
(
	[id_plan_estrategico] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CentroDeCostos]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CentroDeCostos](
	[id_centrocostos] [varchar](30) NOT NULL,
	[descripcion] [text] NOT NULL,
 CONSTRAINT [PK_CentroDeCostos] PRIMARY KEY CLUSTERED 
(
	[id_centrocostos] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cargos]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cargos](
	[id_cargo] [int] NOT NULL,
	[descripcion] [text] NOT NULL,
 CONSTRAINT [PK_Cargos] PRIMARY KEY CLUSTERED 
(
	[id_cargo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Capacitaciones]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Capacitaciones](
	[id_capacitacion] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [text] NOT NULL,
 CONSTRAINT [PK_Capacitaciones] PRIMARY KEY CLUSTERED 
(
	[id_capacitacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id_usuario] [varchar](20) NOT NULL,
	[clave] [varchar](50) NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ubicaciones]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ubicaciones](
	[id_ubicacion] [int] NOT NULL,
	[descripcion] [text] NOT NULL,
 CONSTRAINT [PK_Ubicaciones] PRIMARY KEY CLUSTERED 
(
	[id_ubicacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoEmpleado]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoEmpleado](
	[id_tipoempleado] [int] NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TipoEmpleado] PRIMARY KEY CLUSTERED 
(
	[id_tipoempleado] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[id_rol] [int] NOT NULL,
	[descripcion] [nchar](30) NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[id_rol] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PreguntaAbierta]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PreguntaAbierta](
	[id_pregunta] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [text] NOT NULL,
	[id_plan_estrategico] [char](10) NULL,
	[instrumento] [int] NOT NULL,
 CONSTRAINT [PK_PreguntaAbierta] PRIMARY KEY CLUSTERED 
(
	[id_pregunta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Escalas]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Escalas](
	[id_escala] [int] IDENTITY(1,1) NOT NULL,
	[valor] [float] NOT NULL,
	[descripcion] [varchar](25) NOT NULL,
	[id_plan_estrategico] [char](10) NOT NULL,
 CONSTRAINT [PK_Escalas] PRIMARY KEY CLUSTERED 
(
	[id_escala] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Empleados]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empleados](
	[inss] [char](10) NOT NULL,
	[cedula] [varchar](20) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido] [varchar](50) NOT NULL,
	[centro_costo] [varchar](30) NOT NULL,
	[inss_jefe] [char](10) NOT NULL,
	[inss_jefearea] [char](10) NOT NULL,
	[ubicacion] [int] NULL,
	[activo] [bit] NOT NULL,
	[id_cargo] [int] NOT NULL,
	[id_tipo_empleado] [int] NOT NULL,
	[nivel] [int] NULL,
 CONSTRAINT [PK_Empleados_1] PRIMARY KEY CLUSTERED 
(
	[cedula] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [AK_inss] UNIQUE NONCLUSTERED 
(
	[inss] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetalleRoles]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetalleRoles](
	[id_rol] [int] NOT NULL,
	[id_usuario] [varchar](20) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetalleCapacitaciones]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetalleCapacitaciones](
	[id_capacitacion] [int] NOT NULL,
	[id_centrocostos] [varchar](30) NOT NULL,
 CONSTRAINT [PK_DetalleCapacitaciones] PRIMARY KEY CLUSTERED 
(
	[id_capacitacion] ASC,
	[id_centrocostos] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Criterio]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Criterio](
	[id_criterio] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [text] NOT NULL,
	[plan_estrategico] [char](10) NOT NULL,
 CONSTRAINT [PK_Criterios] PRIMARY KEY CLUSTERED 
(
	[id_criterio] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Auditoria]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Auditoria](
	[id_registro] [int] NOT NULL,
	[id_usuario] [varchar](20) NOT NULL,
	[evento] [text] NOT NULL,
	[fecha_evento] [datetime] NOT NULL,
 CONSTRAINT [PK_Auditoria] PRIMARY KEY CLUSTERED 
(
	[id_registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Periodo]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Periodo](
	[id_periodo] [char](7) NOT NULL,
	[descripcion] [text] NOT NULL,
	[fecha_inicio] [date] NOT NULL,
	[fecha_fin] [date] NOT NULL,
	[id_plan_estrategico] [char](10) NOT NULL,
 CONSTRAINT [PK_Periodo] PRIMARY KEY CLUSTERED 
(
	[id_periodo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Evaluacion]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Evaluacion](
	[id_Evaluacion] [char](20) NOT NULL,
	[evaluado] [char](10) NOT NULL,
	[evaluador] [char](10) NOT NULL,
	[id_instrumento] [int] NOT NULL,
	[id_periodo] [char](7) NOT NULL,
 CONSTRAINT [PK_Asinacion] PRIMARY KEY CLUSTERED 
(
	[id_Evaluacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AsignacionMetodologia]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AsignacionMetodologia](
	[id_nivel] [int] NOT NULL,
	[id_periodo] [char](7) NOT NULL,
	[metodologia] [nchar](10) NOT NULL,
 CONSTRAINT [PK_DetalleNivel] PRIMARY KEY CLUSTERED 
(
	[id_nivel] ASC,
	[id_periodo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PreguntaCerrada]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PreguntaCerrada](
	[id_pregunta] [char](7) NOT NULL,
	[id_criterio] [int] NOT NULL,
 CONSTRAINT [PK_PreguntaCerrada_1] PRIMARY KEY CLUSTERED 
(
	[id_pregunta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ponderacion]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ponderacion](
	[Puntaje] [float] NOT NULL,
	[id_criterio] [int] NOT NULL,
	[id_instrumento] [int] NOT NULL,
 CONSTRAINT [PK_Ponderacion] PRIMARY KEY CLUSTERED 
(
	[id_criterio] ASC,
	[id_instrumento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RespuestaPreguntaCerrada]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RespuestaPreguntaCerrada](
	[pregunta] [char](7) NOT NULL,
	[asignacion] [char](20) NOT NULL,
	[escala] [int] NOT NULL,
 CONSTRAINT [PK_RespuestaPreguntaCerrada] PRIMARY KEY CLUSTERED 
(
	[pregunta] ASC,
	[asignacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RespuestaPreguntaAbierta]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RespuestaPreguntaAbierta](
	[pregunta] [int] NOT NULL,
	[asignacion] [char](20) NOT NULL,
	[descripcion] [text] NOT NULL,
 CONSTRAINT [PK_RespuestaPreguntaAbierta] PRIMARY KEY CLUSTERED 
(
	[pregunta] ASC,
	[asignacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Redacciones]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Redacciones](
	[id_pregunta] [char](7) NOT NULL,
	[Descripcion] [text] NOT NULL,
	[id_instrumento] [int] NOT NULL,
 CONSTRAINT [PK_Redaccione] PRIMARY KEY CLUSTERED 
(
	[id_pregunta] ASC,
	[id_instrumento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Recomendacion]    Script Date: 07/08/2013 12:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Recomendacion](
	[id_asignacion] [char](20) NOT NULL,
	[id_capacitacion] [int] NOT NULL,
 CONSTRAINT [PK_Recomendacion_1] PRIMARY KEY CLUSTERED 
(
	[id_asignacion] ASC,
	[id_capacitacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_PreguntaAbierta_Intrumentos]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[PreguntaAbierta]  WITH CHECK ADD  CONSTRAINT [FK_PreguntaAbierta_Intrumentos] FOREIGN KEY([instrumento])
REFERENCES [dbo].[Instrumentos] ([id_instrumento])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PreguntaAbierta] CHECK CONSTRAINT [FK_PreguntaAbierta_Intrumentos]
GO
/****** Object:  ForeignKey [FK_PreguntaAbierta_PlanEstrategico1]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[PreguntaAbierta]  WITH CHECK ADD  CONSTRAINT [FK_PreguntaAbierta_PlanEstrategico1] FOREIGN KEY([id_plan_estrategico])
REFERENCES [dbo].[PlanEstrategico] ([id_plan_estrategico])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PreguntaAbierta] CHECK CONSTRAINT [FK_PreguntaAbierta_PlanEstrategico1]
GO
/****** Object:  ForeignKey [FK_Escalas_PlanEstrategico1]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Escalas]  WITH CHECK ADD  CONSTRAINT [FK_Escalas_PlanEstrategico1] FOREIGN KEY([id_plan_estrategico])
REFERENCES [dbo].[PlanEstrategico] ([id_plan_estrategico])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Escalas] CHECK CONSTRAINT [FK_Escalas_PlanEstrategico1]
GO
/****** Object:  ForeignKey [FK_Empleados_Cargos]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Empleados]  WITH CHECK ADD  CONSTRAINT [FK_Empleados_Cargos] FOREIGN KEY([id_cargo])
REFERENCES [dbo].[Cargos] ([id_cargo])
GO
ALTER TABLE [dbo].[Empleados] CHECK CONSTRAINT [FK_Empleados_Cargos]
GO
/****** Object:  ForeignKey [FK_Empleados_CentroDeCostos]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Empleados]  WITH CHECK ADD  CONSTRAINT [FK_Empleados_CentroDeCostos] FOREIGN KEY([centro_costo])
REFERENCES [dbo].[CentroDeCostos] ([id_centrocostos])
GO
ALTER TABLE [dbo].[Empleados] CHECK CONSTRAINT [FK_Empleados_CentroDeCostos]
GO
/****** Object:  ForeignKey [FK_Empleados_Empleados]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Empleados]  WITH CHECK ADD  CONSTRAINT [FK_Empleados_Empleados] FOREIGN KEY([inss_jefe])
REFERENCES [dbo].[Empleados] ([inss])
GO
ALTER TABLE [dbo].[Empleados] CHECK CONSTRAINT [FK_Empleados_Empleados]
GO
/****** Object:  ForeignKey [FK_Empleados_Empleados1]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Empleados]  WITH CHECK ADD  CONSTRAINT [FK_Empleados_Empleados1] FOREIGN KEY([inss_jefearea])
REFERENCES [dbo].[Empleados] ([inss])
GO
ALTER TABLE [dbo].[Empleados] CHECK CONSTRAINT [FK_Empleados_Empleados1]
GO
/****** Object:  ForeignKey [FK_Empleados_Niveles]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Empleados]  WITH CHECK ADD  CONSTRAINT [FK_Empleados_Niveles] FOREIGN KEY([nivel])
REFERENCES [dbo].[Niveles] ([id_nivel])
GO
ALTER TABLE [dbo].[Empleados] CHECK CONSTRAINT [FK_Empleados_Niveles]
GO
/****** Object:  ForeignKey [FK_Empleados_TipoEmpleado]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Empleados]  WITH CHECK ADD  CONSTRAINT [FK_Empleados_TipoEmpleado] FOREIGN KEY([id_tipo_empleado])
REFERENCES [dbo].[TipoEmpleado] ([id_tipoempleado])
GO
ALTER TABLE [dbo].[Empleados] CHECK CONSTRAINT [FK_Empleados_TipoEmpleado]
GO
/****** Object:  ForeignKey [FK_Empleados_Ubicaciones]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Empleados]  WITH CHECK ADD  CONSTRAINT [FK_Empleados_Ubicaciones] FOREIGN KEY([ubicacion])
REFERENCES [dbo].[Ubicaciones] ([id_ubicacion])
GO
ALTER TABLE [dbo].[Empleados] CHECK CONSTRAINT [FK_Empleados_Ubicaciones]
GO
/****** Object:  ForeignKey [FK_Empleados_Usuarios]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Empleados]  WITH CHECK ADD  CONSTRAINT [FK_Empleados_Usuarios] FOREIGN KEY([cedula])
REFERENCES [dbo].[Usuarios] ([id_usuario])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Empleados] CHECK CONSTRAINT [FK_Empleados_Usuarios]
GO
/****** Object:  ForeignKey [FK_Detalle_de_Roles_Roles]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[DetalleRoles]  WITH CHECK ADD  CONSTRAINT [FK_Detalle_de_Roles_Roles] FOREIGN KEY([id_rol])
REFERENCES [dbo].[Roles] ([id_rol])
GO
ALTER TABLE [dbo].[DetalleRoles] CHECK CONSTRAINT [FK_Detalle_de_Roles_Roles]
GO
/****** Object:  ForeignKey [FK_Detalle_de_Roles_Usuarios1]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[DetalleRoles]  WITH CHECK ADD  CONSTRAINT [FK_Detalle_de_Roles_Usuarios1] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[Usuarios] ([id_usuario])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DetalleRoles] CHECK CONSTRAINT [FK_Detalle_de_Roles_Usuarios1]
GO
/****** Object:  ForeignKey [FK_DetalleCapacitaciones_Capacitaciones]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[DetalleCapacitaciones]  WITH CHECK ADD  CONSTRAINT [FK_DetalleCapacitaciones_Capacitaciones] FOREIGN KEY([id_capacitacion])
REFERENCES [dbo].[Capacitaciones] ([id_capacitacion])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DetalleCapacitaciones] CHECK CONSTRAINT [FK_DetalleCapacitaciones_Capacitaciones]
GO
/****** Object:  ForeignKey [FK_DetalleCapacitaciones_CentroDeCostos]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[DetalleCapacitaciones]  WITH CHECK ADD  CONSTRAINT [FK_DetalleCapacitaciones_CentroDeCostos] FOREIGN KEY([id_centrocostos])
REFERENCES [dbo].[CentroDeCostos] ([id_centrocostos])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DetalleCapacitaciones] CHECK CONSTRAINT [FK_DetalleCapacitaciones_CentroDeCostos]
GO
/****** Object:  ForeignKey [FK_Criterio_PlanEstrategico]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Criterio]  WITH CHECK ADD  CONSTRAINT [FK_Criterio_PlanEstrategico] FOREIGN KEY([plan_estrategico])
REFERENCES [dbo].[PlanEstrategico] ([id_plan_estrategico])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Criterio] CHECK CONSTRAINT [FK_Criterio_PlanEstrategico]
GO
/****** Object:  ForeignKey [FK_Auditoria_Usuarios1]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Auditoria]  WITH CHECK ADD  CONSTRAINT [FK_Auditoria_Usuarios1] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[Usuarios] ([id_usuario])
GO
ALTER TABLE [dbo].[Auditoria] CHECK CONSTRAINT [FK_Auditoria_Usuarios1]
GO
/****** Object:  ForeignKey [FK_Periodo_PlanEstrategico1]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Periodo]  WITH CHECK ADD  CONSTRAINT [FK_Periodo_PlanEstrategico1] FOREIGN KEY([id_plan_estrategico])
REFERENCES [dbo].[PlanEstrategico] ([id_plan_estrategico])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Periodo] CHECK CONSTRAINT [FK_Periodo_PlanEstrategico1]
GO
/****** Object:  ForeignKey [FK_Asignacion_Empleados]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Evaluacion]  WITH CHECK ADD  CONSTRAINT [FK_Asignacion_Empleados] FOREIGN KEY([evaluado])
REFERENCES [dbo].[Empleados] ([inss])
GO
ALTER TABLE [dbo].[Evaluacion] CHECK CONSTRAINT [FK_Asignacion_Empleados]
GO
/****** Object:  ForeignKey [FK_Asignacion_Empleados1]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Evaluacion]  WITH CHECK ADD  CONSTRAINT [FK_Asignacion_Empleados1] FOREIGN KEY([evaluador])
REFERENCES [dbo].[Empleados] ([inss])
GO
ALTER TABLE [dbo].[Evaluacion] CHECK CONSTRAINT [FK_Asignacion_Empleados1]
GO
/****** Object:  ForeignKey [FK_Asinacion_Intrumentos]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Evaluacion]  WITH CHECK ADD  CONSTRAINT [FK_Asinacion_Intrumentos] FOREIGN KEY([id_instrumento])
REFERENCES [dbo].[Instrumentos] ([id_instrumento])
GO
ALTER TABLE [dbo].[Evaluacion] CHECK CONSTRAINT [FK_Asinacion_Intrumentos]
GO
/****** Object:  ForeignKey [FK_Asinacion_Periodo]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Evaluacion]  WITH CHECK ADD  CONSTRAINT [FK_Asinacion_Periodo] FOREIGN KEY([id_periodo])
REFERENCES [dbo].[Periodo] ([id_periodo])
GO
ALTER TABLE [dbo].[Evaluacion] CHECK CONSTRAINT [FK_Asinacion_Periodo]
GO
/****** Object:  ForeignKey [FK_DetalleNivel_Niveles]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[AsignacionMetodologia]  WITH CHECK ADD  CONSTRAINT [FK_DetalleNivel_Niveles] FOREIGN KEY([id_nivel])
REFERENCES [dbo].[Niveles] ([id_nivel])
GO
ALTER TABLE [dbo].[AsignacionMetodologia] CHECK CONSTRAINT [FK_DetalleNivel_Niveles]
GO
/****** Object:  ForeignKey [FK_DetalleNivel_Periodo]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[AsignacionMetodologia]  WITH CHECK ADD  CONSTRAINT [FK_DetalleNivel_Periodo] FOREIGN KEY([id_periodo])
REFERENCES [dbo].[Periodo] ([id_periodo])
GO
ALTER TABLE [dbo].[AsignacionMetodologia] CHECK CONSTRAINT [FK_DetalleNivel_Periodo]
GO
/****** Object:  ForeignKey [FK_PreguntaCerrada_Criterios]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[PreguntaCerrada]  WITH CHECK ADD  CONSTRAINT [FK_PreguntaCerrada_Criterios] FOREIGN KEY([id_criterio])
REFERENCES [dbo].[Criterio] ([id_criterio])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PreguntaCerrada] CHECK CONSTRAINT [FK_PreguntaCerrada_Criterios]
GO
/****** Object:  ForeignKey [FK_Poderacion_Criterios]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Ponderacion]  WITH CHECK ADD  CONSTRAINT [FK_Poderacion_Criterios] FOREIGN KEY([id_criterio])
REFERENCES [dbo].[Criterio] ([id_criterio])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Ponderacion] CHECK CONSTRAINT [FK_Poderacion_Criterios]
GO
/****** Object:  ForeignKey [FK_Poderacion_Intrumentos]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Ponderacion]  WITH CHECK ADD  CONSTRAINT [FK_Poderacion_Intrumentos] FOREIGN KEY([id_instrumento])
REFERENCES [dbo].[Instrumentos] ([id_instrumento])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Ponderacion] CHECK CONSTRAINT [FK_Poderacion_Intrumentos]
GO
/****** Object:  ForeignKey [FK_Respuesta_a_PreguntaCerrada_Asinacion]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[RespuestaPreguntaCerrada]  WITH CHECK ADD  CONSTRAINT [FK_Respuesta_a_PreguntaCerrada_Asinacion] FOREIGN KEY([asignacion])
REFERENCES [dbo].[Evaluacion] ([id_Evaluacion])
GO
ALTER TABLE [dbo].[RespuestaPreguntaCerrada] CHECK CONSTRAINT [FK_Respuesta_a_PreguntaCerrada_Asinacion]
GO
/****** Object:  ForeignKey [FK_Respuesta_a_PreguntaCerrada_Escalas]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[RespuestaPreguntaCerrada]  WITH CHECK ADD  CONSTRAINT [FK_Respuesta_a_PreguntaCerrada_Escalas] FOREIGN KEY([escala])
REFERENCES [dbo].[Escalas] ([id_escala])
GO
ALTER TABLE [dbo].[RespuestaPreguntaCerrada] CHECK CONSTRAINT [FK_Respuesta_a_PreguntaCerrada_Escalas]
GO
/****** Object:  ForeignKey [FK_RespuestaPreguntaCerrada_PreguntaCerrada]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[RespuestaPreguntaCerrada]  WITH CHECK ADD  CONSTRAINT [FK_RespuestaPreguntaCerrada_PreguntaCerrada] FOREIGN KEY([pregunta])
REFERENCES [dbo].[PreguntaCerrada] ([id_pregunta])
GO
ALTER TABLE [dbo].[RespuestaPreguntaCerrada] CHECK CONSTRAINT [FK_RespuestaPreguntaCerrada_PreguntaCerrada]
GO
/****** Object:  ForeignKey [FK_Respuesta_a_PreguntaAbierta_Asinacion]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[RespuestaPreguntaAbierta]  WITH CHECK ADD  CONSTRAINT [FK_Respuesta_a_PreguntaAbierta_Asinacion] FOREIGN KEY([asignacion])
REFERENCES [dbo].[Evaluacion] ([id_Evaluacion])
GO
ALTER TABLE [dbo].[RespuestaPreguntaAbierta] CHECK CONSTRAINT [FK_Respuesta_a_PreguntaAbierta_Asinacion]
GO
/****** Object:  ForeignKey [FK_Respuesta_a_PreguntaAbierta_PreguntaAbierta]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[RespuestaPreguntaAbierta]  WITH CHECK ADD  CONSTRAINT [FK_Respuesta_a_PreguntaAbierta_PreguntaAbierta] FOREIGN KEY([pregunta])
REFERENCES [dbo].[PreguntaAbierta] ([id_pregunta])
GO
ALTER TABLE [dbo].[RespuestaPreguntaAbierta] CHECK CONSTRAINT [FK_Respuesta_a_PreguntaAbierta_PreguntaAbierta]
GO
/****** Object:  ForeignKey [FK_Redacciones_Instrumentos]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Redacciones]  WITH CHECK ADD  CONSTRAINT [FK_Redacciones_Instrumentos] FOREIGN KEY([id_instrumento])
REFERENCES [dbo].[Instrumentos] ([id_instrumento])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Redacciones] CHECK CONSTRAINT [FK_Redacciones_Instrumentos]
GO
/****** Object:  ForeignKey [FK_Redacciones_PreguntaCerrada]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Redacciones]  WITH CHECK ADD  CONSTRAINT [FK_Redacciones_PreguntaCerrada] FOREIGN KEY([id_pregunta])
REFERENCES [dbo].[PreguntaCerrada] ([id_pregunta])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Redacciones] CHECK CONSTRAINT [FK_Redacciones_PreguntaCerrada]
GO
/****** Object:  ForeignKey [FK_Recomendacion_Asinacion]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Recomendacion]  WITH CHECK ADD  CONSTRAINT [FK_Recomendacion_Asinacion] FOREIGN KEY([id_asignacion])
REFERENCES [dbo].[Evaluacion] ([id_Evaluacion])
GO
ALTER TABLE [dbo].[Recomendacion] CHECK CONSTRAINT [FK_Recomendacion_Asinacion]
GO
/****** Object:  ForeignKey [FK_Recomendacion_Capacitaciones]    Script Date: 07/08/2013 12:20:36 ******/
ALTER TABLE [dbo].[Recomendacion]  WITH CHECK ADD  CONSTRAINT [FK_Recomendacion_Capacitaciones] FOREIGN KEY([id_capacitacion])
REFERENCES [dbo].[Capacitaciones] ([id_capacitacion])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Recomendacion] CHECK CONSTRAINT [FK_Recomendacion_Capacitaciones]
GO
