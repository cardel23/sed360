package uca.sed360.managedBeans;

import java.io.IOException;
import uca.sed360.jpaEntities.Evaluacion;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.EvaluacionFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;
import uca.sed360.jpaEntities.Capacitaciones;
import uca.sed360.jpaEntities.Criterio;
import uca.sed360.jpaEntities.Empleados;
import uca.sed360.jpaEntities.Periodo;
import uca.sed360.jpaEntities.PreguntaAbierta;
import uca.sed360.jpaEntities.PreguntaCerrada;
import uca.sed360.jpaEntities.DetallePreguntaCerrada;
import uca.sed360.jpaEntities.Escalas;
import uca.sed360.jpaEntities.Nivel360;
import uca.sed360.jpaEntities.RespuestaPreguntaAbierta;
import uca.sed360.jpaEntities.RespuestaPreguntaAbiertaPK;
import uca.sed360.jpaEntities.RespuestaPreguntaCerrada;
import uca.sed360.jpaEntities.Usuarios;
import uca.sed360.managedBeans.util.reportes.GeneradorDeReportesPDF;
import uca.sed360.managedBeans.util.PromedioEmpleado;

/**
 *
 * @author Estacion 2
 */
@ManagedBean(name = "evaluacionController")
@SessionScoped
public class EvaluacionController implements Serializable {

    @ManagedProperty(value="#{sessionMB}")
    private SessionMB sessionMB;
    @ManagedProperty(value="#{planEstrategicoController}")
    private PlanEstrategicoController planMB;
    @ManagedProperty(value="#{periodoController}")
    private PeriodoController periodoMB;
    @ManagedProperty(value = "#{capacitacionesController}") 
    private CapacitacionesController controladorDeCapacitaciones;
    
    @ManagedProperty(value = "#{auditoriaController}")
    private AuditoriaController audiController;
    
    /**
     * Definición de variables
     */
    
    private boolean evaluadoSeleccionado=false;
    private HashMap indicesRPC,hashMapEvaluados,map;
    private List<RespuestaPreguntaAbierta> listaRespuestasPreguntasAbierta;
    private List<Criterio> listaCriteriosTipoEmpleado;
    private Evaluacion current;
    private List<Empleados> subordinadosEmp;
    private List<Empleados> paresEmp;
    private boolean  vistaHTML=true,metodologiaAsignada360,validadoMetodologiaAsignada360=false,realizadoValidacionesInciales=false;
    private List<Empleados> listaEvaluacionesSubordinados;
    private double puntajeTotal;       
    private String ruta;
    private GeneradorDeReportesPDF pdf;    
    private String nombreRep;
    private Empleados evaluado;
    private Nivel360 nivel;
    private List listaPuntajes;
    private ArrayList<Evaluacion> evaluacionesPendientesFiiltradas;
    private ArrayList<Evaluacion> evaluacionesASubordinadosPendientesFiltradas;
    private List<Object[]> PromediosGlobales;
    private List<PromedioEmpleado> PromediosGlobalesFiltrados;
    private List<Capacitaciones> Capacitaciones;
    private List<Capacitaciones> CapacitacionesSeleccionadas;
    private Periodo periodoEvaluacion;
    private HashMap<String,DetallePreguntaCerrada> detallesMap;
    private List<Evaluacion> listaEvaluaciones,listaEvaluacionesFiltrado;
    private List<Evaluacion> evaluacionesImpresasFiltradas;
    private List<Evaluacion> evaluacionesNoImpresasFiltradas;
    private List<Escalas> escalasOrdenadas;
    
 
    @EJB
    private uca.sed360.ejbs.PreguntaAbiertaFacade ejbPreguntaAbierta;
    @EJB
    private uca.sed360.ejbs.EvaluacionFacade ejbEvaluacion;
    @EJB
    private uca.sed360.ejbs.EmpleadosFacade ejbEmpleados;
    @EJB
    private uca.sed360.ejbs.PeriodoFacade ejbPeriodo;
    @EJB
    private uca.sed360.ejbs.UsuariosFacade ejbUsuarios;
    @EJB
    private uca.sed360.ejbs.CriterioFacade ejbCriterios;
    @EJB
    private uca.sed360.ejbs.AsignacionMetodologiaFacade ejbAsignacionMetodologia;
    @EJB
    private uca.sed360.ejbs.EscalasFacade ejbEscalas;
    

    /**
     *
     */
    public EvaluacionController() {

    }
    //Getter y Setter de los ManagedProperties
    /**
     *
     * @return
     */
    public SessionMB getSessionMB() {
        return sessionMB;
    }

    /**
     *
     * @param sessionMB
     */
    public void setSessionMB(SessionMB sessionMB) {
        this.sessionMB = sessionMB;
    } 

    /**
     *
     * @return
     */
    public PlanEstrategicoController getPlanMB() {
        return planMB;
    }
    
     
    /**
     *
     * @param planMB
     */
    public void setPlanMB(PlanEstrategicoController planMB) {
        this.planMB = planMB;       
    }

    /**
     *
     * @return
     */
    public PeriodoController getPeriodoMB() {
        return periodoMB;
    }

    /**
     *
     * @param periodoMB
     */
    public void setPeriodoMB(PeriodoController periodoMB) {
        this.periodoMB = periodoMB;
    }

    //Lista Criterios tipo Empleado
    /**
     *
     * @return
     */
    public List<Criterio> getListaCriteriosTipoEmpleado() {
        return listaCriteriosTipoEmpleado;
    }

    /**
     *
     * @param listaCriteriosTipoEmpleado
     */
    public void setListaCriteriosTipoEmpleado(List<Criterio> listaCriteriosTipoEmpleado) {
        this.listaCriteriosTipoEmpleado = listaCriteriosTipoEmpleado;
    }
    
    /**
     *
     * @return
     */
    public double getPuntaje(){
        double sum = 0;
        for(Object o:getListaPuntajes()){
            Object[] a = (Object[]) o;
            sum += ((Double)a[4]).doubleValue();
//            System.out.print(a[4]);
        }
        return sum;
    }
//Evaluacion  
    /**
     * devuelve el objeto current a las vistas
     * @return 
     */
    public Evaluacion getSelected() {
        if (current == null) {
            current = new Evaluacion();
        }
        return current;
    }
    
    /**
     *
     * @param evaluacion
     */
    public void setSelected(Evaluacion evaluacion){
        this.current = evaluacion;
    }
    
    /**
     * devuleve true si el empleado en sesion selecciono a otro empleado para evaluar. 
     * Se utiliza como validacion que el empleado termine una evaluacion luego que la haya empezado
     * @return 
     */  
    public boolean isEvaluadoSeleccionado() {
        return evaluadoSeleccionado;
    }

    /**
     *
     * @param evaluadoSeleccionado
     */
    public void setEvaluadoSeleccionado(boolean evaluadoSeleccionado) {
        this.evaluadoSeleccionado = evaluadoSeleccionado;
    }
    /**
     * retorna al empleado evaluado. Se utiliza principalmente cuando se consulta una evaluacion realizada
     * @return 
     */
    public Empleados getEvaluado() {
        return evaluado;
    }

    /**
     *
     * @param evaluado
     */
    public void setEvaluado(Empleados evaluado) {
        this.evaluado = evaluado;
    }

    /**
     *
     * @return
     */
    public HashMap<String, DetallePreguntaCerrada> getDetallesMap() {
        return detallesMap;
    }

    /**
     *
     * @param detallesMap
     */
    public void setDetallesMap(HashMap<String, DetallePreguntaCerrada> detallesMap) {
        this.detallesMap = detallesMap;
    }
    
    /**
     * Este metodo valida si el empleado tiene asignada metodologia 360
     * @return 
     */  
    public boolean isMetodologiaAsignada360() {
         if(validadoMetodologiaAsignada360==false){
                metodologiaAsignada360=ejbAsignacionMetodologia.isMetodologiaAsignada360(periodoMB.getPeriodoVigente(), 
                                                                                       sessionMB.getCurrentUser().getEmpleados().getNivel());                                     
                validadoMetodologiaAsignada360=true;
           }
        return metodologiaAsignada360;
    }

    /**
     *
     * @param metodologiaAsignada360
     */
    public void setMetodologiaAsignada360(boolean metodologiaAsignada360) {
        this.metodologiaAsignada360 = metodologiaAsignada360;
    }

    /**
     *
     * @return
     */
    public boolean isVistaHTML() {
        return vistaHTML;
    }

    /**
     *
     * @param vistaHTML
     */
    public void setVistaHTML(boolean vistaHTML) {
        this.vistaHTML = vistaHTML;
    }
    
     //retorna la informacion del encabezado durante la evaluacion
    /**
     *
     * @return
     */
    public String getInfoEvaluado(){
        if(current.getEvaluado().equals(current.getEvaluador()))
            return "Realizando Autoevaluación";
        else
            return "Usted está evaluando a ".concat(current.getEvaluado().getNombre()).concat("  ").concat(current.getEvaluado().getApellido());
    }
    
    //punto de decision de aplicacion de los instrumentos
    /**
     *
     * @return
     */
    public String inicializarEvaluacion() {
       current.setEvaluador(sessionMB.getCurrentUser().getEmpleados());
       current.setIdPeriodo(periodoEvaluacion);       
       current.setIdEvaluacion(getFacade().generarId( current.getEvaluador(),current.getEvaluado(), current.getIdPeriodo()));   
       current.setRespuestaPreguntaCerradaCollection(new ArrayList<RespuestaPreguntaCerrada>());
       List<PreguntaAbierta> lp = ejbPreguntaAbierta.findByPlanTipoEmpNivel360(planMB.getPlanVigente(), current.getEvaluado().getIdTipoEmpleado(), current.getIdNivel360());
       listaCriteriosTipoEmpleado= ejbCriterios.getCriteriosByPlanTipoEmpleado(planMB.getPlanVigente(),current.getEvaluado().getIdTipoEmpleado());
       indicesRPC=new HashMap<Integer,Integer>();       
       listaRespuestasPreguntasAbierta=new ArrayList<>();
       for(PreguntaAbierta p : lp){
           RespuestaPreguntaAbierta r = new RespuestaPreguntaAbierta();
           r.setPreguntaAbierta(p);
           r.setEvaluacion(current);
           r.setRespuestaPreguntaAbiertaPK(new RespuestaPreguntaAbiertaPK(p.getIdPregunta(), current.getIdEvaluacion()));
           listaRespuestasPreguntasAbierta.add(r);
       }
       crearMapaDetalles();
       int indiceRPC=0;
       for(Criterio c:listaCriteriosTipoEmpleado){
           for(PreguntaCerrada p:c.getPreguntaCerradaCollection()){
                RespuestaPreguntaCerrada resp=new RespuestaPreguntaCerrada(p.getIdPregunta(),
                                                                            current.getIdEvaluacion());
                resp.setPreguntaCerrada(p); 
                resp.setEvaluacion(current);
                //agragar la respuesta a la coleccion de respuestas de la evaluacion (respuesta aun sin escala asignada)
                current.getRespuestaPreguntaCerradaCollection().add(indiceRPC,resp);
                // agregar al hashmap la entrada. Llave: idPreguntaCerrada, Valor: indice de la Respuesta a la pregunta               
                indicesRPC.put(p.getIdPregunta(), indiceRPC);
                indiceRPC++;
           }
       }
       evaluadoSeleccionado=true;   
       return "Evaluacion?faces-redirect=true";
    }
    
    /**
     *
     */
    public void preRenderRealizarEvaluacion(){
       if(!evaluadoSeleccionado) {  
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response =(HttpServletResponse)context.getExternalContext().getResponse();
            try {
                response.sendRedirect("SeleccionarEvaluacion.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(EvaluacionController.class.getName()).log(Level.SEVERE, null, ex);
            }
            context.responseComplete();
        }
    }   
    
    /**
     * Preselecciona el periodo de evaluación. Si el periodo vigente es <code>null</code>
     * asigna como periodo actual el último periodo de la lista
     */
    public void preSeleccionarPeriodoEvaluacion(){
        if(periodoEvaluacion==null)
            periodoEvaluacion=periodoMB.getPeriodoVigente();
        if(periodoEvaluacion==null)
            periodoEvaluacion=periodoMB.getListaPeriodos().get(periodoMB.getListaPeriodos().size()-1);
    }
      
    /**
     * Se encarga de sumar los puntajes de cada pregunta y devuelve el total
     * @param eva 
     * @return El puntaje total de la evaluación
     * @author Carlos Delgadillo
     * @date 03/03/2013
     */
    public double calcularPuntajeTotal(Evaluacion eva){
       return getFacade().calcularPuntajeTotal(eva);
    }
    
    //Respuesta a preguntas abiertas
    /**
     *
     * @return
     */
    public List<RespuestaPreguntaAbierta> getListaRespuestasPreguntasAbierta() {
        return listaRespuestasPreguntasAbierta;
    }

    /**
     *
     * @param listaRespuestasPreguntasAbierta
     */
    public void setListaRespuestasPreguntasAbierta(List<RespuestaPreguntaAbierta> listaRespuestasPreguntasAbierta) {
        this.listaRespuestasPreguntasAbierta = listaRespuestasPreguntasAbierta;
    }
    
    /**
     * Comprueba si existen evaluaciones en un periodo determinado
     * @param p periodo de evaluación
     * @return verdadero o falso
     */
    public boolean existEvaluacionesPeriodo(Periodo p){
        return getFacade().existEvaluacionesPeriodo(p);
    }

    //Empleados
    
    /**
     * Comprueba si el empleado de la sesión tiene jefe
     * @return verdadero o falso
     */
    public boolean tieneJefe(){
        if(!getSessionMB().getCurrentUser().getEmpleados().equals(getSessionMB().getCurrentUser().getEmpleados().getInssJefearea()))
            return true;
        return false;
    }
     
    /**
     * Este metodo obtiene los subordinados del empleado autenticado
     * @return lista empleados
     */
    public List<Empleados> getSubordinadosEmp() {
        if(subordinadosEmp==null){
            subordinadosEmp=ejbEmpleados.obtenerSubordinados(getSessionMB().getCurrentUser().getEmpleados());
        }
        return subordinadosEmp;
    }

    /**
     *
     * @param subordinadosEmp
     */
    public void setSubordinadosEmp(List<Empleados> subordinadosEmp) {
        this.subordinadosEmp = subordinadosEmp;
    }
    
    /**
     * Este metodo obtiene los empleados pares del empleado autenticado
     * @return lista de pares
     */
    public List<Empleados> getParesEmp() {
        if(paresEmp==null){
            paresEmp=ejbEmpleados.obtenerPares(getSessionMB().getCurrentUser().getEmpleados());
        }
        return paresEmp;
    }

    /**
     *
     * @param paresEmp
     */
    public void setParesEmp(List<Empleados> paresEmp) {
        this.paresEmp = paresEmp;
    }    
   

    private EvaluacionFacade getFacade() {
        return ejbEvaluacion;
    }

   
    /**
     * Consulta los resultados de una evaluación realizada por el usuario
     * El evaluado se establece con un setPropertyActionListener,
     * el evaluador es el usuario en sesion, y el periodo es el periodovigente
     * @return URL hacia el reporte de la evaluación seleccionada
     */
    public String consultarEvaluacion(){
      
        current = getFacade().findEvaluacion(sessionMB.getCurrentUser().getEmpleados(), evaluado, periodoEvaluacion);
        if(current == null){
            JsfUtil.addErrorMessage("No se ha encontrado la evaluacion para el empleado ".concat(evaluado.getNombre()+ " "+evaluado.getApellido()));
            return null;
        }
        else{
            indicesRPC=new HashMap();
            listaCriteriosTipoEmpleado=new ArrayList<Criterio>();
            boolean mismoCriterio;
            int indiceRPC=0;
            for(RespuestaPreguntaCerrada rpc:current.getRespuestaPreguntaCerradaCollection()){
                //reconstruir la listaTemp de criterios evaluados
                    if(listaCriteriosTipoEmpleado.isEmpty())
                        mismoCriterio=false;
                    else                
                        if(listaCriteriosTipoEmpleado.get(listaCriteriosTipoEmpleado.size()-1).equals(rpc.getPreguntaCerrada().getIdCriterio()))
                            mismoCriterio=true;
                        else
                            mismoCriterio=false;
                 if(!mismoCriterio)
                      listaCriteriosTipoEmpleado.add(rpc.getPreguntaCerrada().getIdCriterio());
                //reconstruir el hasmap de indices de respuestas a la preguntas cerrada
                indicesRPC.put(rpc.getPreguntaCerrada().getIdPregunta(), indiceRPC);            
                indiceRPC++;
             }
             puntajeTotal=getFacade().calcularPuntajeTotal(current);
             return "ReporteEvaluacion?faces-redirect=true";
        }
    }
    
    /**
     * Consulta los resultados de una evaluación que ha realizado un empleado
     * @param eva es la evaluacion a consultar
     * @return 
     */
    public String consultarEvaluacion(Evaluacion eva){
        current=eva;
        prepararConsultaEvaluacion();
        return "ReporteEvaluacion?faces-redirect=true";    
    }
    /**
     * Obtiene los detalles de la evaluación que se va a consultar
     */
    private void prepararConsultaEvaluacion(){
        indicesRPC=new HashMap();
        listaCriteriosTipoEmpleado=new ArrayList<Criterio>();
        boolean mismoCriterio;
        int indiceRPC=0;
        for(RespuestaPreguntaCerrada rpc:current.getRespuestaPreguntaCerradaCollection()){
            //reconstruir la lista de criterios evaluados
                if(listaCriteriosTipoEmpleado.isEmpty())
                    mismoCriterio=false;
                else                
                    if(listaCriteriosTipoEmpleado.get(listaCriteriosTipoEmpleado.size()-1).equals(rpc.getPreguntaCerrada().getIdCriterio()))
                        mismoCriterio=true;
                    else
                        mismoCriterio=false;
             if(!mismoCriterio)
                  listaCriteriosTipoEmpleado.add(rpc.getPreguntaCerrada().getIdCriterio());
            //reconstruir el hasmap de indices de respuestas a la preguntas cerrada
            indicesRPC.put(rpc.getPreguntaCerrada().getIdPregunta(), indiceRPC);            
            indiceRPC++;
         }
         puntajeTotal=getFacade().calcularPuntajeTotal(current);
         crearMapaDetalles();
    }
    
    /**
     * Obtiene la lista de evaluaciones que han sido impresas o no impresas
     * @param opcion <code>true</code> si se quiere obtener las que han sido impresas
     *               o <code>false</code> si se quiere obtener las que no han sido impresas
     * @return lista con el criterio seleccionado
     */
    public List<Evaluacion> obtenerEvaluacionesImpresas(boolean opcion){
        return getFacade().obtenerEvaluacionesImpresas(opcion,periodoMB.getPeriodoVigente());
    }
    
    /**
     * Obtiene el puntaje cualitativo de una evaluación
     * @return Excelente, Regular, Muy bueno, N/A, etc
     */
    public Escalas getPuntajeCualitativo(){
        Double p = puntajeTotal/100;
        Escalas eaux = new Escalas();
        List<Escalas> es ;
        es = planMB.getPlanVigente().getEscalasCollection();
       
        List<Escalas> laux = new ArrayList<>();
        for(Escalas e:es){
            if(!e.getDescripcion().equals("N/A"))
                laux.add(e);
        }
        for(Escalas e:laux){
            if(p<=e.getValor())
                eaux = e;
        }        
        return eaux;
   }
    
    /**
     * Este hashMap mapea a los empleados del usuario en sesion poniendo como llave el inss los empleados y como valor un booleado que indica si ya ha sido evaluado o no
     * @return mapa de los empleados a evaluar
     */
    public HashMap getHashMapEvaluados() {
        if(hashMapEvaluados==null)
            hashMapEvaluados=new HashMap();
        return hashMapEvaluados;
    }

    /**
     *
     * @param hashMapEvaluados
     */
    public void setHashMapEvaluados(HashMap hashMapEvaluados) {
        this.hashMapEvaluados = hashMapEvaluados;
    }

    /**
     *
     * @return
     */
    public HashMap getIndicesRPC() {
        return indicesRPC;
    }

    /**
     *
     * @param indicesRPC
     */
    public void setIndicesRPC(HashMap indicesRPC) {
        this.indicesRPC = indicesRPC;
    }
    
    /**
     * Metodo que persiste la evaluacion que realiza el empleado en sesion
     * @return 
     */
    public String create() {
        try { 
            // setear la listaTemp capacitaciones a la evaluación
            current.setCapacitacionesCollection(getCapacitacionesSeleccionadas());
            //elimina de la listaTemp las preguntas que no fueron respondidas
            List<RespuestaPreguntaAbierta> lista = new ArrayList<>();
            for (RespuestaPreguntaAbierta r : listaRespuestasPreguntasAbierta){
                if (!r.getDescripcion().equals("")){
                    lista.add(r);
                }
            }
            // setear la listaTemp de respuesta a pregunta abiertas a la evaluación
            current.setRespuestaPreguntaAbiertaCollection(lista);
            // la evaluacion se registra como impresa false.
            current.setImpresa(false);
            current.setFechaEvaluacion(new Date());
            current.setCentroCosto(current.getEvaluado().getCentroCosto().getIdCentrocostos());
            getFacade().create(current);
            getFacade().limpiarClaseEnCache(current.getClass());
            evaluadoSeleccionado=false;
            // en el hashMapEvaluados se setea al empleado como evaluado (para que aparezca link ¨Evaluacion Realizada¨)
            hashMapEvaluados.put(current.getEvaluado().getInss(), true);
            Capacitaciones = new ArrayList<Capacitaciones>();
            CapacitacionesSeleccionadas = new ArrayList<Capacitaciones>();
            // calcular el puntajeTotal
             puntajeTotal=getFacade().calcularPuntajeTotal(current);
            
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("EvaluacionCreated"));
//            audiController.guardarRegistroActividad("evaluó al empleado "+current.getEvaluado().getNombre());
            
            
            if( isEmpleadoTonto() )
                return "View?faces-redirect=true";
            else
                return "ReporteEvaluacion?faces-redirect=true";
        } catch (Exception e) {
            System.out.println(e);
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + e.toString());
            return null;
        }
    }
    /**
     * Metodo que renderiza la vista de empleados de mas bajo nivel <br/><br/>
     * Autor: Carlos Delgadillo
     * @since 22/11/2013
     * @return verdadero o falso
     */
    public boolean isEmpleadoTonto(){
        if( subordinadosEmp.isEmpty() && !this.isMetodologiaAsignada360() )
            return true;
        else
            return false;
    }
    

    
    /**
     *
     * @return
     */
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("EvaluacionUpdated"));
            return null;
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    


    /**
     *
     * @return
     */
    public List<Evaluacion> getListaEvaluaciones() {
        if(periodoEvaluacion==null)
            periodoEvaluacion=ejbPeriodo.getPeriodoVigente();
        listaEvaluaciones=getFacade().obtenerListaEvaluacionesPorPeriodo(periodoEvaluacion);
        return listaEvaluaciones;
    }

    /**
     *
     * @param listaEvaluaciones
     */
    public void setListaEvaluaciones(List<Evaluacion> listaEvaluaciones) {
        this.listaEvaluaciones = listaEvaluaciones;
    }
    

    /**
     *
     * @return
     */
    public List<Evaluacion> getListaEvaluacionesFiltrado() {
        return listaEvaluacionesFiltrado;
    }

    /**
     *
     * @param listaEvaluacionesFiltrado
     */
    public void setListaEvaluacionesFiltrado(List<Evaluacion> listaEvaluacionesFiltrado) {
        this.listaEvaluacionesFiltrado = listaEvaluacionesFiltrado;
    }
    
    /**
     * Elimina una evaluación de la BD
     */
    public void eliminarEvaluacion() {
        try {
            getFacade().remove(current);
            listaEvaluacionesFiltrado=null;
            JsfUtil.addSuccessMessage("La evaluación se ha eliminado con éxito");
            //audiController.guardarRegistroActividad("eliminó la evaluación "+current.getIdEvaluacion().toString());
            
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Lo sentimos, no se ha podido eliminar la evaluación");
        }
    }
   
    /**
     * Valida si cumplen todos los requerimientos para que el empleado evalúe
     * @throws IOException 
     */
     public void preRenderSeleccionEvaluacion() throws IOException {
        
         FacesContext context = FacesContext.getCurrentInstance();
         ConfigurableNavigationHandler nav = (ConfigurableNavigationHandler)  context.getApplication().getNavigationHandler();
         if(sessionMB.getCurrentUser()==null){ //validando si el usuario esta autenticado      
            nav.performNavigation("/Login?faces-redirect=true");
            return;
         }
         Usuarios u=ejbUsuarios.find(sessionMB.getCurrentUser().getIdUsuario());
         if(!u.getEmpleados().getActivo()){//verifica si el empleado esa activo
              nav.performNavigation("Error?faces-redirect=true&tipo=empleadoInactivo");  
              return;
         }
         if(periodoMB.getPeriodoVigente() == null)  {  //verifica si hay periodoEvaluacion vigente 
           nav.performNavigation("Error?faces-redirect=true&tipo=periodoError");    
           return;
         }
         if(sessionMB.getCurrentUser().getEmpleados().getNivel()==null){  //verifica si ya se definió el nivel del empleado
             nav.performNavigation("Error?faces-redirect=true&tipo=nivelNull"); 
             return;
         }
         if(periodoEvaluacion==null)
             periodoEvaluacion=periodoMB.getPeriodoVigente();        
         if(evaluadoSeleccionado){   //verifica si hay una evaluacion sin finalizar
             nav.performNavigation("Evaluacion?faces-redirect=true");
             return;
         }
         
        validarHashMapEvaluados();
        
    }
     
     /**
      * Metodo que evalua si el empleado ya se evaluo o no para setear 
      * el registro en hashMapEvaluados
      * @param emp el empleado que se va a validar
      */
     private void validarEmpEvaluado(Empleados emp){
        
        if(getFacade().existsEvaluacion(sessionMB.getCurrentUser().getEmpleados(),emp, periodoEvaluacion))
            hashMapEvaluados.put(emp.getInss(), true);
        else
            hashMapEvaluados.put(emp.getInss(), false);

     }
     
     
     private void validarHashMapEvaluados(){
         
     if(getHashMapEvaluados().size()>0) {      
        int contadorDeEvaluados=0;
        Iterator i = getHashMapEvaluados().entrySet().iterator();

        while(i.hasNext()){          
              Map.Entry e = (Map.Entry)i.next();
              if ((boolean) e.getValue() == true)
              contadorDeEvaluados++;                
        }
        if((getFacade().obtenerCantidadEvaluacionesEmpleadoPeríodo(sessionMB.getCurrentUser().getEmpleados(), ejbPeriodo.getPeriodoVigente()))!=(contadorDeEvaluados))
        {
              ejbEvaluacion.limpiarCache();
              InicializarValoresHashMapEvaluados();
         }         
     }
     else {
         ejbEvaluacion.limpiarCache();
         InicializarValoresHashMapEvaluados();
     }
        
}    
     
     
     
     /**
      * Este método inicializa el hashMapEvaluados
      */    
          
     private void InicializarValoresHashMapEvaluados(){  
        hashMapEvaluados = new HashMap();
        validarEmpEvaluado(sessionMB.getCurrentUser().getEmpleados());
        validarEmpEvaluado(sessionMB.getCurrentUser().getEmpleados().getInssJefearea());
        if(getParesEmp()!=null && !getParesEmp().isEmpty())
            for(Empleados emp:getParesEmp())
                validarEmpEvaluado(emp);
        if(getSubordinadosEmp()!=null && !getSubordinadosEmp().isEmpty())
            for(Empleados emp:getSubordinadosEmp()) 
                validarEmpEvaluado(emp); 
     
     }
     
     
     
     
     
    
    /**
     * Obtiene una listaTemp de personas que no han sido evaluadas
     * @author Carlos Delgadillo, Nelson Gadea
     * @date 03/06/2013
     * @return listaTemp de Empleados que no han sido evaluados por sus jefes
     */
    public List<Empleados> getEvaluacionesSubordinadosPendientes(){
        listaEvaluacionesSubordinados = new ArrayList<Empleados>();
        
        if(ejbPeriodo.getPeriodoVigente() != null)
            {
            listaEvaluacionesSubordinados = getFacade().obtenerEvaluacionesPendientes(ejbPeriodo.getPeriodoVigente());                    
            }
        return listaEvaluacionesSubordinados;
        }
    
    /**
     * Obtiene la lista de autoevaluaciones que aún no se han realizado
     * @return la lista con las evaluaciones, si existe
     */
    public List<Empleados> getAutoevaluacionesPendientes(){
        List<Empleados> listaAuto = new ArrayList<Empleados>();
        if(ejbPeriodo.getPeriodoVigente()!=null)
            listaAuto = getFacade().obtenerAutoEvaluacionesPendientes(ejbPeriodo.getPeriodoVigente().getIdPeriodo());
        return listaAuto;
    }
    
    /**
     *
     * @return
     */
    public String reporte(){
        return "ReporteEvaluacion?faces-redirect=true";
    }
    
    /**
     * Crea un mapa con los detalles de cada pregunta cerrada para la evaluación
     */
    public void crearMapaDetalles(){
       detallesMap = new HashMap();
         for(Criterio criteriro: listaCriteriosTipoEmpleado){
            for(PreguntaCerrada pc : criteriro.getPreguntaCerradaCollection()){
                for(DetallePreguntaCerrada dpc:pc.getDetallePreguntaCerradaCollection())
                    getDetallesMap().put(pc.getIdPregunta().toString()+dpc.getNivel360().getIdNivel360().toString(),dpc);
            }
        }
   }

    /**
     * Obtiene el detalle de una pregunta cerrada según el nivel 360
     * de la evaluación
     * @param pc la pregunta cerrada
     * @return el detalle
     */
    public DetallePreguntaCerrada obtenerDetalleEvaNivelPregunta(PreguntaCerrada pc){
       
        String llave = pc.getIdPregunta().toString()+current.getIdNivel360().getIdNivel360().toString();

        DetallePreguntaCerrada dt = (DetallePreguntaCerrada) detallesMap.get(llave);
        return dt;
   }
    
    /**
     * Obtiene la listaTemp de puntajes por pregunta
     * @return los puntajes
     * @author Carlos Delgadillo
     * @date 03/03/2013
     */
    public List getListaPuntajes(){
        listaPuntajes = new ArrayList();
//        listaPuntajes = getFacade().obtenerResultados(this.getSelected().getEvaluado().getCedula(), 
//                this.getSelected().getEvaluador().getCedula(), periodoMB.getPeriodoVigente().getIdPeriodo());
        return listaPuntajes;
    }

    
     /**
     * @return the puntajeTotal
     */
    public double getPuntajeTotal() {
        return puntajeTotal;
    }

    /**
     * @param puntajeTotal the puntajeTotal to set
     */
    public void setPuntajeTotal(double puntajeTotal) {
        double sum;
        sum = puntajeTotal;
        this.puntajeTotal += sum;
    }

    /**
     * @return the PromediosGlobales
     */
    public List<Object[]> getPromediosGlobales() {
        if (this.periodoEvaluacion == null)
            return null;
        this.PromediosGlobales = ejbEvaluacion.ObtenerPromedios(getPeriodoEvaluacion().getIdPeriodo());
        
        return PromediosGlobales;
    }

    /**
     * @param PromediosGlobales the PromediosGlobales to set
     */
    public void setPromediosGlobales(List<Object[]> PromediosGlobales) {
        this.PromediosGlobales = PromediosGlobales;
        
    }

    /**
     * @return the PromediosGlobalesFiltrados
     */
    public List<PromedioEmpleado> getPromediosGlobalesFiltrados() {
        return PromediosGlobalesFiltrados;
    }

    /**
     * @param PromediosGlobalesFiltrados the PromediosGlobalesFiltrados to set
     */
    public void setPromediosGlobalesFiltrados(List<PromedioEmpleado> PromediosGlobalesFiltrados) {
        this.PromediosGlobalesFiltrados = PromediosGlobalesFiltrados;
    }
    
    
    
    
    /**
     * Comprueba si el nivel de la evaluación seleccionada es subordinado
     * @return verdadero o falso
     */
    public boolean isSubordinado(){
        if(this.getSelected().getIdNivel360().getDescripcion().equals("Subordinado"))
            return true;
        return false;
    }
    
    
    
    /**
     * Cambia el estado de la evaluación de no impresa a impresa
     * @return URL con la vista de impresión de reportes
     */
    public String setEvaluacionImpresa(){        
        if(!current.getImpresa()){
            current.setImpresa(true);
            this.update(); 
        }
        return "VistaImpresionEvaluacion?faces-redirect=true";
    }
    
    
    /**
     * Obtiene la lista con los promedios de evaluación de cada empleado ordenados
     * de mayor a menor
     * En ocasiones puede generar que el promedio de uno o mas empleados sea
     * infinito
     * @return el valor promedio de cada empleado
     */

    public List<PromedioEmpleado> obtenerPromedios(){
        List<PromedioEmpleado> listaTemp = new ArrayList<>();
        for(Object o : getFacade().obtenerLista(getPeriodoEvaluacion(), 20)){
            Object a[] = (Object[]) o;
            PromedioEmpleado p = new PromedioEmpleado();
            p.setEmpleado((Empleados)a[0]);
            int evaluaciones = 0;
            for(Evaluacion e : p.getEmpleado().getEvaluacionCollection1()){
                if(e.getIdPeriodo().getIdPeriodo().equals(periodoEvaluacion.getIdPeriodo()))
                    evaluaciones++;
            }
            p.setValor((Double)a[1]/evaluaciones);
            listaTemp.add(p);
        }
        Collections.sort(listaTemp,new Comparator<PromedioEmpleado>(){
            @Override
            public int compare(PromedioEmpleado p1, PromedioEmpleado p2){
                return new Double(p2.getValor()).compareTo(p1.getValor());
            }
        });
        return listaTemp;
    }
    
 
    
    /**
     * Obtiene una lista de puntajes
     * @param e empleado
     * @param p periodo de evaluacion
     * @return la lista con los puntajes del empleado
     */
    public Object obtenerLista(Empleados e,Periodo p){
        return getFacade().obtenerLista(p, e);
     }


    /**
     * @return the ControladorDeCapacitaciones
     */
    public CapacitacionesController getControladorDeCapacitaciones() {
        return controladorDeCapacitaciones;
    }

    /**
     * @param ControladorDeCapacitaciones the ControladorDeCapacitaciones to set
     */
    public void setControladorDeCapacitaciones(CapacitacionesController ControladorDeCapacitaciones) {
        this.controladorDeCapacitaciones = ControladorDeCapacitaciones;
    }
    
    /**
     *
     * @return
     */
    public SelectItem[] getCapacitacionesDisponibles(){
    return JsfUtil.getSelectItems(controladorDeCapacitaciones.obtenerCapacitacionesPorArea(current.getEvaluado()), false);
    }

    /**
     * @return the Capacitaciones
     */
    public List<Capacitaciones> getCapacitaciones() {
        Capacitaciones =  controladorDeCapacitaciones.obtenerCapacitacionesPorArea(current.getEvaluado()); 
        return Capacitaciones;
    }

    /**
     * @param Capacitaciones the Capacitaciones to set
     */
    public void setCapacitaciones(List<Capacitaciones> Capacitaciones) {
        this.Capacitaciones = Capacitaciones;
    }

    /**
     * @return the CapacitacionesSeleccionadas
     */
    public List<Capacitaciones> getCapacitacionesSeleccionadas() {
        return CapacitacionesSeleccionadas;
    }

    /**
     * @param CapacitacionesSeleccionadas the CapacitacionesSeleccionadas to set
     */
    public void setCapacitacionesSeleccionadas(List<Capacitaciones> CapacitacionesSeleccionadas) {
        this.CapacitacionesSeleccionadas = CapacitacionesSeleccionadas;
    }

    /**
     * @return the periodoEvaluacion
     */
    public Periodo getPeriodoEvaluacion() {
        return periodoEvaluacion;
    }

    /**
     * @param periodoEvaluacion the periodoEvaluacion to set
     */
    public void setPeriodoEvaluacion(Periodo periodoEvaluacion) {
        this.periodoEvaluacion = periodoEvaluacion;
    }

    /**
     * @return the nivel
     */
    public Nivel360 getNivel() {
        return nivel;
    }

    /**
     * @param nivel the nivel to set
     */
    public void setNivel(Nivel360 nivel) {
        this.nivel = nivel;
    }

    /**
     * @return the evaluacionesPendientesFiiltradas
     */
    public ArrayList<Evaluacion> getEvaluacionesPendientesFiiltradas() {
        return evaluacionesPendientesFiiltradas;
    }

    /**
     * @param evaluacionesPendientesFiiltradas the evaluacionesPendientesFiiltradas to set
     */
    public void setEvaluacionesPendientesFiiltradas(ArrayList<Evaluacion> evaluacionesPendientesFiiltradas) {
        this.evaluacionesPendientesFiiltradas = evaluacionesPendientesFiiltradas;
    }

    /**
     * @return the evaluacionesASubordinadosPendientesFiltradas
     */
    public ArrayList<Evaluacion> getEvaluacionesASubordinadosPendientesFiltradas() {
        return evaluacionesASubordinadosPendientesFiltradas;
    }

    /**
     * @param evaluacionesASubordinadosPendientesFiltradas the evaluacionesASubordinadosPendientesFiltradas to set
     */
    public void setEvaluacionesASubordinadosPendientesFiltradas(ArrayList<Evaluacion> evaluacionesASubordinadosPendientesFiltradas) {
        this.evaluacionesASubordinadosPendientesFiltradas = evaluacionesASubordinadosPendientesFiltradas;
    }

    /**
     * @return the evaluacionesImpresasFiltradas
     */
    public List<Evaluacion> getEvaluacionesImpresasFiltradas() {
        return evaluacionesImpresasFiltradas;
    }

    /**
     * @param evaluacionesImpresasFiltradas the evaluacionesImpresasFiltradas to set
     */
    public void setEvaluacionesImpresasFiltradas(List<Evaluacion> evaluacionesImpresasFiltradas) {
        this.evaluacionesImpresasFiltradas = evaluacionesImpresasFiltradas;
    }

    /**
     * @return the evaluacionesNoImpresasFiltradas
     */
    public List<Evaluacion> getEvaluacionesNoImpresasFiltradas() {
        return evaluacionesNoImpresasFiltradas;
    }

    /**
     * @param evaluacionesNoImpresasFiltradas the evaluacionesNoImpresasFiltradas to set
     */
    public void setEvaluacionesNoImpresasFiltradas(List<Evaluacion> evaluacionesNoImpresasFiltradas) {
        this.evaluacionesNoImpresasFiltradas = evaluacionesNoImpresasFiltradas;
    }

    /**
     * Ordena las escalas ordenadas según su valor para ser presentadas en la vista
     * @return Excelente, Muy bueno, Bueno, etc
     */
    public List<Escalas> getEscalasOrdenadas() {
        if(escalasOrdenadas == null){
            setEscalasOrdenadas(ejbEscalas.obtenerEscalas(periodoMB.getPlanMB().getPlanVigente()));
            Collections.sort(escalasOrdenadas, Collections.reverseOrder(new Comparator<Escalas>() {
            @Override
            public int compare(Escalas e1, Escalas e2) {
                return new Double(e1.getValor()).compareTo(new Double(e2.getValor()));
            }
             }));

            Escalas e1;
            for(Escalas e: escalasOrdenadas){
                if("N/A".equals(e.getDescripcion())){
                    escalasOrdenadas.remove(e);
                    escalasOrdenadas.add(e);
                    break;
                }
            }
        }
        return escalasOrdenadas;
    }

    /**
     * @param escalasOrdenadas the escalasOrdenadas to set
     */
    public void setEscalasOrdenadas(List<Escalas> escalasOrdenadas) {
        this.escalasOrdenadas = escalasOrdenadas;
    }

    /**
     * @return the audiController
     */
    public AuditoriaController getAudiController() {
        return audiController;
    }

    /**
     * @param audiController the audiController to set
     */
    public void setAudiController(AuditoriaController audiController) {
        this.audiController = audiController;
    }

    /**
     *
     */
    @FacesConverter(forClass = Evaluacion.class)
    public static class EvaluacionControllerConverter implements Converter {
  
        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            EvaluacionController controller = (EvaluacionController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "evaluacionController");
            return controller.ejbEvaluacion.find(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
}

        String getStringKey(java.lang.String value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Evaluacion) {
                Evaluacion o = (Evaluacion) object;
                return getStringKey(o.getIdEvaluacion());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Evaluacion.class.getName());
            }
        }
    }
    
   

}
