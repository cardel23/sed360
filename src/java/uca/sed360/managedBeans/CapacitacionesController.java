package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.Capacitaciones;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.CapacitacionesFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import uca.sed360.jpaEntities.CentroDeCostos;
import uca.sed360.jpaEntities.Empleados;
import uca.sed360.jpaEntities.Periodo;

/**
 *
 * @author Estacion 2
 */
@ManagedBean(name = "capacitacionesController")
@SessionScoped
public class CapacitacionesController implements Serializable {
    
    @ManagedProperty(value = "#{auditoriaController}")
    private AuditoriaController audiController;

    private Capacitaciones current;
    @EJB
    private uca.sed360.ejbs.CapacitacionesFacade ejbFacade;
    @EJB 
    private uca.sed360.ejbs.PeriodoFacade ejbPeriodo;
    @EJB
    private uca.sed360.ejbs.CentroDeCostosFacade ejbCentroCostos;
    @EJB
    private uca.sed360.ejbs.EmpleadosFacade ejbEmpleado;
    
    private List<Capacitaciones> listaCapacitaciones;
    private List<CentroDeCostos> listaCentroCostos;
    private List<CentroDeCostos> allCentroCostos;
    private String ruta;
    private String nombreReporte;
    private HashMap parametros;
    

    /**
     *
     */
    public CapacitacionesController() {
    }

    /**
     *
     * @return
     */
    public Capacitaciones getSelected() {
        if (current == null) {
            current = new Capacitaciones();
        }
        return current;
    }

    private CapacitacionesFacade getFacade() {
        return ejbFacade;
    }
    
    //Inicializador
    /**
     *
     */
    public void inicializar(){
    // allCentroCostos = new ArrayList<CentroDeCostos>();
     // allCentroCostos = ejbCentroCostos.findAll();
     // allCentroCostos.removeAll(current.getCentroDeCostosList());
    }
    
    //Lista de capacitaciones
    /**
     *
     * @return
     */
    public List<Capacitaciones> getListaCapacitaciones() {
       listaCapacitaciones = getFacade().findAll();
        return listaCapacitaciones;
    }

    /**
     *
     * @param listaCapacitaciones
     */
    public void setListaCapacitaciones(List<Capacitaciones> listaCapacitaciones) {
        this.listaCapacitaciones = listaCapacitaciones;
    }
    
    // Getter and Setter for Current
    /**
     *
     * @return
     */
    public Capacitaciones getCurrent() {
        if (current == null) {
            current = new Capacitaciones();
        }
        return current;
    }

    /**
     *
     * @param current
     */
    public void setCurrent(Capacitaciones current) {
        this.current = current;
    }
    
    //Getter y Setter allCentroCostos
    /**
     *
     * @return
     */
    public List<CentroDeCostos> getAllCentroCostos() {
        if(allCentroCostos == null) {
        allCentroCostos = ejbCentroCostos.findAll();
       // allCentroCostos.removeAll(current.getCentroDeCostosList());
        }
       
        return allCentroCostos;
    }

    /**
     *
     * @param allCentroCostos
     */
    public void setAllCentroCostos(List<CentroDeCostos> allCentroCostos) {
        this.allCentroCostos = allCentroCostos;
    }
    
    
    //Agrega un nuevo centro de costos a la lista
    /**
     *
     * @param cc
     */
    public void agregarArea(CentroDeCostos cc) {
        listaCentroCostos.add(cc);
        allCentroCostos.remove(cc); 
        JsfUtil.addSuccessMessage("Se ha asignado la capacitación al área: " + cc.getDescripcion());
    }
    
    /**
     * Asignar una capacitacion a todos los centros de costo
     * @autor Carlos Delgadillo
     * @fecha 26/09/2013
     */
    public void agregarArea(){
        allCentroCostos = ejbCentroCostos.findAll();
        listaCentroCostos.addAll(allCentroCostos);
        allCentroCostos.removeAll(allCentroCostos);
        JsfUtil.addSuccessMessage("Se ha agregado la capacitación correctamente");
    }
    
    //Getter and Setter for Centros de Costos
    /**
     *
     * @return
     */
    public List<CentroDeCostos> getListaCentroCostos() {
        if(listaCentroCostos == null) {
            listaCentroCostos = current.getCentroDeCostosList();
        }
        return listaCentroCostos;
    }

    /**
     *
     * @param listaCentroCostos
     */
    public void setListaCentroCostos(List<CentroDeCostos> listaCentroCostos) {
        this.listaCentroCostos = listaCentroCostos;
    }
    
    /**
     *
     * @param periodo
     * @return
     */
    public List<Capacitaciones> getCapacitacionesPorArea(Periodo periodo){
        listaCapacitaciones = getFacade().listaCapacitacionesPorArea(periodo);
        return listaCapacitaciones;
    }
    
    /**
     *
     * @param evaluado
     * @return
     */
    public List<Capacitaciones> obtenerCapacitacionesPorArea(Empleados evaluado){
       return ejbFacade.obtenerCapacitcionesPorArea(evaluado.getCentroCosto().getIdCentrocostos());
         }
    
    /**
     *
     * @return
     */
    public Periodo getPeriodo(){
        return ejbPeriodo.getPeriodoVigente();
    }
    
    

    /**
     *
     * @return
     */
    public String prepareList() {
        return "List";
    }


    /**
     *
     * @return
     */
    public String prepareCreate() {
      current = new Capacitaciones();
      listaCentroCostos = new ArrayList<CentroDeCostos>();
      //allCentroCostos = new ArrayList<CentroDeCostos>();
      allCentroCostos = ejbCentroCostos.findAll();
      return "AgregarCapacitacion?faces-redirect=true";
  }

    /**
     *
     * @return
     */
    public String create() {
        try {
           current.setCentroDeCostosList(listaCentroCostos); 
           getFacade().create(current);
            JsfUtil.addSuccessMessage("Se ha agregado exitosamente la nueva capacitación");
            audiController.guardarRegistroActividad(" agrego la capacitacion "+current.getDescripcion());
            return "ListaCapacitaciones?faces-redirect=true";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Ha habido un error de persistencia");
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String prepareEdit() {
        //current = (Capacitaciones) getItems().getRowData();
        return "Edit";
    }

    /**
     *
     * @return
     */
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage("La capacitación ha sido actualizada correctamente");
            audiController.guardarRegistroActividad(" actualizo la capacitacion "+current.getDescripcion());
            return "ListaCapacitaciones?faces-redirect=true";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Ha habido un error de persistencia");
            return null;
        }
    }

    /**
     *
     */
    public void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage("La capacitación ha sido eliminada satisfactoriamente");
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Ha habido un error de persistencia");
        }
    }
    
    
    /**
     * @return the ruta
     */
    public String getRuta() {
        return ruta;
    }

    /**
     * @param ruta the ruta to set
     */
    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    /**
     * @return the nombreReporte
     */
    public String getNombreReporte() {
        return nombreReporte;
    }

    /**
     * @param nombreReporte the nombreReporte to set
     */
    public void setNombreReporte(String nombreReporte) {
        this.nombreReporte = nombreReporte;
    }

    /**
     * @return the parametros
     */
    public HashMap getParametros() {
        if (parametros == null){
            parametros = new HashMap();
        }
        return parametros;
    }

    /**
     * @param parametros the parametros to set
     */
    public void setParametros(HashMap parametros) {
        this.parametros = parametros;
    }

    /**
     * @return the audiController
     */
    public AuditoriaController getAudiController() {
        return audiController;
    }

    /**
     * @param audiController the audiController to set
     */
    public void setAudiController(AuditoriaController audiController) {
        this.audiController = audiController;
    }


    
    /**
     *
     */
    @FacesConverter(forClass = Capacitaciones.class, value = "capacitacionesConverter")
    public static class CapacitacionesConverter implements Converter{

        /**
         *
         * @param context
         * @param component
         * @param value
         * @return
         */
        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            if(value == null || value.length() == 0)
                return null;
            CapacitacionesController capacontroler = (CapacitacionesController) context.getApplication()
                    .getELResolver().getValue(context.getELContext(), null, "capacitacionesController");
            return capacontroler.ejbFacade.find(getKey(value));
                
        }
        
        
        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }
        
          String getStringKey(java.lang.Integer value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param context
         * @param component
         * @param value
         * @return
         */
        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            if (value == null) {
                return null;
            }
            if (value instanceof Capacitaciones) {
                Capacitaciones o = (Capacitaciones) value;
                return getStringKey(o.getIdCapacitacion());
            } else {
                throw new IllegalArgumentException("object " + value + " is of type " + value.getClass().getName() + "; expected type: " + Capacitaciones.class.getName());
            }
        }
    
    }

}
