package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.RespuestaPreguntaAbierta;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.RespuestaPreguntaAbiertaFacade;
import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;

/**
 * Controlador que maneja las preguntas abiertas en una evaluación
 * @author Estacion 2
 */
@ManagedBean(name = "respuestaPreguntaAbiertaController")
@SessionScoped
public class RespuestaPreguntaAbiertaController implements Serializable {

    private RespuestaPreguntaAbierta current;

    @EJB
    private uca.sed360.ejbs.RespuestaPreguntaAbiertaFacade ejbFacade;

    /**
     * Constructor
     */
    public RespuestaPreguntaAbiertaController() {
    }

    /**
     *
     * @return
     */
    public RespuestaPreguntaAbierta getSelected() {
        if (current == null) {
            current = new RespuestaPreguntaAbierta();
            current.setRespuestaPreguntaAbiertaPK(new uca.sed360.jpaEntities.RespuestaPreguntaAbiertaPK());
        }
        return current;
    }

    private RespuestaPreguntaAbiertaFacade getFacade() {
        return ejbFacade;
    }


    /**
     *
     * @return
     */
    public String prepareList() {
        return "List";
    }

    /**
     *
     * @return
     */
    public String prepareView() {
        return "View";
    }

    /**
     *
     * @return
     */
    public String prepareCreate() {
        current = new RespuestaPreguntaAbierta();
        current.setRespuestaPreguntaAbiertaPK(new uca.sed360.jpaEntities.RespuestaPreguntaAbiertaPK());
        return "Create";
    }

    /**
     *
     * @return
     */
    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RespuestaPreguntaAbiertaCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String prepareEdit() {
        return "Edit";
    }

    /**
     *
     * @return
     */
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RespuestaPreguntaAbiertaUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String destroy() {
        performDestroy();
        return "List";
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RespuestaPreguntaAbiertaDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }


    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    /**
     *
     */
    @FacesConverter(forClass = RespuestaPreguntaAbierta.class)
    public static class RespuestaPreguntaAbiertaControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            RespuestaPreguntaAbiertaController controller = (RespuestaPreguntaAbiertaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "respuestaPreguntaAbiertaController");
            return controller.ejbFacade.find(getKey(value));
        }

        uca.sed360.jpaEntities.RespuestaPreguntaAbiertaPK getKey(String value) {
            uca.sed360.jpaEntities.RespuestaPreguntaAbiertaPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new uca.sed360.jpaEntities.RespuestaPreguntaAbiertaPK();
            key.setPregunta(Integer.parseInt(values[0]));
            key.setAsignacion(values[1]);
            return key;
        }

        String getStringKey(uca.sed360.jpaEntities.RespuestaPreguntaAbiertaPK value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value.getPregunta());
            sb.append(SEPARATOR);
            sb.append(value.getAsignacion());
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof RespuestaPreguntaAbierta) {
                RespuestaPreguntaAbierta o = (RespuestaPreguntaAbierta) object;
                return getStringKey(o.getRespuestaPreguntaAbiertaPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + RespuestaPreguntaAbierta.class.getName());
            }
        }
    }
}
