package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.RespuestaPreguntaCerrada;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.RespuestaPreguntaCerradaFacade;
import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import uca.sed360.jpaEntities.Criterio;
import uca.sed360.jpaEntities.Nivel360;
import uca.sed360.jpaEntities.PreguntaCerrada;

/**
 * Controlador que maneja las respuestas a una pregunta cerrada en la evaluación
 * @author Estacion 2
 */
@ManagedBean(name = "respuestaPreguntaCerradaController")
@SessionScoped
public class RespuestaPreguntaCerradaController implements Serializable {

    private RespuestaPreguntaCerrada current;
    @EJB
    private uca.sed360.ejbs.RespuestaPreguntaCerradaFacade ejbFacade;


    /**
     * Constructor
     */
    public RespuestaPreguntaCerradaController() {
    }

    /**
     *
     * @return
     */
    public RespuestaPreguntaCerrada getSelected() {
        if (current == null) {
            current = new RespuestaPreguntaCerrada();
            current.setRespuestaPreguntaCerradaPK(new uca.sed360.jpaEntities.RespuestaPreguntaCerradaPK());
        }
        return current;
    }

    private RespuestaPreguntaCerradaFacade getFacade() {
        return ejbFacade;
    }

    /**
     * Verifica si existen respuestas para las preguntas cerradas
     * en un nivel 360 específico
     * @param pc
     * @param nivel360
     * @return verdadero o falso
     */
    public boolean existeRespuestaPreguntaCerradaNivel360(PreguntaCerrada pc,Nivel360 nivel360){
        boolean evaluacionesRealizadasInstrumentoTipoEmpNivel360=getFacade().existRespuestasPreguntaCerradaNivel360(pc, nivel360);
        return evaluacionesRealizadasInstrumentoTipoEmpNivel360;
    }

    /**
     *
     * @return
     */
    public String prepareList() {
        return "List";
    }

    /**
     *
     * @return
     */
    public String prepareView() {
        return "View";
    }

    /**
     *
     * @return
     */
    public String prepareCreate() {
        current = new RespuestaPreguntaCerrada();
        current.setRespuestaPreguntaCerradaPK(new uca.sed360.jpaEntities.RespuestaPreguntaCerradaPK());
        return "Create";
    }

    /**
     *
     * @return
     */
    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RespuestaPreguntaCerradaCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    
    /**
     * Verifica si existen respuestas para las preguntas cerradas de un criterio
     * según el nivel 360
     * @param c
     * @param n
     * @return verdadero o falso
     */
    public boolean existeRespuestasAPreguntasPorCriterioyNivel(Criterio c, Nivel360 n){   
        return getFacade().existeRespuestasAPreguntasPorCriterioyNivel(c, n);
    }

    /**
     *
     * @return
     */
    public String prepareEdit() {
        return "Edit";
    }

    /**
     *
     * @return
     */
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RespuestaPreguntaCerradaUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String destroy() {
        performDestroy();
        return "List";
    }


    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RespuestaPreguntaCerradaDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }


    /**
     *
     */
    @FacesConverter(forClass = RespuestaPreguntaCerrada.class)
    public static class RespuestaPreguntaCerradaControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            RespuestaPreguntaCerradaController controller = (RespuestaPreguntaCerradaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "respuestaPreguntaCerradaController");
            return controller.ejbFacade.find(getKey(value));
        }

        uca.sed360.jpaEntities.RespuestaPreguntaCerradaPK getKey(String value) {
            uca.sed360.jpaEntities.RespuestaPreguntaCerradaPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new uca.sed360.jpaEntities.RespuestaPreguntaCerradaPK();
            key.setPregunta((values[0]));
            key.setAsignacion(values[1]);
            return key;
        }

        String getStringKey(uca.sed360.jpaEntities.RespuestaPreguntaCerradaPK value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value.getPregunta());
            sb.append(SEPARATOR);
            sb.append(value.getAsignacion());
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof RespuestaPreguntaCerrada) {
                RespuestaPreguntaCerrada o = (RespuestaPreguntaCerrada) object;
                return getStringKey(o.getRespuestaPreguntaCerradaPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + RespuestaPreguntaCerrada.class.getName());
            }
        }
    }
}
