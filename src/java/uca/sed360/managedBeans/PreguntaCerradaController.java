package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.PreguntaCerrada;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.PreguntaCerradaFacade;
import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;

/**
 * Controlador que maneja las preguntas cerradas para los criterios de evaluación
 * @author Estacion 2
 */
@ManagedBean(name = "preguntaCerradaController")
@SessionScoped
public class PreguntaCerradaController implements Serializable {

    private PreguntaCerrada current;
    @EJB
    private uca.sed360.ejbs.PreguntaCerradaFacade ejbFacade;


    /**
     * Constructor
     */
    public PreguntaCerradaController() {
    }

    /**
     *
     * @return
     */
    public PreguntaCerrada getSelected() {
        if (current == null) {
            current = new PreguntaCerrada();
        }
        return current;
    }

    private PreguntaCerradaFacade getFacade() {
        return ejbFacade;
    }


    /**
     *
     * @return
     */
    public String prepareList() {
        return "List";
    }

    /**
     *
     * @return
     */
    public String prepareView() {
        return "View";
    }

    /**
     *
     * @return
     */
    public String prepareCreate() {
        current = new PreguntaCerrada();
        return "Create";
    }

    /**
     *
     * @return
     */
    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PreguntaCerradaCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String prepareEdit() {
        return "Edit";
    }

    /**
     *
     * @return
     */
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PreguntaCerradaUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String destroy() {
        performDestroy();
        return "List";
    }


    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PreguntaCerradaDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

   
    /**
     * Obtiene el id de la pregunta como una cadena
     * @param p
     * @return id de la pregunta
     */
    public String getIdAsString(PreguntaCerrada p){
        return p.getIdPregunta();
    }
    

    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    /**
     *
     */
    @FacesConverter(forClass = PreguntaCerrada.class)
    public static class PreguntaCerradaControllerConverter implements Converter {

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PreguntaCerradaController controller = (PreguntaCerradaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "preguntaCerradaController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof PreguntaCerrada) {
                PreguntaCerrada o = (PreguntaCerrada) object;
                return getStringKey(o.getIdPregunta());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + PreguntaCerrada.class.getName());
            }
        }
    }
}
