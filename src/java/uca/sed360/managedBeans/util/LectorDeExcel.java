
package uca.sed360.managedBeans.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Clase que se encarga de leer archivos de excel para procesar sus datos
 * @author Estacion 2
 * @deprecated No se usa porque no se definió el algoritmo de carga masiva para
 * registro y actualización de los datos de los empleados
 */
public class LectorDeExcel {
    
    private FileInputStream archivoExcel;
    private ArrayList<Object[]> listaDeEmpleados;
    private int filas = 0;
    private int columnas = 0;
    
    /**
     * Constructor
     */
    public LectorDeExcel(){

    }
    
    /**
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static ArrayList<Object[]> obtenerListaEmpleados(FileInputStream file) throws IOException{
        
            XSSFWorkbook  workbook = new XSSFWorkbook(file);
            XSSFSheet sheet = workbook.getSheetAt(0);
            
            Iterator<Row> iteradorDeFila = sheet.iterator();            
            while(iteradorDeFila.hasNext()){
                Row fila = iteradorDeFila.next();
                
                Iterator<Cell> iteradorCelda = fila.cellIterator();
                
                while(iteradorCelda.hasNext()){
                    Cell celda = iteradorCelda.next();
                    
                   
                }
                
            }
        
        return null;
    }
    
    /**
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void lectura() throws FileNotFoundException, IOException{
        FileInputStream file = new FileInputStream(new File(""));
        
        XSSFWorkbook libro = new XSSFWorkbook(file);
        XSSFSheet hoja = libro.getSheetAt(0);
        ArrayList<Object[]> lista = new ArrayList<>();
        Iterator<Row> rowIterator = hoja.iterator();
        int numero = 1;
        while (rowIterator.hasNext()){
            Row columna = rowIterator.next();
            Iterator<Cell> cellIterator = columna.cellIterator();
            Object o[] =  new Object[12];
            int contador = 0;
            while(cellIterator.hasNext()){
                Cell celda = cellIterator.next();
                
                switch (celda.getCellType()){
                    case Cell.CELL_TYPE_NUMERIC:
//                        System.out.print(celda.getNumericCellValue()+"t");
                        o[contador] = celda.getNumericCellValue();
                        contador ++;
                        break;
                    case Cell.CELL_TYPE_STRING:
//                        System.out.print(celda.getStringCellValue() +"t");
                        o[contador] = celda.getStringCellValue();
                        contador ++;
                        break;
                }
            }
            System.out.println("----------Registro nº ["+numero+"] procesado correctamente-------------");
            lista.add(o);
            numero++;
        }
        file.close();
    }

    /**
     * @return the filas
     */
    public int getFilas() {
        return filas;
    }

    /**
     * @return the columnas
     */
    public int getColumnas() {
        return columnas;
    }

    /**
     * @param columnas the columnas to set
     */
    public void setColumnas(int columnas) {
        this.columnas = columnas;
    }
}
    