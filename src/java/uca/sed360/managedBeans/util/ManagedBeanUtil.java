/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans.util;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * Se encarga de convertir las cadenas de texto para presentar en pantalla
 * @author Nelson Gadea
 */
@ManagedBean (name = "managedBeanUtil")
@SessionScoped
public class ManagedBeanUtil implements Serializable{
    
    
    /**
     * Convierte las mayúsculas del texto a minúsculas
     * @param texto
     * @return
     */
    
    public String estandarizarNombre(String texto){
      String textAux = texto.trim();
       char[] ArregloAux = textAux.trim().toCharArray();
       for(int i = 0; i < ArregloAux.length; i++){
           if(ArregloAux[i] == ' '){
                            
                    return textAux.substring(0,1).toUpperCase()
                       +textAux.substring(1,i).toLowerCase()
                       +textAux.substring(i,i+1)
                       + estandarizarNombre(textAux.substring(i+1));
           } 
       }
       return textAux.substring(0,1).toUpperCase()+textAux.substring(1).toLowerCase();
    }
    
    
}
