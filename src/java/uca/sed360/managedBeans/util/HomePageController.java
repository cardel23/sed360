/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans.util;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.PieChartModel;  
import uca.sed360.jpaEntities.Nivel360;


/**
 * Controlador que maneja los mensajes de la página de inicio
 * @author Gerardo
 */
@ManagedBean(name = "homePageController")
@SessionScoped
public class HomePageController implements Serializable {

    @EJB
    private uca.sed360.ejbs.PeriodoFacade ejbPeriodos;
    private int tiempoRestanteProximoPeriodo;
       
    /**
     *
     */
    public HomePageController() {
    }

    //Getter y Setter de tiempoRestanteProximoPeriodo

    /**
     *
     * @return el tiempo restante para que inicie 
     * el siguiente periodo de evaluación
     */
    public int getTiempoRestanteProximoPeriodo() {
        tiempoRestanteProximoPeriodo = ejbPeriodos.obtenerDiasProximoPeriodo();
        return tiempoRestanteProximoPeriodo;
    }

    /**
     * @param tiempoRestanteProximoPeriodo the tiempoRestanteProximoPeriodo to set
     */
    public void setTiempoRestanteProximoPeriodo(int tiempoRestanteProximoPeriodo) {
        this.tiempoRestanteProximoPeriodo = tiempoRestanteProximoPeriodo;
    }
  
}
