/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans.util;

import uca.sed360.jpaEntities.DetallePreguntaCerrada;
import uca.sed360.jpaEntities.RespuestaPreguntaCerrada;

/**
 * Clase auxiliar para generación de reportes de evaluación
 * @author Estacion 2
 */
public class PreguntaCerradaReportes {
    private String criterio;
    private String redaccionPreguntaCerrada;
    private String respuestaPreguntaCerrada;
    private double puntajePregunta;
    private DetallePreguntaCerrada detalle;
    private RespuestaPreguntaCerrada respuesta;

    /**
     * @return the criterio
     */
    public String getCriterio() {
        return criterio;
    }

    /**
     * @param criterio the criterio to set
     */
    public void setCriterio(String criterio) {
        this.criterio = criterio;
    }

    /**
     * @return the redaccionPreguntaCerrada
     */
    public String getRedaccionPreguntaCerrada() {
        return redaccionPreguntaCerrada;
    }

    /**
     * @param redaccionPreguntaCerrada the redaccionPreguntaCerrada to set
     */
    public void setRedaccionPreguntaCerrada(String redaccionPreguntaCerrada) {
        this.redaccionPreguntaCerrada = redaccionPreguntaCerrada;
    }

    /**
     * @return the respuestaPreguntaCerrada
     */
    public String getRespuestaPreguntaCerrada() {
        return respuestaPreguntaCerrada;
    }

    /**
     * @param respuestaPreguntaCerrada the respuestaPreguntaCerrada to set
     */
    public void setRespuestaPreguntaCerrada(String respuestaPreguntaCerrada) {
        this.respuestaPreguntaCerrada = respuestaPreguntaCerrada;
    }

    /**
     * @return the puntajePregunta
     */
    public double getPuntajePregunta() {
        return puntajePregunta;
    }

    /**
     * @param puntajePregunta the puntajePregunta to set
     */
    public void setPuntajePregunta(double puntajePregunta) {
        this.puntajePregunta = puntajePregunta;
    }

    /**
     * @return the detalle
     */
    public DetallePreguntaCerrada getDetalle() {
        return detalle;
    }

    /**
     * @param detalle the detalle to set
     */
    public void setDetalle(DetallePreguntaCerrada detalle) {
        this.detalle = detalle;
    }

    /**
     * @return the respuesta
     */
    public RespuestaPreguntaCerrada getRespuesta() {
        return respuesta;
    }

    /**
     * @param respuesta the respuesta to set
     */
    public void setRespuesta(RespuestaPreguntaCerrada respuesta) {
        this.respuesta = respuesta;
    }
}
