/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans.util;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.UploadedFile;



/**
 * Controlador de archivos de excel
 * @author Estacion 2
 * @deprecated 
 */
@ManagedBean
@SessionScoped
public class FileController implements Serializable{
    
    private UploadedFile archivo;
    private ArrayList<Object[]> listaDeEmpleados;
    
    
    /**
     * Constructor
     */
    public FileController(){
    }
    
    /**
     * Carga un archivo de excel
     * @throws IOException
     */
    public void cargar() throws IOException{
        if(archivo==null){
            FacesMessage msg = new FacesMessage("archivo nulo");
            FacesContext.getCurrentInstance().addMessage(null, msg);
                                    
        }
        else{
        FacesMessage ms = new FacesMessage("el archivo "+archivo.getFileName()+" fue subido con éxito");
        FacesContext.getCurrentInstance().addMessage(null, ms);
        }
    }
    
    /**
     * @return the archivo
     */
    public UploadedFile getArchivo() {
        return archivo;
    }

    /**
     * @param archivo the archivo to set
     */
    public void setArchivo(UploadedFile archivo) {
        this.archivo = archivo;
    }




}
