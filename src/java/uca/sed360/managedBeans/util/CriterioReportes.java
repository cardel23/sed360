/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans.util;

import java.util.List;

/**
 * Clase auxuliar que contiene los criterios para los reportes de evaluación
 * @author Carlos Delgadillo
 * @since 10/11/2013
 */
public class CriterioReportes {
    
    private String descripcion;
    private List<PreguntaCerradaReportes> preguntaCerradaList;

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the pregunta
     */
    public List<PreguntaCerradaReportes> getPreguntaCerradaList() {
        return preguntaCerradaList;
    }

    /**
     * @param preguntaCerradaList the pregunta to set
     */
    public void setPreguntaCerradaList(List<PreguntaCerradaReportes> preguntaCerradaList) {
        this.preguntaCerradaList = preguntaCerradaList;
    }
}
