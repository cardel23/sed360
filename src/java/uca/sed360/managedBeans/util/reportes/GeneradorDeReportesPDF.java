/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans.util.reportes;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import net.sf.jasperreports.engine.JRException;
import uca.sed360.ejbs.EvaluacionFacade;
import uca.sed360.jpaEntities.Capacitaciones;
import uca.sed360.jpaEntities.CentroDeCostos;
import uca.sed360.jpaEntities.Criterio;
import uca.sed360.jpaEntities.DetallePreguntaCerrada;
import uca.sed360.jpaEntities.Empleados;
import uca.sed360.jpaEntities.Evaluacion;
import uca.sed360.jpaEntities.Periodo;
import uca.sed360.jpaEntities.PreguntaCerrada;
import uca.sed360.jpaEntities.RespuestaPreguntaAbierta;
import uca.sed360.jpaEntities.RespuestaPreguntaCerrada;
import uca.sed360.managedBeans.CapacitacionesController;
import uca.sed360.managedBeans.CentroDeCostosController;
import uca.sed360.managedBeans.EmpleadosController;
import uca.sed360.managedBeans.EvaluacionController;
import uca.sed360.managedBeans.PeriodoController;
import uca.sed360.managedBeans.SessionMB;
import uca.sed360.managedBeans.util.PreguntaCerradaReportes;


/**
 * Controlador auxiliar que maneja la generación de reportes en formato PDF
 * @author Nelson
 * @param <T>
 */
@ManagedBean(name = "repController")
@SessionScoped
public class GeneradorDeReportesPDF<T> implements Serializable{
    @ManagedProperty(value="#{consultorDePromedios}")
    private ConsultorDePromedios consultor;
    
    @ManagedProperty(value="#{empleadosController}")
    private EmpleadosController empController;
    
    @ManagedProperty(value="#{capacitacionesController}")
    private CapacitacionesController capController;
    
    @ManagedProperty(value="#{centroDeCostosController}")
    private CentroDeCostosController centroController;
    
    @ManagedProperty(value="#{evaluacionController}")
    private EvaluacionController evaluacionController;
    
    @ManagedProperty(value="#{periodoController}")
    private PeriodoController periodoController;
    
    @ManagedProperty(value = "#{sessionMB}")
    private SessionMB sessionMB;
    
    private ReportesUtil ru = new ReportesUtil();
    private ReporteEvaluacion rep = new ReporteEvaluacion();

    @EJB
    private uca.sed360.ejbs.PeriodoFacade periodoejbFacade;
    @EJB
    private uca.sed360.ejbs.PlanEstrategicoFacade planejbFacade;
    @EJB
    private uca.sed360.ejbs.PonderacionFacade ponderacionejbFacade;
    @EJB
    private uca.sed360.ejbs.EvaluacionFacade evaluacionFacade;
    @EJB
    private uca.sed360.ejbs.ConsultasFacade consultasFacade;
    @EJB
    private uca.sed360.ejbs.CentroDeCostosFacade centroFacade;
    
    
    private final String logouca = "/Reportes/images/logouca.jpg";
    private final String logosj = "/Reportes/images/logosj.jpg";
    private HashMap map;
    private String ruta;
    private String nombreRep;
    private Evaluacion EVALUACION;
    private List<ReporteEvaluacion> list;
    private double totalPuntaje;
    
    /**
     * Constructor
     */
    public GeneradorDeReportesPDF() {
    }
     

    /**
     * @deprecated
     * @throws JRException
     * @throws IOException
     */
    public void generarReportePeriodos() throws JRException, IOException{
        HashMap params = new HashMap();
        params.put("logo", FacesContext.getCurrentInstance().getExternalContext().getRealPath("/Reportes/images/logo_uca.jpg"));
        getRu().setParametros(params);
        getRu().setLista(this.getPeriodoejbFacade().findAll());
        getRu().setNombreReporte("Planes Estrategicos con sus criterios");
        getRu().setRuta("/Reportes/ReportesDePeriodos/Periodos.jasper");
        getRu().exportarPDF(true);        
    }
    
    
    
    
    /**
     * @deprecated
     * @throws JRException
     * @throws IOException
     */
    public void generarReportePlanYCriterios() throws JRException, IOException{
        HashMap params = new HashMap();
        params.put("logo", ReportesUtil.CONTEXTO.getRealPath("/Reportes/images/logo_uca.jpg"));
        getRu().setParametros(params);
        getRu().setLista(this.getPlanejbFacade().findAll());
        getRu().setNombreReporte("Planes Estrategicos con sus criterios");
        getRu().setRuta("/Reportes/ReportesDePlanEstrategico/PlanCriteriosBCDS.jasper");
        getRu().exportarPDF(true);        
    }
    
    /**
     * Genera el repoorte de las autoevaluaciones pendientes en PDF <br/>
     * Autor: Carlos Delgadillo
     * @throws JRException
     * @throws IOException
     */
    public void generarReporteAutoEvaluacionesPendientes() throws JRException, IOException{
        if(evaluacionController.getEvaluacionesPendientesFiiltradas() != null)
            reporteGenerico((List<T>) evaluacionController.getEvaluacionesPendientesFiiltradas(), new HashMap<>(), "/Reportes/Evaluaciones/autoevaluacionesPendientes.jasper", "Autoevaluaciones pendientes");       
        else
            reporteGenerico((List<T>) evaluacionController.getAutoevaluacionesPendientes(), new HashMap<>(), "/Reportes/Evaluaciones/autoevaluacionesPendientes.jasper", "Autoevaluaciones pendientes");       
    }   
    
    /**
     * Genera el reporte de las evaluaciones a subordinados pendientes en PDF <br/>
     * Autor: Carlos Delgadillo
     * @throws JRException
     * @throws IOException
     */
    public void generarReporteEvaluacionesASubordinadosPendientes()  throws JRException, IOException{
        if(evaluacionController.getEvaluacionesASubordinadosPendientesFiltradas() != null)
            reporteGenerico((List<T>) evaluacionController.getEvaluacionesASubordinadosPendientesFiltradas(), new HashMap<>(), "/Reportes/Evaluaciones/evaluacionesSubPendientes.jasper", "Evaluaciones a subordinados pendientes ");
        else
            reporteGenerico((List<T>) evaluacionController.getEvaluacionesSubordinadosPendientes(), new HashMap<>(), "/Reportes/Evaluaciones/evaluacionesSubPendientes.jasper", "Evaluaciones a subordinados pendientes ");
            
        }
    
    /**
     *
     * @param opcion
     * @throws JRException
     * @throws IOException
     */
    public void generarReporteEstatusImpresion(boolean opcion)throws JRException, IOException{ 
        map = new HashMap();
        if(opcion == true){
            map.put("imp", " impresas");
            if(getEvaluacionController().getEvaluacionesImpresasFiltradas() != null)
                reporteGenerico((List<T>) getEvaluacionController().getEvaluacionesImpresasFiltradas(), map, "/Reportes/Evaluaciones/evaluacionesImpresas.jasper", "impresas");
            else                
                reporteGenerico((List<T>) getEvaluacionController().obtenerEvaluacionesImpresas(opcion), map, "/Reportes/Evaluaciones/evaluacionesImpresas.jasper", "impresas");}
        else{
            map.put("imp", " no impresas");
            if(getEvaluacionController().getEvaluacionesNoImpresasFiltradas()!= null)
                reporteGenerico((List<T>) getEvaluacionController().getEvaluacionesImpresasFiltradas(), map, "/Reportes/Evaluaciones/evaluacionesImpresas.jasper", "no impresas");
            else                
                reporteGenerico((List<T>) getEvaluacionController().obtenerEvaluacionesImpresas(opcion), map, "/Reportes/Evaluaciones/evaluacionesImpresas.jasper", "no impresas");
        }
    }
    
    /**
     * Genera el reporte de empleados activos en PDF
     * @throws JRException
     * @throws IOException
     */
    
    public void generarReporteDeEmpleadosActivos() throws JRException, IOException{
       HashMap p = new HashMap();
       p.put("logo", ReportesUtil.CONTEXTO.getRealPath(logouca));
       p.put("logosj", ReportesUtil.CONTEXTO.getRealPath(logosj));
       ReportesUtil rep = new ReportesUtil();
       rep.setRuta("/empleados/empleadosactivos.jasper");
       rep.setLista(empController.obtenerActivos());
       rep.setNombreReporte("Reporte de Empleados Activos");
       rep.exportarPDF(true);
    }

    /**
     * 
     * @throws JRException
     * @throws IOException
     */
    
    public void generarReporteDeCapacitacionesPorArea() throws JRException, IOException{
       HashMap p = new HashMap();
       p.put("logo", FacesContext.getCurrentInstance().getExternalContext().getRealPath(logouca));
       p.put("logosj", FacesContext.getCurrentInstance().getExternalContext().getRealPath(logosj));
       p.put("rutaSub", FacesContext.getCurrentInstance().getExternalContext().getRealPath("/Reportes/Capacitaciones"));
       ReportesUtil rep = new ReportesUtil();
       rep.setRuta("/Reportes/Capacitaciones/repCapacitacionesCDC.jasper");
       rep.setLista(centroController.getCentrosConEvaluaciones());
       rep.setNombreReporte("Reporte de capacitaciones por área en período actual");
       rep.setParametros(p);
       rep.exportarPDF(true);             
    }
    
       
       
       
    /**
     *
     * @throws JRException
     * @throws IOException
     */
    public void generarReporteDePromediosPorCriterios() throws JRException, IOException{
        HashMap parametros = new  HashMap();
        parametros.put("logo", FacesContext.getCurrentInstance().getExternalContext().getRealPath("/Reportes/images/logo_uca.jpg"));
        parametros.put("logosj", FacesContext.getCurrentInstance().getExternalContext().getRealPath(logosj));
        ReportesUtil rep = new ReportesUtil();
        rep.setRuta("/Reportes/Evaluaciones/PromediosPorCriterio.jasper");
        rep.setLista(consultor.ObtenerPromediosCentroPorCriterio(consultor.getPeriodo()));
        parametros.put("periodo", consultor.getPeriodo());
        rep.setNombreReporte("Reporte de Promedios Por Criterio");
        rep.exportarPDF(true);
        }

    
    /**
     * Metodo que sirve para crear reportes sencillos con parametros similares
     * @param lista
     * @param p
     * @param ruta
     * @param nombreReporte
     * @throws JRException
     * @throws IOException 
     * @autor Carlos Delgadillo
     * @fecha 19/06/2013
     */
    public void reporteGenerico(List<T> lista, HashMap p, String ruta, String nombreReporte) throws JRException, IOException{
        p.put("logouca", ReportesUtil.CONTEXTO.getRealPath(logouca));
        p.put("logosj", ReportesUtil.CONTEXTO.getRealPath(logosj));
        if(evaluacionController.getPeriodoEvaluacion()!=null)
            p.put("periodo", evaluacionController.getPeriodoEvaluacion().getIdPeriodo());
        else
            p.put("periodo", periodoejbFacade.getPeriodoVigente().getIdPeriodo());
        getRu().setLista(lista);
        getRu().setParametros(p);
        getRu().setNombreReporte(nombreReporte);
        getRu().setRuta(ruta);
        getRu().exportarPDF(true);
    }


    /**
     * 
     * @param lista
     * @param p
     * @param ruta
     * @param nombreReporte
     * @param impresion
     * @throws JRException
     * @throws IOException
     */
    public void reporteGenerico(List<T> lista, HashMap p, String ruta, String nombreReporte, boolean impresion) throws JRException, IOException {
        p.put("logouca", ReportesUtil.CONTEXTO.getRealPath(logouca));
        p.put("logosj", ReportesUtil.CONTEXTO.getRealPath(logosj));
        getRu().setLista(lista);
        getRu().setParametros(p);
        getRu().setNombreReporte(nombreReporte);
        getRu().setRuta(ruta);
        getRu().exportarPDF(impresion);
    }
    
    public void imprimir() throws JRException, IOException{
        getRu().imprimir();
    }
    
    /**
     *
     * @throws JRException
     */
    public void impresion() throws JRException, IOException{
        getRu().imprimir();
    }
    
    /**
     * Crea un objeto ReporteEvaluacion que será mostrado en PDF <br/>
     * Autor: Carlos Delgadillo
     * @param evaluador 
     * @param evaluado
     * @param p
     * @return el objeto que se usa para la creación del PDF
     * @since 04/11/2013
     */
    public ReporteEvaluacion crearReporteEvaluacion(Empleados evaluador, Empleados evaluado, Periodo p){
        totalPuntaje=0;
        List<Criterio> listaCriterio = new ArrayList<>();
        rep.setLISTACRITERIOS(listaCriterio);
        setEVALUACION(getEvaluacionFacade().findEvaluacion(evaluador, evaluado, p));
        //Por cada respuesta a pregunta cerrada en la coleccion de EVALUACION
        Criterio C = new Criterio();
        for(RespuestaPreguntaCerrada R : getEVALUACION().getRespuestaPreguntaCerradaCollection())
        {//Por cada detalle de pregunta en R
            
            if(!C.equals(R.getPreguntaCerrada().getIdCriterio()))
                C=R.getPreguntaCerrada().getIdCriterio(); 
            List<DetallePreguntaCerrada> listDetalle = new ArrayList<>();
            List<RespuestaPreguntaCerrada> listRespuesta = new ArrayList<>();
            for(DetallePreguntaCerrada D : R.getPreguntaCerrada().getDetallePreguntaCerradaCollection())
            {//Crear una lista de preguntas segun nivel 360
                
                if(D.getNivel360() == getEVALUACION().getIdNivel360()){
                    
                    PreguntaCerrada P = D.getPreguntaCerrada();
                    PreguntaCerradaReportes PCR = new PreguntaCerradaReportes();
                    
                    P.setDetallePreguntaCerradaCollection(listDetalle);
                    P.setRespuestaPreguntaCerradaCollection(listRespuesta);
                    if(!P.getDetallePreguntaCerradaCollection().contains(D))
                    P.getDetallePreguntaCerradaCollection().add(D);
                    if(!P.getRespuestaPreguntaCerradaCollection().contains(R))
                    P.getRespuestaPreguntaCerradaCollection().add(R);
                    if(!C.getPreguntaCerradaCollection().contains(P))
                    C.getPreguntaCerradaCollection().add(P);
                    
                }
            }
            if(!rep.getListaCriterios().contains(C))
                        rep.getListaCriterios().add(C);
        }
        List<RespuestaPreguntaAbierta> lista = new ArrayList<>();
        rep.setListaPA(lista);
        for(RespuestaPreguntaAbierta R : getEVALUACION().getRespuestaPreguntaAbiertaCollection()){
            rep.getListaPA().add(R);
        }
        List<Capacitaciones> cap = new ArrayList<>();
        rep.setListaCapacitaciones(cap);
        for(Capacitaciones c : getEVALUACION().getCapacitacionesList()){
            rep.getListaCapacitaciones().add(c);
        }
        for(Criterio c :rep.getListaCriterios()){
            for(PreguntaCerrada pr :c.getPreguntaCerradaCollection()){
                for(DetallePreguntaCerrada d: pr.getDetallePreguntaCerradaCollection()){
                    totalPuntaje+=d.getValor()*d.getPreguntaCerrada().getRespuestaPreguntaCerradaCollection().get(0).getEscala().getValor();
                }
            }
        }   
        return rep;
    }
    
    /**
     * Prepara el reporte genérico para ser impreso <br/>
     * Autor: Carlos Delgadillo
     * @param evaluador 
     * @param evaluado
     * @param p
     * @param impresion 
     * @throws JRException 
     * @throws IOException 
     * @since 04/11/2013
     */
    public void prepararReporteEvaluacion(Empleados evaluador, Empleados evaluado, Periodo p, boolean impresion) throws JRException, IOException{
        nombreRep = "Evaluacion " + evaluado.toString();
        ruta = "/Reportes/Evaluaciones/ReporteEvaluacion/ReporteEvaluacion.jasper";
        map = new HashMap();
        list = new ArrayList<>();
        list.add(crearReporteEvaluacion(evaluador, evaluado, p));
        map.put("evaluador", evaluador.toString());
        map.put("evaluado", evaluado.toString());
        map.put("periodo", p.toString());
        map.put("rutaCrit", ReportesUtil.CONTEXTO.getRealPath("/Reportes/Evaluaciones/ReporteEvaluacion/"));
        map.put("total", totalPuntaje);
        map.put("resultado", evaluacionController.getPuntajeCualitativo().getDescripcion());
        map.put("fechaImpresion",
                DateFormat.getDateInstance(DateFormat.FULL)
                .format(evaluacionController.
                getSelected().getFechaEvaluacion()));
        map.put("fechaInicio",
                DateFormat.getDateInstance(DateFormat.SHORT).format(p.getFechaInicio()));
        map.put("fechaFin",
                DateFormat.getDateInstance(DateFormat.SHORT).format(p.getFechaFin()));
        reporteGenerico((List<T>) list, map, ruta, nombreRep, impresion);  
    }
    
    /**
     * Prepara el reporte de capacitaciones para ser impreso <br/>
     * Autor: Carlos Delgadillo
     * @param impresion 
     * @throws JRException 
     * @throws IOException 
     * @since 16/12/2013
     */
    public void prepararReporteCapacitaciones(boolean impresion) throws JRException, IOException{
        nombreRep = "Reporte de Capacitaciones solicitadas por área periodo "+periodoejbFacade.getPeriodoVigente().getIdPeriodo();
        ruta = "/Reportes/Capacitaciones/repCapacitacionesCDC.jasper";
        map = new HashMap();
        map.put("rutaSub",ReportesUtil.CONTEXTO.getRealPath("/Reportes/Capacitaciones"));
        reporteGenerico((List<T>) getReporteCapacitacionesPorArea(), map, ruta, nombreRep, impresion);
    }
    
    /**
     * Obtiene una lista de objetos ReporteCapacitaciones con las capacitaciones
     * solicitadas por cada centro de costo
     * @return lista
     */
    public List<ReporteCapacitaciones> getReporteCapacitacionesPorArea(){
        String centro = null;
        CentroDeCostos cc;
        Recomendacion r;
        ReporteCapacitaciones repc = new ReporteCapacitaciones();
        List<ReporteCapacitaciones> lista = new ArrayList<>();
        for(CapacitacionCentro c : consultasFacade.capacitacionesPorArea(periodoController.getPeriodoVigente())){
            if(centro == null){
                centro = c.getCentro();
                cc=centroFacade.find(centro);
                r = new Recomendacion(c.getCapacitacion(), c.getCantidad());
//                repc = new ReporteCapacitaciones();
                repc.setCentroDeCosto(cc);
                repc.setListaRecomendaciones(new ArrayList<Recomendacion>());
                repc.getListaRecomendaciones().add(r);
            }
            else if(centro.equals(c.getCentro())){
                repc.getListaRecomendaciones().add(new Recomendacion(c.getCapacitacion(), c.getCantidad()));
            }
            else
                if(centro != c.getCentro()){
                    lista.add(repc);
                    centro = c.getCentro();
                    cc=centroFacade.find(centro);
                    r = new Recomendacion(c.getCapacitacion(), c.getCantidad());
                    repc = new ReporteCapacitaciones();
                    repc.setCentroDeCosto(cc);
                    repc.setListaRecomendaciones(new ArrayList<Recomendacion>());
                    repc.getListaRecomendaciones().add(r);
                }
        }
        lista.add(repc);
        
        return lista;
    }

    /**
     * @return the consultor
     */
    public ConsultorDePromedios getConsultor() {
        return consultor;
    }

    /**
     * @param consultor the consultor to set
     */
    public void setConsultor(ConsultorDePromedios consultor) {
        this.consultor = consultor;
    }

    /**
     * @return the empController
     */
    public EmpleadosController getEmpController() {
        return empController;
    }

    /**
     * @param empController the empController to set
     */
    public void setEmpController(EmpleadosController empController) {
        this.empController = empController;
    }

    /**
     * @return the capController
     */
    public CapacitacionesController getCapController() {
        return capController;
    }

    /**
     * @param capController the capController to set
     */
    public void setCapController(CapacitacionesController capController) {
        this.capController = capController;
    }



    /**
     * @return the centroController
     */
    public CentroDeCostosController getCentroController() {
        return centroController;
    }

    /**
     * @param centroController the centroController to set
     */
    public void setCentroController(CentroDeCostosController centroController) {
        this.centroController = centroController;
    }

    /**
     * @return the evaluacionController
     */
    public EvaluacionController getEvaluacionController() {
        return evaluacionController;
    }

    /**
     * @param evaluacionController the evaluacionController to set
     */
    public void setEvaluacionController(EvaluacionController evaluacionController) {
        this.evaluacionController = evaluacionController;
    }

    /**
     * @return the ru
     */
    public ReportesUtil getRu() {
        return ru;
    }

    /**
     * @param ru the ru to set
     */
    public void setRu(ReportesUtil ru) {
        this.ru = ru;
    }

    /**
     * @return the periodoejbFacade
     */
    public uca.sed360.ejbs.PeriodoFacade getPeriodoejbFacade() {
        return periodoejbFacade;
    }

    /**
     * @param periodoejbFacade the periodoejbFacade to set
     */
    public void setPeriodoejbFacade(uca.sed360.ejbs.PeriodoFacade periodoejbFacade) {
        this.periodoejbFacade = periodoejbFacade;
    }

    /**
     * @return the planejbFacade
     */
    public uca.sed360.ejbs.PlanEstrategicoFacade getPlanejbFacade() {
        return planejbFacade;
    }

    /**
     * @param planejbFacade the planejbFacade to set
     */
    public void setPlanejbFacade(uca.sed360.ejbs.PlanEstrategicoFacade planejbFacade) {
        this.planejbFacade = planejbFacade;
    }

    /**
     * @return the ponderacionejbFacade
     */
    public uca.sed360.ejbs.PonderacionFacade getPonderacionejbFacade() {
        return ponderacionejbFacade;
    }

    /**
     * @param ponderacionejbFacade the ponderacionejbFacade to set
     */
    public void setPonderacionejbFacade(uca.sed360.ejbs.PonderacionFacade ponderacionejbFacade) {
        this.ponderacionejbFacade = ponderacionejbFacade;
    }

    /**
     * @return the evaluacionFacade
     */
    public EvaluacionFacade getEvaluacionFacade() {
        return evaluacionFacade;
    }

    /**
     * @param evaluacionFacade the evaluacionFacade to set
     */
    public void setEvaluacionFacade(EvaluacionFacade evaluacionFacade) {
        this.evaluacionFacade = evaluacionFacade;
    }

    /**
     * @return the EVALUACION
     */
    public Evaluacion getEVALUACION() {
        return EVALUACION;
    }

    /**
     * @param EVALUACION the EVALUACION to set
     */
    public void setEVALUACION(Evaluacion EVALUACION) {
        this.EVALUACION = EVALUACION;
    }

    /**
     * @return the sessionMB
     */
    public SessionMB getSessionMB() {
        return sessionMB;
    }

    /**
     * @param sessionMB the sessionMB to set
     */
    public void setSessionMB(SessionMB sessionMB) {
        this.sessionMB = sessionMB;
    }

    /**
     * @return the periodoController
     */
    public PeriodoController getPeriodoController() {
        return periodoController;
    }

    /**
     * @param periodoController the periodoController to set
     */
    public void setPeriodoController(PeriodoController periodoController) {
        this.periodoController = periodoController;
    }

    /**
     * @return the consultasFacade
     */
    public uca.sed360.ejbs.ConsultasFacade getConsultasFacade() {
        return consultasFacade;
    }

    /**
     * @param consultasFacade the consultasFacade to set
     */
    public void setConsultasFacade(uca.sed360.ejbs.ConsultasFacade consultasFacade) {
        this.consultasFacade = consultasFacade;
    }

    /**
     * @return the centroFacade
     */
    public uca.sed360.ejbs.CentroDeCostosFacade getCentroFacade() {
        return centroFacade;
    }

    /**
     * @param centroFacade the centroFacade to set
     */
    public void setCentroFacade(uca.sed360.ejbs.CentroDeCostosFacade centroFacade) {
        this.centroFacade = centroFacade;
    }
}