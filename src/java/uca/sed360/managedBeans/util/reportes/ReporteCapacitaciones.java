/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans.util.reportes;

import java.util.List;
import uca.sed360.jpaEntities.CentroDeCostos;

/**
 * Clase que se encarga de crear los reportes de las capacitaciones.
 * @author Carlos Delgadillo
 * @since 13/12/2013
 */
public class ReporteCapacitaciones {
    
    private CentroDeCostos centroDeCosto;
    private List<Recomendacion> listaRecomendaciones;
    
    /**
     * Constructor
     */
    public ReporteCapacitaciones(){
        
    }

    
    /**
     * Constructor
     * @param centroDeCosto
     * @param listaCapacitaciones
     * 
     */
    public ReporteCapacitaciones(CentroDeCostos centroDeCosto, List<Recomendacion> listaCapacitaciones){
        this.centroDeCosto = centroDeCosto;
        this.listaRecomendaciones = listaCapacitaciones;
    }
    
    /**
     * @return the centroDeCosto
     */
    public CentroDeCostos getCentroDeCosto() {
        return centroDeCosto;
    }

    /**
     * @param centroDeCosto the centroDeCosto to set
     */
    public void setCentroDeCosto(CentroDeCostos centroDeCosto) {
        this.centroDeCosto = centroDeCosto;
    }

    /**
     * @return the listaRecomendaciones
     */
    public List<Recomendacion> getListaRecomendaciones() {
        return listaRecomendaciones;
    }

    /**
     * @param listaRecomendaciones the listaRecomendaciones to set
     */
    public void setListaRecomendaciones(List<Recomendacion> listaRecomendaciones) {
        this.listaRecomendaciones = listaRecomendaciones;
    }
    
}
