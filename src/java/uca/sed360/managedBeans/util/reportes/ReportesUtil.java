/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans.util.reportes;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

/**
 *
 * @param <T>
 * @autor Carlos Delgadillo
 * @fecha 27/05/2013
 */
public class ReportesUtil<T>{
    
    private JasperPrint jasperPrint;
    private String RUTA_REPORTE;
    private String ruta;
    private List<T> lista;
    private String nombreReporte;
    private Map parametros;
    private HttpServletResponse respuesta;
    private ServletOutputStream servlet;
    private static JRPdfExporter jre;
    public static ExternalContext CONTEXTO = FacesContext.getCurrentInstance().getExternalContext();
    
    public void init(String ruta, List<T> lista) throws JRException{
        setRUTA_REPORTE(FacesContext.getCurrentInstance().getExternalContext().getRealPath(ruta));
        JRBeanCollectionDataSource coleccion = new JRBeanCollectionDataSource(lista);
        jasperPrint = JasperFillManager.fillReport(getRUTA_REPORTE(), getParametros(), coleccion);
    }
    /**
     * Método encargado de la exportación de Reportes a PDF. Recibe un parametro
     * booleano. Si es "true", el reporte aparecerá en pantalla. Si es "false",
     * el navegador descargará el archivo en la máquina cliente.
     * 
     * @autor Carlos Delgadillo
     * @fecha 15/08/2013
     * @param impresion
     * @throws JRException
     * @throws IOException 
     */
    public void exportarPDF(boolean impresion) throws JRException, IOException{
        init(getRuta(), getLista());
        respuesta = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        respuesta.setHeader( "Content-Description", "File Transfer" );
        if(!impresion)
            respuesta.addHeader("Content-disposition", "attachment; filename=Reporte "+getNombreReporte());
        else
            respuesta.addHeader("Content-disposition", "inline; filename=Reporte "+getNombreReporte());
        respuesta.setContentType("application/pdf");
        respuesta.setHeader( "Content-Transfer-Encoding", "binary" );
        servlet = respuesta.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, servlet);
        FacesContext.getCurrentInstance().responseComplete();
        FacesContext.getCurrentInstance().renderResponse();
    }
    
    public void imprimir() throws JRException, IOException{
        jre.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        jre.setParameter(JRExporterParameter.OUTPUT_STREAM, respuesta.getOutputStream());
        jre.setParameter(JRPdfExporterParameter.PDF_JAVASCRIPT, "this.print();");
        jre.exportReport();
        FacesContext.getCurrentInstance().responseComplete();
        FacesContext.getCurrentInstance().renderResponse();
        
    }
    
    /**
     * @return the RUTA_REPORTE
     */
    public String getRUTA_REPORTE() {
        return RUTA_REPORTE;
    }

    /**
     * @param RUTA_REPORTE the RUTA_REPORTE to set
     */
    public void setRUTA_REPORTE(String RUTA_REPORTE) {
        this.RUTA_REPORTE = RUTA_REPORTE;
    }

    /**
     * @return the lista
     */
    public List<T> getLista() {
        return lista;
    }

    /**
     * @param lista the lista to set
     */
    public void setLista(List<T> lista) {
        this.lista = lista;
    }

    /**
     * @return the ruta
     */
    public String getRuta() {
        return ruta;
    }

    /**
     * @param ruta the ruta to set
     */
    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    /**
     * @return the nombreReporte
     */
    public String getNombreReporte() {
        return nombreReporte;
    }

    /**
     * @param nombreReporte the nombreReporte to set
     */
    public void setNombreReporte(String nombreReporte) {
        this.nombreReporte = nombreReporte;
    }

    /**
     * @return the parametros
     */
    public Map getParametros() {
        if (parametros == null){
            parametros = new HashMap();
        }
        parametros.put("logouca", CONTEXTO.getRealPath("/Reportes/images/logouca.jpg"));
        parametros.put("logosj", CONTEXTO.getRealPath("/Reportes/images/logosj.jpg"));
        
        return parametros;
    }

    /**
     * @param parametros the parametros to set
     */
    public void setParametros(Map parametros) {
        this.parametros = parametros;
    }
    
}
