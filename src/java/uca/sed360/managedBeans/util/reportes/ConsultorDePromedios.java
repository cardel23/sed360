/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans.util.reportes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import uca.sed360.jpaEntities.Empleados;
import uca.sed360.jpaEntities.Evaluacion;
import uca.sed360.jpaEntities.Periodo;
import uca.sed360.managedBeans.EmpleadosController;
import uca.sed360.managedBeans.EvaluacionController;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.managedBeans.util.PromedioEmpleado;

/**
 * Controlador auxiliar que maneja los promedios de evaluación
 * @author Nelson Gadea
 */
@ManagedBean(name = "consultorDePromedios")
@SessionScoped
public class ConsultorDePromedios implements Serializable {

    private String centro;
    private String periodo;
    private Empleados empleado_promedios_periodos;
    private boolean primeracarga = false;
    private boolean checPromediosCriteriosPdf;
    private List<Object[]> listadePromediosPorCriterio;
    private List<Object[]> listadePromediosPorCriterioByCentro;
    private CartesianChartModel categoryModelPromediosByArea = new CartesianChartModel(), promediosTrabajadorPorPeriodoCModel;
    private ChartSeries criterios = new ChartSeries(), promediosTrabajadorPorPeriodoSerie;
    JsfUtil ayudante = new JsfUtil();
    private List<PromedioEmpleado> promediosDelempleado;

    /**
     *
     */
    public void prepararReporteEmpleadoPorPeriodo() {
        if (empleado_promedios_periodos == null) {
            empleado_promedios_periodos = getEmpleadoMB().getListaEmpleados().get(getEmpleadoMB().getListaEmpleados().size() - 1);
        }
    }
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;
    @ManagedProperty(value = "#{evaluacionController}")
    private EvaluacionController evMB;
    @ManagedProperty(value = "#{empleadosController}")
    private EmpleadosController empleadoMB;

    /**
     *
     * @param período
     * @return
     */
    public List<Object[]> ObtenerPromediosCentroPorCriterio(String período) {
        Query q = (Query) em.createNamedQuery("PromediodeCentroCostoPorCriterio");
        q.setParameter(1, periodo);
        List<Object[]> filas = (List<Object[]>) q.getResultList();
        return filas;
    }

    /**
     * Método que genera y devuelve el modelo Cartesiano para generar el gráfico
     * de línea con el hisotiral de promedios del un empleado a través de los
     * períodos
     *
     * @param periodos Lista de períodos de los que se obtendrán los promedios
     * para los empleados
     * @return CartesianChartModel con la información para generar el gráfico de
     * línea de promedios del empelado en cada criterio
     */
    public CartesianChartModel obtenerPromediosTrabajadorPorPeriodoCModel(List<Periodo> periodos) {

        promediosTrabajadorPorPeriodoCModel = new CartesianChartModel();
        promediosTrabajadorPorPeriodoSerie = new ChartSeries();

        for (Periodo p : periodos) {
            getPromediosTrabajadorPorPeriodoSerie().set(p.getIdPeriodo(), obtenerPromedioDelEmpleadoPorPeriodo(empleado_promedios_periodos, p).getValor());
        }
        getPromediosTrabajadorPorPeriodoCModel().addSeries(promediosTrabajadorPorPeriodoSerie);

        return promediosTrabajadorPorPeriodoCModel;
    }

    /**
     * Este método obtiene el promedio de un empleado en un período específico.
     *
     * @param empleado El empleado de cuyo promedio se desea obtener
     * @param periodo Período del cual se desea obtener el promedio para un
     * empleado
     * @return De tipo PromedioEmpleado, que contiene el empleado y su promedio
     */
    public PromedioEmpleado obtenerPromedioDelEmpleadoPorPeriodo(Empleados empleado, Periodo periodo) {
        if (empleado == null) 
        {
            return null;
        }

        Object o[] = (Object[]) getEvMB().obtenerLista(empleado, periodo);

        PromedioEmpleado p = new PromedioEmpleado();

        p.setEmpleado(empleado);

        int evaluaciones = 0;

        if (p.getEmpleado().getEvaluacionCollection1() != null && !p.getEmpleado().getEvaluacionCollection1().isEmpty() && o != null) 
        {
            for (Evaluacion e : p.getEmpleado().getEvaluacionCollection1()) 
            {
                if (e.getIdPeriodo().getIdPeriodo().equals(periodo.getIdPeriodo())) 
                {
                    evaluaciones++;
                }
            }
            p.setValor((Double) o[1] / evaluaciones);
        } 
        else 
        {
            p.setValor(0.00);
        }

        return p;
    }

    /**
     * Método que inicializa los elementos que son necesarios para generar el
     * gráfico de promedios de un empleado en los distintos períodos
     */
    public void iniciarListaDePromedios() {
        setPromediosDelempleado(new ArrayList<PromedioEmpleado>());
        setPromediosTrabajadorPorPeriodoCModel(new CartesianChartModel());
        setPromediosTrabajadorPorPeriodoSerie(new ChartSeries());
    }

    /**
     *
     */
    public void cargarDatosPromediosPorArea() {
        if (periodo == null || centro == null || ((periodo == null) && (centro == null))) {
        } else {
            try {
                this.listadePromediosPorCriterioByCentro = new ArrayList<Object[]>();
                this.listadePromediosPorCriterio = ObtenerPromediosCentroPorCriterio(getPeriodo());
                categoryModelPromediosByArea = new CartesianChartModel();
                criterios = new ChartSeries();

                for (Object[] c : listadePromediosPorCriterio) {
                    if ((c[0].toString().trim()).equals(centro)) {
                        listadePromediosPorCriterioByCentro.add(c);
                        criterios.set((String) c[1], (Number) c[2]);
                    }
                }


                categoryModelPromediosByArea.addSeries(criterios);
            } catch (Exception e) {
                System.out.print("--------------->" + e.toString() + "<-------------");
            }

        }
    }

    /**
     * @return the listadePromediosPorCriterio
     */
    public List<Object[]> getListadePromediosPorCriterio() {
        if (listadePromediosPorCriterio != null) {
            return listadePromediosPorCriterio;
        } else {
            return listadePromediosPorCriterio = new ArrayList<Object[]>();
        }
    }

    /**
     * @param listadePromediosPorCriterio the listadePromediosPorCriterio to set
     */
    public void setListadePromediosPorCriterio(List<Object[]> listadePromediosPorCriterio) {
        this.listadePromediosPorCriterio = listadePromediosPorCriterio;
    }

    /**
     * @return the centro
     */
    public String getCentro() {
        return centro;
    }

    /**
     * @param centro the centro to set
     */
    public void setCentro(String centro) {

        this.centro = centro;
    }

    /**
     * @return the listadePromediosPorCriterioByCentro
     */
    public List<Object[]> getListadePromediosPorCriterioByCentro() {

        if (listadePromediosPorCriterioByCentro != null) {
            return listadePromediosPorCriterioByCentro;
        } else {
            return (listadePromediosPorCriterio = new ArrayList<Object[]>());
        }
    }

    /**
     * @param listadePromediosPorCriterioByCentro the
     * listadePromediosPorCriterioByCentro to set
     */
    public void setListadePromediosPorCriterioByCentro(List<Object[]> listadePromediosPorCriterioByCentro) {
        this.listadePromediosPorCriterioByCentro = listadePromediosPorCriterioByCentro;
    }

    /**
     * @return the categoryModelPromediosByArea
     */
    public CartesianChartModel getCategoryModelPromediosByArea() {
        return categoryModelPromediosByArea;
    }

    /**
     * @param categoryModelPromediosByArea the categoryModelPromediosByArea to
     * set
     */
    public void setCategoryModelPromediosByArea(CartesianChartModel categoryModelPromediosByArea) {
        this.categoryModelPromediosByArea = categoryModelPromediosByArea;
    }

    /**
     * @return the periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the primeracarga
     */
    public boolean isPrimeracarga() {
        return primeracarga;
    }

    /**
     * @param primeracarga the primeracarga to set
     */
    public void setPrimeracarga(boolean primeracarga) {
        this.primeracarga = primeracarga;
    }

    /**
     *
     */
    public void initPrimeraCarga() {
        this.primeracarga = false;
    }

    /**
     * @return the checPromediosCriteriosPdf
     */
    public boolean isChecPromediosCriteriosPdf() {
        return checPromediosCriteriosPdf;
    }

    /**
     * @param checPromediosCriteriosPdf the checPromediosCriteriosPdf to set
     */
    public void setChecPromediosCriteriosPdf(boolean checPromediosCriteriosPdf) {
        this.checPromediosCriteriosPdf = checPromediosCriteriosPdf;
    }

    /**
     * @return the promediosTrabajadorPorPeriodoCModel
     */
    public CartesianChartModel getPromediosTrabajadorPorPeriodoCModel() {
        if (promediosTrabajadorPorPeriodoCModel == null) {
            promediosTrabajadorPorPeriodoCModel = new CartesianChartModel();
        }
        return promediosTrabajadorPorPeriodoCModel;
    }

    /**
     * @param promediosTrabajadorPorPeriodoCModel the
     * promediosTrabajadorPorPeriodoCModel to set
     */
    public void setPromediosTrabajadorPorPeriodoCModel(CartesianChartModel promediosTrabajadorPorPeriodoCModel) {
        this.promediosTrabajadorPorPeriodoCModel = promediosTrabajadorPorPeriodoCModel;
    }

    /**
     * @return the promediosTrabajadorPorPeriodoSerie
     */
    public ChartSeries getPromediosTrabajadorPorPeriodoSerie() {

        if (promediosTrabajadorPorPeriodoSerie == null) {
            promediosTrabajadorPorPeriodoSerie = new ChartSeries();
        }

        return promediosTrabajadorPorPeriodoSerie;
    }

    /**
     * @param promediosTrabajadorPorPeriodoSerie the
     * promediosTrabajadorPorPeriodoSerie to set
     */
    public void setPromediosTrabajadorPorPeriodoSerie(ChartSeries promediosTrabajadorPorPeriodoSerie) {
        this.promediosTrabajadorPorPeriodoSerie = promediosTrabajadorPorPeriodoSerie;
    }

    /**
     * @return the evMB
     */
    public EvaluacionController getEvMB() {
        return evMB;
    }

    /**
     * @param evMB the evMB to set
     */
    public void setEvMB(EvaluacionController evMB) {
        this.evMB = evMB;
    }

    /**
     * @return the empleado_promedios_periodos
     */
    public Empleados getEmpleado_promedios_periodos() {
        return empleado_promedios_periodos;
    }

    /**
     * @param empleado_promedios_periodos the empleado_promedios_periodos to set
     */
    public void setEmpleado_promedios_periodos(Empleados empleado_promedios_periodos) {
        this.empleado_promedios_periodos = empleado_promedios_periodos;
    }

    /**
     * @return the empleadoMB
     */
    public EmpleadosController getEmpleadoMB() {
        return empleadoMB;
    }

    /**
     * @param empleadoMB the empleadoMB to set
     */
    public void setEmpleadoMB(EmpleadosController empleadoMB) {
        this.empleadoMB = empleadoMB;
    }

    /**
     * @return the promediosDelempleado
     */
    public List<PromedioEmpleado> getPromediosDelempleado() {
        return promediosDelempleado;
    }

    /**
     * @param promediosDelempleado the promediosDelempleado to set
     */
    public void setPromediosDelempleado(List<PromedioEmpleado> promediosDelempleado) {
        this.promediosDelempleado = promediosDelempleado;
    }
}
