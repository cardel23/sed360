/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans.util.reportes;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import net.sf.jasperreports.engine.JRException;
import uca.sed360.managedBeans.CentroDeCostosController;
import uca.sed360.managedBeans.EmpleadosController;
import uca.sed360.managedBeans.EvaluacionController;
import uca.sed360.managedBeans.util.PromedioEmpleado;

/**
 * Controlador auxiliar que maneja la generación de reportes
 * @author Carlos Delgadillo
 */
@ManagedBean(name = "reportesMB")
@SessionScoped
public class ReportesMB {
    
    @ManagedProperty(value="#{evaluacionController}")
    private EvaluacionController evMB;
    
    @ManagedProperty(value="#{centroDeCostosController}")
    private CentroDeCostosController ccMB;
    
    @ManagedProperty(value="#{empleadosController}")
    private EmpleadosController empMB;
    
    @ManagedProperty(value = "#{repController}")
    private GeneradorDeReportesPDF pdfMB;
    
    private int opcion=0;
    private String rutaReporte="/WEB-INF/FragmentosReportes/_InicioReporte.xhtml";

    /**
     *
     * @return
     */
    public String getRutaReporte() {
        return rutaReporte;
    }

    /**
     *
     * @param rutaReporte
     */
    public void setRutaReporte(String rutaReporte) {
        this.rutaReporte = rutaReporte;
    }
 
    /**
     * Menú de selección de reportes
     * @param opcion
     * @throws JRException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public void seleccionarMetodo(int opcion) throws JRException, IOException, ClassNotFoundException, SQLException{
        switch(opcion){
            case 1:                              
                getPdfMB().generarReporteEstatusImpresion(true);
                break;
            case 2:
                getPdfMB().generarReporteEstatusImpresion(false);
                break;
            case 3:
                getPdfMB().generarReporteDeEmpleadosActivos();
                break;
            case 4:
                getPdfMB().prepararReporteCapacitaciones(true);
                break;
            case 5:
                getPdfMB().generarReporteAutoEvaluacionesPendientes();
                break;
            case 6:
                getPdfMB().generarReporteEvaluacionesASubordinadosPendientes();
                break;
            case 7:
                List<PromedioEmpleado> lista = new ArrayList<>();
                if(evMB.getPromediosGlobalesFiltrados()!=null)
                     lista = evMB.getPromediosGlobalesFiltrados();
                else
                    lista = evMB.obtenerPromedios();
                getPdfMB().reporteGenerico(lista,
                        new HashMap(),
                        "/Reportes/Evaluaciones/Promedios/ReporteDePromediosGlobales.jasper",
                        "Reporte de los mejores trabajadores periodo "+evMB.getPeriodoEvaluacion().getIdPeriodo());
                break;
            case 9:
                getPdfMB().generarReporteDePromediosPorCriterios();
                break;
            case 0:
                break;
            default:
                break;
        }
        
    }
    
    

    
    
    

    /**
     * @return the evMB
     */
    public EvaluacionController getEvMB() {
        return evMB;
    }

    /**
     * @param evMB the evMB to set
     */
    public void setEvMB(EvaluacionController evMB) {
        this.evMB = evMB;
    }

    /**
     * @return the ccMB
     */
    public CentroDeCostosController getCcMB() {
        return ccMB;
    }

    /**
     * @param ccMB the ccMB to set
     */
    public void setCcMB(CentroDeCostosController ccMB) {
        this.ccMB = ccMB;
    }

    /**
     * @return the empMB
     */
    public EmpleadosController getEmpMB() {
        return empMB;
    }

    /**
     * @param empMB the empMB to set
     */
    public void setEmpMB(EmpleadosController empMB) {
        this.empMB = empMB;
    }

    /**
     * @return the opcion
     */
    public int getOpcion() {
        return opcion;
    }

    /**
     * @param numero 
     */
    public void setOpcion(int numero) {
        this.opcion = numero;
    }

    /**
     * @return the pdfMB
     */
    public GeneradorDeReportesPDF getPdfMB() {
        return pdfMB;
    }

    /**
     * @param pdfMB the pdfMB to set
     */
    public void setPdfMB(GeneradorDeReportesPDF pdfMB) {
        this.pdfMB = pdfMB;
    }
}


