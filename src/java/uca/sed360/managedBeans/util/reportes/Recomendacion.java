/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans.util.reportes;




/**
 * Clase que crea un objeto que contiene las capacitaciones que han sido solicitadas
 * y el número de solicitudes de cada capacitación
 * @author Estacion 2
 */
public class Recomendacion{
    
    private String capacitacion;
    private Long solicitudes;

    /**
     * Constructor
     */
    public Recomendacion(){
        
    }
    
    /**
     * Constructor
     * @param capacitacion
     * @param solicitudes
     */
    public Recomendacion(String capacitacion, Long solicitudes){
        this.capacitacion = capacitacion;
        this.solicitudes = solicitudes;
    }
    /**
     * @return the capacitacion
     */
    public String getCapacitacion() {
        return capacitacion;
    }

    /**
     * @param capacitacion the capacitacion to set
     */
    public void setCapacitacion(String capacitacion) {
        this.capacitacion = capacitacion;
    }

    /**
     * @return the solicitudes
     */
    public Long getSolicitudes() {
        return solicitudes;
    }

    /**
     * @param solicitudes the solicitudes to set
     */
    public void setSolicitudes(Long solicitudes) {
        this.solicitudes = solicitudes;
    }
}
