/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans.util.reportes;

import java.util.List;
import uca.sed360.jpaEntities.Capacitaciones;
import uca.sed360.jpaEntities.Criterio;
import uca.sed360.jpaEntities.RespuestaPreguntaAbierta;
import uca.sed360.managedBeans.util.CriterioReportes;

/**
 * Clase auxiliar para la generación de reportes de evaluación
 * @author Carlos Delgadillo
 * @since 04/11/2013
 */
public class ReporteEvaluacion{
    
    private List<CriterioReportes> criterios;
    private List<Criterio> listaCriterios;
    private List<RespuestaPreguntaAbierta> listaPA;
    private List<Capacitaciones> listaCapacitaciones;
    
    private ReporteEvaluacion reporteEvaluacion;
    /**
     *
     */
    public ReporteEvaluacion(){
        
    }
    
    /**
     *
     * @return
     */
    public ReporteEvaluacion reporteEvaluacion(){
        if(reporteEvaluacion == null)
            reporteEvaluacion = new ReporteEvaluacion();
        return reporteEvaluacion;
    }

    /**
     * @return the LISTACRITERIOS
     */
    public List<Criterio> getListaCriterios() {
        return listaCriterios;
    }

    /**
     * @param listaCriterios the LISTACRITERIOS to set
     */
    public void setLISTACRITERIOS(List<Criterio> listaCriterios) {
        this.listaCriterios = listaCriterios;
    }

    /**
     * @return the listaPA
     */
    public List<RespuestaPreguntaAbierta> getListaPA() {
        return listaPA;
    }

    /**
     * @param listaPA the listaPA to set
     */
    public void setListaPA(List<RespuestaPreguntaAbierta> listaPA) {
        this.listaPA = listaPA;
    }

    /**
     * @return the listaCapacitaciones
     */
    public List<Capacitaciones> getListaCapacitaciones() {
        return listaCapacitaciones;
    }

    /**
     * @param listaCapacitaciones the listaCapacitaciones to set
     */
    public void setListaCapacitaciones(List<Capacitaciones> listaCapacitaciones) {
        this.listaCapacitaciones = listaCapacitaciones;
    }

    /**
     * @return the criterios
     */
    public List<CriterioReportes> getCriterios() {
        return criterios;
    }

    /**
     * @param criterios the criterios to set
     */
    public void setCriterios(List<CriterioReportes> criterios) {
        this.criterios = criterios;
    }
}
