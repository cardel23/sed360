/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans.util.reportes;


/**
 * Objeto utilizado en la generación de reportes de capacitaciónes por
 * centros de costo
 * @author Carlos Delgadillo
 * @since 17/12/2013
 */
public class CapacitacionCentro {
    
    private String centro;
    private String capacitacion;
    private Long cantidad;
    
    /**
     * Constructor
     */
    public CapacitacionCentro(){
        
    }
    
    /**
     * Constructor
     * @param centro
     * @param capacitacion
     * @param cantidad
     */
    public CapacitacionCentro(String centro, String capacitacion, Long cantidad){
        this.cantidad = cantidad;
        this.capacitacion = capacitacion;
        this.centro = centro;
    }

    /**
     * @return the centro
     */
    public String getCentro() {
        return centro;
    }

    /**
     * @param centro the centro to set
     */
    public void setCentro(String centro) {
        this.centro = centro;
    }

    /**
     * @return the capacitacion
     */
    public String getCapacitacion() {
        return capacitacion;
    }

    /**
     * @param capacitacion the capacitacion to set
     */
    public void setCapacitacion(String capacitacion) {
        this.capacitacion = capacitacion;
    }

    /**
     * @return the cantidad
     */
    public Long getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }
    
    
}
