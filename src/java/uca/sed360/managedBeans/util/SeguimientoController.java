/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans.util;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.HashMap;
import javax.ejb.EJB;
import org.primefaces.model.chart.PieChartModel;  
import uca.sed360.jpaEntities.CentroDeCostos;
import uca.sed360.jpaEntities.Periodo;

/**
 * Controlador auxiliar que maneja los gráficos de progreso de las evaluaciones
 * @author Gerardo Ortega
 */
@ManagedBean(name = "seguimientoController")
@SessionScoped
public class SeguimientoController implements Serializable {
    private Integer numEvARealizar,numEvRealizadas,numEvPendiente,numEvImpresas,numEvSinImprimir;
    private PieChartModel graficoEvRealizadas,graficoEvImpresas; 
    private Periodo periodoVigente;
    private CentroDeCostos filtroCentroCosto=null;
    private HashMap<CentroDeCostos,Integer> evARealizarPorCentroCosto=new HashMap<CentroDeCostos,Integer>();
    
    @EJB   
    private uca.sed360.ejbs.EvaluacionFacade ejbEvaluaciones;
    @EJB
    private uca.sed360.ejbs.PeriodoFacade ejbPeriodos;
    
    /**
     * Creates a new instance of SeguimientoController
     */
    public SeguimientoController() { 
    }
    
    
    /**
     * Renderiza por primera vez los gráficos
     */
    public void preRenderSeguimiento(){ 
        periodoVigente=ejbPeriodos.getPeriodoVigente();
        if(periodoVigente!=null)
          actualizarDatos();
    }

    /**
     * 
     * @return
     */
    public Integer getNumEvARealizar() {
        return numEvARealizar;
    }

    /**
     *
     * @param numEvARealizar
     */
    public void setNumEvARealizar(Integer numEvARealizar) {
        this.numEvARealizar = numEvARealizar;
    }
    
    /**
     *
     * @return
     */
    public Integer getNumEvRealizadas() {
        return numEvRealizadas;
    }

    /**
     *
     * @param numEvRealizadas
     */
    public void setNumEvRealizadas(Integer numEvRealizadas) {
        this.numEvRealizadas = numEvRealizadas;
    }

    /**
     *
     * @return
     */
    public Integer getNumEvPendiente() {
        return numEvPendiente;
    }

    /**
     *
     * @param numEvPendiente
     */
    public void setNumEvPendiente(Integer numEvPendiente) {
        this.numEvPendiente = numEvPendiente;
    }

    /**
     *
     * @return
     */
    public Integer getNumEvImpresas() {
        return numEvImpresas;
    }

    /**
     *
     * @param numEvImpresas
     */
    public void setNumEvImpresas(Integer numEvImpresas) {
        this.numEvImpresas = numEvImpresas;
    }

    /**
     *
     * @return
     */
    public Integer getNumEvSinImprimir() {
        return numEvSinImprimir;
    }

    /**
     *
     * @param numEvSinImprimir
     */
    public void setNumEvSinImprimir(Integer numEvSinImprimir) {
        this.numEvSinImprimir = numEvSinImprimir;
    }
    
   
    /**
     * Actualiza los datos que se muestran en los gráficos
     */
    public void actualizarDatos(){       
        if(getEvARealizarPorCentroCosto().containsKey(filtroCentroCosto))
            numEvARealizar=evARealizarPorCentroCosto.get(filtroCentroCosto);
        else{  
            numEvARealizar=ejbEvaluaciones.obtenerNumEvaluacioneARealizar(periodoVigente,filtroCentroCosto);
            evARealizarPorCentroCosto.put(filtroCentroCosto, numEvARealizar);
        }
        numEvRealizadas=ejbEvaluaciones.obtenerNumEvaluacionesPorPeriodoCentroCosto(periodoVigente, filtroCentroCosto); 
        numEvPendiente= numEvARealizar-numEvRealizadas;
        numEvImpresas=ejbEvaluaciones.obtenerNumEvaluacionesImpresasPeriodoCentroCosto(periodoVigente, filtroCentroCosto);
        numEvSinImprimir=numEvRealizadas-numEvImpresas;
    }
    
   
    /**
     * Genera un gráfico de pastel para mostra el progreso de las evaluaciones
     * @return gráfico
     */
    public PieChartModel obtenerGraficaGeneral() {  

        try { 
           if(graficoEvRealizadas==null){
             graficoEvRealizadas=new PieChartModel();
             graficoEvRealizadas.set("Realizadas", 0);
             graficoEvRealizadas.set("Pendientes", 0);
           }
           graficoEvRealizadas.getData().put("Realizadas", numEvRealizadas);  
           graficoEvRealizadas.getData().put("Pendientes", numEvPendiente);  
           return graficoEvRealizadas;  
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ",Ha habido un error en la generación del gráfico de evaluaciones realizadas");
            return null;
        }
       
        
    } 
     
    /**
     * Obtiene el gráfico de las evaluaciones impresas
     * @return
     */
    public PieChartModel obtenerGraficaEvaluacionesImpresas() {  
       try { 
           if(graficoEvImpresas==null){
             graficoEvImpresas=new PieChartModel();
             graficoEvImpresas.set("Impresas", 0);
             graficoEvImpresas.set("Sin imprimir", 0);
           }
           graficoEvImpresas.getData().put("Impresas", numEvImpresas);  
           graficoEvImpresas.getData().put("Sin imprimir", numEvSinImprimir);  
           return graficoEvImpresas;  
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ",Ha habido un error en la generación del gráfico de evaluacioens impresas");
            return null;
        }    
    } 

    /**
     * 
     * @return
     */
    public CentroDeCostos getFiltroCentroCosto() {
        return filtroCentroCosto;
    }

    /**
     *
     * @param filtroCentroCosto
     */
    public void setFiltroCentroCosto(CentroDeCostos filtroCentroCosto) {
        this.filtroCentroCosto = filtroCentroCosto;
    }

    /**
     *
     * @return
     */
    public HashMap<CentroDeCostos, Integer> getEvARealizarPorCentroCosto() {
        if(evARealizarPorCentroCosto==null)
            evARealizarPorCentroCosto=new HashMap<CentroDeCostos,Integer>();
        return evARealizarPorCentroCosto;
    }

    /**
     *
     * @param evARealizarPorCentroCosto
     */
    public void setEvARealizarPorCentroCosto(HashMap<CentroDeCostos, Integer> evARealizarPorCentroCosto) {
        this.evARealizarPorCentroCosto = evARealizarPorCentroCosto;
    }
    
}