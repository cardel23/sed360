package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.Ponderacion;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.PonderacionFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import uca.sed360.jpaEntities.Criterio;
import uca.sed360.jpaEntities.Nivel360;
import uca.sed360.jpaEntities.PlanEstrategico;
import uca.sed360.jpaEntities.PonderacionPK;
import uca.sed360.jpaEntities.TipoEmpleado;

/**
 * Controlador que maneja la ponderación de los criterios de evaluación
 * @author Estacion 2
 */
@ManagedBean(name = "ponderacionController")
@SessionScoped
public class PonderacionController implements Serializable {
    

    
    @ManagedProperty(value = "#{auditoriaController}")
    private AuditoriaController audiController;
    
    @ManagedProperty(value = "#{planEstrategicoController}")
    PlanEstrategicoController planMB;
    
    private Ponderacion current;
    @EJB
    private uca.sed360.ejbs.PonderacionFacade ejbFacade;
    @EJB
    private uca.sed360.ejbs.CriterioFacade ejbCriterio;
    @EJB
    private uca.sed360.ejbs.Nivel360Facade ejbNivel360;
        
    private TipoEmpleado tipoEmpleado;
    private HashMap<String,Integer> indicesDeLasPonderaciones;
    private List<Nivel360> listaNiveles360;
    private List<Ponderacion> listaPonderacion;
    private PlanEstrategico planAPonderar;
    boolean existenPonderaciones = false;    
    private boolean ponderacionesComprobadas;
    

    /**
     *
     */
    public PonderacionController() {
    }

    /**
     *
     * @return
     */
    public Ponderacion getCurrent() {
        if (current == null) {
            current = new Ponderacion();
        }
        return current;
    }

    /**
     *
     * @return
     */
    public PlanEstrategicoController getPlanMB() {
        return planMB;
    }

    /**
     *
     * @param planMB
     */
    public void setPlanMB(PlanEstrategicoController planMB) {
        this.planMB = planMB;
    }
    
    


    

    /**
     * 
     * @return el plan que será ponderado
     */
    public PlanEstrategico getPlanAPonderar() {
      if(planAPonderar == null){ 
          planAPonderar = new PlanEstrategico();
        }
        return planAPonderar;
    }

    /**
     *
     * @param planAPonderar
     */
    public void setPlanAPonderar(PlanEstrategico planAPonderar) {
        if(planAPonderar != null)            
        this.planAPonderar = planAPonderar;
        else
            planAPonderar = new PlanEstrategico();
    }
    
   
    //Getter and Setter Lista Nivel360
    /**
     *
     * @return
     */
    public List<Nivel360> getListaNiveles360() {
        listaNiveles360 = ejbNivel360.findAll();
        return listaNiveles360;
    }

    /**
     *
     * @param listaInstrumentos
     */
    public void setListaNiveles360(List<Nivel360> listaInstrumentos) {
        this.listaNiveles360 = listaInstrumentos;
    }
   
    /**
     * Comprueba si se han ponderado los criterios de un plan estratégico para
     * un tipo de empleado específico
     * @param plan
     * @param tipoEmp
     * @return verdadero o falso
     */
    public boolean isPonderadoCriteriosTipoEmpEnPlan(PlanEstrategico plan, TipoEmpleado tipoEmp){
        return ejbFacade.isPonderadoCriteriosTipoEmpEnPlan(plan, tipoEmp);
    }
    
    //Agregar ponderación
    
    /**
     * Obtiene la ponderación de un criterio según el nivel 360
     * @param i
     * @param c
     * @return ponderación
     */
    public Ponderacion obtenerPonderacion(Nivel360 i, Criterio c) {
    if (current == null) {
        current = new Ponderacion();
    }
    current = ejbFacade.findByNivel360Criterio(i, c);

    return current;
    }

    /**
     *
     * @param current
     */
    public void setCurrent(Ponderacion current) {
        this.current = current;
    }


    
    /**
     * Se encarga de agregar la ponderación establecida a un criterio
     */
    public void ponderarCriterios(){    
    
    try{
        comprobarPonderacion();
        if(ponderacionesComprobadas == true){
            for(Criterio c:planAPonderar.getCriteriosCollection())
                {
                    ejbCriterio.edit(c);
                }
                JsfUtil.addSuccessMessage("Los criterios del plan han sido ponderados exitosamente");
                audiController.guardarRegistroActividad(" ponderó los criterios del plan "+planAPonderar.getIdentificador()+" para el tipo de empleado "+tipoEmpleado.getDescripcion());
        }
    }catch(Exception e){
            JsfUtil.addErrorMessage("Error al ponderar los criterios");
        }
    }
    
    /**
     * Cancela la ponderación de los criterios de evaluación
     */
    public void cancelarPonderacion(){
        listaPonderacion = null;
        current = null;
        indicesDeLasPonderaciones = null;
        planAPonderar = null;
        tipoEmpleado = null;
    }
    
    
    

    private PonderacionFacade getFacade() {
        return ejbFacade;
    }
    


    /**
     *
     * @return
     */
    public String prepareList() {
        return "ListaPonderaciones?faces-redirect=true";
    }


    /**
     *
     * @return
     */
    public String prepareCreate() {
        current = new Ponderacion();
        current.setPonderacionPK(new uca.sed360.jpaEntities.PonderacionPK());
        return "Create";
    }
    
    /**
     *
     */
    public void preparar() {
        //getcurrent.setPonderacionPK(new uca.sed360.jpaEntities.PonderacionPK());
        
        this.listaPonderacion = new ArrayList<>();
        if(planAPonderar == null)
            setPlanAPonderar(planMB.getPrimerPlan());
        this.agregarListaPonderaciones();
        
    }

    
    /**
     * Este método inicializa la lista axuliar de las ponderacines (la cual es un arreglo de referencias a las ponderacioens
     * de los criterios. Además se llena el HashMap "IndiceDeLasPonderaciones" mapa que facilita la búsqueda de la ponderación
     * en el arreglo de ponderaciones de un criterio.
     */

    public void agregarListaPonderaciones(){       
       setIndicesDeLasPonderaciones(new HashMap<String, Integer>());           
       setListaPonderacion(new ArrayList<Ponderacion>());
       for(Criterio c: getPlanAPonderar().getCriteriosCollection()){
           if(c.getPonderacionCollection().isEmpty())
               for(Nivel360 n:getListaNiveles360()){
                   Ponderacion p = new Ponderacion(new PonderacionPK(c.getIdCriterio(), n.getIdNivel360()));
                   p.setCriterio(c);
                   p.setNivel360(n);
                   p.setPuntaje(0);
                   c.getPonderacionCollection().add(p);
                   getListaPonderacion().add(p);
                  // ejbCriterio.edit(c);                   
               }else{
               for(Ponderacion p:c.getPonderacionCollection())
                   getListaPonderacion().add(p);
           }
                      
           for(int i=0; i<c.getPonderacionCollection().size();i++){
               getIndicesDeLasPonderaciones().put(c.getIdCriterio().toString().
                       concat(c.getPonderacionCollection().get(i).getNivel360().getIdNivel360().toString()), i);
           }                  
      }             
    }
 
    
    /**
     * Comprueba las ponderaciones para que el puntaje total de las mismas
     * no exceda 100 puntos
     */
    public void comprobarPonderacion() {
        ponderacionesComprobadas = true;
            for (Nivel360 nivel : ejbNivel360.findAll()) {
                int puntaje = 0;
                for (Ponderacion p : listaPonderacion) {
                    if (p.getCriterio().getTipoEmpleado().getIdTipoempleado() == tipoEmpleado.getIdTipoempleado()
                            && p.getNivel360().getIdNivel360() == nivel.getIdNivel360()) {
                        puntaje += p.getPuntaje();
                    }
                }
                if (puntaje == 0) {
                    continue;
                }
                if (puntaje != 100) {
                    JsfUtil.addErrorMessage("La suma de del puntaje de los criterios del tipo de "
                            + "Empleado: " + tipoEmpleado.getDescripcion() + " para el nivel " + nivel.getDescripcion()
                            + "deben sumar 100");
                    ponderacionesComprobadas = false;
                }
            }
        
        
    } 
   
    /**
     *
     * @return
     */
    public String create() {
       
        if(current == null){
            JsfUtil.addErrorMessage("La ponderación debe ser distinta de null");
            return null;
        }
       
        getFacade().create(current);
        JsfUtil.addSuccessMessage("ponderación guardada con éxito");
        return null;
     
    }
    
    /**
     *
     * @return
     */
    public String prepareEdit() {
        //current = (Ponderacion) getItems().getRowData();
        return "Edit";
    }

    /**
     *
     * @return
     */
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PonderacionUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String destroy() {
        performDestroy();
        return "List";
    }


    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PonderacionDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }


   
    /**
     * Obtiene todas las ponderaciones de la BD
     * @return Lista de ponderaciones
     * @autor Carlos Delgadillo
     * @fecha 23/05/2013
     */
    public List<Ponderacion> getPonderaciones(){
        if(getFacade().findAll() == null)
            return new ArrayList<Ponderacion>();
        return getFacade().findAll();
    }



 
    /**
     *
     * @return
     */
    public List<Ponderacion> getListaPonderacion() {
        if(listaPonderacion == null)
        { listaPonderacion = new ArrayList<Ponderacion>();   }
        return listaPonderacion;
    }


    /**
     *
     * @param listaPonderacion
     */
    public void setListaPonderacion(List<Ponderacion> listaPonderacion) {
        this.listaPonderacion = listaPonderacion;
    }



    /**
     * @return the tipoEmpleado
     */
    public TipoEmpleado getTipoEmpleado() {
        return tipoEmpleado;
    }

    /**
     * @param tipoEmpleado the tipoEmpleado to set
     */
    public void setTipoEmpleado(TipoEmpleado tipoEmpleado) {
        this.tipoEmpleado = tipoEmpleado;
    }

    /**
     * @return the indicesDeLasPonderaciones
     */
    public HashMap<String,Integer> getIndicesDeLasPonderaciones() {
        if(indicesDeLasPonderaciones == null)
            indicesDeLasPonderaciones = new HashMap<>();
        return indicesDeLasPonderaciones;
    }

    /**
     * @param indicesDeLasPonderaciones the indicesDeLasPonderaciones to set
     */
    public void setIndicesDeLasPonderaciones(HashMap<String,Integer> indicesDeLasPonderaciones) {
        this.indicesDeLasPonderaciones = indicesDeLasPonderaciones;
    }

    /**
     * @return the ponderacionesComprobadas
     */
    public boolean getPonderacionesComprobadas() {
        return ponderacionesComprobadas;
    }

    /**
     * @param ponderacionesComprobadas the ponderacionesComprobadas to set
     */
    public void setPonderacionesComprobadas(boolean ponderacionesComprobadas) {
        this.ponderacionesComprobadas = ponderacionesComprobadas;
    }

    /**
     * @return the audiController
     */
    public AuditoriaController getAudiController() {
        return audiController;
    }

    /**
     * @param audiController the audiController to set
     */
    public void setAudiController(AuditoriaController audiController) {
        this.audiController = audiController;
    }

       
    
}
