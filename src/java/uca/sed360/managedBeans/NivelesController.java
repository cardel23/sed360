package uca.sed360.managedBeans;

import uca.sed360.managedBeans.util.JsfUtil;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import uca.sed360.jpaEntities.Niveles;

/**
 * Controlador que maneja los niveles de los empleados en el organigrama
 * administrativo de la UCA
 * @author Estacion 2
 */
@ManagedBean(name = "nivelesController")
@SessionScoped
public class NivelesController implements Serializable {

    private Niveles current;
    @EJB
    private uca.sed360.ejbs.NivelesFacade ejbFacade;
    private List<Niveles> listaNiveles;   
    private String oldDesc;
      
    /**
     * Constructor
     */
    public NivelesController() {
        
    }

    /**
     *
     * @return
     */
    public Niveles getCurrent() {
        if(current==null)
            current=new Niveles();
        return current;
    }

    /**
     *
     * @param current
     */
    public void setCurrent(Niveles current) {
        oldDesc=current.getDescripcion();
        this.current = current;
    }

    /**
     *
     * @return
     */
    public List<Niveles> getListaNiveles() {
        if(listaNiveles==null)
            listaNiveles=ejbFacade.findAll();
        return listaNiveles;
    }

    /**
     *
     * @param listaNiveles
     */
    public void setListaNiveles(List<Niveles> listaNiveles) {
        this.listaNiveles = listaNiveles;
    }
  
    /**
     *
     */
    public void createNivel(){
        try {
            if(ejbFacade.findByDesc(current.getDescripcion())==null){
                ejbFacade.create(current);
                JsfUtil.addSuccessMessage("El nuevo nivel de empleado '".concat(current.getDescripcion()).concat("' ha sido registrado"));
                current=null;
                listaNiveles=ejbFacade.findAll();
            }
            else{
               JsfUtil.addErrorMessage("El nivel de empleado '".concat(current.getDescripcion()).concat("' ya ha sido registrado")); 
            }            
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Lo sentimos, no pudo registrarse el nuevo nivel de empleado");
        }
        
    }
    
    /**
     *
     */
    public void update(){
        try {
            if(ejbFacade.findByDesc(current.getDescripcion())==null){
                ejbFacade.edit(current);
                JsfUtil.addSuccessMessage("El nivel de empleado '".concat(oldDesc).concat("' ha sido actualizado a '").concat(current.getDescripcion()).concat("'"));
                current=null;                
             }
            else{
               JsfUtil.addErrorMessage("El nivel de empleado '".concat(current.getDescripcion()).concat("' ya ha sido registrado")); 
               current=null;
            }
            listaNiveles=ejbFacade.findAll();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Lo sentimos, no pudo actualizarse el  nivel de empleado ".concat(current.getDescripcion()));
        }
        
    }
    
    /**
     *
     */
    public void destroy(){
        try {
             ejbFacade.remove(current);
             JsfUtil.addSuccessMessage("El  nivel de empleado: '".concat(current.getDescripcion()).concat("' ha sido eliminado"));
             current=null;
             listaNiveles=ejbFacade.findAll();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Lo sentimos, no es posible eliminar el  nivel ".concat(current.getDescripcion()));
        }
    }
   
    /**
     *
     */
    @FacesConverter(value = "nivelesControllerConverter",forClass = Niveles.class)
    public static class InstrumentosControllerConverter implements Converter {

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            NivelesController controller = (NivelesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "nivelesController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Niveles) {
                Niveles o = (Niveles) object;
                return getStringKey(o.getIdNivel());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Niveles.class.getName());
            }
        }
    }
}
