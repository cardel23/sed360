/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uca.sed360.managedBeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import org.primefaces.context.RequestContext;
import uca.sed360.jpaEntities.CatalogoMetodologias;
import uca.sed360.jpaEntities.Criterio;
import uca.sed360.jpaEntities.DetallePreguntaCerrada;
import uca.sed360.jpaEntities.DetallePreguntaCerradaPK;
import uca.sed360.jpaEntities.PlanEstrategico;
import uca.sed360.jpaEntities.Nivel360;
import uca.sed360.jpaEntities.Ponderacion;
import uca.sed360.jpaEntities.PreguntaAbierta;
import uca.sed360.jpaEntities.PreguntaCerrada;
import uca.sed360.jpaEntities.TipoEmpleado;
import uca.sed360.managedBeans.util.JsfUtil;

/**
 *
 * @author rrhh
 */
@ManagedBean(name = "definicionInstrumentosMB")
@SessionScoped
public class DefinicionInstrumentosMB {
    
    private Nivel360 nivel360;
    private TipoEmpleado instrumentoTipoEmpADefinir;
    private List<Criterio> criteriosTipoEmpleado;
    private Criterio criterio; //se utiliza para saber cual es el indice del criterio cuando a este se le modifica alguna pregunta
    private boolean  inicializadoDefinicionInstrumento;
    private PlanEstrategico planADefinir;
    private PreguntaCerrada preguntaCerrada;//se utiliza cuando se modifica/elimina una pregunta para saber su indice dentro de la lista de preguntas en el criterio
    private PreguntaAbierta preguntaAbierta;
    private int indicePreguntaAbierta;
    private List<PreguntaAbierta> listaPreguntasAbiertas,listaPATemporal,listaPAParaEliminar;
    private List<Nivel360> niveles360ADefinir;
    private boolean distribuirPuntajePC,PregAbiertaParaTodosNiveles,metodologia180;    
    private HashMap<String,Integer> hasMapIndicesDetallesPC;
    private HashMap<Criterio,HashMap<Nivel360,Double>> hashMapPuntajeAcumuladoPorCriterio;
    private HashMap<Criterio,HashMap<Nivel360,Ponderacion>> hashMapPonderacionPorCriterio;
    private List<ColumnModel> columnasNivel360;
    private HashMap<Nivel360,Boolean> existEvaluacionNivel;
    private int activeTabIndexNivel360=0;
    @EJB
    private uca.sed360.ejbs.PlanEstrategicoFacade ejbPlan;
    @EJB
    private uca.sed360.ejbs.CriterioFacade ejbCriterio;
    @EJB
    private uca.sed360.ejbs.EvaluacionFacade ejbEvaluacion;
    @EJB
    private uca.sed360.ejbs.TiposEmpleadoFacade ejbTipoEmp;
    @EJB
    private uca.sed360.ejbs.PonderacionFacade ejbPonderacionCriterio;
    @EJB
    private uca.sed360.ejbs.PreguntaAbiertaFacade ejbPreguntaAbierta;

    /**
     * Creates a new instance of DefinicionInstrumentosMB
     */
    public DefinicionInstrumentosMB() {
    }
    
    @PostConstruct
    private void init(){
        criteriosTipoEmpleado=new ArrayList<Criterio>();
        inicializadoDefinicionInstrumento=false;
        niveles360ADefinir=new ArrayList<Nivel360>();
        PregAbiertaParaTodosNiveles=true;
        metodologia180=true;
        hashMapPuntajeAcumuladoPorCriterio=new HashMap<Criterio,HashMap<Nivel360,Double>>();
        distribuirPuntajePC=true;
        existEvaluacionNivel=new HashMap<Nivel360,Boolean>();
        columnasNivel360 = new ArrayList<ColumnModel>();
        hashMapPonderacionPorCriterio =new HashMap<Criterio,HashMap<Nivel360,Ponderacion>>();
    }

    /**
     *
     * @return
     */
    public int getActiveTabIndexNivel360() {
        return activeTabIndexNivel360;
    }

    /**
     *
     * @param activeTabIndexNivel360
     */
    public void setActiveTabIndexNivel360(int activeTabIndexNivel360) {
        this.activeTabIndexNivel360 = activeTabIndexNivel360;
    }
    
    /**
     * Metodo que devuelve el plan estrategico para el cual el usuario definirá los instrumentos
     * @return 
     */
    public PlanEstrategico getPlanADefinir() {
        if(planADefinir==null){
            planADefinir=ejbPlan.getPlanVigente();
        }
        return planADefinir;
    }

    /**
     *
     * @param planADefinir
     */
    public void setPlanADefinir(PlanEstrategico planADefinir) {
        this.planADefinir = planADefinir;     
    }

    /**
     * Metodo que valida si el plan a definir ya tiene evaluaciones realizadas.
     * @return veradero o falso
     */
    
     public boolean existEvaluacionesTipoEmpPlan(){        
        return ejbEvaluacion.existEvaluacionesTipoEmpPlan(instrumentoTipoEmpADefinir,planADefinir); 

     }

    /**
     *
     * @return
     */
    public HashMap<Nivel360, Boolean> getExistEvaluacionNivel() {
        return existEvaluacionNivel;
    }
    /**
     *
     * @param n
     * @return
     */
    public boolean existeEvaluacionNivel(Nivel360 n){
        boolean existe=existEvaluacionNivel.get(n);
        return existe;
    }
    /**
     *
     * @return
     */
    public boolean isMetodologia180() {
        return metodologia180;
    }

    /**
     *
     * @param metodologia180
     */
    public void setMetodologia180(boolean metodologia180) {
        this.metodologia180 = metodologia180;
    }
    
    

    /**
     *
     * @param existEvaluacionNivel
     */
    public void setExistEvaluacionNivel(HashMap<Nivel360, Boolean> existEvaluacionNivel) {
        this.existEvaluacionNivel = existEvaluacionNivel;
    }
     
              
     /**
      * Metodo que verifica si un nivel360 de tipo de empleado ya fue definido en un plan  y nivel360 especifico
      * @param tipoEmp 
      * @param nivel
      * @return verdadero o falso
      */
    public boolean esInstrumentoTipoEmpNivelDefinidoEnPlan(TipoEmpleado tipoEmp,Nivel360 nivel){
          return ejbTipoEmp.instrumentoTipoEmpNivelDefinidoEnPlan(planADefinir,tipoEmp,nivel);        
    }
    
    /**
     * Metodo que devuelve las columnas dinamicas de niveles360
     * @return lista
     */
    public List<ColumnModel> getColumnsNivel360() {
        columnasNivel360.clear();
        for(Nivel360 nivel:Nivel360.getConstantNiveles360())
            columnasNivel360.add(new ColumnModel(nivel));  
        return columnasNivel360;
    }

    /**
     *
     * @param columnasNivel360
     */
    public void setColumnasNivel360(List<ColumnModel> columnasNivel360) {
        this.columnasNivel360 = columnasNivel360;
    }
    
    
    
    //////////////////////////////////////////////////////////////
    //* MANEJO DE CRITERIOS*//
    
    
    /**
     *
     * @return
     */
    public List<Criterio> getCriteriosTipoEmpleado() {  
        if(criteriosTipoEmpleado==null)
            criteriosTipoEmpleado=new ArrayList<Criterio>();
        return criteriosTipoEmpleado;
    }

    /**
     *
     * @param criteriosTipoEmpleado
     */
    public void setCriteriosTipoEmpleado(List<Criterio> criteriosTipoEmpleado) {
        this.criteriosTipoEmpleado = criteriosTipoEmpleado;
    }

    /**
     *
     * @return
     */
    public Criterio getCriterio() {
        return criterio;
    }

    /**
     *
     * @param criterio
     */
    public void setCriterio(Criterio criterio) {
        this.criterio = criterio;
    }
    
    /////////////////////////////////////////////////////////////
    //*MANEJO DE INSTRUMENTO TIPO DE EMPLEADO Y NIVELES A DEFINIR PARA ESE INSTRUMENTO*//
   
    /**
     *
     * @return
     */
    public TipoEmpleado getInstrumentoTipoEmpADefinir() {        
        return instrumentoTipoEmpADefinir;
    }

    /**
     *
     * @param instrumentoTipoEmpADefinir
     */
    public void setInstrumentoTipoEmpADefinir(TipoEmpleado instrumentoTipoEmpADefinir) {
        this.instrumentoTipoEmpADefinir = instrumentoTipoEmpADefinir;
    }
    
    
    /**
     *
     * @return
     */
    public List<Nivel360> getNiveles360ADefinir() {        
        return niveles360ADefinir;
    }

    /**
     *
     * @param niveles360ADefinir
     */
    public void setNiveles360ADefinir(List<Nivel360> niveles360ADefinir) {
        this.niveles360ADefinir = niveles360ADefinir;
    }

     /**
     *
     * @return
     */
    public Nivel360 getNivel360() {
        if(nivel360==null)
            nivel360=niveles360ADefinir.get(0);
        return nivel360;
    }

    /**
     *
     * @param nivel360
     */
    public void setNivel360(Nivel360 nivel360) {
        this.nivel360 = nivel360;
    }
    
     
    /**
     *
     * @return
     */
    public String prepareList() {
        return "ListaInstrumentos?faces-redirect=true";
    }
    
    /**
     * Metodo que se encarga de ejecutar las intrucciones necesarias previo a definir un instrumento; por ejemplo, que exista un plan vigente con criterios definidos y ponderados
     * @return 
     */
     public String inicializarFormularioInstrumentos() {  
       if(planADefinir!=null){ 
            ejbCriterio.limpiarTodoEnCache();            
            criteriosTipoEmpleado=ejbCriterio.getCriteriosByPlanTipoEmpleado(planADefinir, instrumentoTipoEmpADefinir);
            if(criteriosTipoEmpleado.size()>0){
                 if(ejbPonderacionCriterio.isPonderadoCriteriosTipoEmpEnPlan(planADefinir, instrumentoTipoEmpADefinir)){
                    llenarHashMapIndicesDetallesPC();
                    activeTabIndexNivel360=0;
                    niveles360ADefinir.clear();
                    niveles360ADefinir.add(Nivel360.AUTOEVALUACION);
                    niveles360ADefinir.add(Nivel360.SUBORDINADO);
                    if(!metodologia180){
                        niveles360ADefinir.add(Nivel360.PAR);
                        niveles360ADefinir.add(Nivel360.SUPERIOR);
                    }
                    for(Criterio c:criteriosTipoEmpleado){
                        calcularSumPuntajePreguntasPorCriterio(c); 
                        HashMap<Nivel360,Ponderacion> hasMapPonderacionNivel=new HashMap<Nivel360, Ponderacion>();
                        for(Nivel360 n:niveles360ADefinir){   
                            Ponderacion p=ejbPonderacionCriterio.findByNivel360Criterio(n, c);
                            hasMapPonderacionNivel.put(n, p);
                        } 
                        hashMapPonderacionPorCriterio.put(c, hasMapPonderacionNivel);
                    }
                    
                    nivel360=Nivel360.AUTOEVALUACION;
                    listaPreguntasAbiertas=ejbPreguntaAbierta.findByPlanTipoEmp(planADefinir, instrumentoTipoEmpADefinir);
                    ValidarPreguntasCapacitacion();
                    if(existEvaluacionesTipoEmpPlan())
                          JsfUtil.addWarnMessage("No podrá agregar/quitar preguntas a este plan porque ya existen evaluaciones realizadas en el mismo");
                    for(Nivel360 n:Nivel360.getConstantNiveles360()){
                        existEvaluacionNivel.put(n,ejbEvaluacion.existEvaluacionesTipoEmpPlanNivel(instrumentoTipoEmpADefinir, planADefinir, n));
                    }
                    inicializadoDefinicionInstrumento=true;
                    return "/instrumentos/DefinirInstrumentos?faces-redirect=true"; 
                }
                else{
                    JsfUtil.addErrorMessage("No se pueden definir el instrumento "+instrumentoTipoEmpADefinir.getDescripcion()+" porque no se han ponderado los criterios vinculados a este tipo de trabajador para el plan ["+planADefinir.getDescripcion()+"].");
                    return null;
                }
           }
           else{
               JsfUtil.addErrorMessage("No se puede definir el instrumento "+instrumentoTipoEmpADefinir.getDescripcion() +" porque el plan ["+planADefinir.getDescripcion()+"] no contiene criterios de evaluación para este tipo de trabajador. Edite el plan especificado para agregar los criterios");
               return null;
            }
       }
       else{
           JsfUtil.addErrorMessage("Seleccione el plan para definir los instrumentos de evaluacion");
           return null;
       }
    }

    //////////////////////////////////////////////////////////////
    /*MANEJO DE PREGUNTAS CERRADAS Y DETALLES DE PREGUNTAS CERRADAS*/
    /**
     *
     * @return
     */
    public PreguntaCerrada getPreguntaCerrada() {
        return preguntaCerrada;
    }

    /**
     *
     * @param preguntaCerrada
     */
    public void setPreguntaCerrada(PreguntaCerrada preguntaCerrada) {
        this.preguntaCerrada = preguntaCerrada;
    }

    /**
     *
     * @return
     */
    public boolean isDistribuirPuntajePC() {
        return distribuirPuntajePC;
    }

    /**
     *
     * @param distribuirPuntajePC
     */
    public void setDistribuirPuntajePC(boolean distribuirPuntajePC) {
        this.distribuirPuntajePC = distribuirPuntajePC;
    }    
     
    /**
     * Devuelve un conjunto de entradas (llave, valor) de los detalles de una pregunta cerrada. La llave esta conformada por "idPregunta+idNivel360" y el valor es el indice del detalle dentro del DetalleCollection de una pregunta
     * @return 
     */
    public HashMap<String, Integer> getHasMapIndicesDetallesPC() {
        return hasMapIndicesDetallesPC;
    }

    /**
     *
     * @param hasMapIndicesDetallesPC
     */
    public void setHasMapIndicesDetallesPC(HashMap<String, Integer> hasMapIndicesDetallesPC) {
        this.hasMapIndicesDetallesPC = hasMapIndicesDetallesPC;
    }
   
    /**
     * Este metodo se encarga de llenar el HashMapDetallesPC con cada uno de los detalles de cada pregunta en cada criterio.
     */
    public void llenarHashMapIndicesDetallesPC(){
         hasMapIndicesDetallesPC=new HashMap();
         for(Criterio c:criteriosTipoEmpleado){
             for(PreguntaCerrada p:c.getPreguntaCerradaCollection()){                 
                 for(int i=0;i<p.getDetallePreguntaCerradaCollection().size();i++){
                     agregarDPCHashMapDetallesPC(p, i);                     
                 }                 
             }
         }
     }
    
    /**
     * Este metodo se encarga de agregar una nueva entrada al hashMapDetallesPC
     * @param p A partir de p se crea la llave de la entrada a agregar al hashMap
     * @param indexDetalle es  el valor a agregar al hashMap
     */
    private void agregarDPCHashMapDetallesPC(PreguntaCerrada p,int indexDetalle){
        String id=p.getIdPregunta()+"-"+p.getDetallePreguntaCerradaCollection().get(indexDetalle).getNivel360().getIdNivel360().toString();
        if(hasMapIndicesDetallesPC==null)
            hasMapIndicesDetallesPC=new HashMap<>();
        hasMapIndicesDetallesPC.put(id,indexDetalle);
    }
    
    /**
     * Esta pregunta se encarga de preparar una nueva pregunta cerrada para el criterio espeficicado
     * @param c 
     */
    public void prepararPreguntaCerrada(Criterio c) {
        //generar el id de la pregunta cerrada
        String idPreg=String.valueOf(c.getIdCriterio()).concat("-");
        int numPreguntasCriterio=c.getPreguntaCerradaCollection().size();
        if(numPreguntasCriterio==0)
           idPreg=idPreg.concat("1");
        else{
            String lastId=c.getPreguntaCerradaCollection().get(numPreguntasCriterio-1).getIdPregunta();
                  
            idPreg=idPreg.concat(String.valueOf(Integer.parseInt(lastId.split("-")[1])+1));
        }      
        preguntaCerrada = new PreguntaCerrada(idPreg);
        preguntaCerrada.setIdCriterio(c); 
        preguntaCerrada.setDetallePreguntaCerradaCollection(new ArrayList<DetallePreguntaCerrada>());
        if(c.getPreguntaCerradaCollection()==null)
            c.setPreguntaCerradaCollection(new ArrayList<PreguntaCerrada>());
            
        for(Nivel360 n:Nivel360.getConstantNiveles360()){
            DetallePreguntaCerrada detallePC=new DetallePreguntaCerrada(new DetallePreguntaCerradaPK(preguntaCerrada.getIdPregunta(), n.getIdNivel360()));
            detallePC.setPreguntaCerrada(preguntaCerrada);
            detallePC.setNivel360(n);
            if(distribuirPuntajePC){
                Ponderacion ponderacionCriterio=obtenerPonderacionRealCriterioNivel(c, n);
                if(ponderacionCriterio!=null){
                    double puntajeEquitativo=ponderacionCriterio.getPuntaje()/(c.getPreguntaCerradaCollection().size()+1);
                    detallePC.setValor(puntajeEquitativo);
                }
                else
                    detallePC.setValor(0);
            }
            if(!niveles360ADefinir.contains(n))
                detallePC.setDescripcion("Pendiente de redactar");
             preguntaCerrada.getDetallePreguntaCerradaCollection().add(detallePC);                
           }  
        this.criterio=c;
    }
    
    /**
     * Agrega una pregunta cerrada al instrumento
     */
    public void agregarPreguntaCerrada(){                               
        criterio.getPreguntaCerradaCollection().add(preguntaCerrada);
        if(distribuirPuntajePC)
            redistribuirPuntajes(criterio);  
        for(int i=0;i<preguntaCerrada.getDetallePreguntaCerradaCollection().size();i++)
            agregarDPCHashMapDetallesPC(preguntaCerrada, i);
        calcularSumPuntajePreguntasPorCriterio(criterio);
        RequestContext.getCurrentInstance().execute("nuevaPreguntaCerradaDlg.hide()");
    }
    
    /**
     * Edita una pregunta cerrada de un instrumento
     */
    public void editarPreguntaCerrada() { 
        if(distribuirPuntajePC)
            redistribuirPuntajes(criterio);
         calcularSumPuntajePreguntasPorCriterio(criterio);
         RequestContext.getCurrentInstance().execute("editarPreguntaCerradaDlg.hide()");
    }
    
    /**
     * Elimina una pregunta cerrada de un instrumento
     */
    public void eliminarPreguntaCerrada(){        
        criterio.getPreguntaCerradaCollection().remove(preguntaCerrada);
        if(distribuirPuntajePC)
            redistribuirPuntajes(criterio);
        calcularSumPuntajePreguntasPorCriterio(criterio);
    }
    
    private void redistribuirPuntajes(Criterio c){
        for(PreguntaCerrada p:c.getPreguntaCerradaCollection())
                for(DetallePreguntaCerrada dt:p.getDetallePreguntaCerradaCollection())
                    if(niveles360ADefinir.contains(dt.getNivel360())){
                        Ponderacion ponderacionCriterio=obtenerPonderacionRealCriterioNivel(c, dt.getNivel360());
                        double puntajeEquitativo=ponderacionCriterio.getPuntaje()/( c.getPreguntaCerradaCollection().size());
                        dt.setValor(puntajeEquitativo);
                    }
    }
        
    
    /**
     * Metodo que devuleve una lista de mensajes de error, para notificar al usuario que debe corregir el puntaje de las preguntas del criterio especificado.
     * @param c
     * @return 
     */
    public List<String> getListaMensajePuntajesIncorrecto(Criterio c){
        List<String> listaMensajePuntajesIncorrecta=new ArrayList<String>();
        for(Nivel360 n:niveles360ADefinir){
            Ponderacion ponderacionCriterioNivel360=obtenerPonderacionRealCriterioNivel(c, n);
            double ponderacionActualCriterio=obtenerSumaPuntajeCriterioNivel(c,n);
            if(ponderacionCriterioNivel360.getPuntaje()!=ponderacionActualCriterio && ponderacionActualCriterio!=-1){
                String mensaje="La suma de los puntajes de las preguntas en el nivel "+n.getDescripcion()+" debe ser igual a "+ponderacionCriterioNivel360.getPuntaje();
                listaMensajePuntajesIncorrecta.add(mensaje);
            }
                                            
        }   
        return listaMensajePuntajesIncorrecta;
    }
    
    /**
     * Metodo que retorna desde el hashmap de puntajesPorCriterio la suma de los puntajes de un criterio en un un nivel determinado 
     * @param c es el criterio del cual se quiere conocer la suma de los puntajes de sus preguntas
     * @param nivel360 es el nivel360 a partir del cual se devolvera la suma de los puntajes de la preguntas (una pregunta puede tener distintos puntajes en cada nivel360)
     * @return retorna -1 si no se encuentra la suma de los puntajes de la preguntas  para un criterio en el nivel especificado
     */
    public double obtenerSumaPuntajeCriterioNivel(Criterio c,Nivel360 nivel360){           
        if(hashMapPuntajeAcumuladoPorCriterio.containsKey(c)){
            HashMap<Nivel360,Double> hashMapSumPuntajesPreguntasNivel=hashMapPuntajeAcumuladoPorCriterio.get(c);
            if(hashMapSumPuntajesPreguntasNivel.containsKey(nivel360))
              return hashMapSumPuntajesPreguntasNivel.get(nivel360);
        }
        return -1; 
    }
    
    /**
     *
     * @param c
     * @param nivel360
     * @return
     */
    public Ponderacion obtenerPonderacionRealCriterioNivel(Criterio c,Nivel360 nivel360){
        if(hashMapPonderacionPorCriterio.containsKey(c)){
            HashMap<Nivel360,Ponderacion> hashMapPonderacionesPorNivel=hashMapPonderacionPorCriterio.get(c);
            if(hashMapPonderacionesPorNivel.containsKey(nivel360)){
                Ponderacion p= hashMapPonderacionesPorNivel.get(nivel360);
                return p;
            }
        }
        return null;
    }
    
    
    /**
     * Metodo que calcula la suma de los puntajes de la preguntas en el criterio especificado.
     * @param c 
     */
    public void calcularSumPuntajePreguntasPorCriterio(Criterio c){        
        hashMapPuntajeAcumuladoPorCriterio.remove(c);
        HashMap<Nivel360,Double> hashMapSumPuntajesPreguntasNivel=new HashMap<Nivel360,Double>();
        
        for(PreguntaCerrada p:c.getPreguntaCerradaCollection())
               for(DetallePreguntaCerrada dt:p.getDetallePreguntaCerradaCollection())   {
                   if(niveles360ADefinir.contains(dt.getNivel360())){                        
                        if(!hashMapSumPuntajesPreguntasNivel.containsKey(dt.getNivel360())){
                             hashMapSumPuntajesPreguntasNivel.put(dt.getNivel360(), dt.getValor());
                          }
                        else{
                             double puntajeAcumulado=hashMapSumPuntajesPreguntasNivel.get(dt.getNivel360());
                             hashMapSumPuntajesPreguntasNivel.put(dt.getNivel360(), puntajeAcumulado+dt.getValor());
                        }
                   }
               }
        hashMapPuntajeAcumuladoPorCriterio.put(c, hashMapSumPuntajesPreguntasNivel);
    }

    ////////////////////////////////////////////////////////////
    /*MANEJO DE PREGUNTAS ABIERTAS*/
    
    /**
     * Metodo que valida que existan preguntas de capacitacion para el instrumento que se define y para cada nivel360
     */
    
    public void ValidarPreguntasCapacitacion(){
        for(Nivel360 nivel360:niveles360ADefinir){
            if(ejbPreguntaAbierta.findByPlanTipoEmpNivel360(planADefinir, instrumentoTipoEmpADefinir, nivel360).isEmpty()){
                PreguntaAbierta pa=crearPreguntaAbierta(nivel360);
                switch(pa.getNivel360().getIdNivel360()){
                    case 1: pa.setDescripcion("¿Que capacitaciones considera que necesita recibir?");
                            break;
                    case 2: pa.setDescripcion("¿Que capacitaciones recomienda para su subordinado?");
                            break;
                    case 3: pa.setDescripcion("¿Que capacitaciones recomienda para su compañero?");
                            break;
                    case 4: pa.setDescripcion("¿Que capacitaciones recomienda para su superior?");
                            break;
                  }
                  getListaPreguntasAbiertas().add(pa);

              }  
        }
     }

    /**
     *
     * @return
     */
    public PreguntaAbierta getPreguntaAbierta() {
        return preguntaAbierta;
    }

    /**
     *
     * @param preguntaAbierta
     */
    public void setPreguntaAbierta(PreguntaAbierta preguntaAbierta) {
        this.preguntaAbierta = preguntaAbierta;
    }
    
    /**
     *
     * @return
     */
    public List<PreguntaAbierta> getListaPreguntasAbiertas() {
        if(listaPreguntasAbiertas==null)
           listaPreguntasAbiertas=new ArrayList<PreguntaAbierta>();        
        return listaPreguntasAbiertas;
    }
    
    /**
     *
     * @param listaPreguntasAbiertas
     */
    public void setListaPreguntasAbiertas(List<PreguntaAbierta> listaPreguntasAbiertas) {
        this.listaPreguntasAbiertas = listaPreguntasAbiertas;
    } 

    /**
     *
     * @return
     */
    public int getIndicePreguntaAbierta() {
        return indicePreguntaAbierta;
    }

    /**
     *
     * @param indicePreguntaAbierta
     */
    public void setIndicePreguntaAbierta(int indicePreguntaAbierta) {
        this.indicePreguntaAbierta = indicePreguntaAbierta;
    }
        
    /**
     *
     * @return
     */
    public List<PreguntaAbierta> getListaPATemporal() {
        if(listaPATemporal==null)
            listaPATemporal=new ArrayList<PreguntaAbierta>();
        return listaPATemporal;
    }

    /**
     *
     * @param listaPATemporal
     */
    public void setListaPATemporal(List<PreguntaAbierta> listaPATemporal) {
        this.listaPATemporal = listaPATemporal;
    }
    
    private void prepararListaPATemporalTodoslosNivelesADefinir(){
        listaPATemporal=new ArrayList<PreguntaAbierta>();
        for(Nivel360 nivel360:niveles360ADefinir){           
            listaPATemporal.add(crearPreguntaAbierta(nivel360));
        }
    }

    /**
     *
     * @return
     */
    public List<PreguntaAbierta> getListaPAParaEliminar() {
        if(listaPAParaEliminar==null)
           listaPAParaEliminar=new ArrayList<PreguntaAbierta>(); 
        return listaPAParaEliminar;
    }

    /**
     *
     * @param listaPAParaEliminar
     */
    public void setListaPAParaEliminar(List<PreguntaAbierta> listaPAParaEliminar) {
        this.listaPAParaEliminar = listaPAParaEliminar;
    }
        
    private PreguntaAbierta crearPreguntaAbierta(Nivel360 nivel){
        PreguntaAbierta pa=new PreguntaAbierta();
        pa.setIdPlanEstrategico(planADefinir);
        pa.setTipoEmpleado(instrumentoTipoEmpADefinir);
        pa.setNivel360(nivel);
        return pa;
    }
    
    /**
     *
     * @return
     */
    public boolean isPregAbiertaParaTodosNiveles() {
        return PregAbiertaParaTodosNiveles;
    }

    /**
     *
     * @param PregAbiertaParaTodosNiveles
     */
    public void setPregAbiertaParaTodosNiveles(boolean PregAbiertaParaTodosNiveles) {
        this.PregAbiertaParaTodosNiveles = PregAbiertaParaTodosNiveles;
    }
    
   
    /**
     *
     */
    public void abrirDialogNuevaPregunta(){
        if(PregAbiertaParaTodosNiveles==true){
             prepararListaPATemporalTodoslosNivelesADefinir();
             RequestContext.getCurrentInstance().execute("nuevaListaPADlg.show()");
        }
        else{
             preguntaAbierta=crearPreguntaAbierta(nivel360);
             RequestContext.getCurrentInstance().execute("nuevaPregAbiertaDlg.show()");
        }
    }
      
    /**
     * 
     */
    public void agregarPreguntaAbierta(){
         getListaPreguntasAbiertas().add(preguntaAbierta);
         RequestContext.getCurrentInstance().execute("nuevaPregAbiertaDlg.hide()");
    }
   
    /**
     *
     */
    public void agregarListaPreguntasAbiertas(){        
        for(PreguntaAbierta pa:listaPATemporal)
            getListaPreguntasAbiertas().add(pa);
        RequestContext.getCurrentInstance().execute("nuevaListaPADlg.hide()");
    }
    
    /**
     *
     */
    public void editarPreguntaAbierta(){
        listaPreguntasAbiertas.set(indicePreguntaAbierta, preguntaAbierta);
        RequestContext.getCurrentInstance().execute("editarPreguntaAbiertaDlg.hide()");
    }
    
    /**
     *
     */
    public void eliminarPreguntaAbierta(){
        listaPAParaEliminar.add(listaPreguntasAbiertas.get(indicePreguntaAbierta));
        listaPreguntasAbiertas.remove(indicePreguntaAbierta);
    }
    
    /**
     *
     * @return
     */
    public String guardarInstrumentos() {
        inicializadoDefinicionInstrumento=false;
        boolean errores=false;          
        for(Criterio c:criteriosTipoEmpleado){
            if(c.getPreguntaCerradaCollection().isEmpty()){
                JsfUtil.addErrorMessage("Debe agregar preguntas para el criterio: ".concat(c.getDescripcion()));
                errores=true;
            }
            if(!getListaMensajePuntajesIncorrecto(c).isEmpty()){
                JsfUtil.addErrorMessage("Corrija la suma de los puntajes en el criterio: ".concat(c.getDescripcion()));
                errores=true; 
            }
        }
        
       //si hay errores retornar null
        if(errores){
               return "";
        }
        //si no hay errores guardar la informacion del instrumento
        else{
            try{
                for(PreguntaAbierta pa:getListaPreguntasAbiertas()){
                    if(pa.getIdPregunta()!=null)
                        ejbPreguntaAbierta.edit(pa);
                    else 
                        ejbPreguntaAbierta.create(pa);
                }            
                for(PreguntaAbierta pa:getListaPAParaEliminar()){
                    if(pa.getIdPregunta()!=null)
                        ejbPreguntaAbierta.remove(pa);
                }            
                for(Criterio c:criteriosTipoEmpleado){
                    ejbCriterio.edit(c);
                }
                JsfUtil.addSuccessMessage("El instrumento ".concat(instrumentoTipoEmpADefinir.getDescripcion()).concat(" se ha guardado correctamente"));
                return "ListaInstrumentos?faces-redirect=true";  
            }
            catch(Exception e){
                JsfUtil.addErrorMessage("Lo sentimos, han habido errores durante el proceso de registro en la base de datos");
                System.out.println(e);
                return null;
            }
        }
        
    }
    
    /**
     *
     * @param p
     * @return
     */
    public boolean isPregCapacitacion(PreguntaAbierta p){
        if(p.getDescripcion().toLowerCase().contains("capacita") ||
            p.getDescripcion().toLowerCase().contains("curso") ||
            p.getDescripcion().toLowerCase().contains("taller") ){
             return true;
        }    
        return false;
    }
    
    /**
     *
     */
    public void validarEntradaDefinicionInstrumento() { 
        if(!inicializadoDefinicionInstrumento) {  
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response =(HttpServletResponse)context.getExternalContext().getResponse();
            try {
                response.sendRedirect("ListaInstrumentos.xhtml");
            } catch (IOException ex) {
                System.out.println(ex);
            }
            context.responseComplete();
        }
    }
    
    /**
     * Clase auxiliar utilizada para la creacion de columnas dinamicas segun niveles360
     */
    static public class ColumnModel implements Serializable {    
          
        private Nivel360 nivel360;  
  
        /**
         *
         * @param nivel360
         */
        public ColumnModel(Nivel360 nivel360) {              
            this.nivel360 = nivel360;  
        }  
        /**
         *
         * @return
         */
        public Nivel360 getNivel360() {  
            return nivel360;  
        }  
    }  
    
}
