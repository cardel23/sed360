package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.Nivel360;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.Nivel360Facade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.primefaces.context.RequestContext;
import uca.sed360.jpaEntities.Criterio;
import uca.sed360.jpaEntities.PlanEstrategico;
import uca.sed360.jpaEntities.PreguntaAbierta;
import uca.sed360.jpaEntities.PreguntaCerrada;
import uca.sed360.jpaEntities.DetallePreguntaCerrada;
import uca.sed360.jpaEntities.DetallePreguntaCerradaPK;

/**
 * Controlador que maneja los niveles de la metodología 360
 * @author Estacion 2
 */
@ManagedBean(name = "niveles360Controller")
@SessionScoped
public class Niveles360Controller implements Serializable {

    private Nivel360 current;
    

    @EJB
    private uca.sed360.ejbs.Nivel360Facade ejbFacade;
    @ManagedProperty(value="#{redaccionesController}")
    private DetallePreguntaCerradaController redaccionesMB;    
    private List<Nivel360> listaNiveles360;
    private boolean validadoNiveles360=false;
    
      
    /**
     * Constructor
     */
    public Niveles360Controller() {
        
    }

    /**
     *
     * @return
     */
    public DetallePreguntaCerradaController getRedaccionesMB() {
        return redaccionesMB;
    }

    /**
     *
     * @param redaccionesMB
     */
    public void setRedaccionesMB(DetallePreguntaCerradaController redaccionesMB) {
        this.redaccionesMB = redaccionesMB;
    }

    /**
     *
     * @return
     */
    public List<Nivel360> getListaNiveles360(){       
        if(listaNiveles360==null){            
            listaNiveles360=getFacade().findAll();
        }
        return listaNiveles360;
    }

    /**
     *
     * @param listaNiveles360
     */
    public void setListaNiveles360(List<Nivel360> listaNiveles360) {
        this.listaNiveles360 = listaNiveles360;
    }
    
    /**
     *
     */
    public void validarNiveles360(){
        if(validadoNiveles360==false){
            if(getFacade().count()!=Nivel360.getConstantNiveles360().size()){
                getFacade().validarInstrumentos();
                JsfUtil.addSuccessMessage("Se ha reestablecido el catálogo de instrumentos");
            }    
            validadoNiveles360=true;
        }
    }

    /**
     *
     * @return
     */
    public Nivel360 getSelected() {
        if (current == null) {
            current = new Nivel360();
        }
        return current;
    }

    /**
     *
     * @return
     */
    public Nivel360 getCurrent() {
        return current;
    }
    
   
    /**
     *
     * @param current
     */
    public void setCurrent(Nivel360 current) {
        this.current = current;
    }

    /**
     *
     * @return
     */
    public Nivel360Facade getFacade() {
        return ejbFacade;
    }
    
    /**
     *
     * @return
     */
    public Nivel360 getNivelSuperior(){
        return  Nivel360.SUPERIOR;
    }
    
    /**
     *
     * @return
     */
    public Nivel360 getNivelPar(){
        return Nivel360.PAR;
    }
    
    /**
     *
     * @return
     */
    public Nivel360 getNivelSubordinado(){
        return Nivel360.SUBORDINADO;
    }
    
    /**
     *
     * @return
     */
    public Nivel360 getNivelAutoEvaluacion(){
        return Nivel360.AUTOEVALUACION;
    }


    /**
     *
     */
    @FacesConverter(value = "Niveles360Converter",forClass = Nivel360.class)
    public static class Niveles360Converter implements Converter {

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            Niveles360Controller controller = (Niveles360Controller) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "niveles360Controller");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Nivel360) {
                Nivel360 o = (Nivel360) object;
                //return getStringKey(o.getIdInstrumento());
                return getStringKey(o.getIdNivel360());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Nivel360.class.getName());
            }
        }
    }
}
