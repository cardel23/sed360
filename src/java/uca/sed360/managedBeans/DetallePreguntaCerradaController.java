package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.DetallePreguntaCerrada;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.DetallePreguntaCerradaFacade;
import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;

/**
 * Controlador que maneja el detalle de cada pregunta cerrada según el nivel 360
 * @author Estacion 2
 */
@ManagedBean(name = "detallePreguntaCerradaController")
@SessionScoped
public class DetallePreguntaCerradaController implements Serializable {

    private DetallePreguntaCerrada current;
    @EJB
    private uca.sed360.ejbs.DetallePreguntaCerradaFacade ejbFacade;
    @ManagedProperty(value="#{evaluacionController}")
    private EvaluacionController evaluacionController;
    @ManagedProperty(value="#{planEstrategicoController}")
    private PlanEstrategicoController planMB;
    
    /**
     * Constructor
     */
    public DetallePreguntaCerradaController() {
    }

    /**
     *
     * @return
     */
    public EvaluacionController getEvaluacionController() {
        return evaluacionController;
    }

    /**
     *
     * @return
     */
    public PlanEstrategicoController getPlanMB() {
        return planMB;
    }

    /**
     *
     * @param planMB
     */
    public void setPlanMB(PlanEstrategicoController planMB) {
        this.planMB = planMB;
    }
    
    /**
     *
     * @param evaluacionController
     */
    public void setEvaluacionController(EvaluacionController evaluacionController) {
        this.evaluacionController = evaluacionController;
    }
    
    /**
     *
     * @return
     */
    public DetallePreguntaCerrada getSelected() {
        if (current == null) {
            current = new DetallePreguntaCerrada();
            //current.setRedaccionesPK(new uca.sed360.jpaEntities.DetallePreguntaCerradaPK());
            current.setDetallePreguntaCerradaPK(new uca.sed360.jpaEntities.DetallePreguntaCerradaPK());
        }
        return current;
    }

    private DetallePreguntaCerradaFacade getFacade() {
        return ejbFacade;
    }
    
    /**
     *
     * @return
     */
    public String prepareList() {
        return "List";
    }

    /**
     *
     * @return
     */
    public String prepareView() {
        return "View";
    }

    /**
     *
     * @return
     */
    public String prepareCreate() {
        current = new DetallePreguntaCerrada();
        current.setDetallePreguntaCerradaPK(new uca.sed360.jpaEntities.DetallePreguntaCerradaPK());
        return "Create";
    }

    /**
     *
     * @return
     */
    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RedaccionesCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String prepareEdit() {
        return "Edit";
    }

    /**
     *
     * @return
     */
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RedaccionesUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String destroy() {
        performDestroy();
        return "List";
    }


    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RedaccionesDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }


    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    /**
     *
     */
    @FacesConverter(forClass = DetallePreguntaCerrada.class)
    public static class RedaccionesControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            DetallePreguntaCerradaController controller = (DetallePreguntaCerradaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "redaccionesController");
            return controller.ejbFacade.find(getKey(value));
        }

        uca.sed360.jpaEntities.DetallePreguntaCerradaPK getKey(String value) {
            uca.sed360.jpaEntities.DetallePreguntaCerradaPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new uca.sed360.jpaEntities.DetallePreguntaCerradaPK();
            key.setIdPregunta((values[0]));
            key.setIdNivel360(Integer.parseInt(values[1]));
            return key;
        }

        String getStringKey(uca.sed360.jpaEntities.DetallePreguntaCerradaPK value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value.getIdPregunta());
            sb.append(SEPARATOR);
            sb.append(value.getIdNivel360());
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof DetallePreguntaCerrada) {
                DetallePreguntaCerrada o = (DetallePreguntaCerrada) object;
                return getStringKey(o.getDetallePreguntaCerradaPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + DetallePreguntaCerrada.class.getName());
            }
        }
    }
}
