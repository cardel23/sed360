package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.PlanEstrategico;
import uca.sed360.jpaEntities.Criterio;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.PlanEstrategicoFacade;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;
import org.primefaces.event.CellEditEvent;
import uca.sed360.jpaEntities.Escalas;
import uca.sed360.managedBeans.util.reportes.ReportesUtil;
import uca.sed360.jpaEntities.TipoEmpleado;

/**
 * Controlador que maneja los planes estratégicos
 * @author Estacion 2
 */
@ManagedBean(name = "planEstrategicoController")
@SessionScoped
public class PlanEstrategicoController implements Serializable {
    
    private PlanEstrategico current;
    private PlanEstrategico planTemporal;
    private List<PlanEstrategico> listaPlanes;
    private List<Double> valoresDeEscalas ;
    private Criterio newCriterio;
    private ArrayList<Criterio> criteriosNuevosParaEdición;
    private ArrayList<Criterio> criteriosEliminados;
    private Escalas newEscala;
    private boolean escalaNoAPlica,hayEvaluacionesParaElPlanAEditar, validadoHayPlanesEstrategicos=false;
   // private Date today;
    
    @ManagedProperty(value = "#{auditoriaController}")
    private AuditoriaController audiController;
    
    @EJB
    private uca.sed360.ejbs.PlanEstrategicoFacade ejbFacade;
    @EJB
    private uca.sed360.ejbs.EvaluacionFacade ejbEvaluaciones;
    @EJB
    private uca.sed360.ejbs.TiposEmpleadoFacade ejbTipoE;
    private PlanEstrategico planVigente;
    private int posicion;


    /**
     * Constructor
     */
    public PlanEstrategicoController() {
    }
    
    /**
     * Despues de construir el controlador inicializa los objetos que
     * forman parte del plan
     */
    @PostConstruct
        public void init()
        {
                
                valoresDeEscalas = new ArrayList<Double>();
                for(double i = 1;i<=10;i++)
                {   
                    valoresDeEscalas.add(Double.valueOf(i/10));
                }
                setPosicion(1);
                newCriterio = new Criterio();
                newCriterio.setDescripcion("nuevo criterio");
                newEscala = new Escalas();
                newEscala.setDescripcion("nueva escala");
                
                setEscalaNoAPlica(false);
        }
    
    
    //Criterios

    /**
     *
     * @return
     */
    public Criterio getNewCriterio() {
        if(newCriterio==null) {
            newCriterio=new Criterio();
        }
        return newCriterio;
    }

    /**
     *
     * @param criterio
     */
    public void setNewCriterio(Criterio criterio) {
        this.newCriterio = criterio;
    }

   

    /**
     * Anula el plan estratégico
     */
    public void cancelar(){ 
        current=null;    
        newCriterio = null;
        newEscala = null;
        posicion=1;
        escalaNoAPlica = false;
    }
    
    /**
     * Agrega un criterio al plan
     */
    public void agregarCriterio() {
        
        boolean agregado=false;
        for (TipoEmpleado tipoE : ejbTipoE.findAll()) {
            for (Criterio c : current.getCriteriosCollection()) {
                if (c.getTipoEmpleado().getIdTipoempleado() == tipoE.getIdTipoempleado() && newCriterio.getTipoEmpleado().getIdTipoempleado()==tipoE.getIdTipoempleado()) {
                    if (c.getDescripcion().equals(newCriterio.getDescripcion())) {
                        agregado = true;
                        JsfUtil.addErrorMessage("Ya se ha agregado el criterio: <".concat(newCriterio.getDescripcion().concat(">"))
                                + " para el tipo de empleado " + tipoE.getDescripcion());
                        break;
                    }
                }
            }
        }
        
        
        if(agregado==false){            

            
           newCriterio.setPlanEstrategico(current);                                          
           
           current.getCriteriosCollection().add(newCriterio);
           newCriterio= new Criterio();
           newCriterio.setDescripcion("nuevo criterio");           
        }        
    }
    
    
    /**
     * Elimina un criterio del plan
     * @param criterio
     */
    public void quitarCriterio(Criterio criterio) {                                   
        for(int i=0;i<current.getCriteriosCollection().size();i++){                                
            if(current.getCriteriosCollection().get(i).getDescripcion().equals(criterio.getDescripcion())){
                current.getCriteriosCollection().remove(i);
                break;
            }                
        }                
    }    
    
    //Escalas

    /**
     *
     * @return
     */
    public Escalas getNewEscala() {
            if(newEscala == null) {
            newEscala=new Escalas();}
            return newEscala;
    }

    /**
     *
     * @param newEscala
     */
    public void setNewEscala(Escalas newEscala) {
        if(newEscala.getValor() > 1) {
        JsfUtil.addErrorMessage("El valor de la escala no debe ser mayor que 1");
        this.newEscala = null;
        } else
        this.newEscala = newEscala;
    }

    
    /**
     * Agrega la escala de evaluación que será usada en el plan estratégico
     */
    public void agregarEscala() {
        boolean agregado=false;
        
        
        if(newEscala.getValor() > 1 || newEscala.getValor() <= 0) {
        JsfUtil.addErrorMessage("El valor de la escala no debe ser mayor a 1 ni menor a 0");
        } else {
        
        for(Escalas e:current.getEscalasCollection()){
           if(e.getDescripcion().equals(newEscala.getDescripcion())){
               agregado=true;
               JsfUtil.addErrorMessage("Ya se ha agregado la escala: <".concat(newEscala.getDescripcion().concat(">")));
               break;
           }
           
        }
        if(!agregado){
           newEscala.setIdPlanEstrategico(current);
           current.getEscalasCollection().add(newEscala);
           newEscala= new Escalas();
           newEscala.setDescripcion("nueva escala");
        } 
      
    
                
        }
    }
    
    /**
     * Elimina la escala del plan
     * @param escala
     */
    public void quitarEscala(Escalas escala) {
        for(int i=0;i<current.getEscalasCollection().size();i++){
            if(current.getEscalasCollection().get(i).getDescripcion().equals(escala.getDescripcion())){
                current.getEscalasCollection().remove(i);
                break;
            }                
        }
    }
   
    /**
     * Valida que un plan estratégico pueda ser modificado mientras no existan
     * evaluaciones relacionadas a dicho plan
     */
    public void validarEdicionPlan() {
      
        if (ejbEvaluaciones.obtenerEvaluacionesPorPlanEstrategico(current).size() >= 1) {
                 hayEvaluacionesParaElPlanAEditar = true;                           
       } 
   
   }
   
    /**
     * Modifica el criterio en la celda de la vista html para guardar en la BD
     * @param event
     */
    public void onCellEditCriterio(CellEditEvent event) {  
        Criterio oldValue = (Criterio) event.getOldValue();  
        Criterio newValue = (Criterio) event.getNewValue();  
          
        if(newValue != null && !newValue.equals(oldValue)) {  
            JsfUtil.addSuccessMessage("Se ha modificado el criterio "+oldValue.getDescripcion()+" a "+newValue.getDescripcion() );
        }  
    } 
   
    /**
     * Modifica la escala en la celda de la vista html para guardar en la BD
     * @param event
     */
    public void onCellEditEscala(CellEditEvent event) {  
        Escalas oldValue = (Escalas) event.getOldValue();  
        Escalas newValue = (Escalas) event.getNewValue();  
          
        if(newValue != null && !newValue.equals(oldValue)) {  
            JsfUtil.addSuccessMessage("Se ha modificado la escala "+oldValue.getDescripcion());
        }  
    } 
    //Plan Estrategicos
    /**
     *
     * @return
     */
    public PlanEstrategico getPlanVigente() {
        if(planVigente==null)
            planVigente=getFacade().getPlanVigente();
        return planVigente;
    }

    /**
     *
     * @param planVigente
     */
    public void setPlanVigente(PlanEstrategico planVigente) {
        this.planVigente = planVigente;
    }
  
    /**
     * Busca en la base de datos el plan estratégico anterior o el último
     * @return plan estratégico si existe
     */
    public PlanEstrategico getLastPlan(){
        int listaPlanesSize=getListaPlanes().size();
        if(listaPlanesSize>0)
            return getListaPlanes().get(listaPlanesSize-1);
        return null;
    }
    
    /**
     * Busca en la base de datos el primer plan estratégico
     * @return plan estratégico si existe
     */
    public PlanEstrategico getPrimerPlan(){
        if(getListaPlanes().size() > 0)
            return getListaPlanes().get(0);
        else
            return null;
    }

    /**
     *
     * @return
     */
    public PlanEstrategico getCurrent() {
        if (current == null) {
            current = new PlanEstrategico();
        }
        return current;
    }

    /**
     *
     * @param current
     */
    public void setCurrent(PlanEstrategico current) {
        this.current = current;
    }
    
    private PlanEstrategicoFacade getFacade() {
        return ejbFacade;
    }

    /**
     *
     * @return
     */
    public String prepareCreate() {
        posicion = 1;
        escalaNoAPlica = false;
        current = new PlanEstrategico();        
        return "CrearPlanEstrategico?faces-redirect=true";
    }

    /**
     *
     * @return
     */
    public String create() {
         
            PlanEstrategico planConflictivo;
            planConflictivo = getFacade().findByFechas(current);
       
        
            if (planConflictivo != null) {
                SimpleDateFormat df=new SimpleDateFormat("dd/MM/yyyy");            
                JsfUtil.addErrorMessage("La fecha de inicio del plan debe ser mayor a la fecha final del ultimo plan registrado: ".concat(df.format(planConflictivo.getFechaFin())));
                return null;}
            
            
            if (current.getCriteriosCollection().isEmpty() || current.getEscalasCollection().isEmpty()) {
                    JsfUtil.addErrorMessage("Debe agregar escalas y criterios para el plan");
                    return null;
                }                             
            
            
                
                try {
                    String identificador = getFacade().generateIdPlan(current);
                    current.setIdentificador(identificador);
                    current.setCriteriosCollection(current.getCriteriosCollection());
                    getFacade().create(current);

                    
                    audiController.guardarRegistroActividad("creó el plan estratégico "+current.getIdentificador());
                    JsfUtil.addSuccessMessage("El nuevo plan estratégico, sus criterios y  escalas de evaluacion han sido registrados correctamente");
                    current = null;
                    validadoHayPlanesEstrategicos = false;
                    planVigente = null;
                    listaPlanes = null;                    
                    posicion = 1;
                    return "PlanesEstrategicos?faces-redirect=true";
                } catch (Exception e) {
                    if(current != null)
                    getFacade().remove(current);
                    JsfUtil.addErrorMessage("Lo sentimos, no se ha podido registrar el nuevo plan estratégico");
                    return null;
                }

          

       
    }
    
    
    
           
    /**
     * Crea el objeto "No aplica" que se agrega por defecto a las escalas de
     * evaluación
     */
    public void manejarEscalaNoAplica(){
    
    if(escalaNoAPlica == false)
    {
        newEscala = new Escalas();
        newEscala.setValor(1);
        newEscala.setDescripcion("N/A");     
        agregarEscala();
        escalaNoAPlica = true;
    }
    else
    {
        if(current != null)
        {
            for(Escalas e : current.getEscalasCollection())
            {
                if(e.getDescripcion().contains("N/A")){
                    quitarEscala(e);
                    break;
                }
                    
            }
            escalaNoAPlica = false;
        }
    }
}
      
    
    /**
     *
     * @return
     */
    public String prepareEdit() {
        
        posicion = 1;
        
        for(Escalas e: current.getEscalasCollection())
            if(e.getDescripcion().contains("N/A"))
                    escalaNoAPlica = true;
            else
                    escalaNoAPlica = false;
        
        hayEvaluacionesParaElPlanAEditar = false;
        planTemporal = new PlanEstrategico();
        planTemporal.setCriteriosCollection(current.getCriteriosCollection());
        planTemporal.setEscalasCollection(current.getEscalasCollection());
        planTemporal.setFechaFin(current.getFechaFin());
        planTemporal.setFechaInicio(current.getFechaInicio());
        planTemporal.setDescripcion(current.getDescripcion());
        planTemporal.setIdentificador(current.getIdentificador());
        planTemporal.setPeriodoCollection(current.getPeriodoCollection());
        planTemporal.setPreguntaAbiertaCollection(current.getPreguntaAbiertaCollection());
        
        
        return "EditarPlanEstrategico?faces-redirect=true";    
    }
    
    
    
    

    /**
     *
     * @return
     */
    public String update() {
        try {            
            
            PlanEstrategico planConflictivo;
            planConflictivo = getFacade().findByFechas(current);
       
        
            if (planConflictivo != null) {
                SimpleDateFormat df=new SimpleDateFormat("dd/MM/yyyy");            
                JsfUtil.addErrorMessage("Por favor comprobar las fechas de inicio y fin");
                return null;}
            
            if(getFacade().comprobarConflictosConPeriodo(current))
                {
                        JsfUtil.addErrorMessage("La fecha del plan entra en conflictos con las fechas de períodos, por favor seleccione otra fecha"
                        + " o modifique la fecha del o los períodos cuya fecha de fin está antes que la fecha de fin que intenta definir");
                        return null;
                }
            
            if (current.getCriteriosCollection().isEmpty() || current.getEscalasCollection().isEmpty()) {
                    JsfUtil.addErrorMessage("Debe agregar escalas y criterios para el plan");
                    return null;
                } 
                
            
           
            String identificador=getFacade().generateIdPlan(current);
            current.setIdentificador(identificador);          
            getFacade().edit(current);                                   

            
            JsfUtil.addSuccessMessage("El plan estrategico con id: ".concat(current.getIdentificador()).concat(" se ha actualizado correctamente"));
            audiController.guardarRegistroActividad("editó el plan estratégico "+current.getIdentificador());
            
            current=null;
            validadoHayPlanesEstrategicos=false;
            planVigente=null;
            listaPlanes=null;
            return "PlanesEstrategicos";
        } catch (Exception e) {                                                                               
            current.setCriteriosCollection(getPlanTemporal().getCriteriosCollection());
            current.setEscalasCollection(getPlanTemporal().getEscalasCollection());
            current.setFechaFin(getPlanTemporal().getFechaFin());
            current.setFechaInicio(getPlanTemporal().getFechaInicio());
            current.setIdentificador(getPlanTemporal().getIdentificador());
            current.setDescripcion(getPlanTemporal().getDescripcion());
            current.setPreguntaAbiertaCollection(getPlanTemporal().getPreguntaAbiertaCollection());
            current.setPeriodoCollection(getPlanTemporal().getPeriodoCollection());
            getFacade().edit(current);
            JsfUtil.addErrorMessage("Error al tratar de actualizar el plan estrategico con id: ".concat(current.getIdentificador()));
            return "planesestrategicos";
        }
    }

    
    
    
    
    /**
     *
     */
    public void destroy() {
        String identificador = current.getIdentificador();
        if (ejbEvaluaciones.obtenerEvaluacionesPorPlanEstrategico(current).size() >= 1) {
            JsfUtil.addErrorMessage("No se puede elimitar el plan estrategico, porque ya se han registrado evaluaciones para dicho plan.");
        } else {
        try {
            
            getFacade().remove(current);
            
            JsfUtil.addSuccessMessage("El plan estrategico con id: ".concat(identificador.concat(" se ha eliminado exitosamente")));
            audiController.guardarRegistroActividad("eliminó el plan estratégico "+current.getIdentificador());
            
            current=null;
            validadoHayPlanesEstrategicos=false;
            planVigente=null;
            listaPlanes=null;
            
        } catch (Exception e) {
             JsfUtil.addErrorMessage("Error al tratar de eliminar el plan estrategico con id: ".concat(identificador));
             } 
        }
    }
    
    
    
    
        /**
     *
     * @return
     */
    public String destroyFromView() {
            boolean ok=false;
        if (ejbEvaluaciones.obtenerEvaluacionesPorPlanEstrategico(current).size() >= 1) {
            JsfUtil.addErrorMessage("No se puede elimitar el plan estrategico, porque ya se han registrado evaluaciones para dicho plan.");
        } else {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage("El plan estrategico con id: ".concat(current.getIdentificador()).concat(" se ha eliminado exitosamente"));
            current=null; 
            ok=true;
            validadoHayPlanesEstrategicos=false;
            planVigente=null;
            listaPlanes=null;
            
           
        } catch (Exception e) {
             JsfUtil.addErrorMessage("Error al tratar de eliminar el plan estrategico con id: ".concat(current.getIdentificador()));
             
        } 
       
        }
       if(ok)
             return "PlanesEstrategicos";
       else
        return null;
    }

    /**
     *
     * @return
     */
    public List<PlanEstrategico> getListaPlanes() {
        listaPlanes=getFacade().findAll();
        return listaPlanes;
    }

    /**
     *
     * @param listaPlanes
     */
    public void setListaPlanes(List<PlanEstrategico> listaPlanes) {
        this.listaPlanes = listaPlanes;
    }
    
    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    /**
     * @return the valoresDeEscalas
     */
    public List<Double> getValoresDeEscalas() {
        return valoresDeEscalas;
    }

    /**
     * @param valoresDeEscalas the valoresDeEscalas to set
     */
    public void setValoresDeEscalas(List<Double> valoresDeEscalas) {
        this.valoresDeEscalas = valoresDeEscalas;
    }

    /**
     * @return the posicion
     */
    public int getPosicion() {
        return posicion;
    }

    /**
     * @param posicion the posicion to set
     */
    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }



    /**
     * @return the criteriosNuevosParaEdición
     */
    public ArrayList<Criterio> getCriteriosNuevosParaEdición() {
        if(criteriosNuevosParaEdición==null)
            criteriosNuevosParaEdición = new ArrayList<Criterio>();
        return criteriosNuevosParaEdición;
    }

    /**
     * @param criteriosNuevosParaEdición the criteriosNuevosParaEdición to set
     */
    public void setCriteriosNuevosParaEdición(ArrayList<Criterio> criteriosNuevosParaEdición) {
        this.criteriosNuevosParaEdición = criteriosNuevosParaEdición;
    }

    /**
     * @return the criteriosEliminados
     */
    public ArrayList<Criterio> getCriteriosEliminados() {
        if(criteriosEliminados == null)
            criteriosEliminados = new ArrayList<>();
        return criteriosEliminados;
    }

    /**
     * @param criteriosEliminados the criteriosEliminados to set
     */
    public void setCriteriosEliminados(ArrayList<Criterio> criteriosEliminados) {
        this.criteriosEliminados = criteriosEliminados;
    }

    /**
     * @return the planTemporal
     */
    public PlanEstrategico getPlanTemporal() {
        if(planTemporal == null)
            planTemporal = new PlanEstrategico();
        return planTemporal;
    }

    /**
     * @param planTemporal the planTemporal to set
     */
    public void setPlanTemporal(PlanEstrategico planTemporal) {
        this.planTemporal = planTemporal;
    }

    /**
     * @return the hayEvaluacionesParaElPlanAEditar
     */
    public boolean isHayEvaluacionesParaElPlanAEditar() {
        return hayEvaluacionesParaElPlanAEditar;
    }

    /**
     * @param hayEvaluacionesParaElPlanAEditar the hayEvaluacionesParaElPlanAEditar to set
     */
    public void setHayEvaluacionesParaElPlanAEditar(boolean hayEvaluacionesParaElPlanAEditar) {
        this.hayEvaluacionesParaElPlanAEditar = hayEvaluacionesParaElPlanAEditar;
    }

    /**
     * @return the escalaNoAPlica
     */
    public boolean isEscalaNoAPlica() {
        return escalaNoAPlica;
    }

    /**
     * @param escalaNoAPlica the escalaNoAPlica to set
     */
    public void setEscalaNoAPlica(boolean escalaNoAPlica) {
        this.escalaNoAPlica = escalaNoAPlica;
    }

    /**
     * @return the audiController
     */
    public AuditoriaController getAudiController() {
        return audiController;
    }

    /**
     * @param audiController the audiController to set
     */
    public void setAudiController(AuditoriaController audiController) {
        this.audiController = audiController;
    }

    /**
     *
     */
    @FacesConverter(forClass = PlanEstrategico.class)
    public static class PlanEstrategicoControllerConverter implements Converter {

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null) {
                return null;
            }
            PlanEstrategicoController controller = (PlanEstrategicoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "planEstrategicoController");
            return controller.ejbFacade.find(getKey(value));
        }

      
        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof PlanEstrategico) {
                PlanEstrategico o = (PlanEstrategico) object;
                return getStringKey(o.getIdPlanEstrategico());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + PlanEstrategico.class.getName());
            }
        }                    
    }
    
    
    
}
