package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.CentroDeCostos;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.CentroDeCostosFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import uca.sed360.jpaEntities.Empleados;
import uca.sed360.jpaEntities.Periodo;


/**
 * 
 * @author Estacion 2
 */
@ManagedBean(name = "centroDeCostosController")
@SessionScoped
public class CentroDeCostosController implements Serializable {

    @ManagedProperty(value = "#{auditoriaController}")
    private AuditoriaController audiController;
    
    
    @EJB
    private uca.sed360.ejbs.CentroDeCostosFacade ejbFacade;
    @EJB
    private uca.sed360.ejbs.PeriodoFacade ejbPeriodo;
    
    private CentroDeCostos current;
    private DataModel items = null;
    private List<CentroDeCostos> listaCentroDeCostos;
    private List<CentroDeCostos> listaCentroDeCostosFiltrados;
    private List<CentroDeCostos> centrosConEvaluaciones;
    private int selectedItemIndex;
    
    

    /**
     * Constructor
     */
    public CentroDeCostosController() {
    }
    
    /**
     * 
     * @return
     */
    public CentroDeCostos getSelected() {
        if (current == null) {
            current = new CentroDeCostos();
            selectedItemIndex = -1;
        }
        return current;
    }
    
    /**
     *
     * @param centro
     */
    public void setSelected(CentroDeCostos centro)
    {
        current = centro;
    }

    private CentroDeCostosFacade getFacade() {
        return ejbFacade;
    }

    
    //Getter and Setter Centro de Costos List
    /**
     * Obtiene la lista de centros de costo
     * @return
     */
    public List<CentroDeCostos> getListaCentroDeCostos() {
        listaCentroDeCostos = ejbFacade.findAll();
        Collections.sort(listaCentroDeCostos, new Comparator<CentroDeCostos>(){
            @Override
            public int compare(CentroDeCostos c1, CentroDeCostos c2){
                return c1.getDescripcion().compareTo(c2.getDescripcion());
            }
        });
        return listaCentroDeCostos;
    }

    /**
     *
     * @param listaCentroDeCostos
     */
    public void setListaCentroDeCostos(List<CentroDeCostos> listaCentroDeCostos) {
        this.listaCentroDeCostos = listaCentroDeCostos;
    }
    
    /**
     * Verifica si existen centros de costo
     * @return verdadero o falso
     */
    public boolean hayCentroDeCostos() {
        if(ejbFacade.findAll().size() >= 1 ) {
        return true;

        } else
        {
            return false;
        }   
    
    }
    
    /**
     *
     * @return
     */
    public Periodo getPeriodo(){
        return ejbPeriodo.getPeriodoVigente();
    }
    
    /**
     *
     */
    public void prepararVista()
    {
        current = null;
    }


    /**
     *
     * @return
     */
    public String prepareList() {
        return "List";
    }

    /**
     *
     * @return
     */
    public String prepareView() {
        return "View";
    }

    /**
     *
     * @return
     */
    public String prepareCreate() {
        current = new CentroDeCostos();
        selectedItemIndex = -1;
        return "Create";
    }

    
    
    
    /**
     *
     * @return
     */
    public String create() {
        try {
            
            
            if(getFacade().obtenerPorId(current) != null)
            {
                JsfUtil.addErrorMessage("Ya existe un centro de costos con el id que intenta asignar.");
                return null;
            }
                            
            
            getFacade().create(current);
            JsfUtil.addSuccessMessage("Centro de costos creado exitosamente");
            audiController.guardarRegistroActividad(" creo el centro de costos "+current.getDescripcion());
            current = new CentroDeCostos();
            return "null";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e,"Error al registrar el centro de costos");
            return null;
        }
    }

    
    
    
    /**
     *
     * @return
     */
    public String prepareEdit() {
        return "Edit";
    }

    /**
     *
     * @return
     */
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage("el centro de costos con id "+current.getIdCentrocostos()+" ha sido actualizado correctamente");
            current = new CentroDeCostos();
            return null;
        } catch (Exception e) {
            JsfUtil.addErrorMessage("el centro de costos con id "+current.getIdCentrocostos()+ " no se pudo actualizar. Si el error persiste"
                    + " póngase en contacto con el área de informática.");
            current = new CentroDeCostos();
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String destroy() {
        performDestroy();
        return null;
    }

    /**
     *
     * @return
     */
    public String destroyAndView() {
        performDestroy();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            return "List";
        }
    }

    /**
     * Verifica si existen evaluaciones para un centro de costo
     * @param centro
     * @return verdadero o falso
     */
    public boolean existenEvaluacions_para_centro(CentroDeCostos centro)
    {       
        return getFacade().existenEvaluaciones_para_centro(centro);        
    }
    
    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage("Se ha eliminado el centro de costos "+current.getDescripcion());
            audiController.guardarRegistroActividad(" elimino el centro de costos "+current.getDescripcion());
            current = null;
        } catch (Exception e) {
            JsfUtil.addErrorMessage("El centro de costos con id "+current.getIdCentrocostos()+" no se pudo eliminar. Si el error persiste"
                    + "por favor póngase en contacto con el área de informática.");
            current = null;
        }
    }

    
    /**
     * Crea y retorna una lista de objetos SelectItem, cada uno asociado a un
     * CentroDeCostos. Esta lista es usada en elementos como el 
     * <code>"p:selectOneMenu"</code> para que los CentroDeCostos sean mostrados, seleccionados
     * y filtrados.
     * @param lista
     * @return lista filtrada de centros de costo
     */
    public SelectItem[] crearFiltroCentros(List<CentroDeCostos> lista){
        return JsfUtil.getSelectItems(lista, true, "Todos los centros de costos");
    }
    
    /**
     * Obtiene los centros de costo donde
     * existen evaluaciones registradas.
     * @return lista
     */
    public List<CentroDeCostos> getCentrosConEvaluaciones() {
        boolean hayEvaluacion = false;
        centrosConEvaluaciones = new ArrayList<CentroDeCostos> ();
                for(CentroDeCostos c : getListaCentroDeCostos()){
                for(Empleados e : c.getEmpleadosCollection()){
                    if(!(e.getEvaluacionCollection1().isEmpty())){
                        hayEvaluacion = true;
                    }
                }
                if(hayEvaluacion){
                centrosConEvaluaciones.add(c);
                hayEvaluacion = false;
                }
            }
        return centrosConEvaluaciones;
    }

    /**
     * @param centrosConEvaluaciones the centrosConEvaluaciones to set
     */
    public void setCentrosConEvaluaciones(List<CentroDeCostos> centrosConEvaluaciones) {
        this.centrosConEvaluaciones = centrosConEvaluaciones;
    }

    /**
     * @return the audiController
     */
    public AuditoriaController getAudiController() {
        return audiController;
    }

    /**
     * @param audiController the audiController to set
     */
    public void setAudiController(AuditoriaController audiController) {
        this.audiController = audiController;
    }

    /**
     * @return the listaCentroDeCostosFiltrados
     */
    public ArrayList<CentroDeCostos> getListaCentroDeCostosFiltrados() {
        
                    
        return (ArrayList<CentroDeCostos>) listaCentroDeCostosFiltrados;
    }

    /**
     * @param listaCentroDeCostosFiltrados the listaCentroDeCostosFiltrados to set
     */
    public void setListaCentroDeCostosFiltrados(List<CentroDeCostos> listaCentroDeCostosFiltrados) {
        this.listaCentroDeCostosFiltrados = listaCentroDeCostosFiltrados;
    }
    
     /**
     *
     */
    @FacesConverter(value = "centroCostosControllerConverter",forClass = CentroDeCostos.class)
    public static class CentroCostosControllerConverter implements Converter {

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CentroDeCostosController controller = (CentroDeCostosController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "centroDeCostosController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CentroDeCostos) {
                CentroDeCostos o = (CentroDeCostos) object;
                return getStringKey(o.getIdCentrocostos());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + CentroDeCostos.class.getName());
            }
        }
    }
   
}