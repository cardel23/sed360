package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.PreguntaAbierta;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.PreguntaAbiertaFacade;
import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;

/**
 * Controlador que maneja las preguntas abiertas de las evaluaciones
 * @author Estacion 2
 */
@ManagedBean(name = "preguntaAbiertaController")
@SessionScoped
public class PreguntaAbiertaController implements Serializable {

    private PreguntaAbierta current;
    @EJB
    private uca.sed360.ejbs.PreguntaAbiertaFacade ejbFacade;


    /**
     * Constructor
     */
    public PreguntaAbiertaController() {
    }

    /**
     *
     * @return
     */
    public PreguntaAbierta getSelected() {
        if (current == null) {
            current = new PreguntaAbierta();
        }
        return current;
    }

    private PreguntaAbiertaFacade getFacade() {
        return ejbFacade;
    }

   
    /**
     *
     * @return
     */
    public String prepareList() {
        return "List";
    }

    /**
     *
     * @return
     */
    public String prepareView() {
        return "View";
    }

    /**
     *
     * @return
     */
    public String prepareCreate() {
        current = new PreguntaAbierta();
        return "Create";
    }

    /**
     *
     * @return
     */
    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PreguntaAbiertaCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String prepareEdit() {
        return "Edit";
    }

    /**
     *
     * @return
     */
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PreguntaAbiertaUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String destroy() {
        performDestroy();
        return "List";
    }


    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PreguntaAbiertaDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }


    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    /**
     *
     */
    @FacesConverter(forClass = PreguntaAbierta.class)
    public static class PreguntaAbiertaControllerConverter implements Converter {

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PreguntaAbiertaController controller = (PreguntaAbiertaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "preguntaAbiertaController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof PreguntaAbierta) {
                PreguntaAbierta o = (PreguntaAbierta) object;
                return getStringKey(o.getIdPregunta());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + PreguntaAbierta.class.getName());
            }
        }
    }
}
