package uca.sed360.managedBeans;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import uca.sed360.jpaEntities.TipoEmpleado;

/**
 * Controlador que maneja los distintos tipos de empleado que existen en el
 * sistema
 * @author Estacion 2
 */
@ManagedBean(name = "tiposEmpleadoController")
@SessionScoped
public class TiposEmpleadoController implements Serializable {

    private TipoEmpleado current;
    @EJB
    private uca.sed360.ejbs.TiposEmpleadoFacade ejbFacade;
    private List<TipoEmpleado> listaTiposEmpleado;   
      
    /**
     * Constructor
     */
    public TiposEmpleadoController() {
        
    }

    /**
     *
     * @return
     */
    public TipoEmpleado getCurrent() {
        if(current==null)
           current=new TipoEmpleado();
        return current;
    }

    /**
     *
     * @param current
     */
    public void setCurrent(TipoEmpleado current) {
        this.current = current;
    }

    /**
     *
     * @return
     */
    public List<TipoEmpleado> getListaTiposEmpleado() {           
        listaTiposEmpleado=ejbFacade.findAll();
        return listaTiposEmpleado;
    }

    /**
     *
     * @param listaTiposEmpleado
     */
    public void setListaTiposEmpleado(List<TipoEmpleado> listaTiposEmpleado) {
        this.listaTiposEmpleado = listaTiposEmpleado;
    }
    
   
    /**
     *
     */
    @FacesConverter(value = "tiposEmpleadoControllerConverter",forClass = TipoEmpleado.class)
    public static class TiposEmpleadoControllerConverter implements Converter {

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TiposEmpleadoController controller = (TiposEmpleadoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "tiposEmpleadoController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof TipoEmpleado) {
                TipoEmpleado o = (TipoEmpleado) object;
                return getStringKey(o.getIdTipoempleado());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + TipoEmpleado.class.getName());
            }
        }
    }
}
