package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.Criterio;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.CriterioFacade;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;

/**
 * Controlador que maneja los criterios de evaluación
 * @author Estacion 2
 */
@ManagedBean(name = "criterioController")
@SessionScoped
public class CriterioController implements Serializable {

    private Criterio current;
    private DataModel items = null;
    @EJB
    private uca.sed360.ejbs.CriterioFacade ejbFacade;
    @EJB
    private uca.sed360.ejbs.PlanEstrategicoFacade planFacade;
    @EJB
    private uca.sed360.ejbs.PreguntaCerradaFacade ejbPreguntaC;

    /**
     *
     */
    public CriterioController() {
    }

    //Getter y setter de Plan
    
    
    
    
    /**
     *
     * @return
     */
    public Criterio getCurrent() {
        if (current == null) {
            current = new Criterio();
        }
        return current;
    }
    
    /**
     *
     * @param criterio
     */
    public void setCurrent(Criterio criterio){
        current=criterio;
    }

    private CriterioFacade getFacade() {
        return ejbFacade;
    }


    /**
     *
     * @return
     */
    public String prepareList() {
        return "List";
    }

    /**
     *
     * @return
     */
    public String prepareView() {
        return "View";
    }

    /**
     *
     * @return
     */
    public String prepareCreate() {
        current = new Criterio();
        return "Create";
    }

    /**
     *
     * @return
     */
    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("CriterioCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    
    

    /**
     *
     * @return
     */
    public String prepareEdit() {
        return "Edit";
    }

    /**
     *
     * @return
     */
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("CriterioUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String destroy() {
        performDestroy();
        return "List";
    }


    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("CriterioDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

  
    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    /**
     *
     */
    @FacesConverter(forClass = Criterio.class)
    public static class CriterioControllerConverter implements Converter {

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CriterioController controller = (CriterioController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "criterioController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Criterio) {
                Criterio o = (Criterio) object;
                return getStringKey(o.getIdCriterio());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Criterio.class.getName());
            }
        }
    }
}
