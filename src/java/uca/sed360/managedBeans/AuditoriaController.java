package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.Auditoria;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.AuditoriaFacade;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ManagedProperty;
/**
 * Controlador que maneja el registro de los eventos generados por un usuario
 * del sistema SED360
 * @author Estacion 2
 */
@ManagedBean(name = "auditoriaController")
@SessionScoped
public class AuditoriaController implements Serializable {

    private Auditoria current;
    @EJB
    private uca.sed360.ejbs.AuditoriaFacade ejbFacade;
    
    @ManagedProperty(value="#{sessionMB}")
    private SessionMB sessionMB;
    
    private static final DateFormat FORMATO = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    /**
     *
     */
    public AuditoriaController() {
    }

    
    
    /**
     * Almacena en la base de datos un evento
     * @param descripcion
     */
    public void guardarRegistroActividad(String descripcion)
    {       
                
        //obtiene la sesion http activa
//        HttpSession sesion = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);        
        
        //Prepara la descripcion
        String evento = "El usuario "+ sessionMB.getUsuario()+descripcion;
        
        //Crea y obtiene la fecha con el formato
        Date fecha = new Date();
        FORMATO.format(fecha);
        
        
        current = new Auditoria();
        current.setEvento(descripcion);
        current.setIdUsuario(sessionMB.getCurrentUser());
        current.setFechaEvento(fecha);
        
        create();
        
        current = null;
        
    }
    
    
    /**
     *
     * @return
     */
    public Auditoria getSelected() {
        if (current == null) {
            current = new Auditoria();
        }
        return current;
    }

    private AuditoriaFacade getFacade() {
        return ejbFacade;
    }


    /**
     *
     * @return
     */
    public String prepareList() {
        return "List";
    }

    /**
     *
     * @return
     */
    public String prepareView() {
        return "View";
    }

    /**
     *
     * @return
     */
    public String prepareCreate() {
        current = new Auditoria();
        return "Create";
    }

    /**
     *
     * @return
     */
    public String create() {
        try {
            getFacade().create(current);            
            return prepareCreate();
        } catch (Exception e) {            
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String prepareEdit() {
        return "Edit";
    }

    /**
     *
     * @return
     */
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("AuditoriaUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String destroy() {
        performDestroy();
        return "List";
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("AuditoriaDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    public SessionMB getSessionMB() {
        return sessionMB;
    }

    public void setSessionMB(SessionMB sessionMB) {
        this.sessionMB = sessionMB;
    }
 
}
