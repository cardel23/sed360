/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans;

import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * Controlador auxiliar para la sesión de usuario
 * @author Jose
 */
@ManagedBean
@SessionScoped
public class UtilMB {

    private int indexMenu=0;
    private boolean esNica = true;
    /**
     * Creates a new instance of UtilMB
     */
    public UtilMB() {
    }
    
    /**
     * 
     * @return
     */
    public Date getToday(){
        return new Date();
    }
    

    /**
     *
     * @return
     */
    public int getIndexMenu() {
        return indexMenu;
    }

    /**
     *
     * @param indexMenu
     */
    public void setIndexMenu(int indexMenu) {
        this.indexMenu = indexMenu;
    }

    /**
     *
     * @return
     */
    public boolean isEsNica() {
        return esNica;
    }

    /**
     *
     * @param esNica
     */
    public void setEsNica(boolean esNica) {
        this.esNica = esNica;
    }
    
    
    
    }

    
