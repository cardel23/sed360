package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.Roles;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.RolesFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;

/**
 * Controlador que maneja los roles o permisos del usuario en el sistema SED360
 * @author Estacion 2
 */
@ManagedBean(name = "rolesController")
@SessionScoped
public class RolesController implements Serializable {

    private Roles current;
    private List<Roles> listaRoles,listaRolesParaNoEmpleados,listaRolesParaEmpleados;
    private boolean validadoRoles=false;
    @EJB
    private uca.sed360.ejbs.RolesFacade ejbFacade;

    /**
     * Constructor
     */
    public RolesController() {
    }

    /**
     * 
     * @return
     */
    public Roles getSelected() {
        if (current == null) {
        }
        return current;
    }

    private RolesFacade getFacade() {
        return ejbFacade;
    }

    /**
     *
     * @return
     */
    public List<Roles> getListaRoles() {
        if(listaRoles==null)
            listaRoles=getFacade().findAll();
        return listaRoles;
    }

    /**
     *
     * @param listaRoles
     */
    public void setListaRoles(List<Roles> listaRoles) {
        this.listaRoles = listaRoles;
    }
    
    /**
     *
     */
    public void validarRoles(){
        if(!validadoRoles){
            getFacade().validarRolesEnBD();
            validadoRoles=true;
        }
    }

    /**
     *
     * @return
     */
    public List<Roles> getListaRolesParaEmpleados() {
         if(listaRolesParaEmpleados==null){
              listaRolesParaEmpleados=new ArrayList<Roles>();
              listaRolesParaEmpleados.add(getFacade().find(Roles.ROL_COLABORADOR));
        }
        return listaRolesParaEmpleados;
    }

    /**
     *
     * @param listaRolesParaEmpleados
     */
    public void setListaRolesParaEmpleados(List<Roles> listaRolesParaEmpleados) {
        this.listaRolesParaEmpleados = listaRolesParaEmpleados;
    }
    
    

    /**
     *
     * @return
     */
    public List<Roles> getListaRolesParaNoEmpleados() {
        if(listaRolesParaNoEmpleados==null){
              listaRolesParaNoEmpleados=new ArrayList<Roles>();
              listaRolesParaNoEmpleados.add(getFacade().find(Roles.ROL_CONSULTOR));
              listaRolesParaNoEmpleados.add(getFacade().find(Roles.ROL_ADMINISTRADOR));
        }
        return listaRolesParaNoEmpleados;
    }

    /**
     *
     * @param listaRolesForNonEmployee
     */
    public void setListaRolesParaNoEmpleados(List<Roles> listaRolesForNonEmployee) {
        this.listaRolesParaNoEmpleados = listaRolesForNonEmployee;
    }
    
    
    /**
     *
     * @return
     */
    public String prepareList() {
        return "List";
    }

    /**
     *
     * @return
     */
    public String prepareView() {
        return "View";
    }

    /**
     *
     * @return
     */
    public String prepareCreate() {
        current = new Roles();
        return "Create";
    }
    
    /**
     *
     * @return
     */
    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RolesCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String prepareEdit() {
        return "Edit";
    }

    /**
     *
     * @return
     */
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RolesUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String destroy() {
        performDestroy();
        return "List";
    }


    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RolesDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }


   
    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }
    
    
    
    
    
    
    
    
    /* 
     * Convertidor de la clase Rol. Este "Faces Converter permitira hacer la
     * conversión de un objeto a su representación cadena y viceversa cuando
     * se trabaja con objetos de tipo "Roles"
     */    
    
    /**
     *
     */
    @FacesConverter(forClass = Roles.class,value = "rolesConverter")
    public static class RolesControllerConverter implements Converter {

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            RolesController controller = (RolesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "rolesController");
            return controller.ejbFacade.find(getKey(value));
        }       
        
        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }
      
        /*Este método se asegura de que 
         el id del objeto de tipo Roles sea convertido a un 
         string para representar al objeto*/
        String getStringKey(java.lang.Integer value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }
        

        /*
        Este método regresa el valor del id del objeto como una cadena de 
        * texto para representar a dicho objeto
        */       
        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Roles) {
                Roles o = (Roles) object;
                return getStringKey(o.getIdRol());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Roles.class.getName());
            }
        }
    }
}
