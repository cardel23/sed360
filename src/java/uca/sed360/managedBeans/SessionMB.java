/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.managedBeans;


import java.io.IOException;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.jpaEntities.Usuarios;
import java.io.Serializable;
import javax.ejb.EJB;
import uca.sed360.ejbs.UsuariosFacade;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import uca.sed360.managedBeans.util.reportes.ReportesMB;



/**
 * Controlador que maneja los usuarios del sistema SED360
 * @author CT.INF.01
 */
@ManagedBean(name = "sessionMB")
@SessionScoped

public class SessionMB implements Serializable{
    private String usuario,pass;  
    private boolean esEmpleado;
    @ManagedProperty(value="#{utilMB.esNica}")
    private boolean esNica;
    @ManagedProperty(value="#{reporteSMB}")
    private ReportesMB reporteMB;
   
    @EJB
    private UsuariosFacade ejbUsuario;
   
    
    /**
     * Constructor
     */
    public SessionMB() {
    }
   
    /**
     *
     * @return
     */
    public String getUsuario() {
        return usuario;
    }
    
    //Getter and Setter para esNica()
    /**
     *
     * @return
     */
    public boolean isEsNica() {
        return esNica;
    }

    /**
     *
     * @param esNica
     */
    public void setEsNica(boolean esNica) {
        this.esNica = esNica;
    }

    /**
     *
     * @return
     */
    public ReportesMB getReporteMB() {
        return reporteMB;
    }

    /**
     *
     * @param reporteMB
     */
    public void setReporteMB(ReportesMB reporteMB) {
        this.reporteMB = reporteMB;
    }
        
    /**
     * Elimina los guiones de la máscara de cédula para que coincidan con los
     * registros de la BD
     * @param usuario
     */
    public void setUsuario(String usuario) {
        if(esNica) {
            String replace = usuario.replace("-", "");
        this.usuario = replace;
        } else
        this.usuario = usuario;
    }

    /**
     *
     * @return
     */
    public String getPass() {
        return pass;
    }

    /**
     *
     * @param pass
     */
    public void setPass(String pass) {
        this.pass = pass;
    }
    
    /**
     * Recorta el nombre del usuario para que aparezca en pantalla
     * @return el primer nombre y el primer apellido del empleado 
     * que inicia sesión
     */
    public String getNombreCorto(){
        if(getCurrentUser()==null)
            return null;
        if(getCurrentUser().getEmpleados()==null)
            return getCurrentUser().getIdUsuario();
        else{
            String nombreCorto=getCurrentUser().getEmpleados().getNombre().split(" ")[0];
            nombreCorto=nombreCorto.concat(" ").concat(getCurrentUser().getEmpleados().getApellido().split(" ")[0]);
            return nombreCorto;
        
        }
    }
   
    /**
     * Valida que el usuario exista y tenga un empleado y/o rol
     * de colaborador, consultor o administrador para iniciar sesión en el sistema
     * Si se produce un fallo en la validación de los datos del usuario o
     * no se encuentra activo, mostrará un mensaje de error. 
     * Si el usuario no tiene permisos, redirige a error 404
     * @return el URL de la página a la que será redirigido el usuario. 
     * <br/>
     * Ejemplo:<br/><br/>
     * <b>Colaborador:</b> es el usuario más común del sistema, será redirigido
     * a "/evaluacion/SeleccionarEvaluacion?faces-redirect=true" para iniciar
     * las evaluaciones<br/><br/>
     * <b>Consultor:</b> solamente tiene permiso para consultar reportes de 
     * evaluación de un área específica "/Reportes/ReporteConsultor?faces-redirect=true"<br/><br/>
     * <b>Administrador:</b> administrador del sistema "/Index?faces-redirect=true"
     */
    public String login(){           
        Usuarios user=ejbUsuario.find(usuario);
        
        /**
         * Validando si existe el usuario
         */
        
        if(user==null){ 
               JsfUtil.addErrorMessage("El nombre de usuario no existe\nIntente nuevamente");
               return null;             
        }
        else {
            /**
             * Validando que la contraseña ingresada sea correcta
             */                  
            if(!pass.contentEquals(user.getClave())){
                   JsfUtil.addErrorMessage("Contaseña incorrecta\nIntente nuevamente");             
                   return null;    
              }
              else{
                    /**
                     * Validando que en caso que se trate de un usuario con rol de colaborador, sea también
                     * un empleado registrado
                     *
                     */
                      
                    if(ejbUsuario.isColaborador(user) && !ejbUsuario.isEmpleado(user)){
                        JsfUtil.addErrorMessage("Lo sentimos\nActualmente su usuario no esta asignado a un empleado\nContacte al administrador del sistema");             
                        return null; 
                    }

                    else {

                                if(ejbUsuario.isEmpleado(user) && user.getEmpleados().getActivo() == false){
                                            JsfUtil.addErrorMessage("Lo sentimos\nActualmente su usuario se encuentra inactivo\nContacte al administrador del sistema");             
                                            return null; 
                                }
                                else{
                                
                                HttpSession session=(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                                session.setAttribute("currentUser", user);
                               //validar si es empleado para mantener la variable 'esEmpleado' en memoria
                                if(user.getEmpleados()!=null) {
                                    esEmpleado = true;
                                } else {
                                    esEmpleado = false;
                                }
                                if(ejbUsuario.isAdmin(user)) 
                                    return "/Index?faces-redirect=true";
                                 
                                else 
                                    if(ejbUsuario.isConsultor(user))
                                        return "/Reportes/ReporteConsultor?faces-redirect=true";
                                    
                                    else
                                        if(ejbUsuario.isColaborador(user)) 
                                            return "/evaluacion/SeleccionarEvaluacion?faces-redirect=true";
                                        
                                        else
                                            return "ErrorAutorizacion?faces-redirect=true";
                                        
                            }
                      } 
                   }
              }        
    }
    
       
    /**
     * Valida si el usuario es empleado
     * @return verdadero o falso
     */
    public boolean isEsEmpleado() {
        return esEmpleado;
    }
    
    /**
     * Obtiene el nombre del empleado
     * @return el nombre si es empleado, si no es empleado, el id de usuario
     */
    public String obtenerNombre() {
        if(isEsEmpleado()) {
            return getCurrentUser().getEmpleados().getNombre().concat(" ").concat(getCurrentUser().getEmpleados().getApellido());
        } else {
            return getCurrentUser().getIdUsuario();
        }
    }

    /**
     *
     * @param esEmpleado
     */
    public void setEsEmpleado(boolean esEmpleado) {
        this.esEmpleado = esEmpleado;
    }
    
    /**
     * Obtiene el usuario de la sesión actual
     * @return
     */
    public Usuarios getCurrentUser(){
    HttpSession session=(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    Usuarios user=(Usuarios) session.getAttribute("currentUser");
    return user;
    }
    
    /**
     * Cierra la sesión del usuario
     * @return la URL para el inicio de sesión
     */
    public String logout(){
        ejbUsuario.limpiarCache();
        HttpSession session=(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        session.invalidate();   
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/Login?faces-redirect=true";
    } 
    
    /**
     * Valida que haya un usuario autenticado en sesión activa para la navegación
     * dentro del sistema
     */
    public void validarUsuarioAutenticado() {
       FacesContext context = FacesContext.getCurrentInstance();
       HttpServletResponse response =(HttpServletResponse)context.getExternalContext().getResponse();
       try{ 
            if(getCurrentUser()!=null){
                 if(ejbUsuario.isAdmin(getCurrentUser()))
                     response.sendRedirect("/Index.xhtml");
                 else if (ejbUsuario.isConsultor(getCurrentUser())){
                     reporteMB.setRutaReporte("WEB-INF/FragmentosReportes/_InicioReporte.xhtml");
                     response.sendRedirect("/Reportes/ReporteConsultor.xhtml");
                 }
                 else if(ejbUsuario.isColaborador(getCurrentUser()))
                     response.sendRedirect("/evaluacion/SeleccionarEvaluacion.xhtml");
             }
       }
       catch(Exception e){
           System.out.println(e);
       }
    }
    
    /**
     * Valida que el usuario tenga permisos para hacer uso del sistema
     * @throws IOException
     */
    public void validarPermisosUsuario() throws IOException{
         FacesContext context = FacesContext.getCurrentInstance();
         HttpServletResponse response =(HttpServletResponse)context.getExternalContext().getResponse();
         Usuarios user=getCurrentUser();
         if(!ejbUsuario.isAdmin(user)){
             if(ejbUsuario.isColaborador(user)) {                
                 response.sendRedirect("/evaluacion/SeleccionarEvaluacion.xhtml");
               }
             else{
                 response.sendError(HttpServletResponse.SC_NON_AUTHORITATIVE_INFORMATION);
             }
         }
    }
    
    /**
     * Verifica que el usuario actual en la sesión sea administrador
     * @return verdadero o falso
     */
    public boolean isCurrentUserAdmin(){
        return ejbUsuario.isAdmin(getCurrentUser());
    }

}
