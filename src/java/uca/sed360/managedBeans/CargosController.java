package uca.sed360.managedBeans;

import uca.sed360.managedBeans.util.JsfUtil;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;
import uca.sed360.jpaEntities.Cargos;

/**
 * Controlador que maneja el cargo de los empleados
 * @author Estacion 2
 */
@ManagedBean(name = "cargosController")
@SessionScoped
public class CargosController implements Serializable {

    private Cargos current;
    @EJB
    private uca.sed360.ejbs.CargosFacade ejbFacade;
    private List<Cargos> listaCargos;   
      
    /**
     * Constructor
     */
    public CargosController() {
        
    }

    /**
     *
     * @return
     */
    public Cargos getCurrent() {
        if(current==null)
            current=new Cargos();
        return current;
    }

    /**
     *
     * @param current
     */
    public void setCurrent(Cargos current) {       
        this.current = current;
    }

    /**
     * Obtiene la lista de cargos de empleado
     * @return lista
     */
    public List<Cargos> getListaCargos() {
        listaCargos=ejbFacade.findAll();
        Collections.sort(listaCargos,new Comparator<Cargos>(){
            @Override
            public int compare(Cargos p1, Cargos p2){
                return p1.getDescripcion().compareTo(p2.getDescripcion());
            }
        });
        return listaCargos;
    }

    /**
     *
     * @param listaCargos
     */
    public void setListaCargos(List<Cargos> listaCargos) {
        this.listaCargos = listaCargos;
    }
    
    /**
     * Crea un filtro para mostrar en una tabla
     * @param lista
     * @return
     */
    public SelectItem[] crearFiltroCargos(List<Cargos> lista){
        return JsfUtil.getSelectItems(lista, true, "Todos los cargos");
    }
    /**
     *
     */
    @FacesConverter(value = "cargosControllerConverter",forClass = Cargos.class)
    public static class InstrumentosControllerConverter implements Converter {

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CargosController controller = (CargosController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cargosController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Cargos) {
                Cargos o = (Cargos) object;
                return getStringKey(o.getIdCargo());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Cargos.class.getName());
            }
        }
    }
}
