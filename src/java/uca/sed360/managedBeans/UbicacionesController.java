package uca.sed360.managedBeans;

import uca.sed360.managedBeans.util.JsfUtil;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import uca.sed360.jpaEntities.Ubicaciones;

/**
 * Controlador que maneja las ubicaciones donde desempeñan labores los empleados
 * @author Estacion 2
 */
@ManagedBean(name = "ubicacionesController")
@SessionScoped
public class UbicacionesController implements Serializable {

    private Ubicaciones current;
    @EJB
    private uca.sed360.ejbs.UbicacionesFacade ejbFacade;
    private List<Ubicaciones> listaUbicaciones;
    private String oldDesc;
      
    /**
     * Constructor
     */
    public UbicacionesController() {
        
    }
     /**
     *
     * @return
     */
    public Ubicaciones getCurrent() {
        if(current==null)
            current=new Ubicaciones();
        return current;
    }

    /**
     *
     * @param current
     */
    public void setCurrent(Ubicaciones current) {     
        oldDesc=current.getDescripcion();
        this.current = current;
    }

    /**
     *
     * @return
     */
    public List<Ubicaciones> getListaUbicaciones() {
        if(listaUbicaciones==null)
            listaUbicaciones=ejbFacade.findAll();
        return listaUbicaciones;
    }

    /**
     *
     * @param listaUbicaciones
     */
    public void setListaUbicaciones(List<Ubicaciones> listaUbicaciones) {
        this.listaUbicaciones = listaUbicaciones;
    }
  
    /**
     *
     */
    public void createUbicacion(){
        try {
            if(ejbFacade.findByDesc(current.getDescripcion())==null){
                ejbFacade.create(current);
                JsfUtil.addSuccessMessage("La nuevo ubicación  '".concat(current.getDescripcion()).concat("' ha sido registrado"));
                current=null;
                listaUbicaciones=ejbFacade.findAll();
            }
            else{
               JsfUtil.addErrorMessage("La ubicación ".concat(current.getDescripcion()).concat("' ya ha sido registrado")); 
            }
           
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Lo sentimos, no pudo registrarse la nuevo ubicación");
        }
        
    }
    
    /**
     *
     */
    public void update(){
        try {
            if(ejbFacade.findByDesc(current.getDescripcion())==null){
                ejbFacade.edit(current);                
                JsfUtil.addSuccessMessage("La ubicación : '".concat(oldDesc).concat("' ha sido actualizado a '").concat(current.getDescripcion()).concat("'"));
                current=null;                
             }
            else{
               JsfUtil.addErrorMessage("La ubicación '".concat(current.getDescripcion()).concat("' ya ha sido registrado")); 
               current=null;
            }
            listaUbicaciones=ejbFacade.findAll();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Lo sentimos, no pudo actualizarse la  ubicación ".concat(current.getDescripcion()));
            System.out.println(e);
        }
        
    }
    
    /**
     *
     */
    public void destroy(){
        try {
             ejbFacade.remove(current);
             JsfUtil.addSuccessMessage("La  ubicación  '".concat(current.getDescripcion()).concat("' ha sido eliminado"));
             current=null;
             listaUbicaciones=ejbFacade.findAll();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Lo sentimos, no es posible eliminar la  ubicación ".concat(current.getDescripcion()));
        }
    }
   
    /**
     *
     */
    @FacesConverter(value = "ubicacionesControllerConverter",forClass = Ubicaciones.class)
    public static class UbicacionesControllerConverter implements Converter {

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            UbicacionesController controller = (UbicacionesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "ubicacionesController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Ubicaciones) {
                Ubicaciones o = (Ubicaciones) object;
                return getStringKey(o.getIdUbicacion());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Ubicaciones.class.getName());
            }
        }
    }
}
