package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.Periodo;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.PeriodoFacade;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import uca.sed360.jpaEntities.AsignacionMetodologia;
import uca.sed360.jpaEntities.AsignacionMetodologiaPK;
import uca.sed360.jpaEntities.CatalogoMetodologias;
import uca.sed360.jpaEntities.Niveles;
import uca.sed360.jpaEntities.PlanEstrategico;
import uca.sed360.managedBeans.util.SeguimientoController;
import uca.sed360.managedBeans.util.reportes.ReportesUtil;

/**
 * Controlador que maneja los periodos de evaluación
 * @author Estacion 2
 */
@ManagedBean(name = "periodoController")
@SessionScoped
public class PeriodoController implements Serializable {
    
    @ManagedProperty(value="#{planEstrategicoController}")
    private PlanEstrategicoController planMB;
    
    @ManagedProperty(value="#{nivelesController}")
    private NivelesController nivelesMB;
    
    @ManagedProperty(value="#{seguimientoController}")
    private SeguimientoController seguimientoController;
    
    @ManagedProperty(value = "#{auditoriaController}")
    private AuditoriaController audiController;
    
    private Periodo current;
    /**
     *
     */
    public  List<Periodo> listaPeriodos;
    private Periodo periodoVigente;
    private boolean currentHasEvaluaciones;
    private String metodologia;
    private boolean renderDefPanel;
    private boolean hayPeriodoVigente;
    private List<AsignacionMetodologia> listaAsignacionesMetodologia;
    private List<String> listaMetodologiasCombo,listaMetodologiasOneButton;
    ReportesUtil ru = new ReportesUtil();
   
    @EJB
    private uca.sed360.ejbs.PeriodoFacade ejbFacade;   
    @EJB
    private uca.sed360.ejbs.EvaluacionFacade ejbEvaluaciones;
              
    
    /**
     * Constructor
     */
    public PeriodoController() {
    }

    /**
     *
     * @return
     */
    public PlanEstrategicoController getPlanMB() {
        return planMB;
    }

    /**
     *
     * @param planMB
     */
    public void setPlanMB(PlanEstrategicoController planMB) {
        this.planMB = planMB;
    }

    /**
     *
     * @return
     */
    public NivelesController getNivelesMB() {
        return nivelesMB;
    }

    /**
     *
     * @param nivelesMB
     */
    public void setNivelesMB(NivelesController nivelesMB) {
        this.nivelesMB = nivelesMB;
    }

    /**
     *
     * @return
     */
    public SeguimientoController getSeguimientoController() {
        return seguimientoController;
    }

    /**
     *
     * @param seguimientoController
     */
    public void setSeguimientoController(SeguimientoController seguimientoController) {
        this.seguimientoController = seguimientoController;
    }

    /**
     *
     * @return
     */
    public Periodo getPeriodoVigente() {       
        periodoVigente=getFacade().getPeriodoVigente();       
        return periodoVigente;
    }

    /**
     *
     * @param periodoVigente
     */
    public void setPeriodoVigente(Periodo periodoVigente) {
        this.periodoVigente = periodoVigente;
    }
       
    
    /**
     * Comprueba si existe un periodo de evaluación activo o vigente
     * @return verdadero o falso
     */
    public boolean isHayPeriodoVigente() {  
        hayPeriodoVigente=getFacade().hayPeriodoVigente();
        return hayPeriodoVigente;
    }
    
    //getter y setter Lista Asignaciones de Metodologia

    /**
     *
     * @return
     */
    public List<AsignacionMetodologia> getListaAsignacionesMetodologia() {
        return listaAsignacionesMetodologia;
    }

    /**
     *
     * @param listaAsignacionesMetodologia
     */
    public void setListaAsignacionesMetodologia(List<AsignacionMetodologia> listaAsignacionesMetodologia) {
        this.listaAsignacionesMetodologia = listaAsignacionesMetodologia;
    }

    /**
     *
     * @return
     */
    public boolean existenEvaluacionesEnPeriodoSeleccionado(){
        return ejbEvaluaciones.existEvaluacionesPeriodo(current);
    }
    //getter y setter propiedad metodologia (que selecciona el usuario en la interfaz)
    /**
     *
     * @return
     */
    public String getMetodologia() {
        return metodologia;
    }

    /**
     *
     * @param metodologia
     */
    public void setMetodologia(String metodologia) {
        this.metodologia = metodologia;   
    }
    
    
    
    //getter renderDefPanel
    /**
     *
     * @return
     */
    public boolean isRenderDefPanel() {
       if(metodologia.equals(CatalogoMetodologias.METODOLOGIA_HIBRIDA))
            renderDefPanel=true;
        else
            renderDefPanel=false;
        return renderDefPanel;
    }
    
    //getter lista metodologias (utilizado para posibles opciones de metodologia)
    /**
     *
     * @return
     */
    public List<String> getListaMetodologiasCombo() {
        if(listaMetodologiasCombo==null)
            listaMetodologiasCombo=CatalogoMetodologias.getListaMetologias();
        return listaMetodologiasCombo;
    }

    /**
     *
     * @return
     */
    public List<String> getListaMetodologiasOneButton() {
        if(listaMetodologiasOneButton==null){
            listaMetodologiasOneButton = new ArrayList<String>();
            listaMetodologiasOneButton.add(CatalogoMetodologias.METODOLOGIA_180);
            listaMetodologiasOneButton.add(CatalogoMetodologias.METODOLOGIA_360);
        }
        return listaMetodologiasOneButton;
    }
        

    // Periodo de Evaluación ---------------------------------
    /**
     * Comprueba si hay evaluaciones en el periodo actual seleccionado
     * @return
     */
    public boolean isCurrentHasEvaluaciones() {
        return currentHasEvaluaciones;
    }

    /**
     *
     * @param currentHasEvaluaciones
     */
    public void setCurrentHasEvaluaciones(boolean currentHasEvaluaciones) {
        this.currentHasEvaluaciones = currentHasEvaluaciones;
    }
    
    /**
     * Obtiene la metodología de evaluación aplicada al periodo seleccionado
     * @param p
     * @return 180, 360 o hibrida
     */
    public String getMetodologiaPeriodo(Periodo p){
        boolean met180=false,met360=false;
        
        for(AsignacionMetodologia am: p.getAsignacionMetodologiaList()){
               if (am.getMetodologia().equals(CatalogoMetodologias.METODOLOGIA_180)) {
                   met180=true;
                  }
               else if (am.getMetodologia().equals(CatalogoMetodologias.METODOLOGIA_360)) {
                   met360=true;                   
           }  
        }
        if(met180 && met360)
            return CatalogoMetodologias.METODOLOGIA_HIBRIDA;
        else if(met180)
                return CatalogoMetodologias.METODOLOGIA_180;
             else 
                return CatalogoMetodologias.METODOLOGIA_360;
    }
    
    /**
     *
     * @return
     */
    public Periodo getSelected() {
        if (current == null) {
            current = new Periodo();
        }
        return current;
    }
    
    /**
     *
     * @param current
     */
    public void setSelected(Periodo current) {
        this.current = current;
        this.currentHasEvaluaciones=ejbEvaluaciones.existEvaluacionesPeriodo(current);
    }
    
    private PeriodoFacade getFacade() {
        return ejbFacade;
    }

    /**
     *
     * @return
     */
    public List<Periodo> getListaPeriodos() {
        if(listaPeriodos==null)
          listaPeriodos = getFacade().findAll();
        return listaPeriodos;
    }

    /**
     *
     * @param listaPeriodos
     */
    public void setListaPeriodos(List<Periodo> listaPeriodos) {
        this.listaPeriodos = listaPeriodos;
    }
    /**
     *
     * @return
     */
    public String prepareView(){
        metodologia=getMetodologiaPeriodo(current);
        return "DetallePeriodo?faces-redirect=true";
    }
    
    /**
     *
     * @return
     */
    public String prepareCreate() {
        current = new Periodo();
        renderDefPanel=false;
        metodologia=CatalogoMetodologias.METODOLOGIA_360;
        listaAsignacionesMetodologia=new ArrayList<AsignacionMetodologia>();
        for(Niveles n: nivelesMB.getListaNiveles()){
            AsignacionMetodologia am=new AsignacionMetodologia();
            am.setNiveles(n);
            am.setPeriodo(current);    
            listaAsignacionesMetodologia.add(am);
        }
        return "CrearPeriodo?faces-redirect=true";
    }

    /**
     *
     * @return
     */
    public String create() {
        //Validando que exista un Plan Estratégico Vigente
        PlanEstrategico planVigente=planMB.getPlanVigente();
        SimpleDateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:MM");
        if (planVigente != null) {
            if(current.getFechaFin().after(current.getFechaInicio())){
               
                if(current.getFechaInicio().getYear() >= planVigente.getFechaInicio().getYear() && current.getFechaFin().getYear() <= planVigente.getFechaFin().getYear() ) {
                       Periodo peConflictivo = ejbFacade.findByFechas(current);
                       if(peConflictivo == null) {    
                            //Validando que el periodo no se exceda del rango de años preestablecido
                            if (current.getFechaInicio().getYear() == current.getFechaFin().getYear() || current.getFechaInicio().getYear() == current.getFechaFin().getYear()-1) {   
                                try {
                                     current.setIdPlanEstrategico(planVigente);
                                     current.setIdPeriodo(getFacade().generarIdPeriodo(current.getFechaInicio())); 
                             
                                     for(AsignacionMetodologia am:listaAsignacionesMetodologia){
                                        am.setAsignacionMetodologiaPK(new AsignacionMetodologiaPK(am.getNiveles().getIdNivel(), current.getIdPeriodo()));
                                        if (metodologia.equals(CatalogoMetodologias.METODOLOGIA_180)){
                                          am.setMetodologia(CatalogoMetodologias.METODOLOGIA_180);   
                                        } else if (metodologia.equals(CatalogoMetodologias.METODOLOGIA_360)) {
                                            am.setMetodologia(CatalogoMetodologias.METODOLOGIA_360);
                                        } 
                                     }
                                    current.setAsignacionMetodologiaList(listaAsignacionesMetodologia);
                                    getFacade().create(current);
                                    JsfUtil.addSuccessMessage("El nuevo periodo de evaluación se ha registrado correctamente con id: ".concat(current.getIdPeriodo()));
                                    audiController.guardarRegistroActividad(" registró el período de evaluación "+current.getIdPeriodo());
                                    getFacade().limpiarClaseEnCache(Periodo.class);
                                    current=null;
                                    periodoVigente=null;
                                    listaPeriodos=null;
                                    return "ListaPeriodos?faces-redirect=true";
                                    
                                 } catch (Exception e) {
                                     JsfUtil.addErrorMessage("Lo sentimos, el nuevo periodo de evaluación no se ha podido registrar");
                                     System.out.println(e);
                                     return null;
                                }
                              } else {
                                JsfUtil.addErrorMessage("El rango de fechas es incorrecto");
                                return null;
                              }
                       } 
                        else {
                            JsfUtil.addErrorMessage("Existe intersecciones con otro periodo cuya fecha va del "
                                                    .concat(df.format(peConflictivo.getFechaInicio())).concat(" al ")
                                                    .concat(df.format(peConflictivo.getFechaFin())));
                            return null;
                        }
                }
                else{
                    JsfUtil.addErrorMessage("Se ha exedido de los rangos de fecha del Plan Estrategico vigente: "
                                                    .concat(df.format(planVigente.getFechaInicio())).concat(" al ")
                                                    .concat(df.format(planVigente.getFechaFin())));;
                    return null;
                }
            }
            else{
                JsfUtil.addErrorMessage("La fecha final del periodo debe ser posterior a la fecha de inicio");
                return null;
            }
        }
        else {
            JsfUtil.addErrorMessage("No existe un plan estratégico vigente");
            return null; 
        }
    }
    
    /**
     *
     * @return
     */
    public String prepareUpdate(){
        metodologia=getMetodologiaPeriodo(current);
        return "EditarPeriodo?faces-redirect=true";
    }

    /**
     *
     * @return
     */
    public String update() {
       SimpleDateFormat df=new SimpleDateFormat("dd/MM/yyyy");
       if(current.getFechaFin().after(current.getFechaInicio())){               
                if(current.getFechaInicio().getYear() >= planMB.getPlanVigente().getFechaInicio().getYear() && current.getFechaFin().getYear() <= planMB.getPlanVigente().getFechaFin().getYear() ) {
                       Periodo peConflictivo = ejbFacade.findByFechas(current);
                       if(peConflictivo == null||peConflictivo.equals(current)) {    
                            //Validando que el periodo no se exceda del rango de años preestablecido
                            if (current.getFechaInicio().getYear() == current.getFechaFin().getYear() || current.getFechaInicio().getYear() == current.getFechaFin().getYear()-1) {   
                                try {                                                                      
                                         
                                    if(metodologia.equals(CatalogoMetodologias.METODOLOGIA_180)) {
                                        for(AsignacionMetodologia am:current.getAsignacionMetodologiaList()){
                                                     am.setMetodologia(CatalogoMetodologias.METODOLOGIA_180);
                                                 }                                       
                                    } else if (metodologia.equals(CatalogoMetodologias.METODOLOGIA_360)) {
                                             for(AsignacionMetodologia am:current.getAsignacionMetodologiaList()){
                                                     am.setMetodologia(CatalogoMetodologias.METODOLOGIA_360);
                                                 }
                                    }                                    
                                    getFacade().edit(current);
                                    seguimientoController.setEvARealizarPorCentroCosto(null);
                                    JsfUtil.addSuccessMessage("El periodo ".concat(current.getIdPeriodo()).concat(" se ha actualizado correctamente"));
                                    audiController.guardarRegistroActividad(" editó el período con id "+current.getIdPeriodo());
                                     getFacade().limpiarClaseEnCache(Periodo.class);
                                    current=null;
                                    periodoVigente=null;
                                    listaPeriodos=null;
                                    return "ListaPeriodos?faces-redirect=true";
                                 } catch (Exception e) {
                                     JsfUtil.addErrorMessage("Lo sentimos, no se ha podido actualizar el periodo".concat(current.getIdPeriodo()));
                                     System.out.println(e);
                                     return null;
                                }
                              } else {
                                JsfUtil.addErrorMessage("El rango de fechas es incorrecto");
                                return null;
                              }
                       } 
                        else {
                            JsfUtil.addErrorMessage("Existe intersecciones con otro periodo cuya fecha va del "
                                                    .concat(df.format(peConflictivo.getFechaInicio())).concat(" al ")
                                                    .concat(df.format(peConflictivo.getFechaFin())));
                            return null;
                        }
                }
                else{
                    JsfUtil.addErrorMessage("Se ha exedido de los rangos de fecha del Plan Estrategico vigente: "
                                                    .concat(df.format(planMB.getPlanVigente().getFechaInicio())).concat(" al ")
                                                    .concat(df.format(planMB.getPlanVigente().getFechaFin())));;
                    return null;
                }
            }
            else{
                JsfUtil.addErrorMessage("La fecha final del periodo debe ser posterior a la fecha de inicio");
                return null;
            }
    }

    /**
     *
     */
    public void performDestroy() {
        try {
            if(!ejbEvaluaciones.existEvaluacionesPeriodo(current)){
                getFacade().remove(current);
                JsfUtil.addSuccessMessage("El periodo ".concat(current.getDescripcion()).concat(" ha sido elimanado correctamente"));
//                audiController.guardarRegistroActividad("eliminó el período con id "+current.getIdPeriodo());
                getFacade().limpiarClaseEnCache(Periodo.class);
                current = null;
                periodoVigente=null;
                listaPeriodos=null;
            }
            else{
                JsfUtil.addSuccessMessage("El periodo ".concat(current.getDescripcion()).concat(" no puede ser eliminados porque tiene evaluaciones relacionadas"));
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Lo sentimos, el periodo ".concat(current.getDescripcion()).concat(" no ha podido ser eliminado"));
        }
    }
    
    
    
    /**
     *
     * @return
     */
    public String obtenerRutadeLaClase(){
        String s = null;
        s = Periodo.class.getResource("Periodo.class").toString();
        s = Periodo.class.getPackage().toString();
        return s;
    }

    /**
     * Comprueba si es posible editar un periodo de evaluación
     * @param p
     * @return verdadero o falso
     */
    public boolean isEditable(Periodo p){
        if(listaPeriodos.indexOf(p)==listaPeriodos.size()-1)
            return true;
        return false;
    }
    
    /**
     * Comprueba si el periodo de evaluación seleccionado se puede eliminar
     * @param p
     * @return verdadero o falso
     */
    public boolean  isEliminable(Periodo p){
        if(!ejbEvaluaciones.existEvaluacionesPeriodo(p))
            return true;
        return false;
    }

    /**
     * @return the audiController
     */
    public AuditoriaController getAudiController() {
        return audiController;
    }

    /**
     * @param audiController the audiController to set
     */
    public void setAudiController(AuditoriaController audiController) {
        this.audiController = audiController;
    }

    /**
     *
     */
    @FacesConverter(forClass = Periodo.class)
    public static class PeriodoControllerConverter implements Converter {
  
         /**
          *
          * @param facesContext
          * @param component
          * @param value
          * @return
          */
         public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
}
            PeriodoController controller = (PeriodoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "periodoController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

         /**
          *
          * @param facesContext
          * @param component
          * @param object
          * @return
          */
         public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Periodo) {
                Periodo o = (Periodo) object;
                return getStringKey(o.getIdPeriodo());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Periodo.class.getName());
            }
        }
    }
}
