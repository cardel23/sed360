package uca.sed360.managedBeans;

import java.io.FileNotFoundException;
import java.io.IOException;
import uca.sed360.jpaEntities.Empleados;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.EmpleadosFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import uca.sed360.jpaEntities.Roles;
import uca.sed360.jpaEntities.Usuarios;
import uca.sed360.managedBeans.util.LectorDeExcel;
import uca.sed360.managedBeans.util.SeguimientoController;

/**
 * Controlador que maneja los empleados en la BD
 * @author Estacion 2
 */
@ManagedBean(name = "empleadosController")
@SessionScoped
public class EmpleadosController implements Serializable {

    @ManagedProperty (value = "#{periodoController}")
    private PeriodoController periodoController;
    
    @ManagedProperty(value="#{seguimientoController}") 
    private SeguimientoController seguimientoController;
    
    @ManagedProperty(value = "#{auditoriaController}")
    private AuditoriaController audiController;
    private Empleados current;
    @EJB
    private uca.sed360.ejbs.EmpleadosFacade ejbFacade;
    @EJB
    private uca.sed360.ejbs.UsuariosFacade ejbUsuarios;
    private List<Empleados> listaEmpleados,listaEmpleadosFiltrados;
    private List<Empleados> EmpleadosConEvaluacionesPorPeriodo;
    private List<Empleados> EmpleadosConEvaluacionesPorPeriodoFiltrados;

    /**
     * Constructor
     */
    public EmpleadosController() {
    }

    /**
     *
     * @return
     */
    public SeguimientoController getSeguimientoController() {
        return seguimientoController;
    }

    /**
     *
     * @param seguimientoController
     */
    public void setSeguimientoController(SeguimientoController seguimientoController) {
        this.seguimientoController = seguimientoController;
    }
    

    /**
     *
     * @return
     */
    public Empleados getSelected() {
        if (current == null) {
            prepareCreate();
        }
        return current;
    }
    
    /**
     *
     * @param emp
     */
    public void setSelected(Empleados emp){
        current = emp;
    }

    private EmpleadosFacade getFacade() {
        return ejbFacade;
    }
    
    //getter y setter para lista de empleados
    /**
     *
     * @return
     */
    public List<Empleados> getListaEmpleados() {
        if(listaEmpleados == null) {
            listaEmpleados = ejbFacade.findAll();
        }
        return listaEmpleados;
    }

    /**
     *
     * @param listaEmpleados
     */
    public void setListaEmpleados(List<Empleados> listaEmpleados) {
        this.listaEmpleados = listaEmpleados;
    }

    /**
     *
     * @return
     */
    public List<Empleados> getListaEmpleadosFiltrados() {
        return listaEmpleadosFiltrados;
    }

    /**
     *
     * @param listaEmpleadosFiltrados
     */
    public void setListaEmpleadosFiltrados(List<Empleados> listaEmpleadosFiltrados) {
        this.listaEmpleadosFiltrados = listaEmpleadosFiltrados;
    }
    
    /**
     *
     */
    public void updateIdUsuario(){
        this.current.getUsuarios().setIdUsuario(this.current.getCedula());
    }
    
    /**
     *
     */
    public void updateClaveUsuario(){
        this.current.getUsuarios().setClave(this.current.getInss());
    }

    /**
     *
     * @return
     */
    public String prepareCreate(){
        this.current=new Empleados();
        Usuarios usuario=new Usuarios();
        usuario.setRolesCollection(new ArrayList<Roles>());
        usuario.setEmpleados(current);
        this.current.setUsuarios(new Usuarios());
       
        return "/empleados/RegistrarEmpleado?faces-redirect=true";
    }

    /**
     *
     * @return
     */
    public String create() {
        try {
            ejbUsuarios.create(current.getUsuarios());
            getFacade().create(current);
            seguimientoController.setEvARealizarPorCentroCosto(null);
            JsfUtil.addSuccessMessage("El trabajador con cédula "+current.getCedula()+" se ha registrado exitosamente");
            audiController.guardarRegistroActividad(" registro al empleado "+current.getNombre()+" "+current.getApellido());
            current=null;
            listaEmpleados=null;
            return "ListaEmpleados?faces-redirect=true";
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Se ha producido un error al intentar guardar el nuevo trabajador");
            System.out.println(e);
            return null;
        }
    }

    /**
     *
     * @return
     */
    public List<Empleados> obtenerActivos(){
      return ejbFacade.obtenerActivos();
    }

    /**
     *
     * @return
     */
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage("El empleado ".concat(current.getNombre()).concat(" ").concat(current.getApellido()).concat(" ha sido actualizado exitosamente"));
            audiController.guardarRegistroActividad(" actualizo la informacion del empleado "+current.getNombre()+" "+current.getApellido());
            current=null;
            seguimientoController.setEvARealizarPorCentroCosto(null);
            listaEmpleados=null;
            return "ListaEmpleados?faces-redirect=true";
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Lo sentimos, el empleado ".concat(current.getNombre()).concat(" ").concat(current.getApellido()).concat(" no ha podido ser actualizado"));
            System.out.println(e);
            return null;
        }
    }

 

    private String performDestroy() {
        try {
            getFacade().remove(current);
            seguimientoController.setEvARealizarPorCentroCosto(null);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("EmpleadosDeleted"));
            return "ListaEmpleados?faces-redirect=true";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            System.out.println(e);
            return null;
        }
    }

    /**
     * @return the periodoController
     */
    public PeriodoController getPeriodoController() {
        return periodoController;
    }

    /**
     * @param periodoController the periodoController to set
     */
    public void setPeriodoController(PeriodoController periodoController) {
        this.periodoController = periodoController;
    }

    /**
     * @return the EmpleadosConEvaluacionesPorPeriodo
     */
    public List<Empleados> getEmpleadosConEvaluacionesPorPeriodo() {
        return ejbFacade.obtenerEmpleadosConEvaluacionesPorPeriodo(periodoController.getPeriodoVigente());
    }

    /**
     * @param EmpleadosConEvaluacionesPorPeriodo the EmpleadosConEvaluacionesPorPeriodo to set
     */
    public void setEmpleadosConEvaluacionesPorPeriodo(List<Empleados> EmpleadosConEvaluacionesPorPeriodo) {
        this.EmpleadosConEvaluacionesPorPeriodo = EmpleadosConEvaluacionesPorPeriodo;
    }

    /**
     * @return the EmpleadosConEvaluacionesPorPeriodoFiltrados
     */
    public List<Empleados> getEmpleadosConEvaluacionesPorPeriodoFiltrados() {
        return EmpleadosConEvaluacionesPorPeriodoFiltrados;
    }

    /**
     * @param EmpleadosConEvaluacionesPorPeriodoFiltrados the EmpleadosConEvaluacionesPorPeriodoFiltrados to set
     */
    public void setEmpleadosConEvaluacionesPorPeriodoFiltrados(List<Empleados> EmpleadosConEvaluacionesPorPeriodoFiltrados) {
        this.EmpleadosConEvaluacionesPorPeriodoFiltrados = EmpleadosConEvaluacionesPorPeriodoFiltrados;
    }

    /**
     * @return the audiController
     */
    public AuditoriaController getAudiController() {
        return audiController;
    }

    /**
     * @param audiController the audiController to set
     */
    public void setAudiController(AuditoriaController audiController) {
        this.audiController = audiController;
    }

    /**
     * Lee un archivo de excel para actualizar los datos de los empleados
     * @deprecated No se definió el algoritmo para realizar la actualización
     *             para cada periodo de evaluación
     */
    public void lectura(){
        try {
            LectorDeExcel.lectura();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmpleadosController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EmpleadosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     */
    @FacesConverter(forClass = Empleados.class)
    public static class EmpleadosControllerConverter implements Converter {

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
 }
            EmpleadosController controller = (EmpleadosController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "empleadosController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Empleados) {
                Empleados o = (Empleados) object;
                return getStringKey(o.getCedula());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Empleados.class.getName());
            }
        }
    }
}
