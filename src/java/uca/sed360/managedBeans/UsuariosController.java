package uca.sed360.managedBeans;

import uca.sed360.jpaEntities.Usuarios;
import uca.sed360.managedBeans.util.JsfUtil;
import uca.sed360.ejbs.UsuariosFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import uca.sed360.jpaEntities.Roles;

/**
 * Controlador que maneja los usuarios del sistema en la BD
 * @author Estacion 2
 */
@ManagedBean(name = "usuariosController")
@SessionScoped
public class UsuariosController implements Serializable {

    @ManagedProperty(value = "#{auditoriaController}")
    private AuditoriaController audiController;
    
    private Usuarios current;
    private List<Usuarios> listaUsuarios,listaUsuariosFiltrado;
    private boolean viewPass=false,changePass=false;
    private Roles rolNewUser;
    private String confirmation;
    
    @EJB
    private uca.sed360.ejbs.UsuariosFacade ejbFacade;
    @EJB
    private uca.sed360.ejbs.RolesFacade ejbRol;
    
    /**
     * Constructor
     */
    public UsuariosController() {
    }

    /**
     *
     * @return
     */
    public Usuarios getSelected() {
        if (current == null) {
            current = new Usuarios();
        }
        return current;
    }
    
    /**
     *
     * @param user
     */
    public void setSelected(Usuarios user){
        this.current=user;
    }

    /**
     *
     * @return
     */
    public String getConfirmation() {
        return confirmation;
    }

    /**
     *
     * @param confirmation
     */
    public void setConfirmation(String confirmation) {
        this.confirmation = confirmation;
    }
    
    /**
     *
     * @return
     */
    public Roles getRolNewUser() {
        return rolNewUser;
    }

    /**
     *
     * @param rolNewUser
     */
    public void setRolNewUser(Roles rolNewUser) {
        this.rolNewUser = rolNewUser;
    }

    /**
     *
     * @return
     */
    public boolean isViewPass() {
        return viewPass;
    }

    /**
     *
     * @param viewPass
     */
    public void setViewPass(boolean viewPass) {
        this.viewPass = viewPass;
    }

    /**
     *
     * @return
     */
    public boolean isChangePass() {
        return changePass;
    }

    /**
     *
     * @param changePass
     */
    public void setChangePass(boolean changePass) {
        this.changePass = changePass;
    }
    
 
    private UsuariosFacade getFacade() {
        return ejbFacade;
    }

    /**
     *
     * @return
     */
    public List<Usuarios> getListaUsuarios() {
        if(listaUsuarios==null)
            listaUsuarios=getFacade().findAll();
        return listaUsuarios;
    }

    /**
     *
     * @param listaUsuarios
     */
    public void setListaUsuarios(List<Usuarios> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    /**
     *
     * @return
     */
    public List<Usuarios> getListaUsuariosFiltrado() {
        return listaUsuariosFiltrado;
    }

    /**
     *
     * @param listaUsuariosFiltrado
     */
    public void setListaUsuariosFiltrado(List<Usuarios> listaUsuariosFiltrado) {
        this.listaUsuariosFiltrado = listaUsuariosFiltrado;
    }
  
    /**
     *
     * @return
     */
    public String prepareCreate() {
        current = new Usuarios();
        return "NuevoUsuario?faces-redirect=true";
    }

    /**
     *
     * @return
     */
    public String create() {
        try {            
             if(!current.getClave().equals(confirmation)){
                 JsfUtil.addWarnMessage("La contraseña de confirmación no coincide");
                 return null;
             }
             else{
                current.setRolesCollection(new ArrayList<Roles>());
                current.getRolesCollection().add(rolNewUser);
                getFacade().create(current);
                JsfUtil.addSuccessMessage("El nuevo usuario ".concat(current.getIdUsuario()).concat(" ha sido registrado correctamente"));
                audiController.guardarRegistroActividad(" registró al usuario "+current.getIdUsuario());
                listaUsuarios=null;
                current=null;
                return "ListaUsuarios?faces-redirect=true"; 
             }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Lo sentimos, el usuario ".concat(current.getIdUsuario()).concat(" no pudo ser registrado"));
            System.out.println(e);
            return null;
            
        }
    }

   
    /**
     *
     * @return
     */
    public String update() {
        try {
            if(changePass)
                if(!current.getClave().equals(confirmation)){
                 JsfUtil.addWarnMessage("La contraseña de confirmación no coincide");
                 return null;
             }
                getFacade().edit(current);
                JsfUtil.addSuccessMessage("El  usuario ".concat(current.getIdUsuario()).concat(" ha sido actualizado correctamente"));
                audiController.guardarRegistroActividad(" actualizó al usuario "+current.getIdUsuario());
                listaUsuarios=null;
                current=null;
                return "ListaUsuarios?faces-redirect=true";
           
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Lo sentimos, el  usuario ".concat(current.getIdUsuario()).concat("no  pudo ser actualizado"));
            return null;
        }
    }

    /**
     *
     * @return
     */
    public String performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage("El  usuario ".concat(current.getIdUsuario()).concat(" ha sido eliminado exitosamente"));
            audiController.guardarRegistroActividad(" eliminó al usuario "+current.getIdUsuario());
            listaUsuarios=null;
            current=null;
            return "ListaUsuarios?faces-redirect=true";
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Lo sentimos, el usuario ".concat(current.getIdUsuario()).concat(" no se ha eliminado"));
            return null;
        }
    }

    /**
     * @return the audiController
     */
    public AuditoriaController getAudiController() {
        return audiController;
    }

    /**
     * @param audiController the audiController to set
     */
    public void setAudiController(AuditoriaController audiController) {
        this.audiController = audiController;
    }

    /**
     *
     */
    @FacesConverter(forClass = Usuarios.class)
    public static class UsuariosControllerConverter implements Converter {

        /**
         *
         * @param facesContext
         * @param component
         * @param value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            UsuariosController controller = (UsuariosController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "usuariosController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Usuarios) {
                Usuarios o = (Usuarios) object;
                return getStringKey(o.getIdUsuario());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Usuarios.class.getName());
            }
        }
    }
}
