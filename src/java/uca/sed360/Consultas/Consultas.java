
package uca.sed360.Consultas;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Clase que se encarga de realizar consultas a la base de datos que involucran
 * varias entidades
 * @author Estacion 2
 * @version 1.0
 */
@Entity
@XmlRootElement

@NamedQueries({
    @NamedQuery(name = "Consultas.evaluacionesParaPromedio",
        query = "SELECT e, SUM(es.valor*d.valor)AS valor FROM Evaluacion e, \n" +
                "RespuestaPreguntaCerrada r, Escalas es,\n" +
                "PreguntaCerrada p, DetallePreguntaCerrada d\n" +
                "WHERE p=r.preguntaCerrada\n" +
                "AND e.respuestaPreguntaCerradaCollection = r\n" +
                "AND d.preguntaCerrada = p\n" +
                "AND d.nivel360 = e.idNivel360\n" +
                "AND r.escala = es\n"+
                "AND e.idPeriodo = :p\n "+
                "GROUP BY e\n" +
                "ORDER BY valor DESC"),
    @NamedQuery(name = "Consultas.lista",
        query = "SELECT em, SUM(r.escala.valor*D.valor) AS Valor FROM Evaluacion e, \n" +
                "RespuestaPreguntaCerrada R,\n" +
                "PreguntaCerrada p, DetallePreguntaCerrada D, Empleados em\n" +
                "WHERE p=r.preguntaCerrada\n" +
                "AND e.respuestaPreguntaCerradaCollection = R\n" +
                "AND D.preguntaCerrada = p\n" +
                "AND D.nivel360 = e.idNivel360\n" +
                "AND em = e.evaluado\n" +
                "AND e.idPeriodo = :p\n" +
                "GROUP BY em\n" +
                "ORDER BY Valor DESC"),
    @NamedQuery(name = "Consultas.ListaDeValoresPorEmpleado",
        query = "SELECT em, SUM(r.escala.valor*D.valor) AS Valor FROM Evaluacion e, \n" +
                "RespuestaPreguntaCerrada R,\n" +
                "PreguntaCerrada p, DetallePreguntaCerrada D, Empleados em\n" +
                "WHERE p=r.preguntaCerrada\n" +
                "AND e.respuestaPreguntaCerradaCollection = R\n" +
                "AND D.preguntaCerrada = p\n" +
                "AND D.nivel360 = e.idNivel360\n" +
                "AND em = e.evaluado\n" +
                "AND e.idPeriodo = :p\n" +
                "AND em = :empleado \n"+
                "GROUP BY em\n" +
                "ORDER BY Valor DESC"),
    
    @NamedQuery(name = "Consultas.listaCapacitacionesPorArea",
        query = "SELECT e.CentroCosto, c.descripcion, COUNT(c.descripcion) FROM Evaluacion e, \n" +
                "Capacitaciones c\n" +
                "WHERE SIZE(e.capacitacionesCollection) > 0\n" +
                "AND c=e.capacitacionesCollection\n" +
                "AND e.idPeriodo = :p\n" +
                "GROUP BY c.descripcion, e.CentroCosto\n" +
                "ORDER BY e.CentroCosto, COUNT(c.descripcion) DESC")})
@NamedNativeQueries(
        {    
    @NamedNativeQuery(name = "ObtenerPromedios", query = "select promediosPorPeriodo.nombre, promediosPorPeriodo.apellido, promediosPorPeriodo.c_costo,\n" +
"promediosPorPeriodo.inss, Ubicaciones.descripcion, promediosPorPeriodo.promedio from ubicaciones inner join 	\n" +
"	(Select Empleados.nombre as nombre, Empleados.ubicacion as ubicacion, Empleados.centro_costo as c_costo, Empleados.apellido as apellido, Empleados.inss as inss, promedio as promedio from Empleados inner join (\n"
            + "		select evaluado, AVG(totaldeEvaluacion) as promedio from	\n"
            + "		(select id_Evaluacion, evaluado, evaluador,  SUM(total.puntajeObtenido) as totaldeEvaluacion from (\n"
            + "			select segunda.id_Evaluacion, segunda.evaluado, segunda.evaluador, primera.id_criterio,\n"
            + "				sum((primera.puntajeDeCadaPregunta * segunda.valor)) as puntajeObtenido 				   \n"
            + "				from  (select valoresDePreguntas.id_criterio, valoresDePreguntas.id_instrumento, valoresDePreguntas.puntajeDeCadaPregunta, PreguntaCerrada.id_pregunta  from\n"
            + "				  (select Ponderacion.id_criterio, Ponderacion.id_instrumento,(Ponderacion.Puntaje/cant_Preguntas) as puntajeDeCadaPregunta\n"
            + "				   from   Ponderacion \n"
            + "				     inner join (select PreguntaCerrada.id_criterio as criterio, COUNT(PreguntaCerrada.id_criterio) as cant_Preguntas \n"
            + "						from PreguntaCerrada group by id_criterio) CantidadPreguntasDelCriterio\n"
            + "				         	on Ponderacion.id_criterio = CantidadPreguntasDelCriterio.criterio /*and  Ponderacion.id_instrumento=2 and Ponderacion.id_criterio = 1*/\n"
            + "				  ) valoresDePreguntas\n"
            + "				  inner join 				\n"
            + "				  PreguntaCerrada on PreguntaCerrada.id_criterio =  valoresDePreguntas.id_criterio\n"
            + "				 )    primera\n"
            + "				inner join\n"
            + "				(select Evaluacion.id_Evaluacion, Evaluacion.evaluado, Evaluacion.evaluador,RespuestaPreguntaCerrada.pregunta,\n"
            + "				 Escalas.valor, Evaluacion.id_instrumento\n"
            + "				 from Evaluacion, RespuestaPreguntaCerrada, PreguntaCerrada, Escalas\n"
            + "				 where\n"
            + "				 Evaluacion.id_Evaluacion = RespuestaPreguntaCerrada.asignacion and 	RespuestaPreguntaCerrada.pregunta = PreguntaCerrada.id_pregunta \n"
            + "				 and RespuestaPreguntaCerrada.escala= Escalas.id_escala and  Evaluacion.id_periodo = ?  /*	and  evaluado =  '23456789'*/\n"
            + "				) segunda \n"
            + "				on  primera.id_pregunta = segunda.pregunta where primera.id_instrumento = segunda.id_instrumento\n"
            + "				--where evaluador = '23456789'\n"
            + "				group by id_Evaluacion, evaluado, evaluador, id_criterio \n"
            + "			) total\n"
            + "			group by id_Evaluacion, evaluado, evaluador ) totalesIndividuales group by evaluado ) promedios on Empleados.inss = promedios.evaluado) promediosPorPeriodo\n" +
"					on promediosPorPeriodo.ubicacion = Ubicaciones.id_ubicacion  order by promediosPorPeriodo.promedio desc")
            })
        
public class Consultas implements Serializable{
    @Id
    private Long id;

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
}
