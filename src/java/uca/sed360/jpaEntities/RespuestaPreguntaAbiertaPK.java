/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Gerardo
 */
@Embeddable
public class RespuestaPreguntaAbiertaPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "pregunta")
    private int pregunta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "asignacion")
    private String asignacion;

    public RespuestaPreguntaAbiertaPK() {
    }

    public RespuestaPreguntaAbiertaPK(int pregunta, String asignacion) {
        this.pregunta = pregunta;
        this.asignacion = asignacion;
    }

    public int getPregunta() {
        return pregunta;
    }

    public void setPregunta(int pregunta) {
        this.pregunta = pregunta;
    }

    public String getAsignacion() {
        return asignacion;
    }

    public void setAsignacion(String asignacion) {
        this.asignacion = asignacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) pregunta;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestaPreguntaAbiertaPK)) {
            return false;
        }
        RespuestaPreguntaAbiertaPK other = (RespuestaPreguntaAbiertaPK) object;
        if (this.pregunta != other.pregunta) {
            return false;
        }
        if (!this.asignacion.equals(other.asignacion)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uca.sed360.jpaEntities.RespuestaPreguntaAbiertaPK[ pregunta=" + pregunta + ", asignacion=" + asignacion + " ]";
    }
    
}
