/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Gerardo
 */
@Embeddable
public class RespuestaPreguntaCerradaPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "pregunta")
    private String pregunta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "asignacion")
    private String asignacion;

    public RespuestaPreguntaCerradaPK() {
    }

    public RespuestaPreguntaCerradaPK(String pregunta, String asignacion) {
        this.pregunta = pregunta;
        this.asignacion = asignacion;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getAsignacion() {
        return asignacion;
    }

    public void setAsignacion(String asignacion) {
        this.asignacion = asignacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pregunta != null ? pregunta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestaPreguntaCerradaPK)) {
            return false;
        }
        RespuestaPreguntaCerradaPK other = (RespuestaPreguntaCerradaPK) object;
        if (!this.pregunta.equals(other.pregunta)) {
            return false;
        }
        if (!this.asignacion.equals(other.asignacion)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return pregunta + asignacion ;
    }
    
}
