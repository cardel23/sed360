/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rrhh
 */
@Embeddable
public class DetallePreguntaCerradaPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 7)
    @Column(name = "id_pregunta")
    private String idPregunta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_Nivel360")
    private int idNivel360;

    public DetallePreguntaCerradaPK() {
    }

    public DetallePreguntaCerradaPK(String idPregunta, int idNivel360) {
        this.idPregunta = idPregunta;
        this.idNivel360 = idNivel360;
    }

    public String getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(String idPregunta) {
        this.idPregunta = idPregunta;
    }

    public int getIdNivel360() {
        return idNivel360;
    }

    public void setIdNivel360(int idNivel360) {
        this.idNivel360 = idNivel360;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPregunta != null ? idPregunta.hashCode() : 0);
        hash += (int) idNivel360;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetallePreguntaCerradaPK)) {
            return false;
        }
        DetallePreguntaCerradaPK other = (DetallePreguntaCerradaPK) object;
        if ((this.idPregunta == null && other.idPregunta != null) || (this.idPregunta != null && !this.idPregunta.equals(other.idPregunta))) {
            return false;
        }
        if (this.idNivel360 != other.idNivel360) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uca.sed360.jpaEntities.DetallePreguntaCerradaPK[ idPregunta=" + idPregunta + ", idNivel360=" + idNivel360 + " ]";
    }
    
}
