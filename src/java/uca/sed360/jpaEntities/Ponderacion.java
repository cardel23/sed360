/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rrhh
 */
@Entity
@Table(name = "Ponderacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ponderacion.findAll", query = "SELECT p FROM Ponderacion p"),
    @NamedQuery(name = "Ponderacion.findByPuntaje", query = "SELECT p FROM Ponderacion p WHERE p.puntaje = :puntaje"),
    @NamedQuery(name = "Ponderacion.findByIdCriterio", query = "SELECT p FROM Ponderacion p WHERE p.ponderacionPK.idCriterio = :idCriterio"),
    @NamedQuery(name = "Ponderacion.countByTipoEmpPlan", query = "SELECT COUNT(p) FROM Ponderacion p WHERE p.criterio.planEstrategico = :plan AND p.criterio.tipoEmpleado=:tipoEmp"),
    @NamedQuery(name = "Ponderacion.countByPlan", query = "SELECT COUNT(p) FROM Ponderacion p WHERE p.criterio.planEstrategico = :planVigente"),
    @NamedQuery(name = "Ponderacion.findBynivel360", query = "SELECT p FROM Ponderacion p WHERE p.nivel360 = :nivel360"),
    @NamedQuery(name = "Ponderacion.findBynivel360Criterio", query = "SELECT p FROM Ponderacion p WHERE p.nivel360 = :nivel360 AND p.criterio = :criterio"),
    @NamedQuery(name = "Ponderacion.findByIdnivel360", query = "SELECT p FROM Ponderacion p WHERE p.nivel360 = :nivel360 AND p.criterio.planEstrategico = :plan")})

public class Ponderacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PonderacionPK ponderacionPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Puntaje")
    private double puntaje;
    @JoinColumn(name = "id_Nivel360", referencedColumnName = "id_Nivel360", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Nivel360 nivel360;
    @JoinColumn(name = "id_criterio", referencedColumnName = "id_criterio", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Criterio criterio;

    public Ponderacion() {
    }

    public Ponderacion(PonderacionPK ponderacionPK) {
        this.ponderacionPK = ponderacionPK;
    }

    public Ponderacion(PonderacionPK ponderacionPK, double puntaje) {
        this.ponderacionPK = ponderacionPK;
        this.puntaje = puntaje;
    }

    public Ponderacion(int idCriterio, int idNivel360) {
        this.ponderacionPK = new PonderacionPK(idCriterio, idNivel360);
    }

    public PonderacionPK getPonderacionPK() {
        return ponderacionPK;
    }

    public void setPonderacionPK(PonderacionPK ponderacionPK) {
        this.ponderacionPK = ponderacionPK;
    }

    public double getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(double puntaje) {
        this.puntaje = puntaje;
    }

    public Nivel360 getNivel360() {
        return nivel360;
    }

    public void setNivel360(Nivel360 nivel360) {
        this.nivel360 = nivel360;
    }

    public Criterio getCriterio() {
        return criterio;
    }

    public void setCriterio(Criterio criterio) {
        this.criterio = criterio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ponderacionPK != null ? ponderacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ponderacion)) {
            return false;
        }
        Ponderacion other = (Ponderacion) object;
        if ((this.ponderacionPK == null && other.ponderacionPK != null) || (this.ponderacionPK != null && !this.ponderacionPK.equals(other.ponderacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uca.sed360.jpaEntities.Ponderacion[ ponderacionPK=" + ponderacionPK + " ]";
    }
    
}
