/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Estacion 2
 */
@Entity
@Table(name = "TipoEmpleado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoEmpleado.findAll", query = "SELECT t FROM TipoEmpleado t"),
    @NamedQuery(name = "TipoEmpleado.findByIdTipoempleado", query = "SELECT t FROM TipoEmpleado t WHERE t.idTipoempleado = :idTipoempleado"),
    @NamedQuery(name = "TipoEmpleado.findByDescripcion", query = "SELECT t FROM TipoEmpleado t WHERE t.descripcion = :descripcion")})
public class TipoEmpleado implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoEmpleado")
    private List<Empleados> empleadosList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tipoempleado")
    private Integer idTipoempleado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    
    //Descripción del tipo de empleado
    @Column(name = "descripcion")
    private String descripcion;
    
    //Lista de preguntas abiertas para el tipo de empleados
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoEmpleado")
    private List<PreguntaAbierta> preguntaAbiertaList;
    
    //Lista de criterios del tipo de Empleado
    @OneToMany(mappedBy = "tipoEmpleado")
    private List<Criterio> criteriosList;
    

    public TipoEmpleado() {
    }

    public TipoEmpleado(Integer idTipoempleado) {
        this.idTipoempleado = idTipoempleado;
    }

    public TipoEmpleado(Integer idTipoempleado, String descripcion) {
        this.idTipoempleado = idTipoempleado;
        this.descripcion = descripcion;
    }

    public Integer getIdTipoempleado() {
        return idTipoempleado;
    }

    public void setIdTipoempleado(Integer idTipoempleado) {
        this.idTipoempleado = idTipoempleado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<PreguntaAbierta> getPreguntaAbiertaList() {
        return preguntaAbiertaList;
    }

    public void setPreguntaAbiertaList(List<PreguntaAbierta> preguntaAbiertaList) {
        this.preguntaAbiertaList = preguntaAbiertaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoempleado != null ? idTipoempleado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoEmpleado)) {
            return false;
        }
        TipoEmpleado other = (TipoEmpleado) object;
        if ((this.idTipoempleado == null && other.idTipoempleado != null) || (this.idTipoempleado != null && !this.idTipoempleado.equals(other.idTipoempleado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uca.sed360.jpaEntities.TipoEmpleado[ idTipoempleado=" + idTipoempleado + " ]";
    }

    /**
     * @return the criteriosList
     */
    public List<Criterio> getCriteriosList() {
        return criteriosList;
    }

    /**
     * @param criteriosList the criteriosList to set
     */
    public void setCriteriosList(List<Criterio> criteriosList) {
        this.criteriosList = criteriosList;
    }

    @XmlTransient
    public List<Empleados> getEmpleadosList() {
        return empleadosList;
    }

    public void setEmpleadosList(List<Empleados> empleadosList) {
        this.empleadosList = empleadosList;
    }
    
}
