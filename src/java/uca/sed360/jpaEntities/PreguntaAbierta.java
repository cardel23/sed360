/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Estacion 2
 */
@Entity
@Table(name = "PreguntaAbierta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PreguntaAbierta.findAll", query = "SELECT p FROM PreguntaAbierta p"),
    @NamedQuery(name = "PreguntaAbierta.findByIdPregunta", query = "SELECT p FROM PreguntaAbierta p WHERE p.idPregunta = :idPregunta"),
    @NamedQuery(name="PreguntaAbierta.findByPlanTipoEmp",query="SELECT p FROM PreguntaAbierta p WHERE p.idPlanEstrategico = :plan AND p.tipoEmpleado=:tipoEmp"),
    @NamedQuery(name="PreguntaAbierta.findByPlanTipoEmpleadoNivel360",query = "SELECT p FROM PreguntaAbierta p WHERE  p.idPlanEstrategico= :planEstrategico AND p.tipoEmpleado=:tipoEmp AND p.nivel360= :nivel360"),
    @NamedQuery(name="PreguntaAbierta.eliminarByNivel360Plan",query = "DELETE FROM PreguntaAbierta p WHERE p.nivel360= :nivel360 AND p.idPlanEstrategico= :planEstrategico")})
public class PreguntaAbierta implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "preguntaAbierta")
    private List<RespuestaPreguntaAbierta> respuestaPreguntaAbiertaList;
    private static final long serialVersionUID = 1L;
    @Id  
    @Basic(optional = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pregunta")
    private Integer idPregunta;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "tipo_empleado", referencedColumnName = "id_tipoempleado")
    @ManyToOne(optional = false)
    private TipoEmpleado tipoEmpleado;
    @JoinColumn(name = "id_plan_estrategico", referencedColumnName = "id_plan_estrategico")
    @ManyToOne
    private PlanEstrategico idPlanEstrategico;
    @JoinColumn(name = "id_Nivel360", referencedColumnName = "id_Nivel360")
    @ManyToOne(optional = false)
    private Nivel360 nivel360;

    public PreguntaAbierta() {
    }

    public PreguntaAbierta(Integer idPregunta) {
        this.idPregunta = idPregunta;
    }

    public PreguntaAbierta(Integer idPregunta, String descripcion) {
        this.idPregunta = idPregunta;
        this.descripcion = descripcion;
    }

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(Integer idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoEmpleado getTipoEmpleado() {
        return tipoEmpleado;
    }

    public void setTipoEmpleado(TipoEmpleado tipoEmpleado) {
        this.tipoEmpleado = tipoEmpleado;
    }

    public PlanEstrategico getIdPlanEstrategico() {
        return idPlanEstrategico;
    }

    public void setIdPlanEstrategico(PlanEstrategico idPlanEstrategico) {
        this.idPlanEstrategico = idPlanEstrategico;
    }

    public Nivel360 getNivel360() {
        return nivel360;
    }

    public void setNivel360(Nivel360 idNivel360) {
        this.nivel360 = idNivel360;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPregunta != null ? idPregunta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PreguntaAbierta)) {
            return false;
        }
        PreguntaAbierta other = (PreguntaAbierta) object;
        if ((this.idPregunta == null && other.idPregunta != null) || (this.idPregunta != null && !this.idPregunta.equals(other.idPregunta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uca.sed360.jpaEntities.PreguntaAbierta[ idPregunta=" + idPregunta + " ]";
    }

    @XmlTransient
    public List<RespuestaPreguntaAbierta> getRespuestaPreguntaAbiertaList() {
        return respuestaPreguntaAbiertaList;
    }

    public void setRespuestaPreguntaAbiertaList(List<RespuestaPreguntaAbierta> respuestaPreguntaAbiertaList) {
        this.respuestaPreguntaAbiertaList = respuestaPreguntaAbiertaList;
    }
    
}
