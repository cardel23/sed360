/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Gerardo
 */
@Entity
@Table(name = "CentroDeCostos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CentroDeCostos.findAll", query = "SELECT c FROM CentroDeCostos c"),
    @NamedQuery(name = "CentroDeCostos.tieneEmpleados", query = "SELECT e FROM Empleados e where e.centroCosto.idCentrocostos = :idCentroCostos"),
    @NamedQuery(name = "CentroDeCostos.findByIdCentrocostos", query = "SELECT c FROM CentroDeCostos c WHERE c.idCentrocostos = :idCentrocostos"),
    @NamedQuery(name = "CentroDeCostos.capacitacionesPorArea", query = "SELECT  o FROM Capacitaciones c, Periodo p, Evaluacion e, CentroDeCostos o\n" +
                       "WHERE c.evaluacionCollection = e\n" +
                       "AND e.idPeriodo = p\n" +
                       "AND c.centroDeCostosList = o\n" +
                       "AND e.capacitacionesCollection = c\n" +
                       "AND o.capacitacionesList = c\n" +
                       "AND e.capacitacionesCollection = o.capacitacionesList\n" +
                       "AND p = :periodo"),
    @NamedQuery(name = "CentroDeCostos.centrosPorPeriodo", query = "SELECT c FROM CentroDeCostos c, Capacitaciones cap, Evaluacion e\n" +
"WHERE e.idPeriodo = :p\n" +
"AND c.capacitacionesList = cap\n" +
"AND e = cap.evaluacionCollection\n" +
"AND e.capacitacionesCollection = cap\n" +
"GROUP BY c")})
public class CentroDeCostos implements Serializable {
    @OneToMany(mappedBy = "centroCosto", fetch = FetchType.LAZY)
    private Collection<Empleados> empleadosCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "id_centrocostos")
    private String idCentrocostos;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @ManyToMany(mappedBy = "centroDeCostosList", fetch=FetchType.LAZY)
    private List<Capacitaciones> capacitacionesList;

    public CentroDeCostos() {
    }

    public CentroDeCostos(String idCentrocostos) {
        this.idCentrocostos = idCentrocostos;
    }

    public CentroDeCostos(String idCentrocostos, String descripcion) {
        this.idCentrocostos = idCentrocostos;
        this.descripcion = descripcion;
    }

    public String getIdCentrocostos() {
        return idCentrocostos;
    }

    public void setIdCentrocostos(String idCentrocostos) {
        this.idCentrocostos = idCentrocostos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Capacitaciones> getCapacitacionesList() {
        return capacitacionesList;
    }

    public void setCapacitacionesList(List<Capacitaciones> capacitacionesList) {
        this.capacitacionesList = capacitacionesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCentrocostos != null ? idCentrocostos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CentroDeCostos)) {
            return false;
        }
        CentroDeCostos other = (CentroDeCostos) object;
        if ((this.idCentrocostos == null && other.idCentrocostos != null) || (this.idCentrocostos != null && !this.idCentrocostos.equals(other.idCentrocostos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "uca.sed360.jpaEntities.CentroDeCostos[ idCentrocostos=" + idCentrocostos + " ]";
        return descripcion;
    }

    @XmlTransient
    public Collection<Empleados> getEmpleadosCollection() {
        return empleadosCollection;
    }

    public void setEmpleadosCollection(Collection<Empleados> empleadosCollection) {
        this.empleadosCollection = empleadosCollection;
    }
    
}
