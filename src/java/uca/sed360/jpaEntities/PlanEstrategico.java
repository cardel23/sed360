/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jose
 */
@Entity
@Table(name = "PlanEstrategico")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanEstrategico.findAll", query = "SELECT p FROM PlanEstrategico p"),
    @NamedQuery(name = "PlanEstrategico.findByIdPlanEstrategico", query = "SELECT p FROM PlanEstrategico p WHERE p.idPlanEstrategico = :idPlanEstrategico"),
    @NamedQuery(name = "PlanEstrategico.findByFechaInicio", query = "SELECT p FROM PlanEstrategico p WHERE p.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "PlanEstrategico.findByFechaFin", query = "SELECT p FROM PlanEstrategico p WHERE p.fechaFin = :fechaFin"),
    @NamedQuery(name = "PlanEstrategico.findByFechas", query = "SELECT p FROM PlanEstrategico p WHERE p.fechaInicio != :fechaInicio and p.fechaFin != :fechaFin"
                + " AND ("
                + "p.fechaInicio BETWEEN :fechaInicio AND :fechaFin OR p.fechaFin BETWEEN :fechaInicio AND :fechaFin "
                + "OR :fechaInicio BETWEEN p.fechaInicio AND p.fechaFin OR :fechaFin BETWEEN p.fechaInicio AND p.fechaFin)"),   
    @NamedQuery(name = "PlanEstrategico.planVigente", query = "SELECT p FROM PlanEstrategico p WHERE p.fechaInicio <= CURRENT_DATE AND p.fechaFin >= CURRENT_DATE"),
    @NamedQuery(name = "PlanEstrategico.comprobarConflictoConPeriodo",query = "SELECT count(p) from Periodo p where p.fechaFin > :fechaFin OR p.fechaInicio > :fechaFin")})
public class PlanEstrategico implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPlanEstrategico",orphanRemoval = true)
    private Collection<Periodo> periodoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPlanEstrategico",orphanRemoval = true)
    private List<Escalas> escalasCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "planEstrategico",orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Criterio> criteriosCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = true)    
    @Column(name = "id_plan_estrategico")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPlanEstrategico;
 
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "identificador")
    private String identificador;
    
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPlanEstrategico", orphanRemoval = true)
    private List<PreguntaAbierta> preguntaAbiertaCollection;

    public PlanEstrategico() {
    }

    public PlanEstrategico(Integer idPlanEstrategico) {
        this.idPlanEstrategico = idPlanEstrategico;
    }

    public PlanEstrategico(Integer idPlanEstrategico, String descripcion, Date fechaInicio, Date fechaFin) {
        this.idPlanEstrategico = idPlanEstrategico;
        this.descripcion = descripcion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public Integer getIdPlanEstrategico() {
        return idPlanEstrategico;
    }

    public void setIdPlanEstrategico(Integer idPlanEstrategico) {
        this.idPlanEstrategico = idPlanEstrategico;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }
    
    

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    @XmlTransient
    
    
    public List<PreguntaAbierta> getPreguntaAbiertaCollection() {
        return preguntaAbiertaCollection;
    }

    public void setPreguntaAbiertaCollection(List<PreguntaAbierta> preguntaAbiertaCollection) {
        this.preguntaAbiertaCollection = preguntaAbiertaCollection;
    }
    
    public List<Criterio> getCriteriosCollection() {
        //Ordenando los criterios alfabeticamente
        if(criteriosCollection != null) {
        Collections.sort(criteriosCollection,new Comparator<Criterio>() {
        @Override
        public int compare(Criterio c1, Criterio c2) {
            return c1.getDescripcion().compareToIgnoreCase(c2.getDescripcion());
        }
         }) ;
        }else{        
        criteriosCollection = new ArrayList<>();
            };
        
        return  criteriosCollection;
    }

    public void setCriteriosCollection(List<Criterio> criteriosCollection) {
        this.criteriosCollection = criteriosCollection;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPlanEstrategico != null ? idPlanEstrategico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanEstrategico)) {
            return false;
        }
        PlanEstrategico other = (PlanEstrategico) object;
        if ((this.idPlanEstrategico == null && other.idPlanEstrategico != null) || (this.idPlanEstrategico != null && !this.idPlanEstrategico.equals(other.idPlanEstrategico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return descripcion;
    }

    @XmlTransient
    public List<Escalas> getEscalasCollection() {
        //Devolviendo las escalas del plan vigente ordenada por el valor de la escala
        //en orden descendente        
        if(escalasCollection == null)
            escalasCollection = new ArrayList<>();                
          
        Collections.sort(escalasCollection);        
        return escalasCollection;
    }

    public void setEscalasCollection(List<Escalas> escalasCollection) {
        this.escalasCollection = escalasCollection;
    }

    @XmlTransient
    public Collection<Periodo> getPeriodoCollection() {
        return periodoCollection;
    }

    public void setPeriodoCollection(Collection<Periodo> periodoCollection) {
        this.periodoCollection = periodoCollection;
    }
    
}
