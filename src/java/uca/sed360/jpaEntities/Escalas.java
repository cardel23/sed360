/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Gerardo
 */
@Entity
@Table(name = "Escalas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Escalas.findAll", query = "SELECT e FROM Escalas e"),
    @NamedQuery(name = "Escalas.findByIdEscala", query = "SELECT e FROM Escalas e WHERE e.idEscala = :idEscala"),
    @NamedQuery(name = "Escalas.findByValor", query = "SELECT e FROM Escalas e WHERE e.valor = :valor"),
    @NamedQuery(name = "Escalas.findByDescripcion", query = "SELECT e FROM Escalas e WHERE e.descripcion = :descripcion"),
    @NamedQuery(name = "Escalas.findByPlan", query = "SELECT e FROM Escalas e WHERE e.idPlanEstrategico = :plan")})
public class Escalas implements Serializable, Comparable<Escalas> {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = true)
    @Column(name = "id_escala")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer idEscala;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor")
    private double valor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "id_plan_estrategico", referencedColumnName = "id_plan_estrategico")
    @ManyToOne(optional = false)
    private PlanEstrategico idPlanEstrategico;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "escala")
    private Collection<RespuestaPreguntaCerrada> respuestaPreguntaCerradaCollection;

    public Escalas() {
    }

    public Escalas(Integer idEscala) {
        this.idEscala = idEscala;
    }

    public Escalas(Integer idEscala, double valor, String descripcion) {
        this.idEscala = idEscala;
        this.valor = valor;
        this.descripcion = descripcion;
    }

    public Integer getIdEscala() {
        return idEscala;
    }

    public void setIdEscala(Integer idEscala) {
        this.idEscala = idEscala;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {

          this.valor = valor;
   
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public PlanEstrategico getIdPlanEstrategico() {
        return idPlanEstrategico;
    }

    public void setIdPlanEstrategico(PlanEstrategico idPlanEstrategico) {
        this.idPlanEstrategico = idPlanEstrategico;
    }

    @XmlTransient
    public Collection<RespuestaPreguntaCerrada> getRespuestaPreguntaCerradaCollection() {
        return respuestaPreguntaCerradaCollection;
    }

    public void setRespuestaPreguntaCerradaCollection(Collection<RespuestaPreguntaCerrada> respuestaPreguntaCerradaCollection) {
        this.respuestaPreguntaCerradaCollection = respuestaPreguntaCerradaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEscala != null ? idEscala.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Escalas)) {
            return false;
        }
        Escalas other = (Escalas) object;
        if ((this.idEscala == null && other.idEscala != null) || (this.idEscala != null && !this.idEscala.equals(other.idEscala))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "uca.sed360.jpaEntities.Escalas[ idEscala=" + idEscala + " ]";
        return descripcion;
    }

  

    @Override
    public int compareTo(Escalas o) {
        Escalas e2 = (Escalas) o;
        
        if(this.getDescripcion().contains("N/A"))
            return 1;
        
        if(this.getValor() ==  e2.getValor())
            return 0;
        else 
            {
                if(this.getValor() > e2.getValor())
                { 
                    return -1;
                }
            else
                {
                    return 1;
                }
            }        
        
    }
    
    
    /**
     *
     */
    public static Comparator<Escalas> comparadorEscalas = new Comparator<Escalas>(){

        @Override
        public int compare(Escalas o1, Escalas o2) {
            return o1.compareTo(o2);
        }
    
    };
        
}
