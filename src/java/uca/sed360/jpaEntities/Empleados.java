/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Gerardo
 */
@Entity
@Table(name = "Empleados")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empleados.findAll", query = "SELECT e FROM Empleados e"),
    @NamedQuery(name = "Empleados.findByInss", query = "SELECT e FROM Empleados e WHERE e.inss = :inss"),
    @NamedQuery(name = "Empleados.findByCedula", query = "SELECT e FROM Empleados e WHERE e.cedula = :cedula"),
    @NamedQuery(name = "Empleados.findByNombre", query = "SELECT e FROM Empleados e WHERE e.nombre = :nombre"),
    @NamedQuery(name = "Empleados.findByApellido", query = "SELECT e FROM Empleados e WHERE e.apellido = :apellido"),
    @NamedQuery(name = "Empleados.findByCentroCosto", query = "SELECT e FROM Empleados e WHERE e.centroCosto = :centroCosto"),
    @NamedQuery(name = "Empleados.obtenerSubordinados", query = "SELECT e FROM Empleados e WHERE e.activo=true and e.inssJefearea=:Jefe and e<>:Jefe"),
    @NamedQuery(name = "Empleados.obtenerPares", query = "SELECT e FROM Empleados e WHERE e.activo=true and e.inssJefearea=:Jefe and e<>:Emp and e<>:Jefe"),
    @NamedQuery(name = "Empleados.obtenerActivos", query = "SELECT e FROM Empleados e WHERE e.activo=true"),
    @NamedQuery(name = "Empleados.obtenerActivosPorCentroCosto", query = "SELECT e FROM Empleados e WHERE e.activo=true AND e.centroCosto=:centroCostos"),
    @NamedQuery(name = "Empleados.findByUbicacion", query = "SELECT e FROM Empleados e WHERE e.ubicacion = :ubicacion"),
    @NamedQuery(name = "Empleados.findByActivo", query = "SELECT e FROM Empleados e WHERE e.activo = :activo"),
    @NamedQuery(name = "Empleados.encontrarConEvaluacionesPorPeriodo", query = "select distinct e.evaluado from Evaluacion e where e.idPeriodo.idPeriodo = :periodo")/*,
    @NamedQuery(name = "Empleados.obtenerEmpleadosConEvaluacionesEnPeriodoVigente", 
                query = "SELECT c FROM Empleados c, Evaluacion eva\n" +
                        "WHERE c.evaluacionCollection1 = eva\n" +
                        "AND eva.idPeriodo = :periodo\n" +
                        "GROUP BY c\n" +
                        "ORDER BY c.centroCosto")*/})
public class Empleados implements Serializable {
    
    @JoinColumn(name = "ubicacion", referencedColumnName = "id_ubicacion")
    @ManyToOne
    private Ubicaciones ubicacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private boolean activo;
    @JoinColumn(name = "nivel", referencedColumnName = "id_nivel")
    @ManyToOne
    private Niveles nivel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "inss")
    private String inss;
    @JoinColumn(name = "id_tipo_empleado", referencedColumnName = "id_tipoempleado")
    @ManyToOne
    private TipoEmpleado idTipoEmpleado;
    @JoinColumn(name = "centro_costo", referencedColumnName = "id_centrocostos")
    @ManyToOne
    private CentroDeCostos centroCosto;
    @JoinColumn(name = "id_cargo", referencedColumnName = "id_cargo")
    @ManyToOne
    private Cargos idCargo;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "cedula")
    private String cedula;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "apellido")
    private String apellido;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluador", fetch = FetchType.LAZY)
    private Collection<Evaluacion> evaluacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluado", fetch = FetchType.LAZY)
    private List<Evaluacion> evaluacionCollection1;
    @JoinColumn(name = "cedula", referencedColumnName = "id_usuario", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Usuarios usuarios;
    @OneToMany(mappedBy = "inssJefearea", fetch = FetchType.LAZY)
    private Collection<Empleados> subordinadosDeArea;
    @JoinColumn(name = "inss_jefearea", referencedColumnName = "inss")
    @ManyToOne
    private Empleados inssJefearea;
    @OneToMany(mappedBy = "inssJefe")
    private Collection<Empleados> empleadosCollection1;
    @JoinColumn(name = "inss_jefe", referencedColumnName = "inss")
    @ManyToOne
    private Empleados inssJefe;

    public Empleados() {
    }

    public Empleados(String cedula) {
        this.cedula = cedula;
    }

    public Empleados(String cedula, String inss, String nombre, String apellido, CentroDeCostos centroCosto) {
        this.cedula = cedula;
        this.inss = inss;
        this.nombre = nombre;
        this.apellido = apellido;
        this.centroCosto = centroCosto;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @XmlTransient
    public Collection<Evaluacion> getEvaluacionCollection() {
        return evaluacionCollection;
    }

    public void setEvaluacionCollection(Collection<Evaluacion> evaluacionCollection) {
        this.evaluacionCollection = evaluacionCollection;
    }

    @XmlTransient
    public List<Evaluacion> getEvaluacionCollection1() {
        return evaluacionCollection1;
    }

    public void setEvaluacionCollection1(List<Evaluacion> evaluacionCollection1) {
        this.evaluacionCollection1 = evaluacionCollection1;
    }

    public Usuarios getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }

    @XmlTransient
    public Collection<Empleados> getSubordinadosDeArea() {
        return subordinadosDeArea;
    }

    public void setSubordinadosDeArea(Collection<Empleados> subordinadosDeArea) {
        this.subordinadosDeArea = subordinadosDeArea;
    }

    public Empleados getInssJefearea() {
        return inssJefearea;
    }

    public void setInssJefearea(Empleados inssJefearea) {
        this.inssJefearea = inssJefearea;
    }

    @XmlTransient
    public Collection<Empleados> getEmpleadosCollection1() {
        return empleadosCollection1;
    }

    public void setEmpleadosCollection1(Collection<Empleados> empleadosCollection1) {
        this.empleadosCollection1 = empleadosCollection1;
    }

    public Empleados getInssJefe() {
        return inssJefe;
    }

    public void setInssJefe(Empleados inssJefe) {
        this.inssJefe = inssJefe;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cedula != null ? cedula.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empleados)) {
            return false;
        }
        Empleados other = (Empleados) object;
        if ((this.cedula == null && other.cedula != null) || (this.cedula != null && !this.cedula.equals(other.cedula))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "uca.sed360.jpaEntities.Empleados[ cedula=" + cedula + " ]";
        return nombre+" "+apellido;
    }

    public TipoEmpleado getIdTipoEmpleado() {
        return idTipoEmpleado;
    }

    public void setIdTipoEmpleado(TipoEmpleado idTipoEmpleado) {
        this.idTipoEmpleado = idTipoEmpleado;
    }

    public CentroDeCostos getCentroCosto() {
        return centroCosto;
    }

    public void setCentroCosto(CentroDeCostos centroCosto) {
        this.centroCosto = centroCosto;
    }

    public Cargos getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Cargos idCargo) {
        this.idCargo = idCargo;
    }

    public String getInss() {
        return inss;
    }

    public void setInss(String inss) {
        this.inss = inss;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Niveles getNivel() {
        return nivel;
    }

    public void setNivel(Niveles nivel) {
        this.nivel = nivel;
    }

    public Ubicaciones getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicaciones ubicacion) {
        this.ubicacion = ubicacion;
    }

  
    
}
