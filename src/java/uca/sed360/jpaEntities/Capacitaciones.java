/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Gerardo
 */
@Entity
@Table(name = "Capacitaciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Capacitaciones.findAll", query = "SELECT c FROM Capacitaciones c"),
    @NamedQuery(name = "Capacitaciones.findByIdCapacitacion", query = "SELECT c FROM Capacitaciones c WHERE c.idCapacitacion = :idCapacitacion"),
    @NamedQuery(name = "Capacitaciones.capacitacionesPorArea", query = "SELECT c FROM Capacitaciones c, Periodo p, Evaluacion e, CentroDeCostos o\n" +
                       "WHERE c.evaluacionCollection = e\n" +
                       "AND e.idPeriodo = p\n" +
                       "AND c.centroDeCostosList = o\n" +
                       "AND e.capacitacionesCollection = c\n" +
                       "AND o.capacitacionesList = c\n" +
                       "AND e.capacitacionesCollection = o.capacitacionesList\n" +
                       "AND p = :periodo"),
    @NamedQuery(name = "Capacitaciones.obtenerCapacitacionesPorCentro", 
        query = "SELECT cc.capacitacionesList FROM CentroDeCostos cc WHERE cc.idCentrocostos = :idCentro")
//        ,
//    @NamedQuery(name = "Capacitaciones.recomendaciones", 
//               query = "SELECT DISTINCT (c) FROM Capacitaciones c, c.evaluacionCollection e\n" +
//                       "WHERE SIZE(c.evaluacionCollection) > 0\n" +
//                       "AND e.idPeriodo = :p \n" +
//                       "ORDER BY e.evaluado.centroCosto ASC")
})
public class Capacitaciones implements Serializable {
    @JoinTable(name = "Recomendacion", joinColumns = {
        @JoinColumn(name = "id_capacitacion", referencedColumnName = "id_capacitacion")}, inverseJoinColumns = {
        @JoinColumn(name = "id_asignacion", referencedColumnName = "id_Evaluacion")})
    @ManyToMany
    private List<Evaluacion> evaluacionList;
    public static final String capacitacionRegEx="(curs[os]|capa[cs]itaci[oóò][nes]|forma[cs]i[oóò][nes])xi";
//    @JoinTable(name = "Recomendacion", joinColumns = {
//        @JoinColumn(name = "id_capacitacion", referencedColumnName = "id_capacitacion")}, inverseJoinColumns = {
//        @JoinColumn(name = "id_asignacion", referencedColumnName = "id_Evaluacion")})
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "capacitacionesCollection")
    private Collection<Evaluacion> evaluacionCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = true)
    @Column(name = "id_capacitacion")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer idCapacitacion;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "descripcion")
    private String descripcion;
    @NotNull
    @JoinTable(name = "DetalleCapacitaciones", joinColumns = {
    @JoinColumn(name = "id_capacitacion", referencedColumnName = "id_capacitacion")}, inverseJoinColumns = {
    @JoinColumn(name = "id_centrocostos", referencedColumnName = "id_centrocostos")})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<CentroDeCostos> centroDeCostosList;

    public Capacitaciones() {
    }

    public Capacitaciones(Integer idCapacitacion) {
        this.idCapacitacion = idCapacitacion;
    }

    public Capacitaciones(Integer idCapacitacion, String descripcion) {
        this.idCapacitacion = idCapacitacion;
        this.descripcion = descripcion;
    }

    public Integer getIdCapacitacion() {
        return idCapacitacion;
    }

    public void setIdCapacitacion(Integer idCapacitacion) {
        this.idCapacitacion = idCapacitacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<CentroDeCostos> getCentroDeCostosList() {
     if (centroDeCostosList == null) {
        centroDeCostosList = new ArrayList<CentroDeCostos>();
    }
        return centroDeCostosList;
    }

    public void setCentroDeCostosList(List<CentroDeCostos> centroDeCostosList) {
        if (centroDeCostosList != null) {
         this.centroDeCostosList = centroDeCostosList;
        }
       
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCapacitacion != null ? idCapacitacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Capacitaciones)) {
            return false;
        }
        Capacitaciones other = (Capacitaciones) object;
        if ((this.idCapacitacion == null && other.idCapacitacion != null) || (this.idCapacitacion != null && !this.idCapacitacion.equals(other.idCapacitacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uca.sed360.jpaEntities.Capacitaciones[ idCapacitacion=" + idCapacitacion + " ]";
        //return  String.valueOf(descripcion);
    }

    @XmlTransient
    public Collection<Evaluacion> getEvaluacionCollection() {
        return evaluacionCollection;
    }

    public void setEvaluacionCollection(Collection<Evaluacion> evaluacionCollection) {
        this.evaluacionCollection = evaluacionCollection;
    }

    @XmlTransient
    public List<Evaluacion> getEvaluacionList() {
        return evaluacionList;
    }

    public void setEvaluacionList(List<Evaluacion> evaluacionList) {
        this.evaluacionList = evaluacionList;
    }
    
}
