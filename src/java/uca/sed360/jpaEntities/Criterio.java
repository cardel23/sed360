/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.persistence.FetchType;

/**
 *
 * @author Gerardo
 */
@Entity
@Table(name = "Criterio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Criterio.findAll", query = "SELECT c FROM Criterio c"),
    @NamedQuery(name = "Criterio.findByIdCriterio", query = "SELECT c FROM Criterio c WHERE c.idCriterio = :idCriterio"),
    @NamedQuery(name="Criterio.findByPlanTipoEmp", query = "SELECT c FROM Criterio c WHERE c.planEstrategico=:plan AND c.tipoEmpleado=:tipoEmp"),
@NamedQuery(name="Criterio.countByPlanTipoEmp", query = "SELECT COUNT(c) FROM Criterio c WHERE c.planEstrategico=:plan AND c.tipoEmpleado=:tipoEmp")})
public class Criterio implements Serializable {
    @Id
    @Basic(optional = true)
    @Column(name = "id_criterio")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer idCriterio;
    @JoinColumn(name = "tipo_empleado", referencedColumnName = "id_tipoempleado")
    
    @ManyToOne(optional = false)
    private TipoEmpleado tipoEmpleado;
    private static final long serialVersionUID = 1L;
    
    
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCriterio",orphanRemoval = true, fetch=FetchType.LAZY)
    private List<PreguntaCerrada> preguntaCerradaCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "criterio", orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Ponderacion> ponderacionCollection;
    
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "plan_estrategico", referencedColumnName = "id_plan_estrategico")    
    private PlanEstrategico planEstrategico;

    public Criterio() {
    }

    public Criterio(Integer idCriterio) {
        this.idCriterio = idCriterio;
    }

    public Criterio(Integer idCriterio, String descripcion) {
        this.idCriterio = idCriterio;
        this.descripcion = descripcion;
    }

    public Integer getIdCriterio() {
        return idCriterio;
    }

    public void setIdCriterio(Integer idCriterio) {
        this.idCriterio = idCriterio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<PreguntaCerrada> getPreguntaCerradaCollection() {
        return preguntaCerradaCollection;
    }

    public void setPreguntaCerradaCollection(List<PreguntaCerrada> preguntaCerradaCollection) {
        this.preguntaCerradaCollection = preguntaCerradaCollection;
    }
    

    @XmlTransient
    public List<Ponderacion> getPonderacionCollection() {
        if(ponderacionCollection == null)
            ponderacionCollection = new ArrayList<Ponderacion>();
        return ponderacionCollection;
    }

    public void setPonderacionCollection(List<Ponderacion> ponderacionCollection) {
        this.ponderacionCollection = ponderacionCollection;
    }

    public PlanEstrategico getPlanEstrategico() {
        return planEstrategico;
    }

    public void setPlanEstrategico(PlanEstrategico planEstrategico) {
        this.planEstrategico = planEstrategico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCriterio != null ? idCriterio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Criterio)) {
            return false;
        }
        Criterio other = (Criterio) object;
        if ((this.idCriterio == null && other.idCriterio != null) || (this.idCriterio != null && !this.idCriterio.equals(other.idCriterio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return descripcion;
    }


    public TipoEmpleado getTipoEmpleado() {
        return tipoEmpleado;
    }

    public void setTipoEmpleado(TipoEmpleado tipoEmpleado) {
        this.tipoEmpleado = tipoEmpleado;
    }
    
}