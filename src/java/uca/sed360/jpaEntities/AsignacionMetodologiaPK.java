/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Jose
 */
@Embeddable
public class AsignacionMetodologiaPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_nivel")
    private int idNivel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 7)
    @Column(name = "id_periodo")
    private String idPeriodo;

    public AsignacionMetodologiaPK() {
    }

    public AsignacionMetodologiaPK(int idNivel, String idPeriodo) {
        this.idNivel = idNivel;
        this.idPeriodo = idPeriodo;
    }

    public int getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(int idNivel) {
        this.idNivel = idNivel;
    }

    public String getIdPeriodo() {
        return idPeriodo;
    }

    public void setIdPeriodo(String idPeriodo) {
        this.idPeriodo = idPeriodo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idNivel;
        hash += (idPeriodo != null ? idPeriodo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AsignacionMetodologiaPK)) {
            return false;
        }
        AsignacionMetodologiaPK other = (AsignacionMetodologiaPK) object;
        if (this.idNivel != other.idNivel) {
            return false;
        }
        if ((this.idPeriodo == null && other.idPeriodo != null) || (this.idPeriodo != null && !this.idPeriodo.equals(other.idPeriodo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uca.sed360.jpaEntities.AsignacionMetodologiaPK[ idNivel=" + idNivel + ", idPeriodo=" + idPeriodo + " ]";
    }
    
}
