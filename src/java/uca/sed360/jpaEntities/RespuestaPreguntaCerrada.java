/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gerardo
 */
@Entity
@Table(name = "RespuestaPreguntaCerrada")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RespuestaPreguntaCerrada.findAll", query = "SELECT r FROM RespuestaPreguntaCerrada r"),
    @NamedQuery(name = "RespuestaPreguntaCerrada.findByPregunta", query = "SELECT r FROM RespuestaPreguntaCerrada r WHERE r.respuestaPreguntaCerradaPK.pregunta = :pregunta"),
    @NamedQuery(name= "RespuestaPreguntaCerrada.countByPreguntaCerradaNivel360",query = "SELECT COUNT(r) FROM RespuestaPreguntaCerrada r WHERE r.preguntaCerrada=:pc AND r.evaluacion.idNivel360=:nivel360"),
    @NamedQuery(name = "RespuestaPreguntaCerrada.findByAsignacion", query = "SELECT r FROM RespuestaPreguntaCerrada r WHERE r.respuestaPreguntaCerradaPK.asignacion = :asignacion"),
    @NamedQuery(name = "RespuestaPreguntaCerrada.ObtenerPorCriterioNivel", query ="SELECT count(rpc) FROM RespuestaPreguntaCerrada rpc where RPC.preguntaCerrada.idCriterio.idCriterio = :idCriterio\n" +
                       "AND RPC.evaluacion.idNivel360.idNivel360 = :idNivel")})
public class RespuestaPreguntaCerrada implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RespuestaPreguntaCerradaPK respuestaPreguntaCerradaPK;
    @JoinColumn(name = "pregunta", referencedColumnName = "id_pregunta", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private PreguntaCerrada preguntaCerrada;
    @JoinColumn(name = "asignacion", referencedColumnName = "id_Evaluacion", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Evaluacion evaluacion;
    @JoinColumn(name = "escala", referencedColumnName = "id_escala")
    @ManyToOne(optional = false)
    private Escalas escala;

    public RespuestaPreguntaCerrada() {
    }

    public RespuestaPreguntaCerrada(RespuestaPreguntaCerradaPK respuestaPreguntaCerradaPK) {
        this.respuestaPreguntaCerradaPK = respuestaPreguntaCerradaPK;
    }

    public RespuestaPreguntaCerrada(String pregunta, String asignacion) {
        this.respuestaPreguntaCerradaPK = new RespuestaPreguntaCerradaPK(pregunta, asignacion);
    }

    public RespuestaPreguntaCerrada(PreguntaCerrada preguntaCerrada, Escalas escala) {
        this.preguntaCerrada = preguntaCerrada;
        this.escala = escala;
    }

    public RespuestaPreguntaCerradaPK getRespuestaPreguntaCerradaPK() {
        return respuestaPreguntaCerradaPK;
    }

    public void setRespuestaPreguntaCerradaPK(RespuestaPreguntaCerradaPK respuestaPreguntaCerradaPK) {
        this.respuestaPreguntaCerradaPK = respuestaPreguntaCerradaPK;
    }

    public PreguntaCerrada getPreguntaCerrada() {
        return preguntaCerrada;
    }

    public void setPreguntaCerrada(PreguntaCerrada preguntaCerrada) {
        this.preguntaCerrada = preguntaCerrada;
    }

    public Evaluacion getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(Evaluacion evaluacion) {
        this.evaluacion = evaluacion;
    }

    public Escalas getEscala() {
        return escala;
    }

    public void setEscala(Escalas escala) {
        this.escala = escala;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (respuestaPreguntaCerradaPK != null ? respuestaPreguntaCerradaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestaPreguntaCerrada)) {
            return false;
        }
        RespuestaPreguntaCerrada other = (RespuestaPreguntaCerrada) object;
        if ((this.respuestaPreguntaCerradaPK == null && other.respuestaPreguntaCerradaPK != null) || (this.respuestaPreguntaCerradaPK != null && !this.respuestaPreguntaCerradaPK.equals(other.respuestaPreguntaCerradaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "uca.sed360.jpaEntities.RespuestaPreguntaCerrada[ respuestaPreguntaCerradaPK=" + respuestaPreguntaCerradaPK + " ]";
        return escala.toString();
    }
    
}
 