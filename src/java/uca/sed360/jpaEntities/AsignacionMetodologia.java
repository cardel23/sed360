/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jose
 */
@Entity
@Table(name = "AsignacionMetodologia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AsignacionMetodologia.findAll", query = "SELECT a FROM AsignacionMetodologia a"),
    @NamedQuery(name = "AsignacionMetodologia.findByIdNivel", query = "SELECT a FROM AsignacionMetodologia a WHERE a.asignacionMetodologiaPK.idNivel = :idNivel"),
    @NamedQuery(name = "AsignacionMetodologia.findByPeriodoNivelEmp", query = "SELECT a FROM AsignacionMetodologia a WHERE a.periodo = :periodo AND a.niveles=:nivelEmp"),
    @NamedQuery(name = "AsignacionMetodologia.findByMetodologia", query = "SELECT a FROM AsignacionMetodologia a WHERE a.metodologia = :metodologia")})
public class AsignacionMetodologia implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AsignacionMetodologiaPK asignacionMetodologiaPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "metodologia")
    private String metodologia;
    @JoinColumn(name = "id_periodo", referencedColumnName = "id_periodo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Periodo periodo;
    @JoinColumn(name = "id_nivel", referencedColumnName = "id_nivel", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Niveles niveles;

    public AsignacionMetodologia() {
    }

    public AsignacionMetodologia(AsignacionMetodologiaPK asignacionMetodologiaPK) {
        this.asignacionMetodologiaPK = asignacionMetodologiaPK;
    }

    public AsignacionMetodologia(AsignacionMetodologiaPK asignacionMetodologiaPK, String metodologia) {
        this.asignacionMetodologiaPK = asignacionMetodologiaPK;
        this.metodologia = metodologia;
    }

    public AsignacionMetodologia(int idNivel, String idPeriodo) {
        this.asignacionMetodologiaPK = new AsignacionMetodologiaPK(idNivel, idPeriodo);
    }

    public AsignacionMetodologiaPK getAsignacionMetodologiaPK() {
        return asignacionMetodologiaPK;
    }

    public void setAsignacionMetodologiaPK(AsignacionMetodologiaPK asignacionMetodologiaPK) {
        this.asignacionMetodologiaPK = asignacionMetodologiaPK;
    }

    public String getMetodologia() {
        if(metodologia!=null)
            return metodologia.trim();
        return metodologia;
    }

    public void setMetodologia(String metodologia) {
        this.metodologia = metodologia;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    public Niveles getNiveles() {
        return niveles;
    }

    public void setNiveles(Niveles niveles) {
        this.niveles = niveles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (asignacionMetodologiaPK != null ? asignacionMetodologiaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AsignacionMetodologia)) {
            return false;
        }
        AsignacionMetodologia other = (AsignacionMetodologia) object;
        if ((this.asignacionMetodologiaPK == null && other.asignacionMetodologiaPK != null) || (this.asignacionMetodologiaPK != null && !this.asignacionMetodologiaPK.equals(other.asignacionMetodologiaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uca.sed360.jpaEntities.AsignacionMetodologia[ asignacionMetodologiaPK=" + asignacionMetodologiaPK + " ]";
    }
    
}
