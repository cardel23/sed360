/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rrhh
 */
@Entity
@Table(name = "DetallePreguntaCerrada")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetallePreguntaCerrada.findAll", query = "SELECT dt FROM DetallePreguntaCerrada dt"),
    @NamedQuery(name = "DetallePreguntaCerrada.findByIdPregunta", query = "SELECT dt FROM DetallePreguntaCerrada dt WHERE dt.detallePreguntaCerradaPK = :idPregunta"),
    @NamedQuery(name="DetallePreguntaCerrada.findByPlanTipoEmpNivel",query = "SELECT dt FROM DetallePreguntaCerrada dt WHERE dt.preguntaCerrada.idCriterio.planEstrategico=:plan AND dt.preguntaCerrada.idCriterio.tipoEmpleado=:tipoEmp AND dt.nivel360=:nivel360 "),
    @NamedQuery(name="DetallePreguntaCerrada.countByPlanTipoEmpNivel",query = "SELECT COUNT(dt) FROM DetallePreguntaCerrada dt WHERE dt.preguntaCerrada.idCriterio.planEstrategico=:plan AND dt.preguntaCerrada.idCriterio.tipoEmpleado=:tipoEmp AND dt.nivel360=:nivel360 AND dt.descripcion  NOT  like 'Pendiente%'")})


public class DetallePreguntaCerrada implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DetallePreguntaCerradaPK detallePreguntaCerradaPK;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "Descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor")
    private double valor;
    @JoinColumn(name = "id_pregunta", referencedColumnName = "id_pregunta", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private PreguntaCerrada preguntaCerrada;
    @JoinColumn(name = "id_Nivel360", referencedColumnName = "id_Nivel360", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Nivel360 nivel360;

    public DetallePreguntaCerrada() {
    }

    public DetallePreguntaCerrada(DetallePreguntaCerradaPK detallePreguntaCerradaPK) {
        this.detallePreguntaCerradaPK = detallePreguntaCerradaPK;
    }

    public DetallePreguntaCerrada(DetallePreguntaCerradaPK detallePreguntaCerradaPK, String descripcion, double valor) {
        this.detallePreguntaCerradaPK = detallePreguntaCerradaPK;
        this.descripcion = descripcion;
        this.valor = valor;
    }

    public DetallePreguntaCerrada(String idPregunta, int idNivel360) {
        this.detallePreguntaCerradaPK = new DetallePreguntaCerradaPK(idPregunta, idNivel360);
    }

    public DetallePreguntaCerradaPK getDetallePreguntaCerradaPK() {
        return detallePreguntaCerradaPK;
    }

    public void setDetallePreguntaCerradaPK(DetallePreguntaCerradaPK detallePreguntaCerradaPK) {
        this.detallePreguntaCerradaPK = detallePreguntaCerradaPK;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public PreguntaCerrada getPreguntaCerrada() {
        return preguntaCerrada;
    }

    public void setPreguntaCerrada(PreguntaCerrada preguntaCerrada) {
        this.preguntaCerrada = preguntaCerrada;
    }

    public Nivel360 getNivel360() {
        return nivel360;
    }

    public void setNivel360(Nivel360 nivel360) {
        this.nivel360 = nivel360;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detallePreguntaCerradaPK != null ? detallePreguntaCerradaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetallePreguntaCerrada)) {
            return false;
        }
        DetallePreguntaCerrada other = (DetallePreguntaCerrada) object;
        if ((this.detallePreguntaCerradaPK == null && other.detallePreguntaCerradaPK != null) || (this.detallePreguntaCerradaPK != null && !this.detallePreguntaCerradaPK.equals(other.detallePreguntaCerradaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uca.sed360.jpaEntities.DetallePreguntaCerrada[ detallePreguntaCerradaPK=" + detallePreguntaCerradaPK + " ]";
    }
    
}
