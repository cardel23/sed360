/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


/**
 *
 * @author Nelson
 */
@Entity
@Table(name = "Evaluacion")
@XmlRootElement
@NamedNativeQueries(
        {        }
        )
@NamedQueries({
    @NamedQuery(name = "Evaluacion.findAll", query = "SELECT e FROM Evaluacion e"),
    @NamedQuery(name= "Evaluacion.countEvaluacionByTipoEmpPlan",query = "SELECT COUNT(e) FROM Evaluacion e WHERE e.evaluado.idTipoEmpleado=:tipoEmp AND e.idPeriodo.idPlanEstrategico=:planEstrategico"),
    @NamedQuery(name= "Evaluacion.countEvaluacionByTipoEmpPlanNivel",query = "SELECT COUNT(e) FROM Evaluacion e WHERE e.evaluado.idTipoEmpleado=:tipoEmp AND e.idPeriodo.idPlanEstrategico=:planEstrategico AND e.idNivel360=:nivel360"),
    @NamedQuery(name= "Evaluacion.countEvaluacionByPeriodo",query = "SELECT COUNT(e) FROM Evaluacion e WHERE e.idPeriodo=:periodo"),
     @NamedQuery(name= "Evaluacion.countEvaluacionByPeriodoCentroCostos",query = "SELECT COUNT(e) FROM Evaluacion e WHERE e.idPeriodo=:periodo AND e.evaluado.centroCosto=:centroCosto"),
    @NamedQuery(name= "Evaluacion.findEvaluacionByPeriodo",query = "SELECT e FROM Evaluacion e WHERE e.idPeriodo=:periodo"),
    @NamedQuery(name = "Evaluacion.findByIdEvaluacion", query = "SELECT e FROM Evaluacion e WHERE e.idEvaluacion = :idEvaluacion"),
    @NamedQuery(name = "Evaluacion.findByPeriodoVigente", query = "SELECT e FROM Evaluacion e WHERE e.idPeriodo = :periodoVigente AND e.idNivel360=:idNivel360"),
    @NamedQuery(name = "Evaluacion.findByPlan", query = "SELECT ev FROM Evaluacion ev JOIN ev.idPeriodo pe JOIN pe.idPlanEstrategico pl where pl = :planVigente"),
    @NamedQuery(name = "Evaluacion.findByEvaluadorEvaluadoPeriodo",query = "SELECT e FROM Evaluacion e WHERE e.evaluador = :evaluador and e.evaluado = :evaluado and e.idPeriodo=:periodo"),
    @NamedQuery(name = "Evaluacion.findEvaluacionesSubordinadosPendientes",query = "select e from Empleados e\n" +
                       "where not exists (select a from Evaluacion a\n" +
                       "where e.inssJefe = a.evaluador and e = a.evaluado\n" +
                       "and a.idPeriodo=:periodo)\n" +
                       "and e.inssJefe is not null"),
    @NamedQuery(name = "Evaluacion.findAutoevaluacionesPendientes",query = "select e from Empleados e\n" +
                       "where not exists (select a from Evaluacion a\n" +
                       "where e = a.evaluador and e = a.evaluado\n" +
                       "and a.idPeriodo.idPeriodo=:periodo)\n" +
                       "and e.inssJefe is not null"),
    @NamedQuery(name = "Evaluacion.puntajeEvaluacion", query = "SELECT e,p,r,c,rc,pd"
                        + " FROM Criterio c, PreguntaCerrada p, DetallePreguntaCerrada r, \n" +
                        "Evaluacion e, RespuestaPreguntaCerrada rc, Ponderacion pd, Escalas es\n" +
                        "WHERE c.preguntaCerradaCollection = p\n" +
                        "AND r.preguntaCerrada = p\n" +
                        "AND r.nivel360 = e.idNivel360\n" +
                        "AND e.respuestaPreguntaCerradaCollection = rc\n" +
                        "AND rc.preguntaCerrada = p\n" +
                        "AND pd.criterio = c AND pd.nivel360 = e.idNivel360\n" +
                        "AND rc.escala = es\n" +
                       "AND e.evaluado = :evaluado\n" +
                       "AND e.evaluador = :evaluador\n" +
                       "AND e.idPeriodo = :periodo"),
//                        "SELECT e FROM Evaluacion e WHERE e.evaluado = :evaluado AND e.evaluador = :evaluador AND e.idPeriodo = :periodo"),
    @NamedQuery(name = "Evaluacion.obtenerPreguntasAbiertas", query="SELECT e.respuestaPreguntaAbiertaCollection FROM Evaluacion e where e.idEvaluacion = :idEvaluacion"),
    @NamedQuery(name = "Evaluacion.obtenerEvaluacionImpresa", query= "SELECT e FROM Evaluacion e \n" +
                       "WHERE e.impresa =:impresa \n" +
                       " AND e.idPeriodo.idPeriodo =:periodo"),
    @NamedQuery(name = "Evaluacion.obtenerNumEvaluacionImpresaPeriodoCentroCosto", query= "SELECT COUNT(e) FROM Evaluacion e WHERE e.impresa =TRUE AND  e.idPeriodo =:periodo AND e.evaluado.centroCosto=:centroCosto"),
        @NamedQuery(name = "Evaluacion.obtenerNumEvaluacionImpresaPeriodo", query= "SELECT COUNT(e) FROM Evaluacion e WHERE e.impresa =TRUE AND  e.idPeriodo =:periodo"),
    @NamedQuery(name = "Evaluacion.obtenerEvaluacionNivel", query = "SELECT e FROM Evaluacion e WHERE e.evaluado = :evaluador AND e.idNivel360 = :nivel AND e.idPeriodo = :periodo"),
    @NamedQuery(name = "Evaluacion.evaluacionesParaPromedio",
        query = "SELECT e ,SUM(r.escala.valor*d.valor)AS valor FROM Evaluacion e, \n" +
                "RespuestaPreguntaCerrada R,\n" +
                "PreguntaCerrada p, DetallePreguntaCerrada d\n" +
                "WHERE p=r.preguntaCerrada\n" +
                "AND e.respuestaPreguntaCerradaCollection = R\n" +
                "AND d.preguntaCerrada = p\n" +
                "AND d.nivel360 = e.idNivel360\n" +
                "GROUP BY e\n" +
                "ORDER BY valor DESC")})
public class Evaluacion implements Serializable {
    @Column(name = "fecha_evaluacion")
    @Temporal(TemporalType.DATE)
    private Date fechaEvaluacion;
    @Column(name = "impresa")
    private Boolean impresa;
    @ManyToMany(mappedBy = "evaluacionList")
    private List<Capacitaciones> capacitacionesList;
    @JoinColumn(name = "id_Nivel360", referencedColumnName = "id_Nivel360")
    @ManyToOne(optional = false)
    private Nivel360 idNivel360;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_Evaluacion")
    private String idEvaluacion;
    @JoinTable(name = "Recomendacion", joinColumns = {
    @JoinColumn(name = "id_asignacion", referencedColumnName = "id_Evaluacion")},inverseJoinColumns = {
        @JoinColumn(name = "id_capacitacion", referencedColumnName = "id_capacitacion")}
    )
//    @ManyToMany(mappedBy = "evaluacionCollection", fetch=FetchType.LAZY)
    @ManyToMany (fetch = FetchType.LAZY)
    private Collection<Capacitaciones> capacitacionesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacion", fetch=FetchType.LAZY)
    private Collection<RespuestaPreguntaAbierta> respuestaPreguntaAbiertaCollection;
    @JoinColumn(name = "id_periodo", referencedColumnName = "id_periodo")
    @ManyToOne(optional = false)
    private Periodo idPeriodo;
    @JoinColumn(name = "evaluador", referencedColumnName = "inss")
    @ManyToOne(optional = false)
    private Empleados evaluador;
    @JoinColumn(name = "evaluado", referencedColumnName = "inss")
    @ManyToOne(optional = false)
    private Empleados evaluado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluacion", fetch = FetchType.LAZY)
    private List<RespuestaPreguntaCerrada> respuestaPreguntaCerradaCollection;
    @Basic(optional = false)
    @Column(name = "centro_costo")
    private String CentroCosto;
            
    public Evaluacion() {
    }

    public Evaluacion(String idEvaluacion) {
        this.idEvaluacion = idEvaluacion;
    }

    public String getIdEvaluacion() {
        return idEvaluacion;
    }

    public void setIdEvaluacion(String idEvaluacion) {
        this.idEvaluacion = idEvaluacion;
    }

    @XmlTransient
    public Collection<Capacitaciones> getCapacitacionesCollection() {
        return capacitacionesCollection;
    }

    public void setCapacitacionesCollection(Collection<Capacitaciones> capacitacionesCollection) {
        this.capacitacionesCollection = capacitacionesCollection;
    }

    @XmlTransient
    public Collection<RespuestaPreguntaAbierta> getRespuestaPreguntaAbiertaCollection() {
        return respuestaPreguntaAbiertaCollection;
    }

    public void setRespuestaPreguntaAbiertaCollection(Collection<RespuestaPreguntaAbierta> respuestaPreguntaAbiertaCollection) {
        this.respuestaPreguntaAbiertaCollection = respuestaPreguntaAbiertaCollection;
    }

    public Periodo getIdPeriodo() {
        return idPeriodo;
    }

    public void setIdPeriodo(Periodo idPeriodo) {
        this.idPeriodo = idPeriodo;
    }


    public Empleados getEvaluador() {
        return evaluador;
    }

    public void setEvaluador(Empleados evaluador) {
        this.evaluador = evaluador;
    }

    public Empleados getEvaluado() {
        return evaluado;
    }

    public void setEvaluado(Empleados evaluado) {
        this.evaluado = evaluado;
    }

    public String getCentroCosto() {
        return CentroCosto;
    }

    public void setCentroCosto(String CentroCosto) {
        this.CentroCosto = CentroCosto;
    }
    
    @XmlTransient
    public List<RespuestaPreguntaCerrada> getRespuestaPreguntaCerradaCollection() {
        return respuestaPreguntaCerradaCollection;
    }

    public void setRespuestaPreguntaCerradaCollection(List<RespuestaPreguntaCerrada> respuestaPreguntaCerradaCollection) {
        this.respuestaPreguntaCerradaCollection = respuestaPreguntaCerradaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEvaluacion != null ? idEvaluacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evaluacion)) {
            return false;
        }
        Evaluacion other = (Evaluacion) object;
        if ((this.idEvaluacion == null && other.idEvaluacion != null) || (this.idEvaluacion != null && !this.idEvaluacion.equals(other.idEvaluacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "uca.sed360.jpaEntities.Evaluacion[ idEvaluacion=" + idEvaluacion + " ]";
        return idEvaluacion;
    }

    /**
     * @return the impresa
     */
    public Boolean getImpresa() {
        return impresa;
    }

    /**
     * @param impresa the impresa to set
     */
    public void setImpresa(Boolean impresa) {
        this.impresa = impresa;
    }

    public boolean isImpresa(){
        return impresa;
    }
    
    @XmlTransient
    public List<Capacitaciones> getCapacitacionesList() {
        return capacitacionesList;
    }

    public void setCapacitacionesList(List<Capacitaciones> capacitacionesList) {
        this.capacitacionesList = capacitacionesList;
    }

    public Nivel360 getIdNivel360() {
        return idNivel360;
    }

    public void setIdNivel360(Nivel360 idNivel360) {
        this.idNivel360 = idNivel360;
    }

    public Date getFechaEvaluacion() {
        return fechaEvaluacion;
    }

    public void setFechaEvaluacion(Date fechaEvaluacion) {
        this.fechaEvaluacion = fechaEvaluacion;
    }
    
}
