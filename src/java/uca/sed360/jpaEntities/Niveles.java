/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jose
 */
@Entity
@Table(name = "Niveles")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Niveles.findAll", query = "SELECT n FROM Niveles n"),
    @NamedQuery(name = "Niveles.findByIdNivel", query = "SELECT n FROM Niveles n WHERE n.idNivel = :idNivel"),
    @NamedQuery(name = "Niveles.findByDesc", query = "SELECT n FROM Niveles n WHERE n.descripcion = :desc")})
public class Niveles implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = true)
    @Column(name = "id_nivel")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idNivel;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "niveles")
    private List<AsignacionMetodologia> asignacionMetodologiaList;
    @OneToMany(mappedBy = "nivel")
    private List<Empleados> empleadosList;

    public Niveles() {
    }

    public Niveles(Integer idNivel) {
        this.idNivel = idNivel;
    }

    public Niveles(Integer idNivel, String descripcion) {
        this.idNivel = idNivel;
        this.descripcion = descripcion;
    }

    public Integer getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(Integer idNivel) {
        this.idNivel = idNivel;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Empleados> getEmpleadosList() {
        return empleadosList;
    }

    public void setEmpleadosList(List<Empleados> empleadosList) {
        this.empleadosList = empleadosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNivel != null ? idNivel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Niveles)) {
            return false;
        }
        Niveles other = (Niveles) object;
        if ((this.idNivel == null && other.idNivel != null) || (this.idNivel != null && !this.idNivel.equals(other.idNivel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uca.sed360.jpaEntities.Niveles[ idNivel=" + idNivel + " ]";
    }

    @XmlTransient
    public List<AsignacionMetodologia> getAsignacionMetodologiaList() {
        return asignacionMetodologiaList;
    }

    public void setAsignacionMetodologiaList(List<AsignacionMetodologia> asignacionMetodologiaList) {
        this.asignacionMetodologiaList = asignacionMetodologiaList;
    }
    
}
