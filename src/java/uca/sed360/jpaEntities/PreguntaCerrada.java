/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Gerardo
 */
@Entity
@Table(name = "PreguntaCerrada")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PreguntaCerrada.findAll", query = "SELECT p FROM PreguntaCerrada p"),
    @NamedQuery(name = "PreguntaCerrada.findByPlan", query = "SELECT pe FROM PreguntaCerrada pe JOIN pe.idCriterio cr JOIN cr.planEstrategico pl WHERE pl = :planVigente"),
    @NamedQuery(name = "PreguntaCerrada.findByIdPregunta", query = "SELECT p FROM PreguntaCerrada p WHERE p.idPregunta = :idPregunta")})
public class PreguntaCerrada implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false) 
    @Column(name = "id_pregunta", updatable = true)
    private String idPregunta;
    @JoinColumn(name = "id_criterio", referencedColumnName = "id_criterio")
    @ManyToOne(optional = false)
    private Criterio idCriterio;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "preguntaCerrada",orphanRemoval = true, fetch=FetchType.LAZY)
    private List<DetallePreguntaCerrada> DetallePreguntaCerradaCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "preguntaCerrada", fetch=FetchType.LAZY)
    private List<RespuestaPreguntaCerrada> respuestaPreguntaCerradaCollection;

    public PreguntaCerrada() {
    }

    public PreguntaCerrada(String idPregunta) {
        this.idPregunta = idPregunta;
    }
    
    public String getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(String idPregunta) {
        this.idPregunta = idPregunta;
    }

    public Criterio getIdCriterio() {
        return idCriterio;
    }

    public void setIdCriterio(Criterio idCriterio) {
        this.idCriterio = idCriterio;
    }

    @XmlTransient
    public List<DetallePreguntaCerrada> getDetallePreguntaCerradaCollection() {
        return DetallePreguntaCerradaCollection;
    }

    public void setDetallePreguntaCerradaCollection(List<DetallePreguntaCerrada> DetallePreguntaCerradaCollection) {
        this.DetallePreguntaCerradaCollection = DetallePreguntaCerradaCollection;
    }

    @XmlTransient
    public List<RespuestaPreguntaCerrada> getRespuestaPreguntaCerradaCollection() {
        return respuestaPreguntaCerradaCollection;
    }

    public void setRespuestaPreguntaCerradaCollection(List<RespuestaPreguntaCerrada> respuestaPreguntaCerradaCollection) {
        this.respuestaPreguntaCerradaCollection = respuestaPreguntaCerradaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPregunta != null ? idPregunta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PreguntaCerrada)) {
            return false;
        }
        PreguntaCerrada other = (PreguntaCerrada) object;
        if ((this.idPregunta == null && other.idPregunta != null) || (this.idPregunta != null && !this.idPregunta.equals(other.idPregunta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uca.sed360.jpaEntities.PreguntaCerrada[ idPregunta=" + idPregunta + " ]";
    }
    
}
