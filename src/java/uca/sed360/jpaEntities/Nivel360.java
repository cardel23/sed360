/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author rrhh
 */
@Entity
@Table(name = "Nivel360")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nivel360.findAll", query = "SELECT i FROM Nivel360 i"),
    @NamedQuery(name = "Nivel360.findByIdNivel360", query = "SELECT i FROM Nivel360 i WHERE i.idNivel360 = :idNivel360"),
    @NamedQuery(name = "Nivel360.eliminarNivel360Adicionales", query = "DELETE FROM Nivel360 i where i.idNivel360 NOT IN (1,2,3,4)")})  

public class Nivel360 implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idNivel360")
    private List<Evaluacion> evaluacionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nivel360")
    private List<PreguntaAbierta> preguntaAbiertaList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_Nivel360")
    private Integer idNivel360;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nivel360")
    private List<Ponderacion> ponderacionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nivel360")
    private List<DetallePreguntaCerrada> detallePreguntaCerradaList;
    public final static Nivel360 AUTOEVALUACION=new Nivel360(1, "Autoevaluación"),
                            SUBORDINADO=new Nivel360(2, "Subordinado"),
                            PAR=new Nivel360(3, "Par"),
                            SUPERIOR=new Nivel360(4, "Superior");

    public Nivel360() {
    }

    public Nivel360(Integer idNivel360) {
        this.idNivel360 = idNivel360;
    }

    public Nivel360(Integer idNivel360, String descripcion) {
        this.idNivel360 = idNivel360;
        this.descripcion = descripcion;
    }

    public Integer getIdNivel360() {
        return idNivel360;
    }

    public void setIdNivel360(Integer idNivel360) {
        this.idNivel360 = idNivel360;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
     public static List<Nivel360>  getConstantNiveles360(){
        List<Nivel360> listaNiveles360=new ArrayList<Nivel360>();
        listaNiveles360.add(AUTOEVALUACION);
        listaNiveles360.add(SUBORDINADO);
        listaNiveles360.add(PAR);
        listaNiveles360.add(SUPERIOR);
        return listaNiveles360;
    }

    @XmlTransient
    public List<Ponderacion> getPonderacionList() {
        return ponderacionList;
    }

    public void setPonderacionList(List<Ponderacion> ponderacionList) {
        this.ponderacionList = ponderacionList;
    }

    @XmlTransient
    public List<DetallePreguntaCerrada> getDetallePreguntaCerradaList() {
        return detallePreguntaCerradaList;
    }

    public void setDetallePreguntaCerradaList(List<DetallePreguntaCerrada> detallePreguntaCerradaList) {
        this.detallePreguntaCerradaList = detallePreguntaCerradaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNivel360 != null ? idNivel360.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nivel360)) {
            return false;
        }
        Nivel360 other = (Nivel360) object;
        if ((this.idNivel360 == null && other.idNivel360 != null) || (this.idNivel360 != null && !this.idNivel360.equals(other.idNivel360))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uca.sed360.jpaEntities.Nivel360[ idNivel360=" + idNivel360 + " ]";
    }

    @XmlTransient
    public List<Evaluacion> getEvaluacionList() {
        return evaluacionList;
    }

    public void setEvaluacionList(List<Evaluacion> evaluacionList) {
        this.evaluacionList = evaluacionList;
    }

    @XmlTransient
    public List<PreguntaAbierta> getPreguntaAbiertaList() {
        return preguntaAbiertaList;
    }

    public void setPreguntaAbiertaList(List<PreguntaAbierta> preguntaAbiertaList) {
        this.preguntaAbiertaList = preguntaAbiertaList;
    }
    
}
