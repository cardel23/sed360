/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rrhh
 */
@Embeddable
public class PonderacionPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_criterio")
    private int idCriterio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_Nivel360")
    private int idNivel360;

    public PonderacionPK() {
    }

    public PonderacionPK(int idCriterio, int idNivel360) {
        this.idCriterio = idCriterio;
        this.idNivel360 = idNivel360;
    }

    public int getIdCriterio() {
        return idCriterio;
    }

    public void setIdCriterio(int idCriterio) {
        this.idCriterio = idCriterio;
    }

    public int getIdNivel360() {
        return idNivel360;
    }

    public void setIdNivel360(int idNivel360) {
        this.idNivel360 = idNivel360;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idCriterio;
        hash += (int) idNivel360;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PonderacionPK)) {
            return false;
        }
        PonderacionPK other = (PonderacionPK) object;
        if (this.idCriterio != other.idCriterio) {
            return false;
        }
        if (this.idNivel360 != other.idNivel360) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uca.sed360.jpaEntities.PonderacionPK[ idCriterio=" + idCriterio + ", idNivel360=" + idNivel360 + " ]";
    }
    
}
