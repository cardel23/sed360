/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jose
 */
public class CatalogoMetodologias {
  private String descripcion;
  public static final String   METODOLOGIA_360="360",
                                METODOLOGIA_180="180",
                                METODOLOGIA_HIBRIDA="Hibrida";  
  public CatalogoMetodologias(){
      
  }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
  
    public static List<String> getListaMetologias(){
        List<String> metodologias=new ArrayList<String>();
        metodologias.add(METODOLOGIA_360);
        metodologias.add(METODOLOGIA_180);
        metodologias.add(METODOLOGIA_HIBRIDA);
        return metodologias;
    }
  
}
