/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gerardo
 */
@Entity
@Table(name = "RespuestaPreguntaAbierta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RespuestaPreguntaAbierta.findAll", query = "SELECT r FROM RespuestaPreguntaAbierta r"),
    @NamedQuery(name = "RespuestaPreguntaAbierta.findByPregunta", query = "SELECT r FROM RespuestaPreguntaAbierta r WHERE r.respuestaPreguntaAbiertaPK.pregunta = :pregunta"),
    @NamedQuery(name = "RespuestaPreguntaAbierta.findPregunta", query = "SELECT r FROM RespuestaPreguntaAbierta r WHERE r.preguntaAbierta = :pregunta and r.evaluacion.idEvaluacion = :idEvaluacion"),
    @NamedQuery(name = "RespuestaPreguntaAbierta.findByAsignacion", query = "SELECT r FROM RespuestaPreguntaAbierta r WHERE r.respuestaPreguntaAbiertaPK.asignacion = :asignacion")})
public class RespuestaPreguntaAbierta implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RespuestaPreguntaAbiertaPK respuestaPreguntaAbiertaPK;
    @Basic(optional = true)
    @Lob
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "pregunta", referencedColumnName = "id_pregunta", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private PreguntaAbierta preguntaAbierta;
    @JoinColumn(name = "asignacion", referencedColumnName = "id_Evaluacion", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Evaluacion evaluacion;

    public RespuestaPreguntaAbierta() {
    }

    public RespuestaPreguntaAbierta(RespuestaPreguntaAbiertaPK respuestaPreguntaAbiertaPK) {
        this.respuestaPreguntaAbiertaPK = respuestaPreguntaAbiertaPK;
    }

    public RespuestaPreguntaAbierta(RespuestaPreguntaAbiertaPK respuestaPreguntaAbiertaPK, String descripcion) {
        this.respuestaPreguntaAbiertaPK = respuestaPreguntaAbiertaPK;
        this.descripcion = descripcion;
    }

    public RespuestaPreguntaAbierta(int pregunta, String asignacion) {
        this.respuestaPreguntaAbiertaPK = new RespuestaPreguntaAbiertaPK(pregunta, asignacion);
    }

    public RespuestaPreguntaAbiertaPK getRespuestaPreguntaAbiertaPK() {
        return respuestaPreguntaAbiertaPK;
    }

    public void setRespuestaPreguntaAbiertaPK(RespuestaPreguntaAbiertaPK respuestaPreguntaAbiertaPK) {
        this.respuestaPreguntaAbiertaPK = respuestaPreguntaAbiertaPK;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public PreguntaAbierta getPreguntaAbierta() {
        return preguntaAbierta;
    }

    public void setPreguntaAbierta(PreguntaAbierta preguntaAbierta) {
        this.preguntaAbierta = preguntaAbierta;
    }

    public Evaluacion getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(Evaluacion evaluacion) {
        this.evaluacion = evaluacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (respuestaPreguntaAbiertaPK != null ? respuestaPreguntaAbiertaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestaPreguntaAbierta)) {
            return false;
        }
        RespuestaPreguntaAbierta other = (RespuestaPreguntaAbierta) object;
        if ((this.respuestaPreguntaAbiertaPK == null && other.respuestaPreguntaAbiertaPK != null) || (this.respuestaPreguntaAbiertaPK != null && !this.respuestaPreguntaAbiertaPK.equals(other.respuestaPreguntaAbiertaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uca.sed360.jpaEntities.RespuestaPreguntaAbierta[ respuestaPreguntaAbiertaPK=" + respuestaPreguntaAbiertaPK + " ]";
    }
    
}
