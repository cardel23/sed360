/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.jpaEntities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Gerardo
 */
@Entity
@Table(name = "Periodo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Periodo.findAll", query = "SELECT p FROM Periodo p"),
    @NamedQuery(name = "Periodo.findByIdPeriodo", query = "SELECT p FROM Periodo p WHERE p.idPeriodo = :idPeriodo"),
    @NamedQuery(name = "Periodo.findByFechaInicio", query = "SELECT p FROM Periodo p WHERE p.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "Periodo.periodoVigente", query = "SELECT p FROM Periodo p WHERE p.fechaInicio <= CURRENT_DATE AND p.fechaFin >= CURRENT_DATE"),
    @NamedQuery(name = "Periodo.findByFechas", query = "SELECT p FROM Periodo p WHERE p.fechaInicio <= :fechaInicio AND p.fechaFin >= :fechaFin"),
    @NamedQuery(name = "Periodo.sumPeriodoByAnio", query = "SELECT COUNT(p) FROM Periodo p WHERE p.fechaInicio BETWEEN :startdate AND :enddate"),
    @NamedQuery(name = "Periodo.findByFechaFin", query = "SELECT p FROM Periodo p WHERE p.fechaFin = :fechaFin")})
    
  
public class Periodo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id_periodo")
    @Size(max=7)
    @Basic(optional = false)
    private String idPeriodo;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "periodo")
    private List<AsignacionMetodologia> asignacionMetodologiaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPeriodo")
    private Collection<Evaluacion> evaluacionCollection;
    @JoinColumn(name = "id_plan_estrategico", referencedColumnName = "id_plan_estrategico")
    @ManyToOne(optional = false)
    private PlanEstrategico idPlanEstrategico;

    public Periodo() {
    }

    public Periodo(String idPeriodo) {
        this.idPeriodo = idPeriodo;
    }

    public Periodo(String idPeriodo, String descripcion, Date fechaInicio, Date fechaFin) {
        this.idPeriodo = idPeriodo;
        this.descripcion = descripcion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public String getIdPeriodo() {
        return idPeriodo;
    }

    public void setIdPeriodo(String idPeriodo) {
        this.idPeriodo = idPeriodo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }
        
    @XmlTransient
    public Collection<Evaluacion> getEvaluacionCollection() {
        return evaluacionCollection;
    }

    public void setEvaluacionCollection(Collection<Evaluacion> evaluacionCollection) {
        this.evaluacionCollection = evaluacionCollection;
    }

    public PlanEstrategico getIdPlanEstrategico() {
        return idPlanEstrategico;
    }

    public void setIdPlanEstrategico(PlanEstrategico idPlanEstrategico) {
        this.idPlanEstrategico = idPlanEstrategico;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPeriodo != null ? idPeriodo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Periodo)) {
            return false;
        }
        Periodo other = (Periodo) object;
        if ((this.idPeriodo == null && other.idPeriodo != null) || (this.idPeriodo != null && !this.idPeriodo.equals(other.idPeriodo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "uca.sed360.jpaEntities.Periodo[ idPeriodo=" + idPeriodo + " ]";
        return idPeriodo;
    }

    @XmlTransient
    public List<AsignacionMetodologia> getAsignacionMetodologiaList() {
        return asignacionMetodologiaList;
    }

    public void setAsignacionMetodologiaList(List<AsignacionMetodologia> asignacionMetodologiaList) {
        this.asignacionMetodologiaList = asignacionMetodologiaList;
    }
    
}
