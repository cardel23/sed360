/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.Ubicaciones;

/**
 * Fachada para el controlador de las ubicaciones del empleado
 * @author Darlon
 */
@Stateless
public class UbicacionesFacade extends AbstractFacade<Ubicaciones> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public UbicacionesFacade() {
        super(Ubicaciones.class);
    }
    
    /**
     * Busca una ubicación en la BD
     * @param desc el nombre de la ubicación (Edificio I, Coordinación ISTI)
     * @return la ubicacion
     */
    public Ubicaciones findByDesc(String desc){
        Query q=em.createNamedQuery("Ubicaciones.findByDesc").setParameter("desc", desc);
        try {
            return (Ubicaciones) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    
}
