/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import uca.sed360.jpaEntities.Roles;
import uca.sed360.jpaEntities.Usuarios;

/**
 * Fachada para el controlador de usuarios
 * @author Gerardo Ortega
 */
@Stateless
public class UsuariosFacade extends AbstractFacade<Usuarios> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public UsuariosFacade() {
        super(Usuarios.class);
    }
    
    /**
     * Comprueba su el usuario es administrador del sistema
     * @param user usuario
     * @return verdadero o falso
     */
    public boolean isAdmin(Usuarios user){
        boolean isAdmin=false;
        for(Roles rol:user.getRolesCollection()){
            if(rol.getIdRol()==Roles.ROL_ADMINISTRADOR){
                isAdmin=true;
            }
        }
        return isAdmin;
    }
  
    /**
     * Comprueba si el usuario es consultor del sistema
     * @param user usuario
     * @return verdadero o falso
     */
    public boolean isConsultor(Usuarios user){
        boolean isAdminConsultor=false;
        for(Roles rol:user.getRolesCollection()){
            if(rol.getIdRol()==Roles.ROL_CONSULTOR){
                isAdminConsultor=true;
            }
        }
        return isAdminConsultor;
    }
    
    /**
     * Comprueba si el usuario es colaborador del sistema
     * @param user usuario
     * @return verdadero o falso
     */
    public boolean isColaborador(Usuarios user){
        boolean isColaborador=false;
        for(Roles rol:user.getRolesCollection()){
            if(rol.getIdRol()==Roles.ROL_COLABORADOR){
                isColaborador=true;
            }
        }
        return isColaborador;
    }
        
        
    /**
     * Comprueba si el usuario del sistema es un empleado
     * @param user usuario
     * @return verdadero o falso
     */
    public boolean isEmpleado(Usuarios user) {
            
           if(user.getEmpleados()==null)
               return false;
           return true;
        }
        
    /**
     * Limpia los objetos de la caché de Glassfish
     */
    public void limpiarCache(){
            em.getEntityManagerFactory().getCache().evictAll();
        }
     
}
