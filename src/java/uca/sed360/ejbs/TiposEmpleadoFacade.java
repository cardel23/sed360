/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.Nivel360;
import uca.sed360.jpaEntities.PlanEstrategico;
import uca.sed360.jpaEntities.TipoEmpleado;
import uca.sed360.managedBeans.util.JsfUtil;

/**
 * Fachada para el controlador de tipos de empleado
 * @author Darlon Espinoza
 */
@Stateless
public class TiposEmpleadoFacade extends AbstractFacade<TipoEmpleado> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public TiposEmpleadoFacade() {
        super(TipoEmpleado.class);
    }
    
    /**
     * Comprueba si se ha definido un instrumento
     * @param plan plan estratégico
     * @param tipoEmp tipo de empleado
     * @param nivel360 nivel 360
     * @return verdadero o falso
     */
    public boolean instrumentoTipoEmpNivelDefinidoEnPlan(PlanEstrategico plan,TipoEmpleado tipoEmp,Nivel360 nivel360){
        try {
              Query q= em.createNamedQuery("DetallePreguntaCerrada.countByPlanTipoEmpNivel")
               .setParameter("plan", plan) .setParameter("tipoEmp", tipoEmp).setParameter("nivel360", nivel360);
               Long  numDetalles=(Long) q.getSingleResult();
               if(numDetalles>0)
                   return true;
               return false;
            
        } catch (Exception e) {            
            JsfUtil.addErrorMessage(e.getMessage());
            return false;
        } 
    }
}
