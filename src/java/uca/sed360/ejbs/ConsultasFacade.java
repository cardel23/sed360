/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.Consultas.Consultas;
import uca.sed360.jpaEntities.Periodo;
import uca.sed360.managedBeans.util.reportes.CapacitacionCentro;

/**
 * Fachada que controla consultas que involucran multiples entidades
 * @author Carlos Delgadillo
 * @since 25/10/2013
 */
@Stateless
public class ConsultasFacade extends AbstractFacade<Consultas>{
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;
    private List<Consultas> lista;
    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    /**
     * Constructor
     */
    public ConsultasFacade(){
        super(Consultas.class);
    }
    
    /**
     * 
     * @return lista
     */
    public List<Consultas> obtenerPuntajes(){
        return lista;
    }
    
    /**
     * Obtiene capacitaciones en un centro de costo para un periodo específico
     * @param p
     * @return lista de objetos CapacitacionCentro
     */
    public List<CapacitacionCentro> capacitacionesPorArea(Periodo p){
        Query q = em.createNamedQuery("Consultas.listaCapacitacionesPorArea")
                .setParameter("p", p);
        List<CapacitacionCentro> list = new ArrayList<>();
        for(Object o: q.getResultList()){
            Object a[] = (Object[])o;
            CapacitacionCentro c = new CapacitacionCentro();
            c.setCentro((String)a[0]);
            c.setCapacitacion((String)a[1]);
            c.setCantidad((Long)a[2]);
            list.add(c);
        }
        return list;
    }
}
