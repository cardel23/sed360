/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.Escalas;
import uca.sed360.jpaEntities.PlanEstrategico;

/**
 * Fachada que controla las escalas de evaluación en la BD
 * @author Gerardo
 */
@Stateless
public class EscalasFacade extends AbstractFacade<Escalas> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public EscalasFacade() {
        super(Escalas.class);
    }
    
    /**
     * Obtiene la lista de escalas de evaluación para un plan estratégico
     * @param plan
     * @return lista
     */
    public List<Escalas> obtenerEscalas(PlanEstrategico plan){
        Query q = em.createNamedQuery("Escalas.findByPlan")
                .setParameter("plan", plan);
                
        return q.getResultList();
    }
}
