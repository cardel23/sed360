/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.CentroDeCostos;
import uca.sed360.jpaEntities.Empleados;
import uca.sed360.jpaEntities.Periodo;

/**
 * Fachada que controla los empleados en la BD
 * @author Gerardo
 */
@Stateless
public class EmpleadosFacade extends AbstractFacade<Empleados> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public EmpleadosFacade() {
        super(Empleados.class);
    }
    
    /**
     * Obtiene los subordinados de un empleado
     * @param jefe
     * @return lista de empleados si existen
     */
    public List<Empleados> obtenerSubordinados(Empleados jefe) {
        try{
            Query q= em.createNamedQuery("Empleados.obtenerSubordinados");
            q.setParameter("Jefe", jefe);
            return (List<Empleados>) q.getResultList();
        }
        catch(NoResultException e){
            return new ArrayList<Empleados>();
        }      
    }
    
    /**
     * Obtiene los pares de un empleado
     * @param emp
     * @return lista de pares si existen
     */
    public List<Empleados> obtenerPares(Empleados emp) {        
        try{
            if(!emp.equals(emp.getInssJefearea())){
                Query q= em.createNamedQuery("Empleados.obtenerPares");
                q.setParameter("Jefe", emp.getInssJefearea()).setParameter("Emp", emp);
                return (List<Empleados>) q.getResultList();
            }
            else
                return new ArrayList<Empleados>();
        }
        catch(NoResultException e){
             return new ArrayList<Empleados>();
        }      
    }
        
    /**
     * Obtiene los empleados que se encuentran activos
     * @return lista de empleados
     */
    public List<Empleados> obtenerActivos() {
        List<Empleados> activos = new ArrayList<Empleados>();
        Query q= em.createNamedQuery("Empleados.obtenerActivos");
            try {
                activos = (List<Empleados>) q.getResultList();
            } catch (NoResultException e) {
                return null;
            }

        return activos; 
    }
    
     /**
     * Obtiene los empleados activos de un centro de costo
     * @param c
     * @return lista de empleados
     */
    public List<Empleados> obtenerActivosPorCentroCostos(CentroDeCostos c) {
        List<Empleados> activos = new ArrayList<Empleados>();
        Query q= em.createNamedQuery("Empleados.obtenerActivosPorCentroCosto").setParameter("centroCostos", c);
            try {
                activos = (List<Empleados>) q.getResultList();
            } catch (NoResultException e) {
                return null;
            }

        return activos; 
    }
    
    
    /**
     * Obtiene un empleado específico mediante su cédula
     * @param id
     * @return el empleado si existe
     */
    public Empleados getById(String id){
        try {
            Query q=em.createNamedQuery("Empleados.findByCedula").setParameter("cedula", id);
            return (Empleados) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
        
    /**
     * Obtiene la lista de empleados a los que se le han realizado evaluaciones
     * en un periodo específico
     * @param p
     * @return lista
     */
    public List<Empleados> obtenerEmpleadosConEvaluacionesPorPeriodo(Periodo p){
            try {
                Query q = em.createNamedQuery("Empleados.encontrarConEvaluacionesPorPeriodo").
                        setParameter("periodo", p.getIdPeriodo());
                return q.getResultList();
            } catch (Exception e) {
                return null;
            }
           }
        
    /**
     * Obtiene la lista de empleados evaluados durante el periodo vigente
     * @param periodo
     * @return lista
     */
    public List<Empleados> empleadosEvaluadosEnPeriodoVigente(Periodo periodo){
            Query q = em.createNamedQuery("Empleados.obtenerEmpleadosConEvaluacionesEnPeriodoVigente")
                    .setParameter("periodo", periodo);
                    
            return q.getResultList();
        }
}
