/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.Nivel360;
import uca.sed360.jpaEntities.PlanEstrategico;
import uca.sed360.jpaEntities.DetallePreguntaCerrada;
import uca.sed360.managedBeans.util.JsfUtil;

/**
 * Fachada para el controlador de los niveles de la metodología 360
 * @author Gerardo
 */
@Stateless
public class Nivel360Facade extends AbstractFacade<Nivel360> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public Nivel360Facade() {
        super(Nivel360.class);
    }
    
    /**
     * Verifica si un nivel de la metodología está definido en un plan estratégico
     * @param i nivel360
     * @param p plan estratégico
     * @return verdadero o falso
     */
    public boolean isInstrumentoDefinidoParaPlan(Nivel360 i,PlanEstrategico p){
        Query q=em.createNamedQuery("DetallePreguntaCerrada.findByInstrumentoPlan").setParameter("idNivel360", i).setParameter("planEstrategico", p);
        try {
             List<DetallePreguntaCerrada> listaRedac=q.getResultList();
             if(listaRedac==null||listaRedac.isEmpty()){
                 return false;
             }
             return true;
        } catch (NoResultException e) {
            return false;
        }
    }
    
    /**
     * Valida los instrumentos de evaluación de cada nivel
     */
    public void validarInstrumentos(){
        try{
            for(Nivel360 i:Nivel360.getConstantNiveles360()){
                //Nivel360 aux=find(i.getIdInstrumento());
                Nivel360 aux=find(i.getIdNivel360());
                if(aux==null){
                    create(i);
                }
                else{
                    if(!aux.getDescripcion().equals(i.getDescripcion())){
                        aux.setDescripcion(i.getDescripcion());
                        edit(aux);
                    }
                }         
            }
            eliminarInstrumentosAdicionales();
        }
        catch(Exception e){
            System.out.print(e);
        }
    }
    
    private void eliminarInstrumentosAdicionales(){
        try {
            Query q=em.createNamedQuery("Instrumentos.eliminarInstrumentosAdicionales");
            q.executeUpdate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al tratar de eliminar Instrumentos adicionales");
        }
    }
    

    
}
