/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.AsignacionMetodologia;
import uca.sed360.jpaEntities.CatalogoMetodologias;
import uca.sed360.jpaEntities.Niveles;
import uca.sed360.jpaEntities.Periodo;

/**
 * Fachada que controla la asignación de la metodología de evaluación
 * @author Gerardo
 */
@Stateless
public class AsignacionMetodologiaFacade extends AbstractFacade<AsignacionMetodologia> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    /**
     * 
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public AsignacionMetodologiaFacade() {
        super(AsignacionMetodologia.class);
    }  
    
    /**
     * Devuelve una asignación de metodología basado en el periodo y el nivel
     * del empleado
     * @param periodo
     * @param nivelEmp
     * @return asignacion
     */
    public AsignacionMetodologia findByPeriodoNivelEmp(Periodo periodo,Niveles nivelEmp){
            Query q=em.createNamedQuery("AsignacionMetodologia.findByPeriodoNivelEmp")
                    .setParameter("periodo", periodo).setParameter("nivelEmp", nivelEmp);
            try {
            AsignacionMetodologia a=(AsignacionMetodologia) q.getSingleResult();
            return a;
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * Este metodo valida si el empleado tiene asignada metodologia 360
     * @param periodo 
     * @param nivel 
     * @return 
     */  
    public boolean isMetodologiaAsignada360(Periodo periodo,Niveles nivel) {        
         boolean metodologiaAsignada360=true;
         AsignacionMetodologia a=findByPeriodoNivelEmp(periodo,nivel);
         if(a.getMetodologia().trim().equals(CatalogoMetodologias.METODOLOGIA_180)){
             metodologiaAsignada360=false; 
         }
        return metodologiaAsignada360;
    }
    
}

