/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import uca.sed360.jpaEntities.Cargos;

/**
 * Fachada que controla el cargo del empleado
 * @author Darlon Espinoza
 */
@Stateless
public class CargosFacade extends AbstractFacade<Cargos> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    /**
     * 
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public CargosFacade() {
        super(Cargos.class);
    }
    
}
