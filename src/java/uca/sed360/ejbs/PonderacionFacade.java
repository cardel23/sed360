/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.Criterio;
import uca.sed360.jpaEntities.Nivel360;
import uca.sed360.jpaEntities.PlanEstrategico;
import uca.sed360.jpaEntities.Ponderacion;
import uca.sed360.jpaEntities.TipoEmpleado;

/**
 * Fachada para el controlador de ponderaciones
 * @author Gerardo
 */
@Stateless
public class PonderacionFacade extends AbstractFacade<Ponderacion> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public PonderacionFacade() {
        super(Ponderacion.class);
    }

    /**
     * Obtiene las ponderaciones de un plan
     * @param i nivel de la metodología 360
     * @param p plan estratégico
     * @return la lista con las ponderaciones si las encuentra
     */
    public List<Ponderacion> ObtenerPonderacion(Nivel360 i, PlanEstrategico p){
        Query query = (Query) em.createNamedQuery("Ponderacion.findBynivel360")
                .setParameter("nivel360", i)
                .setParameter("plan", p);
        
        try{      
            return (List<Ponderacion>) query.getResultList();
        } catch(NoResultException e){
            return null;
        }
    }
    
    /**
     * Comprueba si los criterios están ponderados
     * @param plan el plan estratégico al que pertenecen los criterios
     * @param tipoEmp el tipo de empleado
     * @return verdadero o falso
     */
    public boolean isPonderadoCriteriosTipoEmpEnPlan(PlanEstrategico plan,TipoEmpleado tipoEmp){
        Query query = (Query) em.createNamedQuery("Ponderacion.countByTipoEmpPlan")                
                .setParameter("plan", plan).setParameter("tipoEmp", tipoEmp);
        
        try{      
            Long numPonderacionesCriteriosTipoEmp=(Long) query.getSingleResult();
            if(numPonderacionesCriteriosTipoEmp>0)
                return true;
            return false;
        } catch(Exception e){
            System.out.println(e);
            return false;
        } 
    }
    
    
    /**
     * Busca la ponderación según nivel y criterio
     * @param i nivel 360
     * @param c criterio de evaluación
     * @return la ponderación del criterio, si existe
     */
    public Ponderacion findByNivel360Criterio(Nivel360 i, Criterio c) {
        Query q = em.createNamedQuery("Ponderacion.findBynivel360Criterio")
                .setParameter("nivel360", i)
                .setParameter("criterio", c);
        try {
             return (Ponderacion) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }  
    
    }
    
    /**
     * Obtiene la lista de ponderaciones según un nivel 360
     * @param i el nivel 360
     * @return la lista con las ponderaciones
     */
    public List<Ponderacion> findPonderacionByNivel360(Nivel360 i) {
           Query q = em.createNamedQuery("Ponderacion.findBynivel360")
            .setParameter("nivel360", i);
        try {
             return (List<Ponderacion>) q.getResultList();
        } catch (NoResultException e) {
            return null;
        } 
    }
   
    /**
     * Obtiene las ponderaciones según un plan estratégico
     * @param p el plan
     * @return la lista con las ponderaciones
     */
    public List<Ponderacion> findByPlan(PlanEstrategico p) {
       Query q = em.createNamedQuery("Ponderacion.findByPlan")
               .setParameter("planVigente", p);
       try {
           return (List<Ponderacion>) q.getResultList();
       } catch (NoResultException e) {
           return null;
       }
   }
   
    /**
     * Cuenta cuantas ponderaciones hay para un plan estratégico
     * @param p el plan
     * @return la cantidad de ponderaciones
     */
    public long countByPlan(PlanEstrategico p) {
       Query q = em.createNamedQuery("Ponderacion.countByPlan")
               .setParameter("planVigente", p);
       try {
           return  (Long) q.getSingleResult();
       } catch (NoResultException e) {
           return 0;
       }
   }
    

    
}

