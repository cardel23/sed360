/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.Periodo;
import uca.sed360.managedBeans.util.JsfUtil;

/**
 * Fachada para el controlador de periodos
 * @author Gerardo Ortega
 */
@Stateless
public class PeriodoFacade extends AbstractFacade<Periodo> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public PeriodoFacade() {
        super(Periodo.class);
    }
    
    /**
     * Busca un periodo según la fecha
     * @param p el periodo a localizar
     * @return el periodo si lo encuentra
     */
    public Periodo findByFechas(Periodo p) {
       Query q = em.createNamedQuery("Periodo.findByFechas")
               .setParameter("fechaInicio", p.getFechaInicio())
               .setParameter("fechaFin", p.getFechaFin());
        try {
            return (Periodo) q.getSingleResult();
        } catch (NoResultException e) {
            return null;           
        }
    }
    
    /**
     * Obtiene el periodo vigente en el sistema
     * @return periodo si lo encuentra
     */
    public Periodo getPeriodoVigente() {
        Query q= em.createNamedQuery("Periodo.periodoVigente");
        q.setMaxResults(1);
         try{                
                return (Periodo) q.getSingleResult();    
            }
            catch(NoResultException e)
            { 
                return null;
            }               
    }
    
    /**
     * Comprueba si existe un periodo vigente
     * @return verdadero o falso
     */
    public boolean hayPeriodoVigente(){
         Query q= em.createNamedQuery("Periodo.periodoVigente");
         try{                
               q.getSingleResult();    
               return true;
            }
            catch(NoResultException e)
            { 
               return false;
            }   
    }
    
    /**
     * Genera el identificador del periodo en la BD
     * @param fechaInicio
     * @return el id con el que se identificará el periodo
     * @throws ParseException si falla la conversión de fechas
     */
    public String generarIdPeriodo(Date fechaInicio) throws ParseException {
        
        int sumatoriaPorAnio = 0;
        String idPeriodo;  
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaInicio);
                
        String fecha1 = "January 1, "+ Integer.toString(cal.get(Calendar.YEAR));
        String fecha2 = "December 31, "+ Integer.toString(cal.get(Calendar.YEAR));
        
        Date startdate = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).parse(fecha1);
        Date enddate = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).parse(fecha2);
        
        Query q = null;
        try {
            q = em.createNamedQuery("Periodo.sumPeriodoByAnio")                    
                    .setParameter("startdate", startdate)
                    .setParameter("enddate", enddate);
                    sumatoriaPorAnio = ((Number) q.getSingleResult()).intValue();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al intentar general el id del nuevo periodo");
        }

      idPeriodo = Integer.toString(cal.get(Calendar.YEAR)) + "-P" + Integer.toString(sumatoriaPorAnio+1);
      return idPeriodo;
    }
    
    /**
     * Cuenta los días restantes para el siguiente periodo de evaluación
     * @return el número si existe o 0
     */
    public int obtenerDiasProximoPeriodo() {
        
        Query q = em.createNativeQuery("SELECT DATEDIFF(day, GETDATE(), (SELECT TOP 1 p.fecha_inicio FROM Periodo p WHERE p.fecha_inicio > GETDATE() ORDER BY p.fecha_inicio ASC));");
            try{         
               return (Integer) q.getSingleResult(); 
            }
            catch(NullPointerException e)
            { 
               return 0;
            } 
    }

}
