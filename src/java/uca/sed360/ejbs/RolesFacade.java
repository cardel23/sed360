/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import uca.sed360.jpaEntities.Roles;

/**
 * Fachada para el controlador de los roles de usuario
 * @author Gerardo
 */
@Stateless
public class RolesFacade extends AbstractFacade<Roles> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public RolesFacade() {
        super(Roles.class);
    }
    
    /**
     * Valida los roles de usuario en la BD
     */
    public void validarRolesEnBD(){
        Roles colaborador,consultor,admin;
        colaborador=find(Roles.ROL_COLABORADOR);
        consultor=find(Roles.ROL_CONSULTOR);
        admin=find(Roles.ROL_ADMINISTRADOR);
        //verificando el rol de calaborador
        if(colaborador!=null){
            if(!colaborador.getDescripcion().equals("Colaborador(a)")){
                colaborador.setDescripcion("Colaborador(a)");
                edit(colaborador);
            } 
        }
        else{
            colaborador=new Roles(Roles.ROL_COLABORADOR, "Colaborador(a)");
            create(colaborador);
        }
        
        //verificando el rol de consultor
        if(consultor!=null){
             if(!consultor.getDescripcion().equals("Consultor(a)")){
                consultor.setDescripcion("Consultor(a)");
                edit(consultor);
            }
        }
        else{
            consultor=new Roles(Roles.ROL_CONSULTOR, "Consultor(a)");
            create(consultor);
        }
        // verificando el rol de administrador
        if(admin!=null){
           if(!admin.getDescripcion().equals("Administrador(a)")){
                admin.setDescripcion("Administrador(a)");
                edit(admin);
            } 
        }
        else{
            admin=new Roles(Roles.ROL_ADMINISTRADOR, "Administrador(a)");
            create(admin);
        }
    }
}
