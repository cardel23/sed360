/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.Nivel360;
import uca.sed360.jpaEntities.PlanEstrategico;
import uca.sed360.jpaEntities.PreguntaAbierta;
import uca.sed360.jpaEntities.RespuestaPreguntaAbierta;
import uca.sed360.jpaEntities.TipoEmpleado;
import uca.sed360.managedBeans.util.JsfUtil;

/**
 * Fachada para el controlador de preguntas abiertas
 * @author Gerardo Ortega
 */
@Stateless
public class PreguntaAbiertaFacade extends AbstractFacade<PreguntaAbierta> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public PreguntaAbiertaFacade() {
        super(PreguntaAbierta.class);
    }
    
    /**
     * Obtiene una lista de preguntas abiertas según el plan y tipo de empleado
     * @param plan plan estratégico
     * @param tipoEmp tipo de empleado
     * @return lista de preguntas abiertas
     */
    public List<PreguntaAbierta> findByPlanTipoEmp(PlanEstrategico plan,TipoEmpleado tipoEmp){
        Query q=em.createNamedQuery("PreguntaAbierta.findByPlanTipoEmp").setParameter("plan", plan).setParameter("tipoEmp",tipoEmp);
        try {
            return   q.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }
    
    /**
     * Obtiene la lista de preguntas abiertas según el plan, tipo de empleado y
     * nivel 360
     * @param plan plan estratégico
     * @param tipoEmp tipo de empleado
     * @param nivel360 nivel 360
     * @return lista de preguntas abiertas
     */
    public List<PreguntaAbierta> findByPlanTipoEmpNivel360(PlanEstrategico plan,TipoEmpleado tipoEmp,Nivel360 nivel360){
        List<PreguntaAbierta> list = new ArrayList<>();
        list = em.createNamedQuery("PreguntaAbierta.findByPlanTipoEmpleadoNivel360")
                .setParameter("planEstrategico", plan)
                .setParameter("tipoEmp", tipoEmp)
                .setParameter("nivel360", nivel360)
                .getResultList();
        try {
            return list;
        } catch (NoResultException e) {
            return null;
        }
    }
    
    /**
     * Obtiene las preguntas abiertas que existen para una evaluación
     * @param evaluacion evaluación seleccionada
     * @return lista de preguntas abiertas
     */
    public List<RespuestaPreguntaAbierta> obtenerPreguntasAbiertas(String evaluacion){
        List<RespuestaPreguntaAbierta> ev = new ArrayList<RespuestaPreguntaAbierta>();
        ev = (List<RespuestaPreguntaAbierta>) em.createNamedQuery("Evaluacion.obtenerPreguntasAbiertas")
                .setParameter("idEvaluacion", evaluacion)
                .getResultList();
        return ev;
    }
    
    /**
     * Elimina una pregunta abierta
     * @param n nivel 360
     * @param p plan estratégico
     */
    public void eliminarPreguntaByNivel360Plan(Nivel360 n,PlanEstrategico p){
        try {                 
        Query q=em.createNamedQuery("PreguntaAbierta.eliminarByNivel360Plan")
                .setParameter("nivel360", n).setParameter("planEstrategico", p);
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al tratar de eliminar las preguntas abiertas del nivel360 ".concat(n.getDescripcion()));
        }
    }
    
    
}
