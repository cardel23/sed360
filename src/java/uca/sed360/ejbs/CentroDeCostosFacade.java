/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import uca.sed360.jpaEntities.CentroDeCostos;
import uca.sed360.jpaEntities.Periodo;

/**
 * Fachada que controla los centros de costo en la BD
 * @author Gerardo
 */
@Stateless
public class CentroDeCostosFacade extends AbstractFacade<CentroDeCostos> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;
    private List<CentroDeCostos> lista;

    /**
     * 
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public CentroDeCostosFacade() {
        super(CentroDeCostos.class);
    }
    
    
    
    /**
     * Verifica si existen evaluaciones para un centro de costo
     * @param centro
     * @return verdadero o falso
     */
    public boolean existenEvaluaciones_para_centro(CentroDeCostos centro)
    {
            if(em.createNamedQuery("CentroDeCostos.tieneEmpleados")
                    .setParameter("idCentroCostos", centro.getIdCentrocostos())
                    .getResultList().size() > 0)
                return true;
            else 
                return false;              
    }
    
    
    /**
     * Obtiene un centro de costo por su id
     * @param centro
     * @return centro de costo si existe
     */
    public CentroDeCostos obtenerPorId(CentroDeCostos centro)
    {
        CentroDeCostos cc;
        try
        {        
        cc = (CentroDeCostos) em.createNamedQuery("CentroDeCostos.findByIdCentrocostos").setParameter("idCentrocostos", centro.getIdCentrocostos()).getSingleResult();
        }
        catch(NoResultException nre)
                {
                    return null;
                }
        
        return cc;
    }
    
    /**
     * Obtiene una lista de Areas para el reporte de capacitaciones
     * Autor: Carlos Delgadillo
     * @param periodo
     * @since 18/12/2013
     * @return la lista con los centros de costo que tienen evaluaciones en
     * en el periodo seleccionado
     */
    public List<CentroDeCostos> listaCentrosConEvaluaciones(Periodo periodo){
        lista = em.createQuery(
                "SELECT ce FROM Empleados e,\n" +
                "Evaluacion eva, Capacitaciones cap, CentroDeCostos ce\n" +
                "where e.evaluacionCollection1 = eva\n" +
                "and ce = e.centroCosto\n" +
                "and cap = eva.capacitacionesCollection\n" +
                "and SIZE(e.evaluacionCollection1)>0\n" +
                "and eva.idPeriodo = :periodo\n" +
                "GROUP BY ce\n" +
                "ORDER BY ce.idCentrocostos")
                .setParameter("periodo", periodo)
                .getResultList();
        return lista;
    }
    
}
