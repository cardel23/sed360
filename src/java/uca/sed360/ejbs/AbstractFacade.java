/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.util.List;
import javax.persistence.EntityManager;

/**
 * Fachada Abstracta que realiza operaciones sobre la BD
 * @param <T> 
 * @author Gerardo
 */
public abstract class AbstractFacade<T> {
    private Class<T> entityClass;
    
    /**
     * Constructor de la clase AbstractFacade
     * @param entityClass 
     */
    
    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }
    
    /**
     * 
     * @return el objeto entityManager
     */
  
    protected abstract EntityManager getEntityManager();
    
    /**
     * Persiste una entidad en la BD
     * @param entity 
     */
   
    public void create(T entity) {
        getEntityManager().persist(entity);
    }
    /**
     * Limpia de la cache de Glassfish una entidad específica
     * @param entityClass 
     */
    
    public void limpiarClaseEnCache(Class entityClass){
        getEntityManager().getEntityManagerFactory().getCache().evict(entityClass);
    }
    /**
     * Limpia todas las clases de la caché de Glassfish
     */
    
    public void limpiarTodoEnCache(){
         getEntityManager().getEntityManagerFactory().getCache().evictAll();
    }
    
    /**
     * Edita una entidad que se encuentra almacenada en la BD
     * @param entity 
     */
    
    public void edit(T entity) {                
        getEntityManager().merge(entity);        
    }
    
    /**
     * Elimina una entidad de la BD
     * @param entity 
     */
    
    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));        
    }
    
    /**
     * Localiza una entidad de una clase determinada
     * @param id
     * @return la entidad si la encuentra
     */
    
    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }
    
    /**
     * Busca todas las entidades que pertenecen a una clase específica
     * @return lista de entidades
     */

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }
    /**
     * Encuentra todas las entidades que se encuentran en un rango determinado
     * @param range
     * @return lista de entidades, si existen
     */
   
    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    /**
     * Cuenta las entidades de una clase específica que se encuentran en la BD
     * @return el número de entidades que encuentra
     */
    
    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
}
