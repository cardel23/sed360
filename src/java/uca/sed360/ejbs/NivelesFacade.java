/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.Niveles;

/**
 * Fachada para el controlador de los niveles de empleado
 * @author Darlon Espinoza
 */
@Stateless
public class NivelesFacade extends AbstractFacade<Niveles> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public NivelesFacade() {
        super(Niveles.class);
    }
    
    /**
     * Busca un nivel por su nombre
     * @param desc la descripción o nombre del nivel
     * @return el nivel si existe
     */
    public Niveles findByDesc(String desc){
        Query q=em.createNamedQuery("Niveles.findByDesc").setParameter("desc", desc);
        try {
            return (Niveles) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    
}
