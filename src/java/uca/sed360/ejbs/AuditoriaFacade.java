/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import uca.sed360.jpaEntities.Auditoria;

/**
 *
 * @author Gerardo
 */
@Stateless
public class AuditoriaFacade extends AbstractFacade<Auditoria> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public AuditoriaFacade() {
        super(Auditoria.class);
    }
    
}
