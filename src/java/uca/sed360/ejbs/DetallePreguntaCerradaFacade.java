/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.Nivel360;
import uca.sed360.jpaEntities.PlanEstrategico;
import uca.sed360.jpaEntities.DetallePreguntaCerrada;
import uca.sed360.jpaEntities.TipoEmpleado;

/**
 * Fachada que controla el detalle de una pregunta cerrada en la BD
 * @author Gerardo
 */
@Stateless
public class DetallePreguntaCerradaFacade extends AbstractFacade<DetallePreguntaCerrada> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public DetallePreguntaCerradaFacade() {
        super(DetallePreguntaCerrada.class);
    }

    /**
     * Obtiene los detalles de una pregunta según el plan estratégico,
     * el tipo de empleado y el nivel 360
     * @param plan
     * @param tipoEmp
     * @param nivel360
     * @return lista de detalles si existen
     */
    public List<DetallePreguntaCerrada> findByPlanTipoEmpNivel(PlanEstrategico plan,TipoEmpleado tipoEmp,Nivel360 nivel360) {
        try {
              Query q= em.createNamedQuery("DetallePreguntaCerrada.findByPlanTipoEmpNivel")
               .setParameter("plan", plan) .setParameter("tipoEmp", tipoEmp).setParameter("nivel360", nivel360);
                return (List<DetallePreguntaCerrada>) q.getResultList();
            
        } catch (NoResultException e) {
            System.err.println(e);
            return null;
        }
    }
     
}
