/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.Criterio;
import uca.sed360.jpaEntities.Nivel360;
import uca.sed360.jpaEntities.PreguntaCerrada;
import uca.sed360.jpaEntities.RespuestaPreguntaCerrada;

/**
 * Fachada para el controlador de las respuestas a preguntas cerradas
 * @author Gerardo
 */
@Stateless
public class RespuestaPreguntaCerradaFacade extends AbstractFacade<RespuestaPreguntaCerrada> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public RespuestaPreguntaCerradaFacade() {
        super(RespuestaPreguntaCerrada.class);
    } 
    
    /**
     * Comprueba si existen respuestas
     * @param c criterio
     * @param n nivel 360
     * @return verdadero o falso
     */
    public boolean existeRespuestasAPreguntasPorCriterioyNivel(Criterio c, Nivel360 n){
    
        Query q = em.createNamedQuery("RespuestaPreguntaCerrada.ObtenerPorCriterioNivel").setParameter("idCriterio", c.getIdCriterio()).
                setParameter("idNivel", n.getIdNivel360());
             try{   
                Long cantidadResp=(Long) q.getSingleResult();
                if(cantidadResp>0 && cantidadResp!= null)
                        return true;
                 else
                        return false;
             }
             catch(Exception e){
                 System.out.println("------------>"+e.toString()+"<--------------");
                 return false;
             }
    }
    
    /**
     * Comprueba si existen respuestas a una pregunta
     * @param pc pregunta cerrada
     * @param nivel360 nivel 360
     * @return verdadero o falso
     */
    public boolean existRespuestasPreguntaCerradaNivel360(PreguntaCerrada pc,Nivel360 nivel360){
        Query q=em.createNamedQuery("RespuestaPreguntaCerrada.countByPreguntaCerradaNivel360").setParameter("pc", pc).setParameter("nivel360", nivel360);        
        try{
            Long numRespuestasPCNivel360=(Long) q.getSingleResult();
            if(numRespuestasPCNivel360>0){
                return true;
            }
            return false;
        }
        catch(NoResultException e){
             return false;
        }
    }
    
}
