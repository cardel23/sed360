/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.PlanEstrategico;
import uca.sed360.jpaEntities.PreguntaCerrada;

/**
 * Fachada para el controlador de preguntas cerradas
 * @author Gerardo Ortega
 */
@Stateless
public class PreguntaCerradaFacade extends AbstractFacade<PreguntaCerrada> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public PreguntaCerradaFacade() {
        super(PreguntaCerrada.class);
    }
    
    /**
     * Obtiene las preguntas cerradas del plan vigente
     * @param planVigente el plan estratégico
     * @return lista de preguntas cerradas
     */
    public List<PreguntaCerrada> findByPlanVigente(PlanEstrategico planVigente) {
            List<PreguntaCerrada> preguntasCerradas = new ArrayList<PreguntaCerrada>();
            Query q=em.createNamedQuery("PreguntaCerrada.findByPlan")
            .setParameter("planVigente", planVigente);
        try {
            return (List<PreguntaCerrada>) q.getResultList();
            
        } catch (Exception e) {
            
        }
    return preguntasCerradas;
    
    }
    
    
    /**
     * Comprueba si existe una pregunta en la BD
     * @param p
     * @return verdadero o falso
     */
    public boolean existePregunta(PreguntaCerrada p){
        try {
            find(p.getIdPregunta());
            return true;
        } catch (NoResultException e) {
            return false;
        }
    }
}
