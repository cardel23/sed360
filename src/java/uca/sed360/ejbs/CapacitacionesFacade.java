/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.Capacitaciones;
import uca.sed360.jpaEntities.Empleados;
import uca.sed360.jpaEntities.Periodo;
import uca.sed360.managedBeans.util.reportes.CapacitacionCentro;

/**
 * Fachada que controla las capacitaciones en la BD
 * @author Gerardo
 */
@Stateless
public class CapacitacionesFacade extends AbstractFacade<Capacitaciones> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;
    private List<Capacitaciones> lista;
    
    /**
     * 
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public CapacitacionesFacade() {
        super(Capacitaciones.class);
    }
    
    /**
     * Obtiene la lista de capacitaciones por área en un periodo determinado
     * @param p
     * @return lista
     */
    public List<Capacitaciones> listaCapacitacionesPorArea(Periodo p){
        lista = new ArrayList<Capacitaciones>();
        lista = (List<Capacitaciones>) em.createNamedQuery("Capacitaciones.capacitacionesPorArea")
                .setParameter("periodo", p)
                .getResultList();
        return lista;
    }
    
    /**
     * Obtiene la lista de capacitaciones de un área específica
     * @param e
     * @return lista
     */
    public List<Capacitaciones> obtenerCapacitcionesPorArea(String e){
        lista = new ArrayList<Capacitaciones>();
        lista = (List<Capacitaciones>) em.createNamedQuery("Capacitaciones.obtenerCapacitacionesPorCentro")
                .setParameter("idCentro", e)
                .getResultList();

                return lista;
    }
    
    /**
     * Obtiene las recomendaciones realizadas en un periodo de evaluación
     * @param p
     * @return lista
     */
    public List<Capacitaciones> obtenerRecomendaciones(Periodo p){
        lista = new ArrayList<>();
        lista = (List<Capacitaciones>) em.createNamedQuery("Capacitaciones.recomendaciones")
                .setParameter("p", p)
                .getResultList();
        return lista;
    }
    
    /**
     * Obtiene capacitaciones por un centro de costo en un periodo
     * @param periodo
     * @return lista
     * @deprecated Eliminar
     */
    public List<CapacitacionCentro> obtenerCapacitacionesCentro(Periodo periodo){
        List<CapacitacionCentro> list;
        list = (List<CapacitacionCentro>) em.createQuery("SELECT ce FROM Empleados e,\n" +
                                "Evaluacion eva, Capacitaciones cap, CentroDeCostos ce\n" +
                                "where e.evaluacionCollection1 = eva\n" +
                                "and ce = e.centroCosto\n" +
                                "and cap = eva.capacitacionesCollection\n" +
                                "and eva = cap.evaluacionCollection\n" +
                                "and eva.idPeriodo = :periodo\n" +
                                "GROUP BY ce\n" +
                                "ORDER BY ce.idCentrocostos")
                .setParameter("periodo", periodo)
                .getResultList();
        return list;
    }
    
}
