/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.CentroDeCostos;
import uca.sed360.jpaEntities.Empleados;
import uca.sed360.jpaEntities.Evaluacion;
import uca.sed360.jpaEntities.Nivel360;
import uca.sed360.jpaEntities.Periodo;
import uca.sed360.jpaEntities.PlanEstrategico;
import uca.sed360.jpaEntities.RespuestaPreguntaCerrada;
import uca.sed360.jpaEntities.TipoEmpleado;
import uca.sed360.managedBeans.util.PromedioEmpleado;


/**
 *
 * @author Gerardo
 */
@Stateless
public class EvaluacionFacade extends AbstractFacade<Evaluacion> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;
    
     @EJB   
     private uca.sed360.ejbs.EmpleadosFacade ejbEmpleados;
     
     @EJB   
     private uca.sed360.ejbs.AsignacionMetodologiaFacade ejbAsignacionMetodologia;
     
    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public EvaluacionFacade() {
        super(Evaluacion.class);
    }
    
    /**
     * Genera el código de identificación único de una evaluación
     * @param evaluador
     * @param evaluado
     * @param periodo
     * @return el id que creó para la evaluación
     */
    public String generarId(Empleados evaluador, Empleados evaluado, Periodo periodo) {
        String id;
        id=evaluador.getInss()+"-" + evaluado.getInss() +"-"+ periodo.getIdPeriodo();   
        
        return id;    
    }
    
    /**
     * Obtiene la evaluación de un empleado en un periodo según el nivel 360
     * @param e
     * @param n
     * @param p
     * @return evaluación si existe
     */
    public Evaluacion findEvaluacionNivel(Empleados e, Nivel360 n, Periodo p){
        Query q = em.createNamedQuery("Evaluacion.obtenerEvaluacionNivel")
                .setParameter("evaluado", e)
                .setParameter("nivel", n)
                .setParameter("periodo", p);
        try{
            Evaluacion ev = (Evaluacion)q.getSingleResult();
            return ev;
        }
        catch(Exception ex){
            return null;
        }

    }
    
      
    /**
     * Se encarga de sumar los puntajes de cada pregunta y devuelve el total <br/>
     * Autor: Carlos Delgadillo
     * @param eva 
     * @return El puntaje total de la evaluación
     * @since 03/03/2013
     */
     public double calcularPuntajeTotal(Evaluacion eva){
        double sum = 0;
        double puntajePregunta;
        for(RespuestaPreguntaCerrada rpc:eva.getRespuestaPreguntaCerradaCollection()){
            puntajePregunta=rpc.getPreguntaCerrada().
                    getDetallePreguntaCerradaCollection().get(eva.getIdNivel360().getIdNivel360()-1).getValor();
            sum += (puntajePregunta*rpc.getEscala().getValor());
        }
       
        DecimalFormat df = new DecimalFormat("#.##");
        try{ return Double.valueOf(df.format(sum));}catch(Exception e){
        return sum;}
    }
    
    /**
     * Este metodo verifica si existe el empleado en sesion evaluó 
     * o no al empleado pasado como parametro
     * @param evaluador 
     * @param evaluado
     * @param periodo 
     * @return veradero o falso
     */
    public boolean existsEvaluacion(Empleados evaluador,Empleados evaluado,Periodo periodo){
        Evaluacion evaluacion = findEvaluacion(evaluador,evaluado ,periodo );
   
        if(evaluacion==null){
            return false;
        }        
       return true;       
    }
    
    /**
     * Obtiene la cantidad de veces que han evaluado a un empleado
     * en un periodo
     * @param e
     * @param p
     * @return el número de evaluaciones del empleado
     */
    public Long obtenerCantidadEvaluacionesEmpleadoPeríodo(Empleados e, Periodo p){
        
        Query q = em.createQuery("select COUNT(ev) from Evaluacion ev where ev.evaluador =:evaluador "
                                    + "and ev.idPeriodo = :periodo").
                setParameter("evaluador", e).
                setParameter("periodo", p);
        return (Long) (q.getSingleResult());    
    }

    /**
     * Busca una evaluación específica
     * @param evaluador
     * @param evaluado
     * @param periodo
     * @return la evaluación si existe
     */
    public Evaluacion findEvaluacion(Empleados evaluador,Empleados evaluado,Periodo periodo){
        String idEvaluacion=generarId(evaluador, evaluado, periodo);
        Evaluacion ev;
        try {
            Query q = em.createNamedQuery("Evaluacion.findByIdEvaluacion").setParameter("idEvaluacion", idEvaluacion);
            ev = (Evaluacion) q.getSingleResult();
            return ev;            
        } 
        catch (Exception e) {
            return null;
        }
    }
    
    /**
     * Elimina de la caché de Glassfish la entidad evaluación
     */
    public  void limpiarCache(){
        em.getEntityManagerFactory().getCache().evict(Evaluacion.class);
    }
    
    /**
     * Obtiene una lista de promedios de evaluación en un periodo
     * @param idperiodo
     * @return
     */
    public List<Object[]> ObtenerPromedios(String idperiodo){
        Query q = (Query) em.createNamedQuery("ObtenerPromedios").
                setParameter(1,idperiodo);
        List<Object[]> filas = (List<Object[]>) q.getResultList();
        return filas;
    }
    
    /**
     * Obtiene las evaluaciones realizadas en un periodo
     * @param periodo
     * @return lista si existe
     */
    public List<Evaluacion> obtenerListaEvaluacionesPorPeriodo(Periodo periodo) {    
        Query q=em.createNamedQuery("Evaluacion.findEvaluacionByPeriodo")
            .setParameter("periodo", periodo);
        try {
            return (List<Evaluacion>) q.getResultList();            
        } catch (Exception e) {   
           return null;
        }    
    }
    
    /**
     * Obtiene el número evaluaciones realizadas en un centro de costo durante
     * un periodo
     * @param periodo
     * @param c
     * @return la cantidad de evaluaciones
     */
    public Integer obtenerNumEvaluacionesPorPeriodoCentroCosto(Periodo periodo,CentroDeCostos c) {    
        Query q;
        if(c==null)
            q=em.createNamedQuery("Evaluacion.countEvaluacionByPeriodo")
                .setParameter("periodo", periodo);
        else
            q=em.createNamedQuery("Evaluacion.countEvaluacionByPeriodoCentroCostos")
            .setParameter("periodo", periodo).setParameter("centroCosto", c);
        try {
            Long numEv=(Long) q.getSingleResult();       
            return numEv.intValue();
        } catch (Exception e) {  
            System.out.println(e);
           return null;
        }    
    }
    
    /**
     * Obtiene la lista de evaluaciones relacionadas a un plan estratégico
     * @param plan
     * @return lista
     */
    public List<Evaluacion> obtenerEvaluacionesPorPlanEstrategico(PlanEstrategico plan) {
        List<Evaluacion> evaluaciones = new ArrayList<Evaluacion>();
        Query q = em.createNamedQuery("Evaluacion.findByPlan")
                .setParameter("planVigente", plan);

        try {
            return (List<Evaluacion>) q.getResultList();
        } catch (Exception e) {
    
    }   
    
    return evaluaciones;    
    }
    
    /**
     * Obtiene las evaluaciones a subordinados que están pendientes en un periodo
     * @param periodo
     * @return  lista
     */
    public List<Empleados> obtenerEvaluacionesPendientes(Periodo periodo){
        try{
        
        List <Empleados> evaluacionesPendientes = new ArrayList<Empleados>();
        evaluacionesPendientes = (List<Empleados>) em.createNamedQuery("Evaluacion.findEvaluacionesSubordinadosPendientes").setParameter("periodo", periodo).getResultList();
        return evaluacionesPendientes;
        }
        catch(Exception e){
            return null;
        }
    }
    
    /**
     * Obtiene las autoevaluaciones pendientes en un periodo
     * @param periodo
     * @return lista
     */
    public List<Empleados> obtenerAutoEvaluacionesPendientes(String periodo){
        List<Empleados> evaluacionesPendientes = new ArrayList<Empleados>();
        evaluacionesPendientes = (List<Empleados>) em.createNamedQuery("Evaluacion.findAutoevaluacionesPendientes")
                .setParameter("periodo", periodo)
                .getResultList();
        return evaluacionesPendientes;
    }
    /**
     * Obtiene la lista de preguntas, y el puntaje de cada pregunta para una evaluacion
     * en un periodo determinado
     * @param evaluado
     * @param evaluador
     * @param periodo
     * @return la lista con datos para mostrar en pantalla
     * @autor Carlos Delgadillo
     * @fecha 05/06/2013
     */
    public List<String> obtenerResultadosEvaluacion(Empleados evaluado, Empleados evaluador, Periodo periodo){
        List<String> resultados = new ArrayList<String>();
        resultados = (List<String>) em.createNamedQuery("Evaluacion.puntajeEvaluacion")
                .setParameter("evaluador", evaluador)
                .setParameter("evaluado", evaluado)
                .setParameter("periodo", periodo)
                .getResultList();
        return resultados;
    }
    
    /**
     * Obtiene los promedios de evaluación
     * @param p
     * @param resultados
     * @return lista
     */
    public List obtenerLista(Periodo p, int resultados){
//        Query q = em.createNamedQuery("Consultas.lista");        
        Query q = em.createNamedQuery("Consultas.lista")
                .setParameter("p", p);
//                .setMaxResults(resultados);
        return q.getResultList();
    }
    
    /**
     * Este método devuelve la suma de los puntaje obtenidos por un 
     * empleado en las evaluaciones hechas a él en un período.
     * @param periodo
     * @param empleado
     * @return lista de puntajes
     */
     public Object obtenerLista(Periodo periodo, Empleados empleado){       
        Query q = em.createNamedQuery("Consultas.ListaDeValoresPorEmpleado")
                .setParameter("empleado", empleado)
                .setParameter("p", periodo);
        try{
        return (Object) q.getSingleResult();}
        catch(Exception e){
            System.out.println(e.toString());
            return null;
        }
    }
    
    
    /**
     * Obtiene la lista de evaluaciones que han sido impresas
     * @param impresa
     * @param p
     * @return lista
     */
    public List<Evaluacion> obtenerEvaluacionesImpresas(boolean impresa, Periodo p){
        List<Evaluacion> resultados = new ArrayList<Evaluacion>();
        try{            
        
        resultados = (List<Evaluacion>)em.createNamedQuery("Evaluacion.obtenerEvaluacionImpresa")
                .setParameter("impresa", impresa)
                .setParameter("periodo",p.getIdPeriodo())
                .getResultList();
        }catch(Exception e){
            System.out.println(e.toString());
        }
        return resultados;
    }
    
    /**
     * Obtiene la cantidad de evaluaciones que han sido impresas por centro de
     * costo durante un periodo
     * @param p
     * @param c
     * @return cantidad de evaluaciones impresas
     */
    public Integer obtenerNumEvaluacionesImpresasPeriodoCentroCosto(Periodo p,CentroDeCostos c){
        Long numEvaluacionesImpresas;
        try{  
            Query q;
            if(c==null)
                 q=em.createNamedQuery("Evaluacion.obtenerNumEvaluacionImpresaPeriodo")
                    .setParameter("periodo", p);
            else
                q=em.createNamedQuery("Evaluacion.obtenerNumEvaluacionImpresaPeriodoCentroCosto")
                        .setParameter("periodo", p)
                        .setParameter("centroCosto",c);
            numEvaluacionesImpresas=(Long) q.getSingleResult();
            return numEvaluacionesImpresas.intValue();
        }catch(Exception e){
            System.out.println(e.toString());
            return null;
        }
    }
    
    /**
     * Obtiene las evaluaciones que no han sido impresas
     * @return lista
     */
    public List<Evaluacion> obtenerEvaluacionesNoImpresas(){
        List<Evaluacion> resultados;
        resultados = (ArrayList<Evaluacion>)em.createNamedQuery("Evaluacion.obtenerEvaluacionNoImpresa").getResultList();
        return resultados;
    }
    
    /**
     * Verifica si existen evaluaciones en un periodo determinado
     * @param periodo
     * @return verdadero o falso
     */
    public boolean existEvaluacionesPeriodo(Periodo periodo){
          try{
            Integer numEvaluacionesPlan=obtenerNumEvaluacionesPorPeriodoCentroCosto(periodo,null);
            if(numEvaluacionesPlan>0){
                return true;
            }
            return false;
        }
        catch(Exception e){
             return false;
        }
    }
    
    /**
     * Comprueba que existan evaluaciones relacionadas a un plan estratégico
     * para un tipo de empleado según el nivel 360
     * @param tipoEmp
     * @param plan
     * @param nivel360
     * @return verdadero o falso
     */
    public boolean existEvaluacionesTipoEmpPlanNivel(TipoEmpleado tipoEmp,PlanEstrategico plan,Nivel360 nivel360){
        Query q=em.createNamedQuery("Evaluacion.countEvaluacionByTipoEmpPlanNivel").setParameter("tipoEmp", tipoEmp).setParameter("planEstrategico", plan).setParameter("nivel360", nivel360);        
        try{
            Long numEvaluacionesPlan=(Long) q.getSingleResult();
            if(numEvaluacionesPlan>0){
                return true;
            }
            return false;
        }
        catch(NoResultException e){
             return false;
        }
    }
    
    /**
     * Comprueba que existan evaluaciones relacionadas a un plan estratégico
     * según el tipo de empleado
     * @param tipoEmp
     * @param plan
     * @return verdadero o falso
     */
    public boolean existEvaluacionesTipoEmpPlan(TipoEmpleado tipoEmp,PlanEstrategico plan){
        Query q=em.createNamedQuery("Evaluacion.countEvaluacionByTipoEmpPlan").setParameter("tipoEmp", tipoEmp).setParameter("planEstrategico", plan);        
        try{
            Long numEvaluacionesPlan=(Long) q.getSingleResult();
            if(numEvaluacionesPlan>0){
                return true;
            }
            return false;
        }
        catch(NoResultException e){
             return false;
        }
    }
    /**
     * Obtiene las evaluaciones para procesar los promedios de cada empleado
     * @param p
     * @return 
     */
    public List<PromedioEmpleado> obtenerEvaluacionesParaPromedios(Periodo p){
        List<PromedioEmpleado> lista ;
        lista = (List<PromedioEmpleado>) em.createNamedQuery("Evaluacion.evaluacionesParaPromedio")
                .setParameter("periodo", p)
                .getResultList();

        try{
            return lista;
        }
        catch(Exception e){
            return (List) e;
        }
    }
    
    /**
     *
     * @param p
     * @param c
     * @return
     */
    public Integer obtenerNumEvaluacioneARealizar(Periodo p,CentroDeCostos c){
        Integer numEvaluaciones=0;
        List<Empleados> empleadosActivos;
        if(c==null)
            empleadosActivos=ejbEmpleados.obtenerActivos();
        else
            empleadosActivos=ejbEmpleados.obtenerActivosPorCentroCostos(c);
        for(Empleados e:empleadosActivos){
            //sumar la autoevaluacion del empleado
            numEvaluaciones++;
            //sumar las evaluaciones que el empleado debe realizar a sus subordinados
            numEvaluaciones+=ejbEmpleados.obtenerSubordinados(e).size();
            //validar si el empleado tiene asignada la metodologia 360 en el periodo especificado 
            if( getEjbAsignacionMetodologia().isMetodologiaAsignada360(p,e.getNivel())){
                //sumar las evaluaciones que el empleado debe realizar a sus pares
                numEvaluaciones+=ejbEmpleados.obtenerPares(e).size();
                //sumar las evaluaciones que el empleado debe realizar a su superior
                if(e.getInssJefearea()!=e)
                     numEvaluaciones++;
            }            
        }
        return numEvaluaciones;
    }
    
    /**
     *
     * @param p
     * @return
     */
    public List<Evaluacion> evaluacionesConCapacitaciones(Periodo p){
        Query q = em.createQuery("SELECT ev FROM Evaluacion ev\n" +
                                 "WHERE ev.idPeriodo = :p \n" +
                                 "AND SIZE(ev.capacitacionesCollection) > 0\n" +
                                 "ORDER BY ev.CentroCosto")
                .setParameter("p", p);
        List<Evaluacion> lista = (ArrayList<Evaluacion>) q.getResultList();
        return lista;
    }

    /**
     * @return the ejbAsignacionMetodologia
     */
    public uca.sed360.ejbs.AsignacionMetodologiaFacade getEjbAsignacionMetodologia() {
        return ejbAsignacionMetodologia;
    }

    /**
     * @param ejbAsignacionMetodologia the ejbAsignacionMetodologia to set
     */
    public void setEjbAsignacionMetodologia(uca.sed360.ejbs.AsignacionMetodologiaFacade ejbAsignacionMetodologia) {
        this.ejbAsignacionMetodologia = ejbAsignacionMetodologia;
    }
}
