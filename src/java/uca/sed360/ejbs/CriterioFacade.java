/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.Criterio;
import uca.sed360.jpaEntities.TipoEmpleado;
import uca.sed360.jpaEntities.PlanEstrategico;

/**
 * Fachada que controla los criterios en la BD
 * @author Gerardo
 */
@Stateless
public class CriterioFacade extends AbstractFacade<Criterio> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public CriterioFacade() {
        super(Criterio.class);
    }
    
    /**
     * Obtiene los criterios del plan estratégico vigente
     * para un tipo de empleado
     * @param planVigenete
     * @param tipoEmp
     * @return lista
     */
    public List<Criterio> getCriteriosByPlanTipoEmpleado(PlanEstrategico planVigenete,TipoEmpleado tipoEmp){
        Query q=em.createNamedQuery("Criterio.findByPlanTipoEmp").setParameter("plan", planVigenete).setParameter("tipoEmp", tipoEmp);
        List<Criterio> listaCriterio;
        try{
             listaCriterio= q.getResultList();
             return listaCriterio;
        }
        catch(NoResultException e){
              return null;
        }
    }
    
    /**
     * Verifica si existen criterios establecidos dentro de un plan estratégico
     * para un tipo de empleado
     * @param tipoEmp
     * @param plan
     * @return verdadero o falso
     */
    public boolean existenCriteriosTipoEmpEnPlan(TipoEmpleado tipoEmp,PlanEstrategico plan){
        Query q=em.createNamedQuery("Criterio.countByPlanTipoEmp").setParameter("plan", plan).setParameter("tipoEmp", tipoEmp);
        Long numCriterios;
        try{
             numCriterios=(Long) q.getSingleResult();
             if(numCriterios>0)
                 return true;
             return false;
        }
        catch(Exception e){
            System.out.println(e);
            return false;
        }
    }
    
    /**
     * Comprueba si hay un criterio
     * @param c
     * @return verdadero o falso
     */
    public boolean existeCriterio(Criterio c){
        try {
            find(c.getIdCriterio());
            return true;
        } catch (NoResultException e) {
            return false;
        }
    }
    
    /**
     * Limpia la entidad de la caché de Glassfish
     * @param c
     */
    public void refrescarCriterio(Criterio c){
        try{           
                em.flush();
                em.getEntityManagerFactory().getCache().evict(Criterio.class);                
                em.refresh(em.merge(c));                
                em.flush();
           }
        catch(Exception e){
            System.out.println(e.toString());
        }
    }
    
}
