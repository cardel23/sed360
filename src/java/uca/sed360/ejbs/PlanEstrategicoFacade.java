/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uca.sed360.jpaEntities.Criterio;
import uca.sed360.jpaEntities.PlanEstrategico;

/**
 * Fachada para el controlador de los planes estratégicos
 * @author Gerardo
 */
@Stateless
public class PlanEstrategicoFacade extends AbstractFacade<PlanEstrategico> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public PlanEstrategicoFacade() {
        super(PlanEstrategico.class);
    }
    
    /**
     * Busca un plan estratégico según el identificador de plan
     * @param idPlan el identificador del plan a buscar
     * @return el plan, si existe
     */
    public PlanEstrategico findPlan(String idPlan){
        Query q=em.createNamedQuery("PlanEstrategico.findByIdPlanEstrategico").setParameter("idPlanEstrategico", idPlan);
        try {
            return (PlanEstrategico) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * Obtiene un plan estratégico según las fechas del plan
     * @param p el plan a consultar
     * @return el plan si lo encuentra
     */
    public PlanEstrategico findByFechas(PlanEstrategico p) {
    Query q=em.createNamedQuery("PlanEstrategico.findByFechas")
            .setParameter("fechaInicio", p.getFechaInicio())
            .setParameter("fechaFin", p.getFechaFin());
        try {
            return (PlanEstrategico) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }    
    }
    
    /**
     * Busca el plan estratégico vigente
     * @return el plan, si existe
     */
    public PlanEstrategico getPlanVigente(){
        Query q=em.createNamedQuery("PlanEstrategico.planVigente");        
        try{
            PlanEstrategico planVigente=(PlanEstrategico) q.getSingleResult();
            return  planVigente;
        }
        catch(NoResultException e){
             return null;
        }
    }    
    
    /**
     * Genera el identificador único del plan estratégico
     * @param plan el plan
     * @return el identificador del plan estratégico
     */
    public String generateIdPlan(PlanEstrategico plan){
       String fechaInicio,fechaFinal,idPlan=null;
       if(plan!=null && plan.getFechaInicio()!=null&& plan.getFechaFin()!=null){            
            fechaInicio=String.valueOf(plan.getFechaInicio().getYear());
            fechaFinal=String.valueOf(plan.getFechaFin().getYear()); 
            idPlan=fechaInicio.substring(1, 3).concat(fechaFinal.substring(1, 3));            
       }
       return idPlan;
    }
    
    /**
     * Verifica que un plan no genere conflictos con un periodo de evaluación
     * @param p plan
     * @return verdadero o falso
     */
    public boolean comprobarConflictosConPeriodo(PlanEstrategico p){
        try
        {
         Query q = em.createNamedQuery("PlanEstrategico.comprobarConflictoConPeriodo").setParameter("fechaFin", p.getFechaFin());
         if((Long) q.getSingleResult()>0)
                 return true;
         else 
            return false;
        }
        catch(Exception e)
            {
                return false;
            }
    }
    
    /**
     * Actualiza el plan estratégico
     * @param p
     */
    public void Actualizar(PlanEstrategico p){
        try{
            em.getEntityManagerFactory().getCache().evict(Criterio.class);
            em.flush();
            em.refresh(em.merge(p));
            for(Criterio c: p.getCriteriosCollection())
            {   em.getEntityManagerFactory().getCache().evict(Criterio.class);
                em.refresh(em.merge(c));
            }
            }
        catch(Exception e){
            System.out.println(e.toString());
        }
    }
    
}