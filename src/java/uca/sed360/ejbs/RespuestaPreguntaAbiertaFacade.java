/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.ejbs;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import uca.sed360.jpaEntities.PreguntaAbierta;
import uca.sed360.jpaEntities.RespuestaPreguntaAbierta;

/**
 * Fachada para el controlador de las respuestas a preguntas abiertas
 * @author Gerardo Ortega
 */
@Stateless
public class RespuestaPreguntaAbiertaFacade extends AbstractFacade<RespuestaPreguntaAbierta> {
    @PersistenceContext(unitName = "SED360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Constructor
     */
    public RespuestaPreguntaAbiertaFacade() {
        super(RespuestaPreguntaAbierta.class);
    }
    
    /**
     * Obtiene las respuestas de una pregunta específica
     * @param p pregunta abierta
     * @param id id de la evaluación
     * @return lista de respuestas
     */
    public List<RespuestaPreguntaAbierta> getRespuestaPorPregunta(PreguntaAbierta p, String id){
        List<RespuestaPreguntaAbierta> lista = new ArrayList<RespuestaPreguntaAbierta>();
        lista = (List<RespuestaPreguntaAbierta>) em.createNamedQuery("RespuestaPreguntaAbierta.findPregunta")
                .setParameter("pregunta", p)
                .setParameter("idEvaluacion", id)
                .getResultList();
        return lista;
    }
    
}
