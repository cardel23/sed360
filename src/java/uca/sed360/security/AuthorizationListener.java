/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uca.sed360.security;

import uca.sed360.jpaEntities.Usuarios;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

/**
 * Clase que se encarga de controlar la sesión de usuario en el sistema SED360
 * @author CT.INF.01
 */

public class AuthorizationListener implements PhaseListener {

    /**
     * 
     * @param event
     */
    @Override
    public void afterPhase(PhaseEvent event) {

        FacesContext facesContext = event.getFacesContext();
        String currentPage = facesContext.getViewRoot().getViewId();

        boolean isLoginPage = (currentPage.lastIndexOf("/Login.xhtml") > -1);
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        NavigationHandler nh = facesContext.getApplication().getNavigationHandler(); 
        if(session==null){   
        nh.handleNavigation(facesContext, null, "/Login.xhtml");
    }
        
    else{
        Usuarios currentUser = (Usuarios) session.getAttribute("currentUser");
        if (!isLoginPage && (currentUser == null)) {       
            nh.handleNavigation(facesContext, null, "/Login.xhtml");            
        }
         
    }
}

    /**
     *
     * @param event
     */
    @Override
    public void beforePhase(PhaseEvent event) {

    }

    /**
     *
     * @return
     */
    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }
}


